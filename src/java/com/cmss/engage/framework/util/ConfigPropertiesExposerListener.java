/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.engage.framework.util;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author DELL
 */
public class ConfigPropertiesExposerListener implements ServletContextListener {

    public static final String DEFAULT_PROPERTIES_BEAN_NAME = FrameworkAppConstants.APPL_FILE_NAME;
    public static final String DEFAULT_CONTEXT_PROPERTY = FrameworkAppConstants.APPL_FILE_NAME;
    private String propertiesBeanName = DEFAULT_PROPERTIES_BEAN_NAME;
    private String contextProperty = DEFAULT_CONTEXT_PROPERTY;

    public void contextDestroyed(ServletContextEvent sce) {
    }

    public void contextInitialized(ServletContextEvent sce) {
        // TODO add ability to configure non default values via serveltContexParams
        ServletContext servletContext = sce.getServletContext();
        WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        ExposablePropertyPaceholderConfigurer configurer =
                (ExposablePropertyPaceholderConfigurer) context.getBean(propertiesBeanName);
        sce.getServletContext().setAttribute(contextProperty, configurer.getResolvedProps());

    }
}
