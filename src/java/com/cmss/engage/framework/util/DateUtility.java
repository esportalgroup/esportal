package com.cmss.engage.framework.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Locale;

import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.framework.util.log4j.CustomLogger;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author DELL
 */
public class DateUtility {

    public static final String DAYS = "DAYS";
    public static final String HOURS = "HRS";
    public static final String MINUTES = "MIN";
    public static final String SECONDS = "SEC";
    public final static long SECOND_MILLIS = 1000;
    public final static long MINUTE_MILLIS = SECOND_MILLIS * 60;
    public final static long HOUR_MILLIS = MINUTE_MILLIS * 60;
    public final static long DAY_MILLIS = HOUR_MILLIS * 24;
    public final static long YEAR_MILLIS = DAY_MILLIS * 365;

    public static Hashtable hashTab;

    public DateUtility() {
        super();
    }

    public static Timestamp getCurrentDate() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Timestamp(today.getTime());
    }

    public static Timestamp addDays(Timestamp timestamp, int intDays) {
        Calendar c1 = GregorianCalendar.getInstance();
//        System.out.println(timestamp.toString());
//        System.out.println(timestamp.getYear());
        c1.set(Integer.parseInt(timestamp.toString().substring(0, 4)), timestamp.getMonth(), timestamp.getDate()); // 1999 jan 20
        c1.add(Calendar.DATE, intDays);
        java.util.Date date = c1.getTime();
        return new java.sql.Timestamp(date.getTime());
    }

    public static Timestamp stripTime(Timestamp timestamp) {
        return timestamp.valueOf(timestamp.toString().substring(0, 10) + " 00:00:00.000");
    }

    public static Timestamp appendTime(Timestamp timestamp) {
        return timestamp.valueOf(timestamp.toString().substring(0, 10) + " 23:59:59.000");
    }

    public static Timestamp parseStringToDate(String strDate)
            throws CustomNonFatalException {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        try {
            java.util.Date today = df.parse(strDate);
            return new java.sql.Timestamp(today.getTime());
        } catch (ParseException e) {
//            e.printStackTrace();
            CustomLogger.error("Unable to parse Date",e);
            throw new CustomNonFatalException("Unable to parse Date", "Invalid date format");
        }

    }

    public static Timestamp parseStringToDate(String strDate, String strFormat)
            throws CustomNonFatalException {
        DateFormat df = new SimpleDateFormat(strFormat);
        try {
            java.util.Date today = df.parse(strDate);
            return new java.sql.Timestamp(today.getTime());
        } catch (ParseException e) {
            throw new CustomNonFatalException("Unable to parse Date", "Invalid date format");
        }

    }

    public static String parseDateToString(java.util.Date today) {
        DateFormat df = new SimpleDateFormat("dd-MMM-yy");
        return df.format(today);
    }
    
    public static String parseDateToString(java.util.Date today, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(today);
        
    }
    

    public static Timestamp parseDateToDate(Date strDate)
            throws CustomNonFatalException {

        try {

            return new java.sql.Timestamp(strDate.getTime());
        } catch (Exception e) {
//            e.printStackTrace();
            CustomLogger.error("Unable to parse Date", e);
            throw new CustomNonFatalException("Unable to parse Date", "Invalid date format");
        }

    }

    public static Integer getTimeInInteger() {
        java.util.Date today = new java.util.Date();
        DateFormat df = new SimpleDateFormat("kkmmssSSS");
        return Integer.parseInt(df.format(today));
    }

    public static String formatDate(Timestamp tDate, int iDateFormat, Locale userLocale) throws CustomNonFatalException {
        DateFormat df = null;

        if (tDate == null) {
            return ("");
        }

        if (iDateFormat < 3) {
            df = DateFormat.getDateInstance(DateFormat.SHORT, userLocale);
        } else {
            df = DateFormat.getDateInstance(DateFormat.MEDIUM, userLocale);
        }

        try {
            String formattedDate = df.format(tDate);
            return formattedDate;
        } catch (IllegalArgumentException e) {
            return ("");
        }

    }

    public static long getDaysDifference(Timestamp time1, Timestamp time2, String strType) {

        long timeDifference = time1.getTime() - time2.getTime();

//    System.out.println(" DATEUTIL:"+strType+"_"+timeDifference);
        if (strType.equals(DAYS)) {
            time1 = stripTime(time1);
            time2 = stripTime(time2);
            timeDifference = time1.getTime() - time2.getTime();
            timeDifference = timeDifference / (24 * 60 * 60 * 1000);
        } else if (strType.equals(HOURS)) {
            timeDifference = timeDifference / (60 * 60 * 1000);
        } else if (strType.equals(MINUTES)) {
            timeDifference = timeDifference / (60 * 1000);
        } else if (strType.equals(SECONDS)) {
            timeDifference = timeDifference / (1000);
        } else {
            timeDifference = 0L;
        }
        //  System.out.println(" DATEUTIL:"+strType+"_"+timeDifference);
        return timeDifference;
    }

    
      public static String getcurrentSystemDateTime() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Calendar calobj = Calendar.getInstance();
        String date = df.format(calobj.getTime());
        return date;
    }

    public static Date StringToDate(String strDate)
            throws CustomNonFatalException {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            java.util.Date today = df.parse(strDate);
            return  today;
        } catch (ParseException e) {
//            e.printStackTrace();
            CustomLogger.error("Unable to parse Date", e);
            throw new CustomNonFatalException("Unable to parse Date", "Invalid date format");
        }

    }

    /*
     public static String formatDate(Timestamp date, UserProfile userInfo) throws CustomNonFatalException
     {

     try
     {

     if(userInfo.getStrDateFormatPattern()!=null)
     {
     return(formatDate(date,userInfo.getStrDateFormatPattern(),userInfo.getUserLocale()));
     }
     else
     {
     return (formatDate(date, userInfo.getUserDateFormat(), userInfo.getUserLocale()) );
     }

     }
     catch(CustomNonFatalException b)
     {
     return("");
     }

     }




     public static String formatDate(Timestamp tDate, String localeLang, String localeCtry) throws CustomNonFatalException
     {

     Locale userLocale;

     userLocale = new Locale(localeLang,localeCtry);

     try
     {
     return formatDate(tDate,2,userLocale);
     }
     catch(CustomNonFatalException b)
     {
     return("");
     }

     }

     public static String formatDateTime(Timestamp tDate, int iDateFormat, Locale userLocale) throws CustomNonFatalException
     {
     if (tDate == null)
     {
     return ("");
     }

     DateFormat df = null;


     if (iDateFormat < 8)
     {
     df = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, userLocale);
     }
     else
     {
     df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.SHORT,userLocale);
     }

     try
     {

     String formattedDate = df.format(tDate);

     return formattedDate;
     }
     catch(IllegalArgumentException e)
     {
     return ("");
     }


     }




     public static String formatDouble(double d,UserProfile userInfo ) throws CustomNonFatalException,CustomFatalException
     {
     try
     {
     if(userInfo.getAmtFormat()!=null)
     {
     String millionFormat = formatDouble(d ,userInfo.getUserLocale());
     if(userInfo.getAmtFormat().equalsIgnoreCase("0"))
     {
     return formatLakhAmount(millionFormat,userInfo.getUserLocale());
     }
     else
     {
     return millionFormat;
     }
     }
     else
     {
     return formatDouble(d,userInfo.getUserLocale());
     }
     }
     catch(CustomNonFatalException b)
     {
     return("");
     }
     catch(IllegalArgumentException e)
     {
     return ("");
     }

     }

     public static String formatDouble(double d,CacheManager cacheManager) throws CustomNonFatalException,CustomFatalException
     {
     UserProfile userInfo = null;

     userInfo = (UserProfile) cacheManager.getSessionObject("UserProfile");

     try
     {

     return formatDouble(d,userInfo);
     }
     catch(CustomNonFatalException b)
     {
     return("");
     }
     catch(IllegalArgumentException e)
     {
     return ("");
     }

     }


     public static String formatDouble(double d, String sLang_Code,String sCtry_Code) throws CustomNonFatalException,CustomFatalException
     {
     Locale userLocale = null;

     userLocale = new Locale(sLang_Code,sCtry_Code);

     try
     {
     return formatDouble(d,userLocale);
     }
     catch(CustomNonFatalException b)
     {
     return("");
     }
     catch(IllegalArgumentException e)
     {
     return ("");
     }

     }

     public static String formatDouble(double dbl, Locale userLocale) throws CustomNonFatalException
     {

     try
     {
     NumberFormat nf = NumberFormat.getInstance(userLocale);
     String result = nf.format(dbl);
     return result;
     }
     catch(IllegalArgumentException e)
     {
     return ("");
     }
     }





     public static double stringToDouble(String fieldValue) throws CustomNonFatalException
     {
     double dbValue = 0.0;
     int fromIndex = 0;
     int indexOfComma = 0;
     int lengthOfString = 0;
     String stringWithCommas="";
     String stringWithoutCommas = "";

     //checking for the value of the string
     if ((fieldValue == null) || (fieldValue.length() == 0))
     {
     return 0.0;
     }

     lengthOfString = fieldValue.length();
     while (true)
     {
     //finding index at which comma is present
     indexOfComma = fieldValue.indexOf(',', fromIndex);
     //if the comma is found
     if (indexOfComma != -1) {
     stringWithCommas = fieldValue.substring(fromIndex, indexOfComma);
     //concatenating the string without commas
     stringWithoutCommas = stringWithoutCommas.concat(stringWithCommas);
     fromIndex = indexOfComma+1;
     }
     else
     {
     stringWithCommas = fieldValue.substring(fromIndex, lengthOfString);
     //concatenating the string without commas
     stringWithoutCommas = stringWithoutCommas.concat(stringWithCommas);
     break;
     }
     }
     try
     {
     dbValue = Double.parseDouble(stringWithoutCommas);
     }
     catch(NumberFormatException nfe)
     {
     throw new CustomNonFatalException("Invalid Amount",123, nfe);
     }
     return dbValue;
     }


     public static String getDateTimeFormat(String strDtFmt, UserProfile userInfo,CacheManager cacheManager)
     throws CustomNonFatalException, CustomFatalException {
     // Save the date format for the user.
     String dateFmt = null;
     int intDateFmt;
     DateFormat df = null;
     String dateFormatPattern = "";
     try {
     String dateFmtString = strDtFmt.substring(0, 2);
     dateFmt = dateFmtString;
     if (dateFmt == null) {
     throw new CustomFatalException("Date format in user record is null");
     }

     intDateFmt = Integer.parseInt(dateFmt);
     } catch (NumberFormatException bnfe) {
     throw new CustomFatalException(
     "Date format in user's record is not a valid value. Value found["
     + dateFmt
     + "], Error:"
     + bnfe.getMessage(),
     bnfe);
     }
     if ((intDateFmt != 2) && (intDateFmt != 3)) {


     dateFormatPattern =
     CommonCodeDescription.getDesc(
     COCDConstants.DATE_FORMAT_PATTERN,
     strDtFmt,
     cacheManager);
     if((dateFormatPattern==null)||(dateFormatPattern.equalsIgnoreCase(strDtFmt)))
     {
     throw new CustomFatalException(
     "Date format in user's record is not a valid value. Value found["
     + dateFmt
     + "], Error:"
     );
     }

     df = SimpleDateFormat.getTimeInstance(DateFormat.MEDIUM,userInfo.getUserLocale());
     String timepattern = ((SimpleDateFormat)(df)).toPattern();

     return(dateFormatPattern + " "+timepattern);
     } else {
     // Get the Locale specific instance of DateFormat
     // If the Date format is < 3 then use DateFormat.SHORT, else use
     // DateFormat.MEDIUM. This is to maintain bkwd compatibility.
     // dateFormat = 2 ==> DateFormat.SHORT
     // dateFormat = 3 ==> DateFormat.MEDIUM

     if (intDateFmt < 3) {
     df = SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM,userInfo.getUserLocale());
     } else {
     df = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,userInfo.getUserLocale());
     }
     dateFormatPattern= ((SimpleDateFormat)(df)).toPattern();

     }

     return dateFormatPattern;
     }


     public static String getDateFormat(String strDtFmt, UserProfile userInfo,CacheManager cacheManager)
     throws
     CustomNonFatalException,
     CustomFatalException {
     // Save the date format for the user.
     String dateFmt = null;
     int intDateFmt;
     String dateFormatPattern = "";
     try {
     String dateFmtString = strDtFmt.substring(0, 2);
     dateFmt = dateFmtString;
     if (dateFmt == null) {
     throw new CustomFatalException("Date format in user record is null");
     }

     intDateFmt = Integer.parseInt(dateFmt);
     } catch (NumberFormatException bnfe) {
     throw new CustomFatalException(
     "Date format in user's record is not a valid value. Value found["
     + dateFmt
     + "], Error:"
     + bnfe.getMessage(),
     bnfe);
     }
     if ((intDateFmt != 2) && (intDateFmt != 3)) {

     dateFormatPattern =
     CommonCodeDescription.getDesc(
     COCDConstants.DATE_FORMAT_PATTERN,
     strDtFmt,
     cacheManager);
     if((dateFormatPattern==null)||(dateFormatPattern.equalsIgnoreCase(strDtFmt)))
     {
     throw new CustomFatalException(
     "Date format in user's record is not a valid value. Value found["
     + dateFmt
     + "], Error:"
     );
     }

     return dateFormatPattern;
     } else {
     // Get the Locale specific instance of DateFormat
     // If the Date format is < 3 then use DateFormat.SHORT, else use
     // DateFormat.MEDIUM. This is to maintain bkwd compatibility.
     // dateFormat = 2 ==> DateFormat.SHORT
     // dateFormat = 3 ==> DateFormat.MEDIUM
     DateFormat df = null;
     if (intDateFmt < 3) {
     df = SimpleDateFormat.getDateInstance(DateFormat.SHORT, userInfo.getUserLocale());
     } else {
     df = SimpleDateFormat.getDateInstance(DateFormat.MEDIUM, userInfo.getUserLocale());
     }
     dateFormatPattern= ((SimpleDateFormat)(df)).toPattern();

     }

     return dateFormatPattern;
     }


     public static String formatDate(Timestamp tDate, String pattern,Locale userLocale) throws CustomNonFatalException
     {

     SimpleDateFormat formatter;
     String result="";
     formatter = new SimpleDateFormat(pattern, userLocale);
     formatter.setLenient(false);

     try
     {
     result = formatter.format(tDate);
     return result;
     }
     catch(Exception e)
     {
     return("");
     }

     }

     */
    public static int hoursDiff(Date earlierDate, Date laterDate) {
        if (earlierDate == null || laterDate == null) {
            return 0;
        }

        return (int) ((laterDate.getTime() / HOUR_MILLIS) - (earlierDate.getTime() / HOUR_MILLIS));
    }

}
