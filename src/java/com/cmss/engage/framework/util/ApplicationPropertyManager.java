/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.engage.framework.util;

import com.engage.components.core.entity.bean.ESApplicationConfigPropertiesTbl;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import com.engage.components.core.helper.ESApplicationConfigListener;

public class ApplicationPropertyManager {

    private static ResourceBundle configProp = ResourceBundle.getBundle("APPLICATION_CONFIG");
    private static HashMap hashMap = new HashMap();
    private static boolean blnRefereshFlg = false;
    public static String applicationPath = "";

    public ApplicationPropertyManager() {
        ApplicationPropertyManager property = new ApplicationPropertyManager();
        property.loadProperty();
    }

    private static void loadProperty() {
//        Enumeration e = configProp.getKeys();
//        while (e.hasMoreElements()) {
//            String key = (String) e.nextElement();
//            hashMap.put(key, configProp.getString(key));            
//        }
//        blnRefereshFlg = true;
        
        List<ESApplicationConfigPropertiesTbl> lst=new ArrayList<ESApplicationConfigPropertiesTbl>();
        try {
            lst=ESApplicationConfigListener.Properties;
//            CustomLogger.info(".... ApplicationConfigManager:"+lst.size());
            for(int i=0;i<lst.size();i++)
            {
                hashMap.put(lst.get(i).getPropertyName(), lst.get(i).getPropertyValue());
            }
        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        blnRefereshFlg = true;
    }

    public static String getProperty(String strPropertyName) {
        if (strPropertyName == null) {
            return null;
        }
        if (hashMap.get(strPropertyName) != null) {
            return hashMap.get(strPropertyName).toString();
        } else {
            if (!blnRefereshFlg) {
                loadProperty();
                return hashMap.get(strPropertyName).toString();
            }
            return null;
        }
    }

    public static void refreshProperty() {
        hashMap = new HashMap();
        loadProperty();
    }
    //    public static String applicationPath=ResourceBundle.getBundle("applicationPath").getString("APP.SYSTEM.PATH");

//    private String strClass=this.getClass().get
    private static void readPropertiesFile() {
        Properties props = new Properties();
        try {
            if (applicationPath != null && !"".equals(applicationPath)) {
                props.load(new FileInputStream(applicationPath + "applicationParams.properties"));
            }
            Enumeration e = props.propertyNames();

            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                hashMap.put(key, props.getProperty(key));
            }
            blnRefereshFlg = true;
        } catch (IOException e) {
//            e.printStackTrace();
            CustomLogger.error(applicationPath, e);
        }
    }

}
