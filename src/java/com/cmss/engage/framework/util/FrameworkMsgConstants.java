package com.cmss.engage.framework.util;

public class FrameworkMsgConstants {

    // Analytics
    public static final String ANALYTIC_START_DATE_EMPTY = "Please select From date.";
    public static final String ANALYTIC_END_DATE_EMPTY = "Please select To date.";
    public static final String ANALYTIC_COMPARE_DATE_EMPTY = "To date cannot be greater than From date.";
    public static final String CHANNEL_EMPTY = "Please select social channel";
    public static final String TAG_EMPTY = "Please select Tag";
   
    

    // Login user
    public static final String USER_LOGIN_NOT_FOUND = "The username or password you entered is incorrect.";
    public static final String USER_LOGIN_BAD_CREDENTIALS = "The username or password you entered is incorrect.";
    public static final String USER_LOGIN_MAX_ATTEMPTS = "You have exceeded maximum attempts";
    public static final String USER_LOGIN_LOCKED = "You account has been locked. Please contact administrator";
    public static final String USER_LOGIN_DISABLED = "You account has been disabled";
    public static final String USER_LOGIN_ATTEMPTS_LEFT = "You have ${0} attemtpts left";
    public static final String USER_LOGIN_ATTEMPTS_LAST = "This is your last login attempt. Your account will be locked, if you make another invalid attempt.";
    public static final String USER_GENERAL_DETAILS_NOT_FOUND = "Your preference details not found. Please contact administrator";

    // User profile
    public static final String PROFILE_EMPTY = "Profile not entered";
    public static final String PROFILE_NOT_FOUND = "User Profile not found";
    public static final String PROFILE_INVALID = "Invalid user profile";
    public static final String PROFILE_NO_MENU = "No links mapped to this user";

    //Change Password
    public static final String USER_OLD_PASSWORD_EMPTY = "Current password field is blank";
    public static final String USER_NEW_PASSWORD_EMPTY = "New password field is blank";
    public static final String USER_RETYPE_PASSWORD_EMPTY = "Re-type password field is blank";
    public static final String USER_NEW_PASSWORD_LESS_6 = "New password less than 6 charcter";
    public static final String USER_NEW_PASSWORD_GREATER_20 = "New password greater than 20 charcter";
    public static final String USER_NEW_RETYPE_PASSWORD_MATCH = "New password and Re-type password must be same";
    public static final String USER_OLD_PASSWORD_NOT_MATCH = "Current password not match";
    public static final String USER_OLD_PASSWORD_NEW_PASS_SAME = "Current and new password entered cannot be same";

        
    //Admin Config
    public static final String USER_CHECKBOX_EMPTY = "Please select atleast one checkbox to enable or disable user";
    public static final String USER_LOCK_SUCCESS = "User blocked successfully";
    public static final String USER_UNLOCK_SUCCESS = "User unblocked successfully";
    public static final String USER_LOGIN_ID_EMPTY = "Login Id field is blank";
    public static final String USER_LOGIN_ID_PRESENT = "Login Id already present";
    public static final String USER_BLOCK_NOT_SELECTED = "please select either block or unblock";
    public static final String USER_LOGIN_ID_ADD_ERROR = "User add error";
    public static final String USER_LOGIN_ID_ADD_SUCCESS = "Login Id added successfully";
    
    public static final String DEVICE_CHECKBOX_EMPTY = "Please select atleast one checkbox to enable or disable device";
    public static final String DEVICE_LOCK_SUCCESS = "Device blocked successfully";
    public static final String DEVICE_UNLOCK_SUCCESS = "Device unblocked successfully";
    public static final String DEVICE_ID_EMPTY = "Device Id field is blank";
    public static final String DEVICE_ID_PRESENT = "Device Id already present";
    public static final String DEVICE_ID_ADD_ERROR = "Device add error";
    public static final String DEVICE_ID_ADD_SUCCESS = "Device Id added successfully";
    
    public static final String USER_PROFILE_EMPTY = "Profile not selected";
    public static final String USER_PROFILE_ADD_SUCCESS = "User profile added successfully";
    public static final String USER_PROFILE_ADD_ERROR = "User Profile add error";
    
    public static final String PROFILE_ADD_ERROR = "Profile add error";
    public static final String PROFILE_ADD_SUCCESS = "Profile added successfully";
    public static final String PROFILE_DESCRIPTION_EMPTY = "Profile description field is blank";
    public static final String PROFILE_NAME_EMPTY = "Profile name field is blank";
    public static final String PROFILE_PRESENT = "Profile name already present";
    public static final String PROFILE_DELETE_SUCCESS = "Profile deleted successfully";
    
    // User Config
    public static final String USER_CONFIG_PROFILE_EMPTY = "User profile field empty";
    public static final String USER_CONFIG_TYPE_EMPTY = "User type field empty";
    public static final String USER_CONFIG_PASSWORD_EMPTY = "User password field empty";
    public static final String USER_CONFIG_RE_PASSWORD_EMPTY = "User re-type password field empty";
    public static final String USER_CONFIG_PASSWORD_MATCH = "User password and re-type password are not match";
    public static final String USER_CONFIG_TITLE_EMPTY = "User title field empty";
    public static final String USER_CONFIG_USER_NAME_EMPTY ="User name field empty ";
    public static final String USER_CONFIG_DISPLAY_NAME_EMPTY = "User display name field empty ";
    public static final String USER_CONFIG_FIRST_NAME_EMPTY = "User first name field empty ";
    public static final String USER_CONFIG_LAST_NAME_EMPTY = "User last name field empty ";
    public static final String USER_CONFIG_EMAIL_EMPTY = "User email field empty ";
    public static final String USER_CONFIG_EMAIL_FORMAT = "User email not in format ";
    public static final String USER_CONFIG_RIP_EMPTY = "User RIP name field empty ";
    public static final String USER_CONFIG_DEPARTMENT_EMPTY = "User department field empty ";
    public static final String USER_CONFIG_UPDATE_SUCCESS = "User details update successfully";
    public static final String USER_CONFIG_USER_NAME_PRESENT = "User name present";
    public static final String USER_CONFIG_UNLOCK_SUCCESS = "User unlock successfully";
   
    public static final String USER_CONFIG_UPLOAD_SIZE = "Image size in between 1KB to 50KB";
    public static final String USER_CONFIG_UPLOAD_SUCCESS = "Image upload successfully";
    public static final String USER_CONFIG_UPLOAD_FORMAT = "Please enter Image File Extensions .jpg or .jpeg";
    
    public static final String USER_CONFIG_PASSWORD_LESS_6 = "Password less than 6 charcter";
    public static final String USER_CONFIG_PASSWORD_GREATER_20 = "Password greater than 20 charcter";
            
    public static final String USER_CONFIG_DELETE_FAIL = "User delete fail";
    public static final String USER_CONFIG_DELETE_SUCCESS = "User deleted successfully";
    
    public static final String USER_CONFIG_DELETE_OWN_ACCOUNT_FAIL = "You cannot delete your own account";
    
    
     //profile
    public static final String LIBRARY_PROFILE_NAME_EMPTY = "Profile name field is blank";
    public static final String LIBRARY_PROFILE_DESC_EMPTY = "Profile description field is blank";
    public static final String LIBRARY_PROFILE_PRESENT = "Profile is alredy present";
    
    public static final String LIBRARY_PROFILE_EDIT_UPDATE_ERROR = "Profile is not update";
    
    public static final String LIBRARY_PROFILE_EDIT_UPDATE_SUCCESS = "Profile updated successfully";
    
    public static final String LIBRARY_PROFILE_DELETE_ERROR = "Profile is not deleted";
    public static final String LIBRARY_PROFILE_DELETE_SUCCESS = "Profile deleted successfully";
    
    
    public static final String LIBRARY_PROFILE_ADD_ERROR = "Profile is not added";
    public static final String LIBRARY_PROFILE_ADD_SUCCESS = "Profile added successfully";
    
    
    //otp
    public static final String REG_INFO_OTP_PSWD_EMPTY = "OTP field is empty";
    public static final String REG_INVALID_REGEN_REQ = "New OTP requested";
    public static final String REG_INVALID_OTP_NOT_FOUND = "OTP not found. Please regenerate";
    public static final String REG_INVALID_OTP_EXPIRED = "OTP expired";
    public static final String REG_INVALID_OTP_MAX_ATTEMPTS = "Maximum number of attempts";
    public static final String REG_INVALID_CREDENTIAL_INFO = "Invalid OTP / password";
}