/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cmss.engage.framework.util;

/**
 *
 * @author DELL
 */
public class FrameworkAppConstants {

    /*Application configuration*/
    public static final String APPL_FILE_NAME = "APPLICATION_CONFIG";
    public static final String APPL_PROPERTY_FILE_EXTENSION = ".properties";

    public static final Character SUBSCRIPTION_TYPE_FREE = 'F';
    public static final Character SUBSCRIPTION_TYPE_INSTANT = 'I';
    public static final Character SUBSCRIPTION_TYPE_CUSTOM = 'C';

    public static final String MGR_DWN = "MGR_DWN";
    public static final String ADM_DWN = "ADM_DWN";
    public static final String RTE_DWN = "RTE_DWN";

    public static final Character PROFILE_MENU_ACCESS_TYPE_PROFILE = 'P';
    public static final Character PROFILE_MENU_ACCESS_TYPE_USER = 'U';

    public static final Character CONSTANT_YES = 'Y';
    public static final Character CONSTANT_NO = 'N';

    public static final String STR_CONSTANT_YES = "Y";
    public static final String STR_CONSTANT_NO = "N";

    public static final String STR_CONSTANT_YES_VALUE = "YES";
    public static final String STR_CONSTANT_NO_VALUE = "NO";

    public static final String ANALYTIC_TYPE_TRAN = "T";
    public static final String ANALYTIC_TYPE_VIEW = "V";

    public static final Character VIEW_ACTION_TYPE_POPUP = 'P';
    public static final Character VIEW_ACTION_TYPE_LINK = 'L';
    public static final Character VIEW_ACTION_TYPE_TEXT = 'T';

    public static final String SIGN_IN = "SNG_IN";
    public static final String SIGN_OUT = "SNG_OT";
    public static final String PASS_CHANGE_SUCCESS = "PASS_SUC";
    public static final String PASS_INVALID = "PASS_INC";
    public static final String PASS_FORGOT = "FRG_PASS";

    public static final Character LAUNCH_STATUS_LAUNCH = 'L';
    public static final Character LAUNCH_STATUS_OPEN = 'O';
    public static final Character LAUNCH_STATUS_PENDDING = 'P';
    public static final Character LAUNCH_STATUS_COMPLETED = 'Q';
    public static final Character LAUNCH_STATUS_REJECTED = 'R';

    public static final Character REPORT_GEN_STATUS_NO = 'N';
    public static final Character REPORT_GEN_STATUS_YES = 'Y';
    public static final Character REPORT_GEN_STATUS_PENDING = 'P';

    public static final Character PROFILE_MENU_ACCESS_TYPE_ADMIN = 'A';

    public static final String COMPANY_LOGO_PATH = "D:\\360\\Hrvichaar\\hrvichaar\\web\\company_logo\\";

    public static final String USER_IMAGE_PATH = "D:\\360\\Hrvichaar\\company_employee\\";

    public static final Character USER_TYPE_USR = 'A';
    public static final String USER_CREATE_BY = "AMS_SYSTEM";

    public static final Integer PROPERTY_MAX_REG_OTP_ATMPS = 3;
    public static final Integer OTP_VALID_TIME = 5 * 60;//in seconds

    //sangam app constants
    public static final String PRODUCT_FLAG = "2WH";
    public static final String PRODUCT_FLAG_F = "KG";

    public static final Character APPLICANT_TYPE_PRIMARY = 'P';
    public static final Character APPLICANT_TYPE_CO_APP = 'C';

    public static final Character ADDRESS_TYPE_CUR = 'C';
    public static final Character ADDRESS_TYPE_PER = 'P';
    public static final Character ADDRESS_TYPE_OFFICE = 'F';
    public static final Character ADDRESS_TYPE_EKYC = 'E';
    public static final Character ADDRESS_TYPE_MOD_EKYC = 'M';
    public static final Character ADDRESS_TYPE_NA = 'N';

    public static final String HL_ADDRESS_TYPE_CUR = "CURRES";
    public static final String HL_ADDRESS_TYPE_PER = "PER";
    public static final String HL_ADDRESS_TYPE_OFFICE = "OFFICE";
    public static final String SOURCING_STATUS_DESC = "LMSSuccess";
//       public static final String HL_ADDRESS_TYPE_EKYC ='E';
//       public static final String HL_ADDRESS_TYPE_MOD_EKYC ='M';

    public static final String APPLICATION_STATUS_SAVED = "A";
    public static final String APPLICATION_STATUS_SUBMITED = "B";
    public static final String APPLICATION_STATUS_REJECTED = "C";
    public static final String APPLICATION_STATUS_APPROVED = "D";

    public static final String APP_STS_LPC_SAVED = "E";
    public static final String APP_STS_LPC_SUBMITED = "F";
    public static final String APP_STS_LPC_REJECTED = "G";
    public static final String APP_STS_LPC_APPROVED = "H";

    public static final String POST_SANCTION_SAVED = "I";
    public static final String POST_SANCTION_SUBMITTED = "J";
    public static final String POST_SANCTION_REJECTED = "K";
    public static final String POST_SANCTION_APPROVED = "L";

    public static final String APPLICATION_CANCELLED = "M";

    public static final String APPLICATION_COUNTER_OFFER = "N";

    public static final String APPLICATION_AUTO_RETRY_PRE = "O";
    public static final String APPLICATION_AUTO_RETRY_POST = "P";
    public static final String APPLICATION_AUTO_RETRY_LPC = "Q";
    public static final String APPLICATION_AUTO_RETRY_DIHOLD = "Z";
    public static final String APPLICATION_AUTO_RETRY_UWHOLD = "AE";

    public static final String APPLICATION_BRE_AUTO_CANCEL = "R";

    public static final String APPLICATION_COUNTER_OFFER_REJECTED = "S";

    public static final String APPLICATION_AUTO_RETRY_PRE_Cancel = "T";
    public static final String APPLICATION_AUTO_RETRY_POST_Cancel = "U";
    public static final String APPLICATION_AUTO_RETRY_LPC_Cancel = "V";
    public static final String APPLICATION_AUTO_RETRY_DIHOLD_Cancel = "AA";
    public static final String APPLICATION_AUTO_RETRY_UWHOLD_Cancel = "AF";

    public static final String MOVE_TO_PRE = "W";
    public static final String MOVE_TO_POST = "X";
    public static final String MOVE_TO_LPC = "Y";
    public static final String MOVE_TO_DIHOLD = "AB";
    public static final String MOVE_TO_UWHOLD = "AI";

    public static final Long OCCUPATION_SALARIED = 12L;
    public static final String OCCUPATION_SALARIED_STR = "SAL";

    public static final Long OCCUPATION_SELF_EMPLOYED = 13L;
    public static final String OCCUPATION_SELF_EMPLOYED_STR = "SEP";

    public static final Long OCCUPATION_FARM_SELF_EMPLOYED = 12L;

    public static final String OCCUPATION_NULL_STR = "NUL";

    public static final String NSDL_RESP_CODE_EXISTING = "E";
    public static final String NSDL_RESP_CODE_NON_EXISTING = "N";

    public static final String PRE_DOC_STAGE = "PRD";
    public static final String QDE_STAGE = "QDE";
    public static final String POST_DOC_STAGE = "POD";

    public static final Character BUSS_NATURE_STATUS = 'A';

    public static final String ASSET_MAKE_FLAG = "MA";
    public static final String ASSET_MODEL_FLAG = "MO";

    public static int APPLICATION_FORM_NO_SIZE = 12;

    public static final String KEY2_INCOME_HEAD = "P";

    public static final String KEY1_DISBURSAL_MODE = "LNDISBMODE";

//       public static final String SOURCING_STATUS_DESC="LMSSuccess";
    public static final String PDD_PARTIAL_SUBMIT = "Y";
    public static final String PDD_COMPLETE_SUBMIT = "N";
    public static final String PDD_BRANCH_PARTIAL_STATUS = "PDD Partial Submitted";
    public static final String PDD_BRANCH_COMPLETE_STATUS = "Sales Submitted";
    public static final String PDD_DOCUMENT_SUBMITTED = "Submitted";
    public static final String PDD_DOCUMENT_ACCEPTED = "Accepted";
    public static final String PDD_DOCUMENT_REJECTED = "Rejected";
    public static final String PDD_OPS_SUBMITTED = "PDD Complete";
    public static final String PDD_OPS_PARTIAL_SUBMITTED = "PDD Incomplete Re-Submit";
    public static final String PDD_OPS_REJECTED = "OPS Rejected Re-Submit";
    public static final int PDD_INVOICE_ID = 607;
    public static final int PDD_INSURANCE_ID = 608;
    public static final int PDD_RC_ID = 609;
    public static final int PDD_DOC_INSURANCE_ID = 307;
    public static final int PDD_DOC_INVOICE_ID = 309;

//Aadhar Status
    public static final String NSDL_UIDAI_RESPONSE_SUCCESS_DESC = "SUCCESS";
    public static final String NSDL_UIDAI_RESPONSE_SUCCESS_CODE = "S";
    public static final String NSDL_UIDAI_RESPONSE_INCORRECT_DESC = "Aadhaar number is incorrect. Resident shall use correct Aadhaar.";
    public static final String NSDL_UIDAI_RESPONSE_INCORRECT_CODE = "I";
    public static final String NSDL_UIDAI_RESPONSE_MISMATCH_DESC = "Identity data mismatch";
    public static final String NSDL_UIDAI_RESPONSE_MISMATCH_CODE = "M";
    public static final String NSDL_UIDAI_RESPONSE_VERIFICATION_CODE = "U";
    public static final String NSDL_UIDAI_RESPONSE_VERIFICATION_DESC = "Unable to perform ID Verification at this time";
    public static final String NSDL_UIDAI_RESPONSE_ERROR_CODE = "E";
    public static final String NSDL_UIDAI_NOT_APPLICABLE_CODE = "N";

    public static final String PDB_BY_TYPE_APPLICANT = "A";
    public static final String PDB_BY_TYPE_CO_APP = "C";
    public static final String PDB_BY_TYPE_GAURANTOR= "G";
    public static final Character IS_REPAYMENT_ACCOUNT = 'Y';
    public static final String PDC_BY_APPLICANT = "A";
    public static final String PDC_BY_CO_APPLICANT = "C";
    public static final String SANGAM_MC_STATUS = "A";
    public static final String BP_TYPE_SUPPLIER = "S1";
    public static final String BP_TYPE_CUSTOMER = "LS";
    public static final String RCPTPMNT_FLAG = "R";
    public static final Character SANGAM_STATUS = 'A';
    public static final String CHANNEL_CODE = "TWCCP";

    public static final Long CHARGES_DEF_PROMOTION = 0L;
    //LDAP 

    public static final String SUCCESS_AUTHENTICATION = "ok";
    public static final String FAILURE_AUTHENTICATION = "Logon failure: unknown user name or bad password.";
    public static final String NETWORK_ERROR = "Could not send Message.";

    //DAtta-----------
    public static final String Super_Elite_PromotionID = "31";
    public static final String PAN_VALID = "V";
    public static final String PAN_INVALID = "I";
    public static final String PAN_ERROR = "E";
    public static final String PAN_NA = "N";
    public static final String SALES_TRACKED_DOCUMENT_SUBMIT_STATUS = "Submitted to branch";
    public static final String BRANCH_APPROVE_STATUS = "Sent to RMC";
    public static final String BRANCH_REJECT_STATUS = "Sent back to  Sales team";

    public static final String NOT_SUBMITTED_BRANCH = "Pending for Submission";
    public static final Character RETRY_INACTIVE = 'Y';
    public static final String CHANNEL_CODE_AGRI = "TWFMR";

    public static final String RCU_PENDING = "PENDING FOR SCREENING";
    public static final String RCU_SCREENED = "SCREENED";
    public static final String RCU_SAMPLED = "SAMPLED";
    public static final String RCU_SAMPLED_PENDING = "SAMPLED & VERIFYING";
    public static final String RCU_HOLD = "HOLD";
    //RCU TRIGGER
    public static final String RCU_TRIGGER_PENDING = "Pending for screening";
    public static final String RCU_TRIGGER_SAVED = "Saved";
    public static final String RCU_TRIGGER_IN_PROCESS = "Screening in process";
    public static final String RCU_TRIGGER_SUBMISSION_COMPLETED = "Submission completed";
    public static final String TOKEN_TYPE="TOKEN";
    public static final String RCU_TRIGGER_TYPE_REQUEST="REQUEST";
    public static final String TOKEN_USERNAME="username";
    public static final String TOKEN_PASSWORD="password";
    public static final String TOKEN_CLIENTID="client_id";
    public static final String TOKEN_CLIENT_SECRET="client_secret";
    public static final String TOKEN_GRANT_TYPE="grant_type";
    public static final String TOKEN_TOKEN_URL="token_url";
    public static final String RCU_TRIGGER_TOKEN_WS_URL="los_ws_url";
    public static final String RCU_TRIGGER_TOKEN_MIDDLEWARE_URL="rcu_trigger_update";
    public static final String RCU_TRIGGER_UNAUTHORIZED="401 Unauthorized";
   public static final String RCU_TRIGGER_NOT_FOUND= "404 Not Found";
   public static final String RCU_TRIGGER_DOCUMENT= "DOCUMENT";
   public static final String RCU_TRIGGER_DOCUMENT_URL= "doc_download_url";
   public static final String RCU_TRIGGER_LOG_DATA= "DATA";
   public static final String RCU_TRIGGER_LOG_DOCUMENT= "DOCUMENT";
   
    
    
    public static final String AUTO_RETRY = "AUTO_RETRY";

    public static final String AUTO_RETRY_PRE_DWS = "PREE_DWS";
    public static final String AUTO_RETRY_POST_DWS = "POST_DWS";
    public static final String AUTO_RETRY_PRE_HL = "PREE_HL";
    public static final String AUTO_RETRY_POST_HL = "POST_HL";
    public static final String AUTO_RETRY_LPC_HL = "LPC_HL";
    public static final String AUTO_RETRY_DIHOLD_DWS = "DIHOLD_DWS";
    public static final String AUTO_RETRY_DIHOLD_HL = "DIHOLD_HL";
    public static final String AUTO_RETRY_UWHOLD_DWS = "UWHOLD_DWS";
    public static final String AUTO_RETRY_UWHOLD_HL = "UWHOLD_HL";

    public static final String AUTO_RETRY_TYPE_PRE = "PRE";
    public static final String AUTO_RETRY_TYPE_POST = "POST";
    public static final String AUTO_RETRY_TYPE_LPC = "LPC";
    public static final String AUTO_RETRY_TYPE_DIHOLD = "DIHOLD";
    public static final String AUTO_RETRY_TYPE_UWHOLD = "UWHOLD";

    public static final String AUTO_RETRY_FOR_DWS = "DWS";
    public static final String AUTO_RETRY_FOR_HL = "HL";

    public static final String DI_HOLD_STATUS = "DI On Hold";
    public static final String DI_HOLD_SUBMIT_STATUS = "Application under Post-Sanction Process";
    public static final String DI_HOLD_RESUBMIT_STATUS = "DIHOLD Re-Submit";
    
    public static final String UW_HOLD_STATUS = "On Hold";
    public static final String UW_HOLD_SUBMIT_STATUS = "Application under Process";
    public static final String UW_HOLD_RESUBMIT_STATUS = "UW-HOLD Re-Submit";

    public static final String Pre_Sanction_Dashboard = "Pre Sanction";
    public static final String Post_Sanction_Dashboard = "Post Sanction";
    public static final String Status_Dashboard = "Status";
    public static final String Ops_Dashboard = "Ops";
    public static final String Rcu_Dashboard = "Rcu";
    public static final String Ftt_Dashboard = "Ftt";
    public static final String Lpc_Dashboard = "Lpc";
    public static final String Sales_Dashboard = "Sales";
    public static final String Di_Hold_Dashboard = "Di Hold";
    public static final String Pdd_Dashboard = "Pdd";
    public static final String QDE_SUBMITTED_DESC = "FI Saved";
    public static final String FI_SAVE = "AC";
    public static final String FI_SUBMITTED = "AD";
    public static final String FI_SAVE_DESC = "FI Saved";
    public static final String FI_SUBMITTED_DESC = "FI Submitted";

    public static final String Duplicate_Record_Msg_Pattern = "Duplicate Transaction entry for";
    public static final String APPLICATION_ALREADY_EXIST="Loan application with source_app_id";
    public static final String APPLICATION_ALREADY_EXIST_POST="exists in PostSourcingStg";

//       public static final String NOT_SUBMITTED="Pending for Submission";
//       public static final Character SALES_DOCUMENT_CHECKED='Y'; 
//     REPORTS PROPERTY
    public static final String FILE_NAME = "REPORT_FILE_NAME";
    public static final String ASM_REPORT_NAME = "ASM";
    public static final String ASM_COUNT_REPORT_NAME = "ASM_COUNT";
    public static final String DAY_COUNT = "DAY_COUNT";
    public static final String HOURLY_DIFFERENCE = "HOURLY_DIFFERENCE";
    public static final String OPS_INV_REPORT_NAME = "INVOICE";
//       public static final String OPS_INV_FILE_NAME="INVOICE_FILE_NAME";
    public static final String OPS_INS_REPORT_NAME = "INSURANCE";
//       public static final String OPS_INS_FILE_NAME="INSURANCE_FILE_NAME";
    public static final String OPS_RC_REPORT_NAME = "RC";
//       public static final String OPS_RC_FILE_NAME="RC_FILE_NAME";
    public static final String OPS_REPORT = "OPS_REPORT";
//       public static final String OPS_REPORT_TIME="OPS_REPORT_TIME";

    //gride for Document Upload
    public static final String DOC_COMPRESS_HEIGHT = "UPLOAD_DOC_HEIGHT";
    public static final String DOC_COMPRESS_WIDTH = "UPLOAD_DOC_WIDTH";
    public static final String DOC_NOT_COMPRESS_SIZE = "UPLOAD_DOC_MIN_SIZE";
    public static final String DOC_COMPRESS_QUALITY = "UPLOAD_DOC_QUALITY";
    public static final String DOC_UPLOAD_MAX_SIZE = "UPLOAD_DOC_MAX_SIZE";
 
    public static final Short PROFILE_SALES=1;
    public static final String ES_USER_TYPE="DLR";

    public static final String DOCUMENT_UPLOAD_URL = "DOCUMENT_UPLOAD_URL";
    public static final String DOCUMENT_DOWNLOAD_URL = "DOCUMENT_DOWNLOAD_URL";
    
    public static final String EMAIL_ID ="EMAIL_ID";
    public static final String EMAIL_PASSWORD ="EMAIL_PASSWORD";
    public static final String EMAIL_LOGIN_ID ="EMAIL_LOGIN_ID";
    public static final String PRODUCT_CONS="TW";
    public static final String PENDING_STATUS="Pending";
    
    public static final String RM_APPLICATION_SAVE = "SAVE";
    public static final String RM_APPLICATION_SUBMITTED = "SUBMITTED";
     public static final String RM_APPLICATION_VERIFIED = "VERIFIED";
    public static final String UID_DOCUMENT_TYPE_OTP_REQUEST = "OTP_REQUEST";
    public static final String UID_DOCUMENT_TYPE_OTP_VERIFY = "OTP_VERIFY";
    
    //-------------MCM
    
    public static final String MCM_ACCOUNT_VALIDATE_URL = "MCM_ACCOUNT_VALIDATE_URL";
    public static final String VALID_STATUS="pass";//valid
    public static String IN_VALID_STATUS="failed";
    public static String INITIATE_DISBURSEMENT_SUCCESS="DISB_SUCCESS";
    public static String INITIATE_DISBURSEMENT_FAILS="DISB_FAILED";
    
    public static final String DLR_PENDING_STATUS="Pending Invoice Confirmation";
    public static final String DLR_SANCTIONED_STATUS="Loan Sanctioned, Invoice Awaited";
    
    public static String INITIATE_DOC_GENERATION_SUCCESS="INI_DOCGEN_SUCCESS";
    public static String INITIATE_DOC_GENERATION_FAILS="INI_DOCGEN_FAILED";
    
    public static String INI_DOC_GEN_SUCCESS="E-Sign Pending";
    public static Character IS_ESIGNED_SUCCESS='Y';
    public static String ESIGN_SUCESS="E-Sign Success";
    
}
