
package com.equifax.services.eport.ws.schemas._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TelecomResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TelecomResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TRField1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRField11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelecomResponseType", propOrder = {
    "trField1",
    "trField2",
    "trField3",
    "trField4",
    "trField5",
    "trField6",
    "trField7",
    "trField8",
    "trField9",
    "trField10",
    "trField11"
})
public class TelecomResponseType {

    @XmlElement(name = "TRField1")
    protected String trField1;
    @XmlElement(name = "TRField2")
    protected String trField2;
    @XmlElement(name = "TRField3")
    protected String trField3;
    @XmlElement(name = "TRField4")
    protected String trField4;
    @XmlElement(name = "TRField5")
    protected String trField5;
    @XmlElement(name = "TRField6")
    protected String trField6;
    @XmlElement(name = "TRField7")
    protected String trField7;
    @XmlElement(name = "TRField8")
    protected String trField8;
    @XmlElement(name = "TRField9")
    protected String trField9;
    @XmlElement(name = "TRField10")
    protected String trField10;
    @XmlElement(name = "TRField11")
    protected String trField11;

    /**
     * Gets the value of the trField1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField1() {
        return trField1;
    }

    /**
     * Sets the value of the trField1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField1(String value) {
        this.trField1 = value;
    }

    /**
     * Gets the value of the trField2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField2() {
        return trField2;
    }

    /**
     * Sets the value of the trField2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField2(String value) {
        this.trField2 = value;
    }

    /**
     * Gets the value of the trField3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField3() {
        return trField3;
    }

    /**
     * Sets the value of the trField3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField3(String value) {
        this.trField3 = value;
    }

    /**
     * Gets the value of the trField4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField4() {
        return trField4;
    }

    /**
     * Sets the value of the trField4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField4(String value) {
        this.trField4 = value;
    }

    /**
     * Gets the value of the trField5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField5() {
        return trField5;
    }

    /**
     * Sets the value of the trField5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField5(String value) {
        this.trField5 = value;
    }

    /**
     * Gets the value of the trField6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField6() {
        return trField6;
    }

    /**
     * Sets the value of the trField6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField6(String value) {
        this.trField6 = value;
    }

    /**
     * Gets the value of the trField7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField7() {
        return trField7;
    }

    /**
     * Sets the value of the trField7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField7(String value) {
        this.trField7 = value;
    }

    /**
     * Gets the value of the trField8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField8() {
        return trField8;
    }

    /**
     * Sets the value of the trField8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField8(String value) {
        this.trField8 = value;
    }

    /**
     * Gets the value of the trField9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField9() {
        return trField9;
    }

    /**
     * Sets the value of the trField9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField9(String value) {
        this.trField9 = value;
    }

    /**
     * Gets the value of the trField10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField10() {
        return trField10;
    }

    /**
     * Sets the value of the trField10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField10(String value) {
        this.trField10 = value;
    }

    /**
     * Gets the value of the trField11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRField11() {
        return trField11;
    }

    /**
     * Sets the value of the trField11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRField11(String value) {
        this.trField11 = value;
    }

}
