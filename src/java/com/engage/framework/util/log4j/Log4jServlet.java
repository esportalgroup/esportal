/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.framework.util.log4j;


import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.parsers.FactoryConfigurationError;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DELL
 */
public class Log4jServlet extends HttpServlet {

    CustomLogger logger;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            logger = new CustomLogger("Log4jServlet");
            System.setProperty("jcifs.smb.client.dfs.disabled","true");
            System.setProperty("https.protocols", "TLSv1");
            ApplicationPropertyManager.applicationPath=config.getServletContext().getRealPath("")+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator");
        }catch (FactoryConfigurationError fce) {
//            fce.printStackTrace();
            throw new ServletException();
        } catch (Exception e) {
//            e.printStackTrace();
            logger.fatal("Could not load Log4jServlet & web directory.Exception Follows;", e);
            throw new ServletException();
        }
        
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
//        System.out.println("HELLO");
        try {  
            
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
