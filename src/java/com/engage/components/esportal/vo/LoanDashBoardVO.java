/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author ven00288
 */
public class LoanDashBoardVO {
    
    
private String applicationId;
private String customerName;
private String projectName;
private String towerName;
private String flatNo;
private String sanctionedAmt;
private String disburseAmt;
private String noOfTranchesDisbursed;
private String constStageOfNextTrancheDue;
private String progress;
private String caseCount;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTowerName() {
        return towerName;
    }

    public void setTowerName(String towerName) {
        this.towerName = towerName;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getSanctionedAmt() {
        return sanctionedAmt;
    }

    public void setSanctionedAmt(String sanctionedAmt) {
        this.sanctionedAmt = sanctionedAmt;
    }

    public String getDisburseAmt() {
        return disburseAmt;
    }

    public void setDisburseAmt(String disburseAmt) {
        this.disburseAmt = disburseAmt;
    }

    public String getNoOfTranchesDisbursed() {
        return noOfTranchesDisbursed;
    }

    public void setNoOfTranchesDisbursed(String noOfTranchesDisbursed) {
        this.noOfTranchesDisbursed = noOfTranchesDisbursed;
    }

    public String getConstStageOfNextTrancheDue() {
        return constStageOfNextTrancheDue;
    }

    public void setConstStageOfNextTrancheDue(String constStageOfNextTrancheDue) {
        this.constStageOfNextTrancheDue = constStageOfNextTrancheDue;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getCaseCount() {
        return caseCount;
    }

    public void setCaseCount(String caseCount) {
        this.caseCount = caseCount;
    }




    
}
