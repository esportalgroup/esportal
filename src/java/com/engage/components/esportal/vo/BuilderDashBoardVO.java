/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author VEN00288
 */
public class BuilderDashBoardVO {
    private String builderC;
     private String builderIdC;
     private String builderCode;
     private String accountName;
     private String apfStatus;
     private String apfProjectC;
     private String progress;
    private String caseCount;

    public String getBuilderC() {
        return builderC;
    }

    public void setBuilderC(String builderC) {
        this.builderC = builderC;
    }

    public String getBuilderIdC() {
        return builderIdC;
    }

    public void setBuilderIdC(String builderIdC) {
        this.builderIdC = builderIdC;
    }

    public String getBuilderCode() {
        return builderCode;
    }

    public void setBuilderCode(String builderCode) {
        this.builderCode = builderCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getApfStatus() {
        return apfStatus;
    }

    public void setApfStatus(String apfStatus) {
        this.apfStatus = apfStatus;
    }

    public String getApfProjectC() {
        return apfProjectC;
    }

    public void setApfProjectC(String apfProjectC) {
        this.apfProjectC = apfProjectC;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getCaseCount() {
        return caseCount;
    }

    public void setCaseCount(String caseCount) {
        this.caseCount = caseCount;
    }
    
     
     
}
