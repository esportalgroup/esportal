/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author DattatrayT
 */
public class OpsDocumentVO {
    private String Id;
    private String Ops_Decision__c;
    private String Ops_Remarks__c;
    private String Ops_Tagging_Description__c;

    @JsonProperty("Id")
    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    @JsonProperty("Ops_Decision__c")
    public String getOps_Decision__c() {
        return Ops_Decision__c;
    }

    public void setOps_Decision__c(String Ops_Decision__c) {
        this.Ops_Decision__c = Ops_Decision__c;
    }

    @JsonProperty("Ops_Remarks__c")
    public String getOps_Remarks__c() {
        return Ops_Remarks__c;
    }

    public void setOps_Remarks__c(String Ops_Remarks__c) {
        this.Ops_Remarks__c = Ops_Remarks__c;
    }

    @JsonProperty("Ops_Tagging_Description__c")
    public String getOps_Tagging_Description__c() {
        return Ops_Tagging_Description__c;
    }

    public void setOps_Tagging_Description__c(String Ops_Tagging_Description__c) {
        this.Ops_Tagging_Description__c = Ops_Tagging_Description__c;
    }
    
}
