/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import java.util.List;

/**
 *
 * @author SurajK
 */
public class RCUTriggerApplicantVo {
    
    public String applicantName;
    public String applicantId;
    public String applicantType;
    public String applicantAddress;
    public String profileStatus;
    public String applicantAction;
    public List<RCUTriggerDocumentVo> documents;

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getApplicantAddress() {
        return applicantAddress;
    }

    public void setApplicantAddress(String applicantAddress) {
        this.applicantAddress = applicantAddress;
    }

    public List<RCUTriggerDocumentVo> getDocuments() {
        return documents;
    }

    public void setDocuments(List<RCUTriggerDocumentVo> documents) {
        this.documents = documents;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getApplicantAction() {
        return applicantAction;
    }

    public void setApplicantAction(String applicantAction) {
        this.applicantAction = applicantAction;
    }
    
}
