/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author ven00288
 */
public class PendindStatusDashBoardVO {
    private String applicationId;
    private String applicatName;
    private String applicationDate;
    private String dsaName;
     private String flsName;
         private String Supplier_Name;
    private String Supplier_ID;
    private String StampDuty;
    private String Processing_Fees;
    private String MarginMoney_DocID;
    private String Margin_Money;
    private String advEmi;
    private String Loan_Amount;
    private String InvoiceNumber;
    private String Invoice_DocID;
    private String Id;
    private String FLS_Name;
    private String FLS_ID;
    private String EngineNumber;
    private String ECS_charges;
    private String DSA_Name;
    private String DSA_ID;
    private String Doc_Charges;
    private String Co_Applicant_Name;
    private String CLI_Premium;
    private String ChassisNumber;
    private String AssetModel;
    private String AssetCost_Tax;
    private String AssetCost_OnboardingPrice;
    private String AssetCost_Ins;
    private String AssetCost_ESR;
    private String Asset_Manufacturer;
    private String Asset_Make;
    private String Application_ID;
    private String Application_Date;
    private String Applicant_Name;
    private String product;
    private String status;
    private String progress;
    private int caseCount;
    private Float Other_charges;
    private Float paymentToDlr;
    

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicatName() {
        return applicatName;
    }

    public void setApplicatName(String applicatName) {
        this.applicatName = applicatName;
    }

   

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

 

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public int getCaseCount() {
        return caseCount;
    }

    public void setCaseCount(int caseCount) {
        this.caseCount = caseCount;
    }

    public String getDsaName() {
        return dsaName;
    }

    public void setDsaName(String dsaName) {
        this.dsaName = dsaName;
    }

    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    public String getSupplier_Name() {
        return Supplier_Name;
    }

    public void setSupplier_Name(String Supplier_Name) {
        this.Supplier_Name = Supplier_Name;
    }

    public String getSupplier_ID() {
        return Supplier_ID;
    }

    public void setSupplier_ID(String Supplier_ID) {
        this.Supplier_ID = Supplier_ID;
    }

    public String getStampDuty() {
        return StampDuty;
    }

    public void setStampDuty(String StampDuty) {
        this.StampDuty = StampDuty;
    }

    public String getProcessing_Fees() {
        return Processing_Fees;
    }

    public void setProcessing_Fees(String Processing_Fees) {
        this.Processing_Fees = Processing_Fees;
    }

    public String getMarginMoney_DocID() {
        return MarginMoney_DocID;
    }

    public void setMarginMoney_DocID(String MarginMoney_DocID) {
        this.MarginMoney_DocID = MarginMoney_DocID;
    }

    public String getMargin_Money() {
        return Margin_Money;
    }

    public void setMargin_Money(String Margin_Money) {
        this.Margin_Money = Margin_Money;
    }

    public String getLoan_Amount() {
        return Loan_Amount;
    }

    public void setLoan_Amount(String Loan_Amount) {
        this.Loan_Amount = Loan_Amount;
    }

    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String InvoiceNumber) {
        this.InvoiceNumber = InvoiceNumber;
    }

    public String getInvoice_DocID() {
        return Invoice_DocID;
    }

    public void setInvoice_DocID(String Invoice_DocID) {
        this.Invoice_DocID = Invoice_DocID;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getFLS_Name() {
        return FLS_Name;
    }

    public void setFLS_Name(String FLS_Name) {
        this.FLS_Name = FLS_Name;
    }

    public String getFLS_ID() {
        return FLS_ID;
    }

    public void setFLS_ID(String FLS_ID) {
        this.FLS_ID = FLS_ID;
    }

    public String getEngineNumber() {
        return EngineNumber;
    }

    public void setEngineNumber(String EngineNumber) {
        this.EngineNumber = EngineNumber;
    }

    public String getECS_charges() {
        return ECS_charges;
    }

    public void setECS_charges(String ECS_charges) {
        this.ECS_charges = ECS_charges;
    }

    public String getDSA_Name() {
        return DSA_Name;
    }

    public void setDSA_Name(String DSA_Name) {
        this.DSA_Name = DSA_Name;
    }

    public String getDSA_ID() {
        return DSA_ID;
    }

    public void setDSA_ID(String DSA_ID) {
        this.DSA_ID = DSA_ID;
    }

    public String getDoc_Charges() {
        return Doc_Charges;
    }

    public void setDoc_Charges(String Doc_Charges) {
        this.Doc_Charges = Doc_Charges;
    }

    public String getCo_Applicant_Name() {
        return Co_Applicant_Name;
    }

    public void setCo_Applicant_Name(String Co_Applicant_Name) {
        this.Co_Applicant_Name = Co_Applicant_Name;
    }

    public String getCLI_Premium() {
        return CLI_Premium;
    }

    public void setCLI_Premium(String CLI_Premium) {
        this.CLI_Premium = CLI_Premium;
    }

    public String getChassisNumber() {
        return ChassisNumber;
    }

    public void setChassisNumber(String ChassisNumber) {
        this.ChassisNumber = ChassisNumber;
    }

    public String getAssetModel() {
        return AssetModel;
    }

    public void setAssetModel(String AssetModel) {
        this.AssetModel = AssetModel;
    }

    public String getAssetCost_Tax() {
        return AssetCost_Tax;
    }

    public void setAssetCost_Tax(String AssetCost_Tax) {
        this.AssetCost_Tax = AssetCost_Tax;
    }

    public String getAssetCost_OnboardingPrice() {
        return AssetCost_OnboardingPrice;
    }

    public void setAssetCost_OnboardingPrice(String AssetCost_OnboardingPrice) {
        this.AssetCost_OnboardingPrice = AssetCost_OnboardingPrice;
    }

    public String getAssetCost_Ins() {
        return AssetCost_Ins;
    }

    public void setAssetCost_Ins(String AssetCost_Ins) {
        this.AssetCost_Ins = AssetCost_Ins;
    }

    public String getAssetCost_ESR() {
        return AssetCost_ESR;
    }

    public void setAssetCost_ESR(String AssetCost_ESR) {
        this.AssetCost_ESR = AssetCost_ESR;
    }

    public String getAsset_Manufacturer() {
        return Asset_Manufacturer;
    }

    public void setAsset_Manufacturer(String Asset_Manufacturer) {
        this.Asset_Manufacturer = Asset_Manufacturer;
    }

    public String getAsset_Make() {
        return Asset_Make;
    }

    public void setAsset_Make(String Asset_Make) {
        this.Asset_Make = Asset_Make;
    }

    public String getApplication_ID() {
        return Application_ID;
    }

    public void setApplication_ID(String Application_ID) {
        this.Application_ID = Application_ID;
    }

    public String getApplication_Date() {
        return Application_Date;
    }

    public void setApplication_Date(String Application_Date) {
        this.Application_Date = Application_Date;
    }

    public String getApplicant_Name() {
        return Applicant_Name;
    }

    public void setApplicant_Name(String Applicant_Name) {
        this.Applicant_Name = Applicant_Name;
    }

    public Float getOther_charges() {
        return Other_charges;
    }

    public void setOther_charges(Float Other_charges) {
        this.Other_charges = Other_charges;
    }

    public Float getPaymentToDlr() {
        return paymentToDlr;
    }

    public void setPaymentToDlr(Float paymentToDlr) {
        this.paymentToDlr = paymentToDlr;
    }

    public String getAdvEmi() {
        return advEmi;
    }

    public void setAdvEmi(String advEmi) {
        this.advEmi = advEmi;
    }
    
}
