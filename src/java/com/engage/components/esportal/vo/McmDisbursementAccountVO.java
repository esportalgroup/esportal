/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author DattatrayT
 */
public class McmDisbursementAccountVO {
     private String mcCode;
     private String disbBank;
     private String disbaccount;
     private String accountid;
     private String signatories;

    public String getMcCode() {
        return mcCode;
    }

    public void setMcCode(String mcCode) {
        this.mcCode = mcCode;
    }

    public String getDisbBank() {
        return disbBank;
    }

    public void setDisbBank(String disbBank) {
        this.disbBank = disbBank;
    }

    public String getDisbaccount() {
        return disbaccount;
    }

    public void setDisbaccount(String disbaccount) {
        this.disbaccount = disbaccount;
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public String getSignatories() {
        return signatories;
    }

    public void setSignatories(String signatories) {
        this.signatories = signatories;
    }
     
}
