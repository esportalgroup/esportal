/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author Shrikant Katakdhond
 */
public class PddDocMarginMoneyDetailVo {
    
    private String marginMoneyDocUuid;

    public String getMarginMoneyDocUuid() {
        return marginMoneyDocUuid;
    }

    public void setMarginMoneyDocUuid(String marginMoneyDocUuid) {
        this.marginMoneyDocUuid = marginMoneyDocUuid;
    }
    
}
