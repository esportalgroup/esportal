/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author DattatrayT
 */
public class OpsUpdateContentVO {
    private OpsApplicationVO opsApplicationVO;
    private List<OpsDocumentVO> opsDocumentVO;

    @JsonProperty("application")
    public OpsApplicationVO getOpsApplicationVO() {
        return opsApplicationVO;
    }

    public void setOpsApplicationVO(OpsApplicationVO opsApplicationVO) {
        this.opsApplicationVO = opsApplicationVO;
    }

    @JsonProperty("Documents")
    public List<OpsDocumentVO> getOpsDocumentVO() {
        return opsDocumentVO;
    }

    public void setOpsDocumentVO(List<OpsDocumentVO> opsDocumentVO) {
        this.opsDocumentVO = opsDocumentVO;
    }
    
}
