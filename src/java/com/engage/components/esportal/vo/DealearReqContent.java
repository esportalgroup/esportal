/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ven00288
 */
public class DealearReqContent {
    
     private String dealerId ;
     private String limitQ ;
     private String lastNoDays ;
     private String Status ;
     private String ProductId ;

      @JsonProperty("dealerId")
    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

     @JsonProperty("limitQ")
    public String getLimitQ() {
        return limitQ;
    }

    public void setLimitQ(String limitQ) {
        this.limitQ = limitQ;
    }

     @JsonProperty("lastNoDays")
    public String getLastNoDays() {
        return lastNoDays;
    }

    public void setLastNoDays(String lastNoDays) {
        this.lastNoDays = lastNoDays;
    }

     @JsonProperty("Status")
    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

     @JsonProperty("ProductId")
    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String ProductId) {
        this.ProductId = ProductId;
    }

   
     
     
    
     
                          
    
}
