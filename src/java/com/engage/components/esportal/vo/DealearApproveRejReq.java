/*
 * To change this license header choose License Headers in Project Properties.
 * To change this template file choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ven00288
 */
public class DealearApproveRejReq {

    private String Id;
    private String Dealer_Portal_Status__c;
    private String Chassis_number__c;
    private String Engine_number__c;
    private String Invoice_number__c;

    
    @JsonProperty("Id")
    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    @JsonProperty("Dealer_Portal_Status__c")
    public String getDealer_Portal_Status__c() {
        return Dealer_Portal_Status__c;
    }

    public void setDealer_Portal_Status__c(String Dealer_Portal_Status__c) {
        this.Dealer_Portal_Status__c = Dealer_Portal_Status__c;
    }

    @JsonProperty("Chassis_number__c")
    public String getChassis_number__c() {
        return Chassis_number__c;
    }

    public void setChassis_number__c(String Chassis_number__c) {
        this.Chassis_number__c = Chassis_number__c;
    }

    @JsonProperty("Engine_number__c")
    public String getEngine_number__c() {
        return Engine_number__c;
    }

    public void setEngine_number__c(String Engine_number__c) {
        this.Engine_number__c = Engine_number__c;
    }

    @JsonProperty("Invoice_number__c")
    public String getInvoice_number__c() {
        return Invoice_number__c;
    }

    public void setInvoice_number__c(String Invoice_number__c) {
        this.Invoice_number__c = Invoice_number__c;
    }

}
