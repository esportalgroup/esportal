/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author SurajK
 */
public class RCUTriggerDocumentVo {
    
    public String docId;
    public String docName;
    public String childDocId;
    public String childDocName;
    public String documentUid;
    public String fileName;
    public Boolean screeningCompleted;
    public String pickForSampling;
    public String samplingStatus;
    public String docVersion;
    public String uuid;
    
    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getChildDocId() {
        return childDocId;
    }

    public void setChildDocId(String childDocId) {
        this.childDocId = childDocId;
    }

    public String getChildDocName() {
        return childDocName;
    }

    public void setChildDocName(String childDocName) {
        this.childDocName = childDocName;
    }

    public String getDocVersion() {
        return docVersion;
    }

    public void setDocVersion(String docVersion) {
        this.docVersion = docVersion;
    }

    public String getDocumentUid() {
        return documentUid;
    }

    public void setDocumentUid(String documentUid) {
        this.documentUid = documentUid;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Boolean getScreeningCompleted() {
        return screeningCompleted;
    }

    public void setScreeningCompleted(Boolean screeningCompleted) {
        this.screeningCompleted = screeningCompleted;
    }

    public String getPickForSampling() {
        return pickForSampling;
    }

    public void setPickForSampling(String pickForSampling) {
        this.pickForSampling = pickForSampling;
    }

    public String getSamplingStatus() {
        return samplingStatus;
    }

    public void setSamplingStatus(String samplingStatus) {
        this.samplingStatus = samplingStatus;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
  
}
