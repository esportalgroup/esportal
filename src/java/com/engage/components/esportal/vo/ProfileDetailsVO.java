/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author Shrikant Katakdhond
 */
public class ProfileDetailsVO {
    
private String Address_Line1;
private String Address_Line2;
private String Address_Line3;
private String Address_Line4;
private String Address_Type;
private String City;
private String Country;
private String State;
private String STD_ISD;
private String Phone;

private String clcommon_Account;
private String clcommon_Bank_Name;
private String Branch_Name;
private String Location;
private String clcommon_Bank_Account_Number;
private String Payment_Mode;
private String clcommon_Bank_Account_Name;

private String Supplier_Id;
private String Description;
private String Sales_ST_Number;
private String Central_ST_Number;
private String Income_Tax_Number;
private String Supplier_Dealer_Flag;
private String Internal_Flag;
private String Dealer_Commission;
private String VAT_Account_Number;
private String Aggregate_Limit;
private String OS_RC_AMT;
private String Active;
private String Valid_From;
private String Valid_To;
private String PAN_ID;
private String GST_Number;
private String Registered_Under_GST;
private String Exemption_Flag;
private String Exemption_Period_From;
private String Exemption_Period_To;
private String RecordTypeId;

    public String getAddress_Line1() {
        return Address_Line1;
    }

    public void setAddress_Line1(String Address_Line1) {
        this.Address_Line1 = Address_Line1;
    }

    public String getAddress_Line2() {
        return Address_Line2;
    }

    public void setAddress_Line2(String Address_Line2) {
        this.Address_Line2 = Address_Line2;
    }

    public String getAddress_Line3() {
        return Address_Line3;
    }

    public void setAddress_Line3(String Address_Line3) {
        this.Address_Line3 = Address_Line3;
    }

    public String getAddress_Line4() {
        return Address_Line4;
    }

    public void setAddress_Line4(String Address_Line4) {
        this.Address_Line4 = Address_Line4;
    }

    public String getAddress_Type() {
        return Address_Type;
    }

    public void setAddress_Type(String Address_Type) {
        this.Address_Type = Address_Type;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }

    public String getSTD_ISD() {
        return STD_ISD;
    }

    public void setSTD_ISD(String STD_ISD) {
        this.STD_ISD = STD_ISD;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getClcommon_Account() {
        return clcommon_Account;
    }

    public void setClcommon_Account(String clcommon_Account) {
        this.clcommon_Account = clcommon_Account;
    }

    public String getClcommon_Bank_Name() {
        return clcommon_Bank_Name;
    }

    public void setClcommon_Bank_Name(String clcommon_Bank_Name) {
        this.clcommon_Bank_Name = clcommon_Bank_Name;
    }

    public String getBranch_Name() {
        return Branch_Name;
    }

    public void setBranch_Name(String Branch_Name) {
        this.Branch_Name = Branch_Name;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public String getClcommon_Bank_Account_Number() {
        return clcommon_Bank_Account_Number;
    }

    public void setClcommon_Bank_Account_Number(String clcommon_Bank_Account_Number) {
        this.clcommon_Bank_Account_Number = clcommon_Bank_Account_Number;
    }

    public String getPayment_Mode() {
        return Payment_Mode;
    }

    public void setPayment_Mode(String Payment_Mode) {
        this.Payment_Mode = Payment_Mode;
    }

    public String getClcommon_Bank_Account_Name() {
        return clcommon_Bank_Account_Name;
    }

    public void setClcommon_Bank_Account_Name(String clcommon_Bank_Account_Name) {
        this.clcommon_Bank_Account_Name = clcommon_Bank_Account_Name;
    }

    public String getSupplier_Id() {
        return Supplier_Id;
    }

    public void setSupplier_Id(String Supplier_Id) {
        this.Supplier_Id = Supplier_Id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getSales_ST_Number() {
        return Sales_ST_Number;
    }

    public void setSales_ST_Number(String Sales_ST_Number) {
        this.Sales_ST_Number = Sales_ST_Number;
    }

    public String getCentral_ST_Number() {
        return Central_ST_Number;
    }

    public void setCentral_ST_Number(String Central_ST_Number) {
        this.Central_ST_Number = Central_ST_Number;
    }

    public String getIncome_Tax_Number() {
        return Income_Tax_Number;
    }

    public void setIncome_Tax_Number(String Income_Tax_Number) {
        this.Income_Tax_Number = Income_Tax_Number;
    }

    public String getSupplier_Dealer_Flag() {
        return Supplier_Dealer_Flag;
    }

    public void setSupplier_Dealer_Flag(String Supplier_Dealer_Flag) {
        this.Supplier_Dealer_Flag = Supplier_Dealer_Flag;
    }

    public String getInternal_Flag() {
        return Internal_Flag;
    }

    public void setInternal_Flag(String Internal_Flag) {
        this.Internal_Flag = Internal_Flag;
    }

    public String getDealer_Commission() {
        return Dealer_Commission;
    }

    public void setDealer_Commission(String Dealer_Commission) {
        this.Dealer_Commission = Dealer_Commission;
    }

    public String getVAT_Account_Number() {
        return VAT_Account_Number;
    }

    public void setVAT_Account_Number(String VAT_Account_Number) {
        this.VAT_Account_Number = VAT_Account_Number;
    }

    public String getAggregate_Limit() {
        return Aggregate_Limit;
    }

    public void setAggregate_Limit(String Aggregate_Limit) {
        this.Aggregate_Limit = Aggregate_Limit;
    }

    public String getOS_RC_AMT() {
        return OS_RC_AMT;
    }

    public void setOS_RC_AMT(String OS_RC_AMT) {
        this.OS_RC_AMT = OS_RC_AMT;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String Active) {
        this.Active = Active;
    }

    public String getValid_From() {
        return Valid_From;
    }

    public void setValid_From(String Valid_From) {
        this.Valid_From = Valid_From;
    }

    public String getValid_To() {
        return Valid_To;
    }

    public void setValid_To(String Valid_To) {
        this.Valid_To = Valid_To;
    }

    public String getPAN_ID() {
        return PAN_ID;
    }

    public void setPAN_ID(String PAN_ID) {
        this.PAN_ID = PAN_ID;
    }

    public String getGST_Number() {
        return GST_Number;
    }

    public void setGST_Number(String GST_Number) {
        this.GST_Number = GST_Number;
    }

    public String getRegistered_Under_GST() {
        return Registered_Under_GST;
    }

    public void setRegistered_Under_GST(String Registered_Under_GST) {
        this.Registered_Under_GST = Registered_Under_GST;
    }

    public String getExemption_Flag() {
        return Exemption_Flag;
    }

    public void setExemption_Flag(String Exemption_Flag) {
        this.Exemption_Flag = Exemption_Flag;
    }

    public String getExemption_Period_From() {
        return Exemption_Period_From;
    }

    public void setExemption_Period_From(String Exemption_Period_From) {
        this.Exemption_Period_From = Exemption_Period_From;
    }

    public String getExemption_Period_To() {
        return Exemption_Period_To;
    }

    public void setExemption_Period_To(String Exemption_Period_To) {
        this.Exemption_Period_To = Exemption_Period_To;
    }

    public String getRecordTypeId() {
        return RecordTypeId;
    }

    public void setRecordTypeId(String RecordTypeId) {
        this.RecordTypeId = RecordTypeId;
    }

    
}
