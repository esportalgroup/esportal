/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author Datta
 */
public class RCUDashboardVO {
    String applicantId;
    String applicationNo;
    String Branch_Name;
    String Associate_Name;
    String PS_NO;
    String Status;
    String custName;
    String productCategory;
    String caseCount;
    
    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getApplicationNo() {
        return applicationNo;
    }

    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    public String getBranch_Name() {
        return Branch_Name;
    }

    public void setBranch_Name(String Branch_Name) {
        this.Branch_Name = Branch_Name;
    }

    public String getAssociate_Name() {
        return Associate_Name;
    }

    public void setAssociate_Name(String Associate_Name) {
        this.Associate_Name = Associate_Name;
    }

    public String getPS_NO() {
        return PS_NO;
    }

    public void setPS_NO(String PS_NO) {
        this.PS_NO = PS_NO;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

     public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }
    
    public String getCaseCount() {
        return caseCount;
    }

    public void setCaseCount(String caseCount) {
        this.caseCount = caseCount;
    }

   
        
}
