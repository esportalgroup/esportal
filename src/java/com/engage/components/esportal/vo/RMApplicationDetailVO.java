/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import java.util.List;

/**
 *
 * @author Shrikant Katakdhond
 */
public class RMApplicationDetailVO {
    
    private Long applicationId;
    private Long lanNumber;
    private List<RMDetailVO> lstApplicants;

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public Long getLanNumber() {
        return lanNumber;
    }

    public void setLanNumber(Long lanNumber) {
        this.lanNumber = lanNumber;
    }

    public List<RMDetailVO> getLstApplicants() {
        return lstApplicants;
    }

    public void setLstApplicants(List<RMDetailVO> lstApplicants) {
        this.lstApplicants = lstApplicants;
    }
    
    
}
