/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import java.util.Date;

/**
 *
 * @author Shrikant Katakdhond
 */
public class PddDocInvoiceDetailVo {
    
    private String invoiceNumber;
    private String invoiceDate;
    private String engineNumber;
    private String chassisNumber;
    private String hypothicationStatus;
    private String invoiceDocUuid;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getHypothicationStatus() {
        return hypothicationStatus;
    }

    public void setHypothicationStatus(String hypothicationStatus) {
        this.hypothicationStatus = hypothicationStatus;
    }

    public String getInvoiceDocUuid() {
        return invoiceDocUuid;
    }

    public void setInvoiceDocUuid(String invoiceDocUuid) {
        this.invoiceDocUuid = invoiceDocUuid;
    }
    
}
