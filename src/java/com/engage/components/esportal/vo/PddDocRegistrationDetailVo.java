/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import java.util.Date;

/**
 *
 * @author Shrikant Katakdhond
 */
public class PddDocRegistrationDetailVo {

    private String registrationNumber;
    private String registrationDate;
    private String hypothicationStatus;
    private String registrationDocUuid;

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getHypothicationStatus() {
        return hypothicationStatus;
    }

    public void setHypothicationStatus(String hypothicationStatus) {
        this.hypothicationStatus = hypothicationStatus;
    }

    public String getRegistrationDocUuid() {
        return registrationDocUuid;
    }

    public void setRegistrationDocUuid(String registrationDocUuid) {
        this.registrationDocUuid = registrationDocUuid;
    }

}
