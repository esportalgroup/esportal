/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author SurajK
 */
public class LosRequestVO {
    
    public String FI_Status__c;

    public String getFI_Status__c() {
        return FI_Status__c;
    }

    public void setFI_Status__c(String FI_Status__c) {
        this.FI_Status__c = FI_Status__c;
    }

    @Override
    public String toString() {
        return "LosRequestVO{" + "FI_Status__c=" + FI_Status__c + '}';
    }
    
}
