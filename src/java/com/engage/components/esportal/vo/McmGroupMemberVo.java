/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.engage.components.esportal.mcm.vo.McmApplicantsVO;
import java.util.List;

/**
 *
 * @author Shrikant Katakdhond
 */
public class McmGroupMemberVo {
    
    private String grpId;
    private String memberId;
    private String memberName;
    private String loanAmount;
    private String loanTenure;
    private String emi;
    private String procFee;
    private String cliAmount;
    private String netDisbursementAmt;
    private String appraisalflag;
    private String modeOfDisbursement;
    private String bankAccountNumber;
    private String ifscCode;
    private String accountValidationStatus;
    private String validAccountWSCount;
    private String createBy;
    private String createDate;
    private String modDate;
    private String modBy;
    private String isActive;
    private String applicationId;
    private String adharNumber;
//    private String primaryApplicant;
//    private String primaryApplicantId;
//    private String coApplicant;
//    private String coApplicantId;
//    private String coRelation;
    private String flsCode;
    private String flsName;
    private String mobileNo;
    private String isAccountValid;
    
     private String groupId;
    private List<McmApplicantsVO> applicants;
    public String getGrpId() {
        return grpId;
    }

    public void setGrpId(String grpId) {
        this.grpId = grpId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getLoanTenure() {
        return loanTenure;
    }

    public void setLoanTenure(String loanTenure) {
        this.loanTenure = loanTenure;
    }

    public String getEmi() {
        return emi;
    }

    public void setEmi(String emi) {
        this.emi = emi;
    }

    public String getProcFee() {
        return procFee;
    }

    public void setProcFee(String procFee) {
        this.procFee = procFee;
    }

    public String getCliAmount() {
        return cliAmount;
    }

    public void setCliAmount(String cliAmount) {
        this.cliAmount = cliAmount;
    }

    public String getNetDisbursementAmt() {
        return netDisbursementAmt;
    }

    public void setNetDisbursementAmt(String netDisbursementAmt) {
        this.netDisbursementAmt = netDisbursementAmt;
    }

    public String getAppraisalflag() {
        return appraisalflag;
    }

    public void setAppraisalflag(String appraisalflag) {
        this.appraisalflag = appraisalflag;
    }

    public String getModeOfDisbursement() {
        return modeOfDisbursement;
    }

    public void setModeOfDisbursement(String modeOfDisbursement) {
        this.modeOfDisbursement = modeOfDisbursement;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccountValidationStatus() {
        return accountValidationStatus;
    }

    public void setAccountValidationStatus(String accountValidationStatus) {
        this.accountValidationStatus = accountValidationStatus;
    }

    public String getValidAccountWSCount() {
        return validAccountWSCount;
    }

    public void setValidAccountWSCount(String validAccountWSCount) {
        this.validAccountWSCount = validAccountWSCount;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getModDate() {
        return modDate;
    }

    public void setModDate(String modDate) {
        this.modDate = modDate;
    }

    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public List<McmApplicantsVO> getApplicants() {
        return applicants;
    }

    public void setApplicants(List<McmApplicantsVO> applicants) {
        this.applicants = applicants;
    }

//    public String getPrimaryApplicant() {
//        return primaryApplicant;
//    }
//
//    public void setPrimaryApplicant(String primaryApplicant) {
//        this.primaryApplicant = primaryApplicant;
//    }
//
//    public String getPrimaryApplicantId() {
//        return primaryApplicantId;
//    }
//
//    public void setPrimaryApplicantId(String primaryApplicantId) {
//        this.primaryApplicantId = primaryApplicantId;
//    }

//    public String getCoApplicant() {
//        return coApplicant;
//    }
//
//    public void setCoApplicant(String coApplicant) {
//        this.coApplicant = coApplicant;
//    }
//
//    public String getCoApplicantId() {
//        return coApplicantId;
//    }
//
//    public void setCoApplicantId(String coApplicantId) {
//        this.coApplicantId = coApplicantId;
//    }
//
//    public String getCoRelation() {
//        return coRelation;
//    }
//
//    public void setCoRelation(String coRelation) {
//        this.coRelation = coRelation;
//    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getFlsCode() {
        return flsCode;
    }

    public void setFlsCode(String flsCode) {
        this.flsCode = flsCode;
    }

    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIsAccountValid() {
        return isAccountValid;
    }

    public void setIsAccountValid(String isAccountValid) {
        this.isAccountValid = isAccountValid;
    }

    public String getAdharNumber() {
        return adharNumber;
    }

    public void setAdharNumber(String adharNumber) {
        this.adharNumber = adharNumber;
    }
    
    

}
