/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ven00288
 */
public class DealerApproveRejVO {
    
    private DealearApproveRejReq Content; 

    @JsonProperty("Content")
    public DealearApproveRejReq getContent() {
        return Content;
    }

    public void setContent(DealearApproveRejReq Content) {
        this.Content = Content;
    }
    
}
