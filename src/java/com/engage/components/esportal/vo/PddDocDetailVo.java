/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author Shrikant Katakdhond
 */
public class PddDocDetailVo {
    
    private String applicationId;
    private String customerName;
    private String branchName;
    private PddDocRegistrationDetailVo docRegistrationDetailVo;
    private PddDocInvoiceDetailVo invoiceDetailVo;
    private PddDocMarginMoneyDetailVo marginMoneyDetailVo;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public PddDocRegistrationDetailVo getDocRegistrationDetailVo() {
        return docRegistrationDetailVo;
    }

    public void setDocRegistrationDetailVo(PddDocRegistrationDetailVo docRegistrationDetailVo) {
        this.docRegistrationDetailVo = docRegistrationDetailVo;
    }

    public PddDocInvoiceDetailVo getInvoiceDetailVo() {
        return invoiceDetailVo;
    }

    public void setInvoiceDetailVo(PddDocInvoiceDetailVo invoiceDetailVo) {
        this.invoiceDetailVo = invoiceDetailVo;
    }

    public PddDocMarginMoneyDetailVo getMarginMoneyDetailVo() {
        return marginMoneyDetailVo;
    }

    public void setMarginMoneyDetailVo(PddDocMarginMoneyDetailVo marginMoneyDetailVo) {
        this.marginMoneyDetailVo = marginMoneyDetailVo;
    }
    
}
