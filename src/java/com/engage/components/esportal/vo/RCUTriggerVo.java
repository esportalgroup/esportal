/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import java.util.List;

/**
 *
 * @author SurajK
 */
public class RCUTriggerVo {

    public String applicationId;
    public String flsName;
    public String rcuVendorName;
    public String city;
    public String product;
    public String productCategory;
    public String rcuStatus;
    public String samplingOutcome;
    public String remark;
    public List<RCUTriggerApplicantVo> applicantDetails;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    public String getRcuVendorName() {
        return rcuVendorName;
    }

    public void setRcuVendorName(String rcuVendorName) {
        this.rcuVendorName = rcuVendorName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getRcuStatus() {
        return rcuStatus;
    }

    public void setRcuStatus(String rcuStatus) {
        this.rcuStatus = rcuStatus;
    }

    public List<RCUTriggerApplicantVo> getApplicantDetails() {
        return applicantDetails;
    }

    public void setApplicantDetails(List<RCUTriggerApplicantVo> applicantDetails) {
        this.applicantDetails = applicantDetails;
    }

    public String getSamplingOutcome() {
        return samplingOutcome;
    }

    public void setSamplingOutcome(String samplingOutcome) {
        this.samplingOutcome = samplingOutcome;
    }

     public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
   
}
