/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ven00288
 */
public class FusyServiceReqDetailsVO
{
private String firstName;
private String lastName;
private String firstNamePan;
private String lastNamePan;
private String referenceID;

    public FusyServiceReqDetailsVO() {
    }
    
@JsonProperty("FirstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("LastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("FirstNamePan")
    public String getFirstNamePan() {
        return firstNamePan;
    }

    public void setFirstNamePan(String firstNamePan) {
        this.firstNamePan = firstNamePan;
    }

    @JsonProperty("LastNamePan")
    public String getLastNamePan() {
        return lastNamePan;
    }

    public void setLastNamePan(String lastNamePan) {
        this.lastNamePan = lastNamePan;
    }

    @JsonProperty("ReferenceID")
    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }



    
}
