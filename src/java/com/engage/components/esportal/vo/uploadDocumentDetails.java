/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author Shrikant Katakdhond
 */
public class uploadDocumentDetails {
    
    public String applicantId;
      public String docType;
      public String docOriginalName;
      public String mimeType;
      public String ext;
      public String documentString;

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocOriginalName() {
        return docOriginalName;
    }

    public void setDocOriginalName(String docOriginalName) {
        this.docOriginalName = docOriginalName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getDocumentString() {
        return documentString;
    }

    public void setDocumentString(String documentString) {
        this.documentString = documentString;
    }
      
}
