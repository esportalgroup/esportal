/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 *
 * @author DattatrayT
 */
public class OpsUpdateReqVO {
    private OpsUpdateContentVO opsUpdateContentVO;

    @JsonProperty("Content")
    public OpsUpdateContentVO getOpsUpdateContentVO() {
        return opsUpdateContentVO;
    }

    public void setOpsUpdateContentVO(OpsUpdateContentVO opsUpdateContentVO) {
        this.opsUpdateContentVO = opsUpdateContentVO;
    }
    
}
