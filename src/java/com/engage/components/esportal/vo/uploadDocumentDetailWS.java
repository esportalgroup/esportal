/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author Shrikant Katakdhond
 */
public class uploadDocumentDetailWS {
    public String applicationName;
    public String applicationId;
    public List<uploadDocumentDetails> documentDetailses;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @JsonProperty("documentDetails")
    public List<uploadDocumentDetails> getDocumentDetailses() {
        return documentDetailses;
    }

    public void setDocumentDetailses(List<uploadDocumentDetails> documentDetailses) {
        this.documentDetailses = documentDetailses;
    }

}
