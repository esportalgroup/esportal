/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author Shrikant Katakdhond
 */
public class DealerStatusDashboardVO {

    private String applicationId;
    private String applicatName;
    private String applicationDate;
    private String dsaName;
    private String flsName;
    private String supplierName;
    private String supplierID;
    private String stampDuty;
    private String processingFees;
    private String marginMoneyDocID;
    private String marginMoney;
    private String advEmi;
    private String loanAmount;
    private String invoiceNumber;
    private String invoiceDocID;
    private String id;
    private String flsID;
    private String engineNumber;
    private String ecsCharges;
    private String dsaId;
    private String docCharges;
    private String coApplicantName;
    private String cliPremium;
    private String chassisNumber;
    private String assetModel;
    private String assetCostTax;
    private String assetCostOnboardingPrice;
    private String assetCostIns;
    private String assetCostESR;
    private String assetManufacturer;
    private String assetMake;
    private String applicantName;
    private String product;
    private String status;
    private String progress;
    private int caseCount;
    private Float otherCharges;
    private String submittedDate;
    private String invoiceDate;
    private Float paymentPaidToDlr;
    private String pddStatus;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicatName() {
        return applicatName;
    }

    public void setApplicatName(String applicatName) {
        this.applicatName = applicatName;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getDsaName() {
        return dsaName;
    }

    public void setDsaName(String dsaName) {
        this.dsaName = dsaName;
    }

    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(String stampDuty) {
        this.stampDuty = stampDuty;
    }

    public String getProcessingFees() {
        return processingFees;
    }

    public void setProcessingFees(String processingFees) {
        this.processingFees = processingFees;
    }

    public String getMarginMoneyDocID() {
        return marginMoneyDocID;
    }

    public void setMarginMoneyDocID(String marginMoneyDocID) {
        this.marginMoneyDocID = marginMoneyDocID;
    }

    public String getMarginMoney() {
        return marginMoney;
    }

    public void setMarginMoney(String marginMoney) {
        this.marginMoney = marginMoney;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDocID() {
        return invoiceDocID;
    }

    public void setInvoiceDocID(String invoiceDocID) {
        this.invoiceDocID = invoiceDocID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlsID() {
        return flsID;
    }

    public void setFlsID(String flsID) {
        this.flsID = flsID;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getEcsCharges() {
        return ecsCharges;
    }

    public void setEcsCharges(String ecsCharges) {
        this.ecsCharges = ecsCharges;
    }

    public String getDsaId() {
        return dsaId;
    }

    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }

    public String getDocCharges() {
        return docCharges;
    }

    public void setDocCharges(String docCharges) {
        this.docCharges = docCharges;
    }

    public String getCoApplicantName() {
        return coApplicantName;
    }

    public void setCoApplicantName(String coApplicantName) {
        this.coApplicantName = coApplicantName;
    }

    public String getCliPremium() {
        return cliPremium;
    }

    public void setCliPremium(String cliPremium) {
        this.cliPremium = cliPremium;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(String assetModel) {
        this.assetModel = assetModel;
    }

    public String getAssetCostTax() {
        return assetCostTax;
    }

    public void setAssetCostTax(String assetCostTax) {
        this.assetCostTax = assetCostTax;
    }

    public String getAssetCostOnboardingPrice() {
        return assetCostOnboardingPrice;
    }

    public void setAssetCostOnboardingPrice(String assetCostOnboardingPrice) {
        this.assetCostOnboardingPrice = assetCostOnboardingPrice;
    }

    public String getAssetCostIns() {
        return assetCostIns;
    }

    public void setAssetCostIns(String assetCostIns) {
        this.assetCostIns = assetCostIns;
    }

    public String getAssetCostESR() {
        return assetCostESR;
    }

    public void setAssetCostESR(String assetCostESR) {
        this.assetCostESR = assetCostESR;
    }

    public String getAssetManufacturer() {
        return assetManufacturer;
    }

    public void setAssetManufacturer(String assetManufacturer) {
        this.assetManufacturer = assetManufacturer;
    }

    public String getAssetMake() {
        return assetMake;
    }

    public void setAssetMake(String assetMake) {
        this.assetMake = assetMake;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public int getCaseCount() {
        return caseCount;
    }

    public void setCaseCount(int caseCount) {
        this.caseCount = caseCount;
    }

    public Float getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(Float otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(String submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Float getPaymentPaidToDlr() {
        return paymentPaidToDlr;
    }

    public void setPaymentPaidToDlr(Float paymentPaidToDlr) {
        this.paymentPaidToDlr = paymentPaidToDlr;
    }

    public String getPddStatus() {
        return pddStatus;
    }

    public void setPddStatus(String pddStatus) {
        this.pddStatus = pddStatus;
    }

    public String getAdvEmi() {
        return advEmi;
    }

    public void setAdvEmi(String advEmi) {
        this.advEmi = advEmi;
    }
}
