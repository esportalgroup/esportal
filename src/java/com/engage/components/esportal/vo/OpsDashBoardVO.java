/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

/**
 *
 * @author VEN00288
 */
public class OpsDashBoardVO {

    Long ROWNUM;
    Long refid;
    String applicationId;
    String applicantName;
    String flsName;
    String branchName;
    String amount;
    String sanctionDate;
    String status;
    String lockBy;
    String lockByName;
    Long caseCount;

    public Long getRefid() {
        return refid;
    }

    public void setRefid(Long refid) {
        this.refid = refid;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSanctionDate() {
        return sanctionDate;
    }

    public void setSanctionDate(String sanctionDate) {
        this.sanctionDate = sanctionDate;
    }

    public Long getROWNUM() {
        return ROWNUM;
    }

    public void setROWNUM(Long ROWNUM) {
        this.ROWNUM = ROWNUM;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLockBy() {
        return lockBy;
    }

    public void setLockBy(String lockBy) {
        this.lockBy = lockBy;
    }

    public String getLockByName() {
        return lockByName;
    }

    public void setLockByName(String lockByName) {
        this.lockByName = lockByName;
    }

    public Long getCaseCount() {
        return caseCount;
    }

    public void setCaseCount(Long caseCount) {
        this.caseCount = caseCount;
    }
    
    
}
