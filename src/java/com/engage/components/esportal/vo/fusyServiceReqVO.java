/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author ven00288
 */
public class fusyServiceReqVO {
    
    private List<FusyServiceReqDetailsVO> applicantsDetails;

    
    @JsonProperty("applicants")
    public List<FusyServiceReqDetailsVO> getApplicantsDetails() {
        return applicantsDetails;
    }

    public void setApplicantsDetails(List<FusyServiceReqDetailsVO> applicantsDetails) {
        this.applicantsDetails = applicantsDetails;
    }
    
    
    
    
    
    
}
