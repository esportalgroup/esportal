/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.uid.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author dattatrayt
 */
public class UidSendOTPVO {
    public String aadhaar_number;

    public UidSendOTPVO() {
    }

    public UidSendOTPVO(String aadhaar_number) {
        this.aadhaar_number = aadhaar_number;
    }

    @JsonProperty("aadhaar_number")
    public String getAadhaar_number() {
        return aadhaar_number;
    }

    public void setAadhaar_number(String aadhaar_number) {
        this.aadhaar_number = aadhaar_number;
    }
    
}
