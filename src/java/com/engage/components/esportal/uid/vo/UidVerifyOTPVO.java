/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.uid.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author dattatrayt
 */
public class UidVerifyOTPVO {
    public String aadhaar_number;
    public String consent;
    public String otp;
    public String otp_tx_id;

    public UidVerifyOTPVO() {
    }

    public UidVerifyOTPVO(String aadhaar_number, String consent, String otp, String otp_tx_id) {
        this.aadhaar_number = aadhaar_number;
        this.consent = consent;
        this.otp = otp;
        this.otp_tx_id = otp_tx_id;
    }

    @JsonProperty("aadhaar_number")
    public String getAadhaar_number() {
        return aadhaar_number;
    }

    public void setAadhaar_number(String aadhaar_number) {
        this.aadhaar_number = aadhaar_number;
    }

    @JsonProperty("consent")
    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    @JsonProperty("otp")
    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @JsonProperty("otp_tx_id")
    public String getOtp_tx_id() {
        return otp_tx_id;
    }

    public void setOtp_tx_id(String otp_tx_id) {
        this.otp_tx_id = otp_tx_id;
    }

}
