/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ven00288
 */
public class mcmPaymentVO1 {
    
    public mcmPaymentMethodVO methodVO;
    public mcmPaymentSenderVO senderVO;
    public mcmPaymentReceiverVO1 receiverVO;
    public mcmPaymentSenderInstrumentVO instrumentVO;
    public mcmPaymentInstructionVO instructionVO;

     @JsonProperty("method")
    public mcmPaymentMethodVO getMethodVO() {
        return methodVO;
    }

    public void setMethodVO(mcmPaymentMethodVO methodVO) {
        this.methodVO = methodVO;
    }

    @JsonProperty("sender")
    public mcmPaymentSenderVO getSenderVO() {
        return senderVO;
    }

    public void setSenderVO(mcmPaymentSenderVO senderVO) {
        this.senderVO = senderVO;
    }

    @JsonProperty("receiver")
    public mcmPaymentReceiverVO1 getReceiverVO() {
        return receiverVO;
    }

    public void setReceiverVO(mcmPaymentReceiverVO1 receiverVO) {
        this.receiverVO = receiverVO;
    }

    @JsonProperty("instrument")
    public mcmPaymentSenderInstrumentVO getInstrumentVO() {
        return instrumentVO;
    }

    public void setInstrumentVO(mcmPaymentSenderInstrumentVO instrumentVO) {
        this.instrumentVO = instrumentVO;
    }

    @JsonProperty("instruction")
    public mcmPaymentInstructionVO getInstructionVO() {
        return instructionVO;
    }

    public void setInstructionVO(mcmPaymentInstructionVO instructionVO) {
        this.instructionVO = instructionVO;
    }
    
    
    
}
