/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmOrderCartVO {

    public List<mcmOrderCartItemVO> itemVO;
    public String reference;
    public String identifier;
    public String description;
 
    @JsonProperty("item")
    public List<mcmOrderCartItemVO> getItemVO() {
        return itemVO;
    }

    public void setItemVO(List<mcmOrderCartItemVO> itemVO) {
        this.itemVO = itemVO;
    }
    

    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

  
    
}
