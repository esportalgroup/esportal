/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author VEN00288
 */
public class mcmRequestGroupFormationVO {
    
//    private List<mcmRequestGroupVO> content;
        
   private  mcmRequestGroupVO contents;
    

//    @JsonProperty("Contents")
//    public List<mcmRequestGroupVO> getContent() {
//        return content;
//    }
//
//    public void setContent(List<mcmRequestGroupVO> content) {
//        this.content = content;
//    }

    @JsonProperty("Content")
    public mcmRequestGroupVO getContents() {
        return contents;
    }

    public void setContents(mcmRequestGroupVO contents) {
        this.contents = contents;
    }
    
    
    
}
