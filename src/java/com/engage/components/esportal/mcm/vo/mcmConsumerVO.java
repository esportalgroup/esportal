/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmConsumerVO {
    
    public String name;
    public Long mobileNumber;
    public String emailID;
    public String identifier;
    public String paynimoCustRegId;
    public String accouontNo;
    public String hashURL;
    public String invitationCode;
    public String custSecurityQuestID;
    public String custSecurityAns;
    public String custSecretKey;
    public String custRegStatus;
    public String custType;
    public String roleId;
    public List<mcmConsumerConsumerAliasVO> aliasVO;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("mobileNumber")
       public Long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    @JsonProperty("emailID")
    public String getEmailID() {
        return emailID;
    }
 
    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("paynimoCustRegId")
    public String getPaynimoCustRegId() {
        return paynimoCustRegId;
    }

    public void setPaynimoCustRegId(String paynimoCustRegId) {
        this.paynimoCustRegId = paynimoCustRegId;
    }

    @JsonProperty("accouontNo")
    public String getAccouontNo() {
        return accouontNo;
    }

    public void setAccouontNo(String accouontNo) {
        this.accouontNo = accouontNo;
    }

    @JsonProperty("hashURL")
    public String getHashURL() {
        return hashURL;
    }

    public void setHashURL(String hashURL) {
        this.hashURL = hashURL;
    }

    @JsonProperty("invitationCode")
    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    @JsonProperty("custSecurityQuestID")
    public String getCustSecurityQuestID() {
        return custSecurityQuestID;
    }

    public void setCustSecurityQuestID(String custSecurityQuestID) {
        this.custSecurityQuestID = custSecurityQuestID;
    }

    @JsonProperty("custSecurityAns")
    public String getCustSecurityAns() {
        return custSecurityAns;
    }

    public void setCustSecurityAns(String custSecurityAns) {
        this.custSecurityAns = custSecurityAns;
    }

    @JsonProperty("custSecretKey")
    public String getCustSecretKey() {
        return custSecretKey;
    }

    public void setCustSecretKey(String custSecretKey) {
        this.custSecretKey = custSecretKey;
    }

    @JsonProperty("custRegStatus")
    public String getCustRegStatus() {
        return custRegStatus;
    }

    public void setCustRegStatus(String custRegStatus) {
        this.custRegStatus = custRegStatus;
    }

    @JsonProperty("custType")
    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    @JsonProperty("roleId")
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @JsonProperty("consumerAlias")
    public List<mcmConsumerConsumerAliasVO> getAliasVO() {
        return aliasVO;
    }

    public void setAliasVO(List<mcmConsumerConsumerAliasVO> aliasVO) {
        this.aliasVO = aliasVO;
    }
    
    
}
