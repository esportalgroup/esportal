/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmTransactionVO {

    public String deviceIdentifier;
    public String channelType;
    public String channelName;
    public String utm_source;
    public String utm_medium;
    public String utm_term;
    public String utm_content;
    public String utm_campaign;
    public String smsSending;
    public String amount;
    public String forced3DSCall;
    public String type;
    public String description;
    public String currency;
    public String isRegistration;
    public String identifier;
    public String dateTime;
    public String token;
    public String txnErrorDesc;
    public String txnErrorCode;
    public String subType;
    public String requestType;
    public String action;
    public Long reference;
    public mcmPaymentVO1 paymentVO;

    @JsonProperty("deviceIdentifier")
    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    @JsonProperty("channelType")
    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    @JsonProperty("channelName")
    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    @JsonProperty("utm_source")
    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    @JsonProperty("utm_medium")
    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    @JsonProperty("utm_term")
    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    @JsonProperty("utm_content")
    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    @JsonProperty("utm_campaign")
    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    @JsonProperty("smsSending")
    public String getSmsSending() {
        return smsSending;
    }

    public void setSmsSending(String smsSending) {
        this.smsSending = smsSending;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("forced3DSCall")
    public String getForced3DSCall() {
        return forced3DSCall;
    }

    public void setForced3DSCall(String forced3DSCall) {
        this.forced3DSCall = forced3DSCall;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("isRegistration")
    public String getIsRegistration() {
        return isRegistration;
    }

    public void setIsRegistration(String isRegistration) {
        this.isRegistration = isRegistration;
    }

    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("dateTime")
    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("txnErrorDesc")
    public String getTxnErrorDesc() {
        return txnErrorDesc;
    }

    public void setTxnErrorDesc(String txnErrorDesc) {
        this.txnErrorDesc = txnErrorDesc;
    }

    @JsonProperty("txnErrorCode")
    public String getTxnErrorCode() {
        return txnErrorCode;
    }

    public void setTxnErrorCode(String txnErrorCode) {
        this.txnErrorCode = txnErrorCode;
    }

    @JsonProperty("subType")
    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    @JsonProperty("requestType")
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @JsonProperty("action")
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @JsonProperty("reference")
    public Long getReference() {
        return reference;
    }

    public void setReference(Long reference) {
        this.reference = reference;
    }

    @JsonProperty("payment")
    public mcmPaymentVO1 getPaymentVO() {
        return paymentVO;
    }

    public void setPaymentVO(mcmPaymentVO1 paymentVO) {
        this.paymentVO = paymentVO;
    }

}
