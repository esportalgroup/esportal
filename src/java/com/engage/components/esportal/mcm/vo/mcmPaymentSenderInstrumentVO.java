/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmPaymentSenderInstrumentVO {

    public String provider;
    public String iFSC;
    public String bic;
    public String type;
    public String action;
    public String micr;
    public String verificationCode;
    public String iban;
    public String processor;
    public String toVault;
    public String alias;
    public String identifier;
    public String token;
    public String subType;
    public String issuer;
    public String acquirer;
    public mcmPaymentSenderInstrumentExpiryVO expiryVO;
    public mcmPaymentSenderInstrumentHolderVO holderVO;
    public mcmPaymentSenderInstrumentIssuanceVO issuanceVO;
    public mcmPaymentSenderInstrumentAuthenticationVO authenticationVO;

    @JsonProperty("provider")
    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @JsonProperty("iFSC")
    public String getiFSC() {
        return iFSC;
    }

    public void setiFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    @JsonProperty("bic")
    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("action")
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @JsonProperty("micr")
    public String getMicr() {
        return micr;
    }

    public void setMicr(String micr) {
        this.micr = micr;
    }

    @JsonProperty("verificationCode")
    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    @JsonProperty("iban")
    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @JsonProperty("processor")
    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    @JsonProperty("toVault")
    public String getToVault() {
        return toVault;
    }

    public void setToVault(String toVault) {
        this.toVault = toVault;
    }

    @JsonProperty("alias")
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("subType")
    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    @JsonProperty("issuer")
    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    @JsonProperty("acquirer")
    public String getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(String acquirer) {
        this.acquirer = acquirer;
    }

    @JsonProperty("expiry")
    public mcmPaymentSenderInstrumentExpiryVO getExpiryVO() {
        return expiryVO;
    }

    public void setExpiryVO(mcmPaymentSenderInstrumentExpiryVO expiryVO) {
        this.expiryVO = expiryVO;
    }

    @JsonProperty("holder")
    public mcmPaymentSenderInstrumentHolderVO getHolderVO() {
        return holderVO;
    }

    public void setHolderVO(mcmPaymentSenderInstrumentHolderVO holderVO) {
        this.holderVO = holderVO;
    }

    @JsonProperty("issuance")
    public mcmPaymentSenderInstrumentIssuanceVO getIssuanceVO() {
        return issuanceVO;
    }

    public void setIssuanceVO(mcmPaymentSenderInstrumentIssuanceVO issuanceVO) {
        this.issuanceVO = issuanceVO;
    }

    @JsonProperty("authentication")
    public mcmPaymentSenderInstrumentAuthenticationVO getAuthenticationVO() {
        return authenticationVO;
    }

    public void setAuthenticationVO(mcmPaymentSenderInstrumentAuthenticationVO authenticationVO) {
        this.authenticationVO = authenticationVO;
    }

}
