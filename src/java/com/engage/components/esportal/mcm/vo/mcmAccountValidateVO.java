/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmAccountValidateVO {

    public mcmMerchantVO merchantVO;
    public mcmOrderVO orderVO;
    public mcmPaymentVO paymentVO;
    public mcmTransactionVO transactionVO;
    public mcmConsumerVO consumerVO;

    @JsonProperty("merchant")
    public mcmMerchantVO getMerchantVO() {
        return merchantVO;
    }

    public void setMerchantVO(mcmMerchantVO merchantVO) {
        this.merchantVO = merchantVO;
    }

    @JsonProperty("order")
    public mcmOrderVO getOrderVO() {
        return orderVO;
    }

    public void setOrderVO(mcmOrderVO orderVO) {
        this.orderVO = orderVO;
    }

    @JsonProperty("payment")
    public mcmPaymentVO getPaymentVO() {
        return paymentVO;
    }

    public void setPaymentVO(mcmPaymentVO paymentVO) {
        this.paymentVO = paymentVO;
    }

    @JsonProperty("transaction")
    public mcmTransactionVO getTransactionVO() {
        return transactionVO;
    }

    public void setTransactionVO(mcmTransactionVO transactionVO) {
        this.transactionVO = transactionVO;
    }

    @JsonProperty("consumer")
    public mcmConsumerVO getConsumerVO() {
        return consumerVO;
    }

    public void setConsumerVO(mcmConsumerVO consumerVO) {
        this.consumerVO = consumerVO;
    }

}
