/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmPaymentSenderVO {

    public String paynimoSenderAliasToken;
    public String paynimoSenderName;
    public String paynimoSenderInstrumentToken;
    public String paynimoSenderInstrumentNumber;
    public String paynimoSendTxnID;
    public String paynimoSendTxnStatus;
    public mcmPaymentSenderInstrumentVO instrumentVO;

    @JsonProperty("paynimoSenderAliasToken")
    public String getPaynimoSenderAliasToken() {
        return paynimoSenderAliasToken;
    }

    public void setPaynimoSenderAliasToken(String paynimoSenderAliasToken) {
        this.paynimoSenderAliasToken = paynimoSenderAliasToken;
    }

    @JsonProperty("paynimoSenderName")
    public String getPaynimoSenderName() {
        return paynimoSenderName;
    }

    public void setPaynimoSenderName(String paynimoSenderName) {
        this.paynimoSenderName = paynimoSenderName;
    }

    @JsonProperty("paynimoSenderInstrumentToken")
    public String getPaynimoSenderInstrumentToken() {
        return paynimoSenderInstrumentToken;
    }

    public void setPaynimoSenderInstrumentToken(String paynimoSenderInstrumentToken) {
        this.paynimoSenderInstrumentToken = paynimoSenderInstrumentToken;
    }

    @JsonProperty("paynimoSenderInstrumentNumber")
    public String getPaynimoSenderInstrumentNumber() {
        return paynimoSenderInstrumentNumber;
    }

    public void setPaynimoSenderInstrumentNumber(String paynimoSenderInstrumentNumber) {
        this.paynimoSenderInstrumentNumber = paynimoSenderInstrumentNumber;
    }

    @JsonProperty("paynimoSendTxnID")
    public String getPaynimoSendTxnID() {
        return paynimoSendTxnID;
    }

    public void setPaynimoSendTxnID(String paynimoSendTxnID) {
        this.paynimoSendTxnID = paynimoSendTxnID;
    }

    @JsonProperty("paynimoSendTxnStatus")
    public String getPaynimoSendTxnStatus() {
        return paynimoSendTxnStatus;
    }

    public void setPaynimoSendTxnStatus(String paynimoSendTxnStatus) {
        this.paynimoSendTxnStatus = paynimoSendTxnStatus;
    }

    @JsonProperty("instrument")
    public mcmPaymentSenderInstrumentVO getInstrumentVO() {
        return instrumentVO;
    }

    public void setInstrumentVO(mcmPaymentSenderInstrumentVO instrumentVO) {
        this.instrumentVO = instrumentVO;
    }

    
}
