/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author VEN00624
 */
public class mcmRequestGroupDetailsDocDisbursement {

    private String VillageName;
    private String City;
    private String groupId;
    private String FLSCode;
    private String MCCode;
    private String MCMAccountNumber;
    private String MCMIFSC_code;
    private String MCMBank_name;
    private String MCMBank_branch_name;
    private String CollectionDate;
    private String CollectionTime;
    private List<mcmDisbursementDocDetailVO> Application;

    @JsonProperty("VillageName")
    public String getVillageName() {
        return VillageName;
    }

    public void setVillageName(String VillageName) {
        this.VillageName = VillageName;
    }

    @JsonProperty("City")
    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    @JsonProperty("groupId")
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("FLSCode")
    public String getFLSCode() {
        return FLSCode;
    }

    public void setFLSCode(String FLSCode) {
        this.FLSCode = FLSCode;
    }

    @JsonProperty("MCCode")
    public String getMCCode() {
        return MCCode;
    }

    public void setMCCode(String MCCode) {
        this.MCCode = MCCode;
    }

    @JsonProperty("MCMAccountNumber")
    public String getMCMAccountNumber() {
        return MCMAccountNumber;
    }

    public void setMCMAccountNumber(String MCMAccountNumber) {
        this.MCMAccountNumber = MCMAccountNumber;
    }

    @JsonProperty("MCMIFSC_code")
    public String getMCMIFSC_code() {
        return MCMIFSC_code;
    }

    public void setMCMIFSC_code(String MCMIFSC_code) {
        this.MCMIFSC_code = MCMIFSC_code;
    }

    @JsonProperty("MCMBank_name")
    public String getMCMBank_name() {
        return MCMBank_name;
    }

    public void setMCMBank_name(String MCMBank_name) {
        this.MCMBank_name = MCMBank_name;
    }

    @JsonProperty("MCMBank_branch_name")
    public String getMCMBank_branch_name() {
        return MCMBank_branch_name;
    }

    public void setMCMBank_branch_name(String MCMBank_branch_name) {
        this.MCMBank_branch_name = MCMBank_branch_name;
    }

    @JsonProperty("CollectionDate")
    public String getCollectionDate() {
        return CollectionDate;
    }

    public void setCollectionDate(String CollectionDate) {
        this.CollectionDate = CollectionDate;
    }

    @JsonProperty("CollectionTime")
    public String getCollectionTime() {
        return CollectionTime;
    }

    public void setCollectionTime(String CollectionTime) {
        this.CollectionTime = CollectionTime;
    }

    @JsonProperty("Application")
    public List<mcmDisbursementDocDetailVO> getApplication() {
        return Application;
    }

    public void setApplication(List<mcmDisbursementDocDetailVO> Application) {
        this.Application = Application;
    }

}
