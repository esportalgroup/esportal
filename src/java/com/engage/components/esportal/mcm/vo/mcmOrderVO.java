/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmOrderVO {
    
    public mcmOrderCartVO cartVO;

    @JsonProperty("cart")
    public mcmOrderCartVO getCartVO() {
        return cartVO;
    }

    public void setCartVO(mcmOrderCartVO cartVO) {
        this.cartVO = cartVO;
    }
    
    
}
