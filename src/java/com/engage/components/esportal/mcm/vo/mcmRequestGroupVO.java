/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author VEN00288
 */
public class mcmRequestGroupVO {
    
    private List<mcmRequestGroupDetails> groupLst;

    
    @JsonProperty("Groups")
    public List<mcmRequestGroupDetails> getGroupLst() {
        return groupLst;
    }

    public void setGroupLst(List<mcmRequestGroupDetails> groupLst) {
        this.groupLst = groupLst;
    }

    
    
}
