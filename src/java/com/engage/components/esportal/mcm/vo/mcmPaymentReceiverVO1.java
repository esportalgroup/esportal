/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ven00288
 */
public class mcmPaymentReceiverVO1 {
    
    public String paynimoReceiverAliasToken;
    public String paynimoBeneficiaryName;
    public String paynimoBeneficiaryInstrumentToken;
    public String paynimoBeneficiaryInstrumentNumber;
    public String paynimoSenderSourceOfFunds;
    public String paynimoReceiveTxnID;
    public String paynimoReceiveTxnStatus;
    public String name;
    public mcmPaymentSenderInstrumentVO instrumentVO;

    @JsonProperty("paynimoReceiverAliasToken")
    public String getPaynimoReceiverAliasToken() {
        return paynimoReceiverAliasToken;
    }

    public void setPaynimoReceiverAliasToken(String paynimoReceiverAliasToken) {
        this.paynimoReceiverAliasToken = paynimoReceiverAliasToken;
    }

    @JsonProperty("paynimoBeneficiaryName")
    public String getPaynimoBeneficiaryName() {
        return paynimoBeneficiaryName;
    }

    public void setPaynimoBeneficiaryName(String paynimoBeneficiaryName) {
        this.paynimoBeneficiaryName = paynimoBeneficiaryName;
    }

    @JsonProperty("paynimoBeneficiaryInstrumentToken")
    public String getPaynimoBeneficiaryInstrumentToken() {
        return paynimoBeneficiaryInstrumentToken;
    }

    public void setPaynimoBeneficiaryInstrumentToken(String paynimoBeneficiaryInstrumentToken) {
        this.paynimoBeneficiaryInstrumentToken = paynimoBeneficiaryInstrumentToken;
    }

    @JsonProperty("paynimoBeneficiaryInstrumentNumber")
    public String getPaynimoBeneficiaryInstrumentNumber() {
        return paynimoBeneficiaryInstrumentNumber;
    }

    public void setPaynimoBeneficiaryInstrumentNumber(String paynimoBeneficiaryInstrumentNumber) {
        this.paynimoBeneficiaryInstrumentNumber = paynimoBeneficiaryInstrumentNumber;
    }

    @JsonProperty("paynimoSenderSourceOfFunds")
    public String getPaynimoSenderSourceOfFunds() {
        return paynimoSenderSourceOfFunds;
    }

    public void setPaynimoSenderSourceOfFunds(String paynimoSenderSourceOfFunds) {
        this.paynimoSenderSourceOfFunds = paynimoSenderSourceOfFunds;
    }

    @JsonProperty("paynimoReceiveTxnID")
    public String getPaynimoReceiveTxnID() {
        return paynimoReceiveTxnID;
    }

    public void setPaynimoReceiveTxnID(String paynimoReceiveTxnID) {
        this.paynimoReceiveTxnID = paynimoReceiveTxnID;
    }

    @JsonProperty("paynimoReceiveTxnStatus")
    public String getPaynimoReceiveTxnStatus() {
        return paynimoReceiveTxnStatus;
    }

    public void setPaynimoReceiveTxnStatus(String paynimoReceiveTxnStatus) {
        this.paynimoReceiveTxnStatus = paynimoReceiveTxnStatus;
    }

     @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("instrument")
    public mcmPaymentSenderInstrumentVO getInstrumentVO() {
        return instrumentVO;
    }

    public void setInstrumentVO(mcmPaymentSenderInstrumentVO instrumentVO) {
        this.instrumentVO = instrumentVO;
    }

    
    
    
}
