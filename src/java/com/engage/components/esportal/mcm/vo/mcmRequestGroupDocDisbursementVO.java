/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author VEN00624
 */
public class mcmRequestGroupDocDisbursementVO {

    private List<mcmRequestGroupDetailsDocDisbursement> groupLst;

    @JsonProperty("Groups")
    public List<mcmRequestGroupDetailsDocDisbursement> getGroupLst() {
        return groupLst;
    }

    public void setGroupLst(List<mcmRequestGroupDetailsDocDisbursement> groupLst) {
        this.groupLst = groupLst;
    }

}
