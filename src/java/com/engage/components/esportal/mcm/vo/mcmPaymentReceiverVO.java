/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmPaymentReceiverVO {

    public String paynimoReceiverAliasToken;
    public String paynimoBeneficiaryName;
    public String paynimoBeneficiaryInstrumentToken;
    public String paynimoBeneficiaryInstrumentNumber;
    public String paynimoSenderSourceOfFunds;
    public String paynimoReceiveTxnID;
    public String paynimoReceiveTxnStatus;
    
    @JsonProperty("paynimoReceiverAliasToken")
    public String getPaynimoReceiverAliasToken() {
        return paynimoReceiverAliasToken;
    }

    public void setPaynimoReceiverAliasToken(String paynimoReceiverAliasToken) {
        this.paynimoReceiverAliasToken = paynimoReceiverAliasToken;
    }

    @JsonProperty("paynimoBeneficiaryName")
    public String getPaynimoBeneficiaryName() {
        return paynimoBeneficiaryName;
    }

    public void setPaynimoBeneficiaryName(String paynimoBeneficiaryName) {
        this.paynimoBeneficiaryName = paynimoBeneficiaryName;
    }

    @JsonProperty("paynimoBeneficiaryInstrumentToken")
    public String getPaynimoBeneficiaryInstrumentToken() {
        return paynimoBeneficiaryInstrumentToken;
    }

    public void setPaynimoBeneficiaryInstrumentToken(String paynimoBeneficiaryInstrumentToken) {
        this.paynimoBeneficiaryInstrumentToken = paynimoBeneficiaryInstrumentToken;
    }

    @JsonProperty("paynimoBeneficiaryInstrumentNumber")
    public String getPaynimoBeneficiaryInstrumentNumber() {
        return paynimoBeneficiaryInstrumentNumber;
    }

    public void setPaynimoBeneficiaryInstrumentNumber(String paynimoBeneficiaryInstrumentNumber) {
        this.paynimoBeneficiaryInstrumentNumber = paynimoBeneficiaryInstrumentNumber;
    }

    @JsonProperty("paynimoSenderSourceOfFunds")
    public String getPaynimoSenderSourceOfFunds() {
        return paynimoSenderSourceOfFunds;
    }

    public void setPaynimoSenderSourceOfFunds(String paynimoSenderSourceOfFunds) {
        this.paynimoSenderSourceOfFunds = paynimoSenderSourceOfFunds;
    }

    @JsonProperty("paynimoReceiveTxnID")
    public String getPaynimoReceiveTxnID() {
        return paynimoReceiveTxnID;
    }

    public void setPaynimoReceiveTxnID(String paynimoReceiveTxnID) {
        this.paynimoReceiveTxnID = paynimoReceiveTxnID;
    }

    @JsonProperty("paynimoReceiveTxnStatus")
    public String getPaynimoReceiveTxnStatus() {
        return paynimoReceiveTxnStatus;
    }

    public void setPaynimoReceiveTxnStatus(String paynimoReceiveTxnStatus) {
        this.paynimoReceiveTxnStatus = paynimoReceiveTxnStatus;
    }

}
