/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmConsumerConsumerAliasVO {

    public String custAlias;
    public String custAliasId;
    public String custAliasType;
    public String custAliasSource;

    @JsonProperty("custAlias")
    public String getCustAlias() {
        return custAlias;
    }

    public void setCustAlias(String custAlias) {
        this.custAlias = custAlias;
    }

    @JsonProperty("custAliasId")
    public String getCustAliasId() {
        return custAliasId;
    }

    public void setCustAliasId(String custAliasId) {
        this.custAliasId = custAliasId;
    }

    @JsonProperty("custAliasType")
    public String getCustAliasType() {
        return custAliasType;
    }

    public void setCustAliasType(String custAliasType) {
        this.custAliasType = custAliasType;
    }

    @JsonProperty("custAliasSource")
    public String getCustAliasSource() {
        return custAliasSource;
    }

    public void setCustAliasSource(String custAliasSource) {
        this.custAliasSource = custAliasSource;
    }

}
