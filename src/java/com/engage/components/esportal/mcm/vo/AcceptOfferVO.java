/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ven00288
 */
public class AcceptOfferVO {
    
    
    private String losId;
    private String lonaAmt;
    private String loanTerm;
    private String emiAmount;

    
    @JsonProperty("id")
    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    @JsonProperty("genesis__Loan_Amount__c")
    public String getLonaAmt() {
        return lonaAmt;
    }

    public void setLonaAmt(String lonaAmt) {
        this.lonaAmt = lonaAmt;
    }

    @JsonProperty("genesis__Term__c")
    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    @JsonProperty("Loan_EMI__c")
    public String getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(String emiAmount) {
        this.emiAmount = emiAmount;
    }
    
    
    
    
}
