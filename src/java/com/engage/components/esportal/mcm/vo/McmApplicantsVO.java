/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

/**
 *
 * @author ven00288
 */
public class McmApplicantsVO {
    
    private String applicantId;
    private String applicantName;
    private String realationWithApplicant;
    private String groupId;
    private String applicationId;

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getRealationWithApplicant() {
        return realationWithApplicant;
    }

    public void setRealationWithApplicant(String realationWithApplicant) {
        this.realationWithApplicant = realationWithApplicant;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }
    
    
}
