/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmPaymentSenderInstrumentHolderVO {
    
    public String name;
    public mcmPaymentSenderInstrumentHolderAddressVO addressVO;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("address")
    public mcmPaymentSenderInstrumentHolderAddressVO getAddressVO() {
        return addressVO;
    }

    public void setAddressVO(mcmPaymentSenderInstrumentHolderAddressVO addressVO) {
        this.addressVO = addressVO;
    }
    
}
