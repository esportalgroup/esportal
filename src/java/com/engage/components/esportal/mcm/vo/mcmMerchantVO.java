/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Shrikant Katakdhond
 */
public class mcmMerchantVO {

    public String webhookEndpointURL;
    public String responseType;
    public String responseEndpointURL;
    public String description;
    public String identifier;
    public String cardRegPGMID;
    public String TxnPGMID;
    public String webhookType;
    public String aspMasterId;

    @JsonProperty("webhookEndpointURL")
    public String getWebhookEndpointURL() {
        return webhookEndpointURL;
    }

    public void setWebhookEndpointURL(String webhookEndpointURL) {
        this.webhookEndpointURL = webhookEndpointURL;
    }

    @JsonProperty("responseType")
    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    @JsonProperty("responseEndpointURL")
    public String getResponseEndpointURL() {
        return responseEndpointURL;
    }

    public void setResponseEndpointURL(String responseEndpointURL) {
        this.responseEndpointURL = responseEndpointURL;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @JsonProperty("cardRegPGMID")
    public String getCardRegPGMID() {
        return cardRegPGMID;
    }

    public void setCardRegPGMID(String cardRegPGMID) {
        this.cardRegPGMID = cardRegPGMID;
    }

    @JsonProperty("TxnPGMID")
    public String getTxnPGMID() {
        return TxnPGMID;
    }

    public void setTxnPGMID(String TxnPGMID) {
        this.TxnPGMID = TxnPGMID;
    }

    @JsonProperty("webhookType")
    public String getWebhookType() {
        return webhookType;
    }

    public void setWebhookType(String webhookType) {
        this.webhookType = webhookType;
    }

    @JsonProperty("aspMasterId")
    public String getAspMasterId() {
        return aspMasterId;
    }

    public void setAspMasterId(String aspMasterId) {
        this.aspMasterId = aspMasterId;
    }

}
