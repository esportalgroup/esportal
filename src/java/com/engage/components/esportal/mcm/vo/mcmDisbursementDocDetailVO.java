/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.esportal.mcm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author VEN00624
 */
public class mcmDisbursementDocDetailVO {

    private String ApplicationId;
    private String LOSId;
    private String LoanAmount;
    private String ModeofDiscursement;
    private String AccountNumber;
    private String IFSC_code;
    private String Bank_name;
    private String Bank_branch_name;
    private String memberPresent;
   

    public mcmDisbursementDocDetailVO() {
    }

    @JsonProperty("ApplicationId")
    public String getApplicationId() {
        return ApplicationId;
    }

    public void setApplicationId(String ApplicationId) {
        this.ApplicationId = ApplicationId;
    }

    @JsonProperty("LOSId")
    public String getLOSId() {
        return LOSId;
    }

    public void setLOSId(String LOSId) {
        this.LOSId = LOSId;
    }

    @JsonProperty("LoanAmount")
    public String getLoanAmount() {
        return LoanAmount;
    }

    public void setLoanAmount(String LoanAmount) {
        this.LoanAmount = LoanAmount;
    }

    @JsonProperty("ModeofDiscursement")
    public String getModeofDiscursement() {
        return ModeofDiscursement;
    }

    public void setModeofDiscursement(String ModeofDiscursement) {
        this.ModeofDiscursement = ModeofDiscursement;
    }

    @JsonProperty("AccountNumber")
    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    @JsonProperty("IFSC_code")
    public String getIFSC_code() {
        return IFSC_code;
    }

    public void setIFSC_code(String IFSC_code) {
        this.IFSC_code = IFSC_code;
    }

    @JsonProperty("Bank_name")
    public String getBank_name() {
        return Bank_name;
    }

    public void setBank_name(String Bank_name) {
        this.Bank_name = Bank_name;
    }

    @JsonProperty("Bank_branch_name")
    public String getBank_branch_name() {
        return Bank_branch_name;
    }

    public void setBank_branch_name(String Bank_branch_name) {
        this.Bank_branch_name = Bank_branch_name;
    }

    @JsonProperty("MemberPresent")
    public String getMemberPresent() {
        return memberPresent;
    }

    
    public void setMemberPresent(String memberPresent) {
        this.memberPresent = memberPresent;
    }

    
    
}
