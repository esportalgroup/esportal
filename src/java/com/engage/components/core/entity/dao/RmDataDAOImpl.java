/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.RmDataApplicantDetail;
import com.engage.components.core.entity.bean.RmDataApplication;
import com.engage.components.core.entity.bean.RmLogUid;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author dattatrayt
 */

@Repository
public class RmDataDAOImpl implements RmDataDAO {
    
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public RmDataApplication getRmDataApplcation(Long applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RmDataApplication.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<RmDataApplication> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public RmDataApplication setRmDataApplcation(RmDataApplication rmDataApplcation) {
        sessionFactory.getCurrentSession().saveOrUpdate(rmDataApplcation);
        return rmDataApplcation;
    }

    @Override
    @Transactional
    public List<RmDataApplicantDetail> getRmDataApplicantDetail(Long applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RmDataApplicantDetail.class).
                createAlias("rmDataApplication", "rmDataApplication");
        cr.add(Restrictions.eq("rmDataApplication.applicationId", applicationId));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<RmDataApplicantDetail> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public RmDataApplicantDetail getRmDataApplicantDetailByApplicantsID(Long applicationId, Long applicantId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RmDataApplicantDetail.class).
                createAlias("rmDataApplication", "rmDataApplication");
        cr.add(Restrictions.eq("rmDataApplication.applicationId", applicationId));
        cr.add(Restrictions.eq("applicantId", applicantId));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<RmDataApplicantDetail> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }
    
    @Override
    @Transactional
    public List<RmDataApplicantDetail> getRmDataApplicantDetailByApplicationID(Long applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RmDataApplicantDetail.class).
                createAlias("rmDataApplication", "rmDataApplication");
        cr.add(Restrictions.eq("rmDataApplication.applicationId", applicationId));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<RmDataApplicantDetail> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public RmDataApplicantDetail setRmDataApplicantDetail(RmDataApplicantDetail rmDataApplicantDetail) {
        sessionFactory.getCurrentSession().saveOrUpdate(rmDataApplicantDetail);
        return rmDataApplicantDetail;
    }

    @Override
    @Transactional
    public RmLogUid getRmLogUid(Long applicationId, Long applicantId, String typeOfReq) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RmLogUid.class).
                createAlias("rmDataApplication", "rmDataApplication").
                createAlias("rmDataApplicantDetail", "rmDataApplicantDetail");
        cr.add(Restrictions.eq("rmDataApplication.applicationId", applicationId));
        cr.add(Restrictions.eq("rmDataApplicantDetail.applicantId", applicantId));
        cr.add(Restrictions.eq("typeOfReq", typeOfReq));
        cr.addOrder(Order.desc("modDate"));
        List<RmLogUid> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public RmLogUid setRmLogUid(RmLogUid rmLogUid) {
        sessionFactory.getCurrentSession().saveOrUpdate(rmLogUid);
        return rmLogUid;
    }
    @Override
     @Transactional
    public List<RmDataApplicantDetail> getRMDetais(Long applicationId) {
        
         Criteria cr = sessionFactory.getCurrentSession().createCriteria(RmDataApplicantDetail.class).
                createAlias("rmDataApplication", "rmDataApplication");
        cr.add(Restrictions.eq("rmDataApplication.applicationId", applicationId));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<RmDataApplicantDetail> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }
}
