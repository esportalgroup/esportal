/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

/**
 *
 * @author dattatrayt
 */
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.MCMGroupFormationLog;
import com.engage.components.core.entity.bean.McCodeMstr;
import com.engage.components.core.entity.bean.McmApplicantsDetails;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmGroupMemberDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import com.engage.components.core.entity.bean.McmTriggerRequestLog;
import com.engage.components.core.entity.bean.SoundExDetailsBean;
import com.engage.components.esportal.vo.SingleListVO;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class McmTriggerDAOImpl implements McmTriggerDAO {
    
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public McmTriggerRequestLog setMcmTriggerRequestLog(McmTriggerRequestLog mcmTriggerRequestLog) {
        sessionFactory.getCurrentSession().saveOrUpdate(mcmTriggerRequestLog);
        return mcmTriggerRequestLog;
    }
    
    @Override
    @Transactional
    public McmGroupMemberDetail setMcmGroupMemberDetail(McmGroupMemberDetail memberDetail){
        sessionFactory.getCurrentSession().saveOrUpdate(memberDetail);
        return memberDetail;
    }
    
    @Override
    @Transactional
    public McmGroupDetail setMcmGroupDetail(McmGroupDetail groupDetail){
        sessionFactory.getCurrentSession().saveOrUpdate(groupDetail);
        return groupDetail;
    }
    
    @Override
    @Transactional
   public McmGroupDetail getMcmGroupDetailByGroupId(String groupId){

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmGroupDetail.class);
        criteria.add(Restrictions.eq("groupId", groupId.toString()));
//        criteria.add(Restrictions.ne("status", FrameworkAppConstants.INITIATE_DISBURSEMENT_SUCCESS));
        List<McmGroupDetail> groupDetails = criteria.list();
        if (groupDetails.isEmpty()) {
            return null;
        } else {
            return groupDetails.get(0);
        }

    }
   
    @Override
    @Transactional
   public List<McmGroupMemberDetail> getMcmGroupMemberDetailByGroupId(Long groupId){

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmGroupMemberDetail.class);
        criteria.add(Restrictions.eq("grpId", groupId));
        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmGroupMemberDetail> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details;
        }

    }

    @Override
    @Transactional
    public String saveSoundexDetails(SoundExDetailsBean details) {
        String response=null;
        try
        {
            sessionFactory.getCurrentSession().saveOrUpdate(details);
            response="sucess";
        }catch(Exception e)
        {
            response="fails";
        }
        
        return response;
        
    }

    @Override
    @Transactional
    public void saveMcmApplicationDetailsDetail(McmApplicationDetails appdetails) {
        sessionFactory.getCurrentSession().saveOrUpdate(appdetails);
    }

    @Override
    @Transactional
    public void saveMcmApplicantDetail(McmApplicantsDetails applicantDetails) {
        sessionFactory.getCurrentSession().saveOrUpdate(applicantDetails);
    }

    @Override
    @Transactional
    public McmApplicationDetails getMcmApplicationDetailsDetail(String appId) {
        
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicationDetails.class);
        criteria.add(Restrictions.eq("applicationId", appId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmApplicationDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    }

    @Override
    @Transactional
    public McmApplicantsDetails getMcmApplicantDetail(String applicantId, String appId) {
        
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicantsDetails.class);
        criteria.add(Restrictions.eq("applicantId", applicantId));
        criteria.add(Restrictions.eq("applicationId", appId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmApplicantsDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    }

    @Override
    @Transactional
    public McmGroupDetail getMcmGroupDetails(String groupId) {
        
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmGroupDetail.class);
        criteria.add(Restrictions.eq("groupId", groupId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmGroupDetail> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    }

    @Override
    @Transactional
    public List<McmApplicationDetails> getMcmApplicationDetailsByGroupId(String groupId) {
       
         Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicationDetails.class);
        criteria.add(Restrictions.eq("groupId", groupId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmApplicationDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details;
        }
    }

    @Override
    @Transactional
    public List<McmApplicantsDetails> getMcmApplicantsDetailsByGroupId(String groupId, String applicationId) {
        
          Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicantsDetails.class);
//        criteria.add(Restrictions.eq("groupId", groupId));
        criteria.add(Restrictions.eq("applicationId", applicationId));
        List<McmApplicantsDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details;
        }
    }

    @Override
    @Transactional
    public McCodeMstr getMcmMstrDetails(String mcmCode) {
        
          Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McCodeMstr.class);
        criteria.add(Restrictions.eq("mcCode",mcmCode));
//        criteria.add(Restrictions.eq("applicationId", applicationId));
        List<McCodeMstr> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    }
    
    @Override
    @Transactional
    public List<McmMpngDisbursementAccount> getMcmMpngDisbursementAccount(String mcCode){
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmMpngDisbursementAccount.class);
        criteria.add(Restrictions.eq("mcCode",mcCode));
        List<McmMpngDisbursementAccount> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details;
        }
    }

    @Override
    @Transactional
    public List<McmGroupDetail> getMCMGroups(String groupId, String mcCode) {
       
           Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmGroupDetail.class);
        criteria.add(Restrictions.eq("mcCode",mcCode));
        criteria.add(Restrictions.eq("groupId",groupId));
//        criteria.add(Restrictions.eq("applicationId", applicationId));
        List<McmGroupDetail> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details;
        }
    }

    @Override
    @Transactional
    public List<McmApplicationDetails> getMcmApplicationDetailsFilterByGroupId(String groupId, String mcCode) {
       
//    String sql1="select groupId as value from\n" +
//"	(select a.groupId, count(*) as count\n" +
//"	from\n" +
//"	es_mcm_group_detail g\n" +
//"	inner join es_mcm_applications_detail a\n" +
//"	on g.group_id=a.groupId\n" +
//"	where \n" +
//"	g.group_id <> '"+groupId+"' and (a.appraisal_flag='Positive' or a.appraisal_flag='Not Required')\n" +
//"	and g.mc_code=(select mc_code from es_mcm_group_detail where group_id='"+groupId+"')\n" +
//"	and g.flo_id=(select flo_id from es_mcm_group_detail where group_id='"+groupId+"')\n" +
//"	and g.locality_id=(select locality_id from es_mcm_group_detail where group_id='"+groupId+"')\n" +
//"	group by a.groupId having count(*) < 3) v";
        
    String sql="select groupId as value from  \n" +
" 	(select a.groupId, count(*) as count  \n" +
" 	from  \n" +
" 	es_mcm_group_detail g  \n" +
" 	inner join es_mcm_applications_detail a  \n" +
" 	on g.group_id=a.groupId  \n" +
" 	where   \n" +
" 	g.group_id <> '"+groupId+"'and (g.status is null or g.status='"+FrameworkAppConstants.INITIATE_DISBURSEMENT_FAILS+"') and a.appraisal_flag in('Positive','Not Required')\n" +
" 	and g.mc_code=(select mc_code from es_mcm_group_detail where group_id='"+groupId+"')  \n" +
" 	and g.flo_id=(select flo_id from es_mcm_group_detail where group_id='"+groupId+"')  \n" +
" 	and g.locality_id=(select locality_id from es_mcm_group_detail where group_id= '"+groupId+"')  \n" +
" 	group by a.groupId having count(*) < 3) v";

        List<SingleListVO> lstGrps = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("value", StringType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(SingleListVO.class))
                .list();
        
        if(lstGrps == null || lstGrps.isEmpty() || lstGrps.size() <= 0){
            return null;
        }
        
        List<String> grpIdLst=new ArrayList<String>();
        for(SingleListVO o : lstGrps){
            
            System.out.println("groups"+o.getValue());
            grpIdLst.add(o.getValue());
        }
        
          Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicationDetails.class);
//        criteria.add(Restrictions.eq("mcCode",mcCode));
        criteria.add(Restrictions.ne("groupId",groupId));
        criteria.add(Restrictions.in("groupId", grpIdLst));
        Criterion appraisalFlagPsitive = Restrictions.eq("appraisalflag","Positive");
        Criterion appraisalFlagNotReq = Restrictions.eq("appraisalflag","Not Required");
        criteria.add(Restrictions.or(appraisalFlagPsitive, appraisalFlagNotReq));
        List<McmApplicationDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details;
        }
    }

    @Override
    @Transactional
    public McmGroupDetail getMcmGroupDetailBtGroupId(String groupId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmGroupDetail.class);
        criteria.add(Restrictions.eq("groupId", groupId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmGroupDetail> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    
    }

    @Override
    @Transactional
    public void updateMCMGroupDetail(McmGroupDetail groupDetails) {
        sessionFactory.getCurrentSession().saveOrUpdate(groupDetails);
    }

    @Override
    @Transactional
    public McmApplicationDetails getMcmApplicationByGroupId(String groupId, String appId) {
       
         Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicationDetails.class);
        criteria.add(Restrictions.eq("applicationId", appId));
        criteria.add(Restrictions.eq("groupId", groupId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmApplicationDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    }

    @Override
    @Transactional
    public void updateMCMApplicationDetails(McmApplicationDetails applications) {
      sessionFactory.getCurrentSession().saveOrUpdate(applications);  
    }

    @Override
    @Transactional
    public McmApplicantsDetails getMcmApplicantByGroupId(String groupId, String applicantId) {
        
         Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicantsDetails.class);
        criteria.add(Restrictions.eq("applicantId", applicantId));
        criteria.add(Restrictions.eq("groupId", groupId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmApplicantsDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    }

    @Override
    @Transactional
    public void upDateMCMApplicantDetails(McmApplicantsDetails applicants) {
       sessionFactory.getCurrentSession().saveOrUpdate(applicants);  
    }

    @Override
    @Transactional
    public McmMpngDisbursementAccount getAccountDetailsByAccountNo(String bankAccountNo) {
       Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmMpngDisbursementAccount.class);
        criteria.add(Restrictions.eq("disbaccount", bankAccountNo));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmMpngDisbursementAccount> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        } 
    }

    @Override
    @Transactional
    public List<McmApplicantsDetails> getApplicantDetails(String groupId) {
         Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicantsDetails.class);
        criteria.add(Restrictions.eq("groupId", groupId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmApplicantsDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details;
        }
    }

    @Override
    @Transactional
    public void setMCMGroupFormationLog(MCMGroupFormationLog log) {
        sessionFactory.getCurrentSession().save(log);
    }

    @Override
    @Transactional
    public List<McmApplicationDetails> getApplicatonsDetails(String groupId) {
        
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmApplicationDetails.class);
        criteria.add(Restrictions.eq("groupId", groupId));
        criteria.add(Restrictions.isNull("isEsigned"));
        List<McmApplicationDetails> details = criteria.list();
        return details;
       
    }
   
   
}
