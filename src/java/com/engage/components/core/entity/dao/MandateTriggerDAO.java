/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

/**
 *
 * @author DattatrayT
 */
import java.util.List;
import com.engage.components.core.entity.bean.MandateTriggerTokenUser;
import com.engage.components.core.entity.bean.MandateTriggerTokenRequestLog;
import com.engage.components.core.entity.bean.MandateTriggerRequestLog;
import com.engage.components.core.entity.bean.MandateTriggerDataApplication;
import com.engage.components.core.entity.bean.MandateTriggerDataDocument;
import com.engage.components.core.entity.bean.MandateTriggerDataApplicantsDetails;

public interface MandateTriggerDAO {
    public MandateTriggerTokenUser getMandateTriggerTokenUser(String userId);
    public MandateTriggerTokenRequestLog setMandateTriggerTokenRequestLog(MandateTriggerTokenRequestLog log);
    public MandateTriggerTokenUser setMandateTriggerTokenUser(MandateTriggerTokenUser mandateTriggerTokenUser);
    public MandateTriggerRequestLog setMandateTriggerRequestLog(MandateTriggerRequestLog mandateTriggerRequestLog);
    public MandateTriggerDataApplication getMandateTriggerDataApplication(String applicationId);
    public MandateTriggerDataApplication setMandateTriggerDataApplication(MandateTriggerDataApplication mandateTriggerDataApplication);
    public MandateTriggerDataDocument setMandateTriggerDataDocument(MandateTriggerDataDocument mandateTriggerDataDocument);
    public List<MandateTriggerDataDocument> getMandateTriggerDataDocument(Long refid);
    public MandateTriggerDataApplication getMandateTriggerDataApplicationByRefid(Long refid);
    public List<MandateTriggerDataApplicantsDetails> getMandateTriggerDataApplicantsDetails(Long refid);
    public MandateTriggerDataApplicantsDetails setMandateTriggerDataApplicantsDetails(MandateTriggerDataApplicantsDetails mandateTriggerDataApplicantsDetails);
    public MandateTriggerRequestLog getMandateTriggerRequestLog(Long id);
    public MandateTriggerDataDocument getMandateTriggerDataDocumentById(Long id, Long refid);
    public List<MandateTriggerDataApplication> getMandateTriggerDataApplicationLst(String applicationId);
}
