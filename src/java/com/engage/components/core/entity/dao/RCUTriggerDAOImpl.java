/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.RcuTriggerApplicant;
import com.engage.components.core.entity.bean.RcuTriggerConstants;
import com.engage.components.core.entity.bean.RcuTriggerDocument;
import com.engage.components.core.entity.bean.RcuTriggerLog;
import com.engage.components.core.entity.bean.RcuTriggerLosLog;
import com.engage.components.core.entity.bean.RcuTriggerStatus;
import com.engage.components.core.entity.bean.RcuTriggerStatusLog;
import com.engage.components.core.entity.bean.RcuTriggerToken;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author SurajK
 */
@Repository
@Component
@Transactional
public class RCUTriggerDAOImpl implements RCUTriggerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public RcuTriggerLog setSangamRcuTriggerLog(RcuTriggerLog sangamRcuTriggerLog) {
        sessionFactory.getCurrentSession().saveOrUpdate(sangamRcuTriggerLog);
        return sangamRcuTriggerLog;
    }

    @Override
    @Transactional
    public RcuTriggerLog getSangamRcuTriggerLogById(Long Id) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerLog.class);
        cr.add(Restrictions.eq("logId", Id));
        List<RcuTriggerLog> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public RcuTriggerStatus getSangamRcuTriggerStatus(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerStatus.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        List<RcuTriggerStatus> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public void setSangamRcuTriggerStatus(RcuTriggerStatus sangamRcuTriggerStatus) {
        sessionFactory.getCurrentSession().saveOrUpdate(sangamRcuTriggerStatus);
        RcuTriggerStatusLog sangamRcuTriggerStatusLog = new RcuTriggerStatusLog();
        sangamRcuTriggerStatusLog.setApplicationId(sangamRcuTriggerStatus.getApplicationId());
        sangamRcuTriggerStatusLog.setIsSubmited(sangamRcuTriggerStatus.getIsSubmited());
        sangamRcuTriggerStatusLog.setRcuModBy(sangamRcuTriggerStatus.getRcuModBy());
        sangamRcuTriggerStatusLog.setRcuModDate(sangamRcuTriggerStatus.getRcuModDate());
        sangamRcuTriggerStatusLog.setRcuOutcomeStatus(sangamRcuTriggerStatus.getRcuOutcomeStatus());
        sangamRcuTriggerStatusLog.setRcuRemark(sangamRcuTriggerStatus.getRcuRemark());
        sangamRcuTriggerStatusLog.setRcuStatus(sangamRcuTriggerStatus.getRcuStatus());
        sangamRcuTriggerStatusLog.setSubmitedDate(sangamRcuTriggerStatus.getSubmitedDate());

        sessionFactory.getCurrentSession().save(sangamRcuTriggerStatusLog);

    }

    @Override
    @Transactional
    public RcuTriggerApplicant getSangamRcuTriggerApplicant(String applicationId, String applicantId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerApplicant.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        cr.add(Restrictions.eq("applicantId", applicantId));
        List<RcuTriggerApplicant> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public List<RcuTriggerApplicant> getSangamRcuTriggerApplicantByApplication(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerApplicant.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        List<RcuTriggerApplicant> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public void setSangamRcuTriggerApplicant(RcuTriggerApplicant sangamRcuTriggerApplicant) {
        sessionFactory.getCurrentSession().saveOrUpdate(sangamRcuTriggerApplicant);
    }

    @Override
    @Transactional
    public RcuTriggerDocument getSangamRcuTriggerDocument(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerDocument.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        List<RcuTriggerDocument> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public List<RcuTriggerDocument> getSangamRcuTriggerDocumentByApplicant(String applicationId, String applicantId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerDocument.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        cr.add(Restrictions.eq("applicantId", applicantId));
        List<RcuTriggerDocument> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public void setSangamRcuTriggerDocument(RcuTriggerDocument sangamRcuTriggerDocument) {
        sessionFactory.getCurrentSession().saveOrUpdate(sangamRcuTriggerDocument);
    }

    @Override
    @Transactional
    public RcuTriggerDocument getDocumentById(Long docId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerDocument.class);
        cr.add(Restrictions.eq("id", docId));
        List<RcuTriggerDocument> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public List<RcuTriggerConstants> getRcuConstants(String type) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerConstants.class);
        cr.add(Restrictions.eq("status", FrameworkAppConstants.SANGAM_MC_STATUS));
        cr.add(Restrictions.eq("type", type));
        List<RcuTriggerConstants> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public void setToken(RcuTriggerToken sangamRcuTriggerToken) {
        sessionFactory.getCurrentSession().saveOrUpdate(sangamRcuTriggerToken);
    }

    @Override
    @Transactional
    public RcuTriggerToken getToken() {
        DetachedCriteria subCriteria = DetachedCriteria.forClass(RcuTriggerToken.class);
        subCriteria.setProjection(Projections.max("tokenId"));
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(RcuTriggerToken.class);
        cr.add(Subqueries.propertyIn("tokenId", subCriteria));
        List<RcuTriggerToken> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }

    }

    @Override
    @Transactional
    public void setLosRequest(RcuTriggerLosLog sangamRcuTriggerLosLog) {
        sessionFactory.getCurrentSession().saveOrUpdate(sangamRcuTriggerLosLog);
    }

}
