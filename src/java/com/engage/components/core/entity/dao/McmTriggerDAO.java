/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.MCMGroupFormationLog;
import com.engage.components.core.entity.bean.McCodeMstr;
import com.engage.components.core.entity.bean.McmApplicantsDetails;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmGroupMemberDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import com.engage.components.core.entity.bean.McmTriggerRequestLog;
import com.engage.components.core.entity.bean.SoundExDetailsBean;
import java.util.List;

/**
 *
 * @author dattatrayt
 */
public interface McmTriggerDAO {
    public McmTriggerRequestLog setMcmTriggerRequestLog(McmTriggerRequestLog mcmTriggerRequestLog);
    public McmGroupMemberDetail setMcmGroupMemberDetail(McmGroupMemberDetail memberDetail);
    public McmGroupDetail setMcmGroupDetail(McmGroupDetail groupDetail);
    public McmGroupDetail getMcmGroupDetailByGroupId(String groupId);
    public List<McmGroupMemberDetail> getMcmGroupMemberDetailByGroupId(Long groupId);
    public String saveSoundexDetails(SoundExDetailsBean details);

    public void saveMcmApplicationDetailsDetail(McmApplicationDetails appdetails);

    public void saveMcmApplicantDetail(McmApplicantsDetails applicantDetails);

    public McmApplicationDetails getMcmApplicationDetailsDetail(String appId);

    public McmApplicantsDetails getMcmApplicantDetail(String applicantId, String appId);

    public McmGroupDetail getMcmGroupDetails(String groupId);

    public List<McmApplicationDetails> getMcmApplicationDetailsByGroupId(String groupId);

    public List<McmApplicantsDetails> getMcmApplicantsDetailsByGroupId(String groupId, String applicationId);

    public McCodeMstr getMcmMstrDetails(String mcmCode);
    
    public List<McmMpngDisbursementAccount> getMcmMpngDisbursementAccount(String mcCode);

    public List<McmGroupDetail> getMCMGroups(String groupId, String mcCode);

    public List<McmApplicationDetails> getMcmApplicationDetailsFilterByGroupId(String groupId, String mcCode);

    public McmGroupDetail getMcmGroupDetailBtGroupId(String groupId);

    public void updateMCMGroupDetail(McmGroupDetail groupDetails);

    public McmApplicationDetails getMcmApplicationByGroupId(String groupId, String appId);

    public void updateMCMApplicationDetails(McmApplicationDetails applications);

    public McmApplicantsDetails getMcmApplicantByGroupId(String groupId, String applicantId);

    public void upDateMCMApplicantDetails(McmApplicantsDetails applicants);

    public McmMpngDisbursementAccount getAccountDetailsByAccountNo(String bankAccountNo);

    public List<McmApplicantsDetails> getApplicantDetails(String groupId);

    public void setMCMGroupFormationLog(MCMGroupFormationLog log);

    public List<McmApplicationDetails> getApplicatonsDetails(String groupId);
}
