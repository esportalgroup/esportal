/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.McmApplicantsDetails;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmDisbursementDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import java.util.List;

/**
 *
 * @author ven00253
 */
public interface MCMPostDisbursementDao {

    public McmDisbursementDetails getMCMAppDisbursementDetails(String applicationId);

    public String saveInitiateEsignDetails(McmDisbursementDetails disbursementDetail);
    
}
