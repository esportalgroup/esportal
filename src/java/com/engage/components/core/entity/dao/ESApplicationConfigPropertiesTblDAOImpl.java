/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.ESApplicationConfigPropertiesTbl;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cmssjava
 */
@Repository
public class ESApplicationConfigPropertiesTblDAOImpl implements ESApplicationConfigPropertiesTblDAO{
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public List<ESApplicationConfigPropertiesTbl> getProperty() {
        List<ESApplicationConfigPropertiesTbl> lst=null;
        try {
            Criteria cr = sessionFactory.getCurrentSession().createCriteria(ESApplicationConfigPropertiesTbl.class);
            lst=cr.list();
        } catch (Exception e) {
        }
        return lst;
    }
    
}
