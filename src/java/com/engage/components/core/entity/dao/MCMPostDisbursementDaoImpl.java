/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.McmApplicantsDetails;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmDisbursementDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ven00253
 */
@Repository
public class MCMPostDisbursementDaoImpl implements MCMPostDisbursementDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public McmDisbursementDetails getMCMAppDisbursementDetails(String applicationId) {
         Criteria criteria = sessionFactory.getCurrentSession().createCriteria(McmDisbursementDetails.class);
        criteria.add(Restrictions.eq("applicationId", applicationId));
//        criteria.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<McmDisbursementDetails> details = criteria.list();
        if (details.isEmpty()) {
            return null;
        } else {
            return details.get(0);
        }
    }

    @Override
    @Transactional
    public String saveInitiateEsignDetails(McmDisbursementDetails disbursementDetail) {
        String response=null;
        try
        {
           sessionFactory.getCurrentSession().saveOrUpdate(disbursementDetail); 
           response="success";
        }catch(Exception e)
        {
            response="fails";
        
        }
        
        return response;
        
    }
        
    }
