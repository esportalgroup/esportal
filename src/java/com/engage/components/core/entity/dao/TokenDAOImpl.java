/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

/**
 *
 * @author DattatrayT
 */

import com.engage.components.core.entity.bean.EsTokenRequestLog;
import com.engage.components.core.entity.bean.EsTokenUser;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class TokenDAOImpl implements TokenDAO{
    
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
    
    @Override
    @Transactional
    public EsTokenUser getEsTokenUser(String userId){
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsTokenUser.class);
        cr.add(Restrictions.eq("userId", userId));
        List<EsTokenUser> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }
    
    @Override
    @Transactional
    public EsTokenRequestLog setEsTokenRequestLog(EsTokenRequestLog esTokenRequestLog){
        sessionFactory.getCurrentSession().saveOrUpdate(esTokenRequestLog);
        return esTokenRequestLog;
    }
}
