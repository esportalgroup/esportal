package com.engage.components.core.entity.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CoreComponentDAOImpl implements CoreComponentDAO {
	
	
	private static final String CacheRegion = "socialbanking";
	
	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	
	

	@Override
	@Transactional
	public List getModules() {
		return null;
//		return getCurrentSession().createCriteria(Module.class).list();
	}

}