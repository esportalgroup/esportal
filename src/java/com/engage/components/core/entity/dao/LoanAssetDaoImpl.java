/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Shrikant Katakdhond
 */
@Repository
public class LoanAssetDaoImpl implements LoanAssetDao {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public EsApplicantsDetailBean getApplicantsDetailsByAppId(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsApplicantsDetailBean.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsApplicantsDetailBean> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public EsAssetDetailBean getAssetDetailByAppId(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsAssetDetailBean.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsAssetDetailBean> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public EsLoanDetailBean getLoanDetailByAppId(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsLoanDetailBean.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsLoanDetailBean> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public String saveApplicantDetails(EsApplicantsDetailBean applicantDetails) {
        String response = "";
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(applicantDetails);
            response = "success";
        } catch (Exception e) {
            response = "fails";
        }
        return response;
    }

    @Override
    @Transactional
    public String saveLoanDetails(EsLoanDetailBean loanDetails) {
        String response = "";
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(loanDetails);
            response = "success";
        } catch (Exception e) {
            response = "fails";
        }
        return response;
    }

    @Override
    @Transactional
    public String saveAssetDetails(EsAssetDetailBean assetDetails) {
        String response = "";
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(assetDetails);
            response = "success";
        } catch (Exception e) {
            response = "fails";
        }
        return response;
    }

    @Override
    @Transactional
    public EsInvoiceDocBean getInvoiceDetailsByAppId(String applicationId) {
         Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsInvoiceDocBean.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsInvoiceDocBean> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public String saveInvoiceDocDetails(EsInvoiceDocBean invoiceDoc) {
        
        String response = "";
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(invoiceDoc);
            response = "success";
        } catch (Exception e) {
            response = "fails";
        }
        return response;
    }
}
