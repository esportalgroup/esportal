package com.engage.components.core.entity.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.SangamMstrSelectCriteria;
import com.engage.components.esportal.vo.BuilderDashBoardVO;
import com.engage.components.esportal.vo.DealerDashbordVO;
import com.engage.components.esportal.vo.DealerStatusDashboardVO;
import com.engage.components.esportal.vo.McmDashBoardVO;
import com.engage.components.esportal.vo.McmGroupDetailVO;
import com.engage.components.esportal.vo.OpsDashBoardVO;
import com.engage.components.esportal.vo.RCUDashboardVO;
import com.engage.components.esportal.vo.RMDashboardVO;
import com.engage.session.beans.UserInfoBean;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;

/**
 *
 * @author Shrikant Katakdhond
 */
@Repository
public class DashboardDAOImpl implements DashboardDAO {

    private static final String CacheRegion = "socialbanking";

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    UserInfoBean userInfoBean;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public List<SangamMstrSelectCriteria> getSelectCriterias(String forDashboard) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(SangamMstrSelectCriteria.class);
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        cr.add(Restrictions.eq("forDashboard", forDashboard));
        List<SangamMstrSelectCriteria> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public List<DealerStatusDashboardVO> getDealerDashbord(String userId, Long fromRowNum, Long toRowNum, Long profileID) {
//        String sql = "select PRE.* \n"
//                + "                from(\n"
//                + "                select ROW_NUMBER() OVER (ORDER BY A.application_id) AS ROWNUM,\n"
////                + "                	A.application_id as applicantId,\n"
//                + "                	A.application_id as applicationId,\n"
//                + "                	A.applicant_name as applicatName,\n"
//                + "					A.dsa_name as dsaName,\n"
//                + "					A.fls_name as flsName,\n"
//                + "					A.supplier_name as supplierName,\n"
//                + "					A.supplier_id as supplierID,\n"
//                + "					L.stamp_duty as stampDuty,\n"
////                + "					L.proccessing_fee as processingFees,\n"
//                + "					L.margin_money_docId as marginMoneyDocID,\n"
//                + "					L.margin_money as marginMoney,\n"
//                + "					L.loan_amount as loanAmount,\n"
//                + "					L.invoice_number as invoiceNumber,\n"
//                + "					L.invoice_DocId as invoiceDocID,\n"
//                + "					A.fls_id as flsID,\n"
//                + "					I.engine_number as engineNumber,\n"
//                + "					cast(I.invoice_date as date) as invoiceDate,\n"
//                + "					L.ecs_charges as ecsCharges,\n"
//                + "					A.dsa_id as dsaId,\n"
//                + "					L.doc_charges as docCharges,\n"
//                +"(cast(L.stamp_duty as float)+cast(L.ecs_charges as float)+cast(L.doc_charges as float)+cast(L.proccessing_fee as float)) as otherCharges, \n"
//                +"(cast(L.margin_money as float)+cast(L.cli_premium as float)+cast(L.stamp_duty as float)+cast(L.ecs_charges as float)+cast(L.doc_charges as float)+cast(L.proccessing_fee as float)) as paymentPaidToDlr, \n"
//                + "					A.co_applicant_name as coApplicantName,\n"
//                + "					L.cli_premium as cliPremium,\n"
//                + "					I.chassis_number as chassisNumber,\n"
//                + "					DA.asset_model as assetModel,\n"
//                + "					DA.assetCost_cost_tax as assetCostTax,\n"
//                + "					DA.assetCost_Onboarding_price as assetCostOnboardingPrice,\n"
//                + "					DA.assetCost_cost_ins as assetCostIns,\n"
//                + "					DA.assetCost_cost_esr as assetCostESR,\n"
//                + "					DA.asset_manufacturer as assetManufacturer,\n"
//                + "					DA.asset_make as assetMake,\n"
//                + "					'2WH' as product,\n"
//                + "					A.status as status,\n"
//                + "					case when(A.pdd_status is null) then 'PENDING' else A.pdd_status end as pddStatus,\n"
//                + "\n"
//                + "					cast(A.application_date as date) as applicationDate,\n"
//                + "                	cast(A.cre_date as date) as submittedDate,\n"
//                + "                	1 as progress,\n"
//                + "                	(select count(application_id) from es_applicants_details) as caseCount \n"
//                + "                from es_applicants_details A\n"
//                + "				inner join es_dealer_asset_details DA on\n"
//                + "				DA.application_id = A.application_id\n"
//                + "				inner join es_dealer_loan_details L on\n"
//                + "				L.application_id = A.application_id\n"
//                + "				inner join es_dealer_invoice_doc_details I on\n"
//                + "				I.application_id = A.application_id\n"
//                + "                )PRE\n"
//                + "                where PRE.ROWNUM between 1 and 15 order by PRE.applicationId asc";

        String sql = "exec SPgetDealerStatusDashboard " + userId + "," + fromRowNum + "," + toRowNum + "," + profileID;
        List<DealerStatusDashboardVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
//                .addScalar("applicantId", StringType.INSTANCE)
                .addScalar("applicationId", StringType.INSTANCE)
                .addScalar("applicatName", StringType.INSTANCE)
                .addScalar("dsaName", StringType.INSTANCE)
                .addScalar("flsName", StringType.INSTANCE)
                .addScalar("supplierName", StringType.INSTANCE)
                .addScalar("supplierID", StringType.INSTANCE)
                .addScalar("stampDuty", StringType.INSTANCE)
                .addScalar("otherCharges", FloatType.INSTANCE)
                .addScalar("paymentPaidToDlr", FloatType.INSTANCE)
//                .addScalar("processingFees", StringType.INSTANCE)
                .addScalar("marginMoneyDocID", StringType.INSTANCE)
                .addScalar("marginMoney", StringType.INSTANCE)
                .addScalar("loanAmount", StringType.INSTANCE)
                .addScalar("invoiceNumber", StringType.INSTANCE)
                .addScalar("invoiceDocID", StringType.INSTANCE)
                .addScalar("flsID", StringType.INSTANCE)
                .addScalar("engineNumber", StringType.INSTANCE)
                .addScalar("invoiceDate", StringType.INSTANCE)
                .addScalar("ecsCharges", StringType.INSTANCE)
                .addScalar("dsaId", StringType.INSTANCE)
                .addScalar("docCharges", StringType.INSTANCE)
                .addScalar("coApplicantName", StringType.INSTANCE)
                .addScalar("cliPremium", StringType.INSTANCE)
                .addScalar("chassisNumber", StringType.INSTANCE)
                .addScalar("assetModel", StringType.INSTANCE)
                .addScalar("assetCostTax", StringType.INSTANCE)
                .addScalar("assetCostOnboardingPrice", StringType.INSTANCE)
                .addScalar("assetCostIns", StringType.INSTANCE)
                .addScalar("assetCostESR", StringType.INSTANCE)
                .addScalar("assetManufacturer", StringType.INSTANCE)
                .addScalar("assetMake", StringType.INSTANCE)
                .addScalar("product", StringType.INSTANCE)
                .addScalar("status", StringType.INSTANCE)
                .addScalar("pddStatus", StringType.INSTANCE)
                .addScalar("applicationDate", StringType.INSTANCE)
                .addScalar("submittedDate", StringType.INSTANCE)
                .addScalar("progress", StringType.INSTANCE)
                .addScalar("caseCount", IntegerType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(DealerStatusDashboardVO.class))
                .list();

//		System.out.println("Size: "+lst.size());
        return lst;
    }

    @Override
    @Transactional
    public List<OpsDashBoardVO> getOpsDashbord(String supplierId, Long fromRowNum, Long toRowNum, Character isPending) {
//        String sql = "exec SPgetOpsPortalDashboard 0," + fromRowNum + "," + toRowNum;
        String sql = "";
        if(isPending.equals(FrameworkAppConstants.CONSTANT_YES)){
            sql = sql + "exec SPgetOpsDashboard "+fromRowNum+", "+toRowNum+", 'PENDING'";
        }else{
            sql = sql + "exec SPgetOpsDashboard "+fromRowNum+", "+toRowNum+", 'NonPENDING'";
        }
        System.out.println("sql : "+sql);
        List<OpsDashBoardVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("ROWNUM", LongType.INSTANCE)
                .addScalar("refid", LongType.INSTANCE)
                .addScalar("applicationId", StringType.INSTANCE)
                .addScalar("applicantName", StringType.INSTANCE)
                .addScalar("flsName", StringType.INSTANCE)
                .addScalar("branchName", StringType.INSTANCE)
                .addScalar("amount", StringType.INSTANCE)
                .addScalar("sanctionDate", StringType.INSTANCE)
                .addScalar("status", StringType.INSTANCE)
                .addScalar("lockBy", StringType.INSTANCE)
                .addScalar("lockByName", StringType.INSTANCE)
                .addScalar("caseCount", LongType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(OpsDashBoardVO.class))
                .list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }
    
    @Override
    @Transactional
    public List<OpsDashBoardVO> getOpsDashbordByApplicationId(String supplierId, Long fromRowNum, Long toRowNum, Character isPending, String ApplicationId) {
//        String sql = "exec SPgetOpsPortalDashboard 0," + fromRowNum + "," + toRowNum;
        String sql = "	select  " +
"	ROW_NUMBER() OVER (ORDER BY refid) AS ROWNUM, " +
"	refid, application_id as applicationId, FLS_name as flsName, branch_name as branchName, amount, sanction_date as sanctionDate, " +
"	status " +
"	from mandate_trigger_data_application where application_id like '%"+ApplicationId.trim()+"%'";
        if(isPending.equals(FrameworkAppConstants.CONSTANT_YES)){
            sql = sql + " and status='PENDING'";
        }
        List<OpsDashBoardVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("ROWNUM", LongType.INSTANCE)
                .addScalar("refid", LongType.INSTANCE)
                .addScalar("applicationId", StringType.INSTANCE)
                .addScalar("flsName", StringType.INSTANCE)
                .addScalar("branchName", StringType.INSTANCE)
                .addScalar("amount", StringType.INSTANCE)
                .addScalar("sanctionDate", StringType.INSTANCE)
                .addScalar("status", StringType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(OpsDashBoardVO.class))
                .list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }
       
     @Override
    @Transactional
    public List<McmDashBoardVO> getMcmDashboard(String supplierId, Long fromRowNum, Long toRowNum,String type,String localityId,String villageName,String mcCode) {
//    System.out.println("fromRowNum"+fromRowNum +"toRowNum"+toRowNum);

    String  sql = "exec [SPgetMcmDashboard] '"+fromRowNum+"', '"+toRowNum+"', '"+mcCode+"','"+type+"'";

     List<McmDashBoardVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("grpId", LongType.INSTANCE)
                .addScalar("groupId", StringType.INSTANCE)
                .addScalar("groupName", StringType.INSTANCE)
                .addScalar("floName", StringType.INSTANCE)
                .addScalar("villageName", StringType.INSTANCE)
                .addScalar("count", StringType.INSTANCE)   
                .addScalar("localityId", StringType.INSTANCE)
                .addScalar("stateId", StringType.INSTANCE)
                .addScalar("districtId", StringType.INSTANCE)
                .addScalar("branchId", StringType.INSTANCE)
                .addScalar("sanctionDate", StringType.INSTANCE)
                .addScalar("mcCode", StringType.INSTANCE)
                .addScalar("collectionDate", StringType.INSTANCE)
                .addScalar("status", StringType.INSTANCE)
                .addScalar("caseCount", LongType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(McmDashBoardVO.class))
                .list();
     
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

       @Override
    @Transactional
    public List<McmDashBoardVO> getMcmDashbordSearch(Long fromRowNum, Long toRowNum, String mcCode, String searchParameter, String searchParameterValue) {
        String sql = "";
                    sql = sql + "exec [SPgetMcmDashboardSearch] '"+searchParameter.trim()+"','"+searchParameterValue.trim()+"','"+fromRowNum+"', '"+toRowNum+"', '"+mcCode+"'";
        
        System.out.println("sql : "+sql);
        List<McmDashBoardVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("ROWNUM", LongType.INSTANCE)
                .addScalar("grpId", LongType.INSTANCE)
                .addScalar("groupId", StringType.INSTANCE)
                .addScalar("groupName", StringType.INSTANCE)
                .addScalar("floName", StringType.INSTANCE)
                .addScalar("villageName", StringType.INSTANCE)
                .addScalar("count", StringType.INSTANCE)   
                .addScalar("localityId", StringType.INSTANCE)
                .addScalar("stateId", StringType.INSTANCE)
                .addScalar("districtId", StringType.INSTANCE)
                .addScalar("branchId", StringType.INSTANCE)
                .addScalar("sanctionDate", StringType.INSTANCE)
                .addScalar("mcCode", StringType.INSTANCE)
                .addScalar("collectionDate", StringType.INSTANCE)
                .addScalar("caseCount", LongType.INSTANCE)
                
                
                .setResultTransformer(Transformers.aliasToBean(McmDashBoardVO.class))
                .list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }    
    }

     
    
    @Override
    @Transactional
    public List<BuilderDashBoardVO> getBuilderDashbord(String userId, Long fromRowNum, Long toRowNum, Long profileID) {
        List<BuilderDashBoardVO> lst = null;
        try {
            String sql1 = "select ROW_NUMBER() OVER (ORDER BY account.builder_C) AS ROWNUM,\n"
                    + "	account.builder_C as builderC,\n"
                    + "	account.builder_id_c as builderIdC,\n"
                    + "	account.builder_code as builderCode,\n"
                    + "	account.account_name as accountName,\n"
                    + "	apfProject.apf_project_c as apfProjectC,\n"
                    + "  account.APF_status as apfStatus,\n"
                    + "	1 as progress,\n"
                    + "	(select count(account.builder_C) from es_mstr_account as account) as caseCount  \n"
                    + "from es_mstr_account as account inner join es_mstr_apf_project as apfProject on account.builder_C=apfProject.builder_id\n";

//        String sql = "exec SPgetPostSanctionDashboard " + userId + "," + fromRowNum + "," + toRowNum + "," + profileID;
            lst = sessionFactory.getCurrentSession().createSQLQuery(sql1)
                    .addScalar("builderC", StringType.INSTANCE)
                    .addScalar("builderIdC", StringType.INSTANCE)
                    .addScalar("builderCode", StringType.INSTANCE)
                    .addScalar("accountName", StringType.INSTANCE)
                    .addScalar("apfProjectC", StringType.INSTANCE)
                    .addScalar("apfStatus", StringType.INSTANCE)
                    .addScalar("progress", StringType.INSTANCE)
                    .addScalar("caseCount", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(BuilderDashBoardVO.class))
                    .list();

//            System.out.println("Size: " + lst.size() + lst.get(0));
        } catch (HibernateException e) {
            e.printStackTrace();
        }

        return lst;
    }

    @Override
    @Transactional
    public List<RCUDashboardVO> getRCUTriggerApplications(String userId, Long fromRowNum, Long toRowNum) {
        String sql = "exec [dbo].[SPgetRCUTriggerDasbboard] " + fromRowNum + "," + toRowNum;

        List<RCUDashboardVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("applicationNo", StringType.INSTANCE)
                .addScalar("Associate_Name", StringType.INSTANCE)
                .addScalar("Status", StringType.INSTANCE)
                .addScalar("productCategory", StringType.INSTANCE)
                .addScalar("custName", StringType.INSTANCE)
                .addScalar("caseCount", StringType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(RCUDashboardVO.class))
                .list();

        return lst;
    }

    @Override
    @Transactional
    public List<RMDashboardVO> getRMDashboard(String userId, Long fromRowNum, Long toRowNum) {

        List<RMDashboardVO> lst = null;
        try {
            String sql1 = "select ROW_NUMBER() OVER (ORDER BY application_id) AS ROWNUM,\n"
                    + "                    application_id as applicationId,\n"
                    + "                    cre_date as creDate,\n"
                    + "                   status as Status,\n"
                    + "                    1 as progress,\n"
                    + "                    (select count(application_id) from rm_data_application) as caseCount  \n"
                    + "                    from rm_data_application";

//        String sql = "exec SPgetPostSanctionDashboard " + userId + "," + fromRowNum + "," + toRowNum + "," + profileID;
            lst = sessionFactory.getCurrentSession().createSQLQuery(sql1)
                    .addScalar("applicationId", StringType.INSTANCE)
                    .addScalar("creDate", StringType.INSTANCE)
                    .addScalar("caseCount", StringType.INSTANCE)
                    .addScalar("Status", StringType.INSTANCE)
                    .addScalar("progress", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(RMDashboardVO.class))
                    .list();

//            System.out.println("Size: " + lst.size() + lst.get(0));
        } catch (HibernateException e) {
            e.printStackTrace();
        }

        return lst;
    }

    @Override
    @Transactional
    public EsApplicantsDetailBean getApplicantsDetailsByAppId(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsApplicantsDetailBean.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsApplicantsDetailBean> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public List<OpsDashBoardVO> getOpsDashbordSearch(String supplierId, Long fromRowNum, Long toRowNum, Character isPending, String searchParameter, String searchParameterValue) {
        String sql = "";
        if(isPending.equals(FrameworkAppConstants.CONSTANT_YES)){
            sql = sql + "exec [SPgetOpsDashboardSearch] '"+searchParameter.trim()+"','"+searchParameterValue.trim()+"', "+fromRowNum+", "+toRowNum+", 'PENDING'";
        }else{
            sql = sql + "exec [SPgetOpsDashboardSearch] '"+searchParameter.trim()+"','"+searchParameterValue.trim()+"', "+fromRowNum+", "+toRowNum+", 'NonPENDING'";
        }
        System.out.println("sql : "+sql);
        List<OpsDashBoardVO> lst = sessionFactory.getCurrentSession().createSQLQuery(sql)
                .addScalar("ROWNUM", LongType.INSTANCE)
                .addScalar("refid", LongType.INSTANCE)
                .addScalar("applicationId", StringType.INSTANCE)
                .addScalar("applicantName", StringType.INSTANCE)
                .addScalar("flsName", StringType.INSTANCE)
                .addScalar("branchName", StringType.INSTANCE)
                .addScalar("amount", StringType.INSTANCE)
                .addScalar("sanctionDate", StringType.INSTANCE)
                .addScalar("status", StringType.INSTANCE)
                .addScalar("lockBy", StringType.INSTANCE)
                .addScalar("lockByName", StringType.INSTANCE)
                .addScalar("caseCount", LongType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(OpsDashBoardVO.class))
                .list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }
}
