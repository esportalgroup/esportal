/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Shrikant Katakdhond
 */

@Repository
public class PasswordResetDAOImpl implements PasswordResetDAO {

    private static final String CacheRegion = "socialbanking";

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public FrameworkMstrLogin getFrameworkMstrLoginByUserName(String userName){
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(FrameworkMstrLogin.class);
        cr.add(Restrictions.eq("userName", userName));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<FrameworkMstrLogin> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }
    
    
    @Override
    @Transactional
    public FrameworkMstrLogin getFrameworkMstrLoginByUserId(Long userId){
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(FrameworkMstrLogin.class);
        cr.add(Restrictions.eq("userId", userId));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<FrameworkMstrLogin> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }
    
    @Override
    @Transactional
    public void setFrameworkMstrLogin(FrameworkMstrLogin frameworkMstrLogin) {
        sessionFactory.getCurrentSession().saveOrUpdate(frameworkMstrLogin);
    }
}
