/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsLogDocumentDownload;
import com.engage.components.core.entity.bean.EsLogDocumentUpload;
import com.engage.components.core.entity.bean.EsPddDocumentDetail;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Shrikant Katakdhond
 */

@Repository
public class PddDocumentDaoImpl implements PddDocumentDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
    
    @Override
    @Transactional
    public EsPddDocumentDetail getEsPddDocumentDetail(String applicationId){
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsPddDocumentDetail.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsPddDocumentDetail> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }
    
    @Override
    @Transactional
    public EsApplicantsDetailBean getApplicantsDetailByApplicantionID(String applicationId){
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsApplicantsDetailBean.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsApplicantsDetailBean> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }
    
    @Override
    @Transactional
    public void setEsPddDocumentDetail(EsPddDocumentDetail documentDetail) {
        sessionFactory.getCurrentSession().saveOrUpdate(documentDetail);
    }
    
    @Override
    @Transactional
    public void setEsLogDocumentUpload(EsLogDocumentUpload documentUpload) {
        sessionFactory.getCurrentSession().saveOrUpdate(documentUpload);
    }
    
    @Override
    @Transactional
    public void setEsLogDocumentDownload(EsLogDocumentDownload documentDownload){
        sessionFactory.getCurrentSession().saveOrUpdate(documentDownload);
    }

    @Override
    @Transactional
    public EsInvoiceDocBean getEsInvoiceDocDetail(String applicationId) {
         Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsInvoiceDocBean.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsInvoiceDocBean> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public void setEsInvoiceDocDetail(EsInvoiceDocBean documentDetail) {
        sessionFactory.getCurrentSession().saveOrUpdate(documentDetail);
    }

    @Override
    @Transactional
    public EsApplicantsDetailBean setEsApplicantsDetailBean(EsApplicantsDetailBean esApplicantsDetailBean) {
        sessionFactory.getCurrentSession().saveOrUpdate(esApplicantsDetailBean);
        return esApplicantsDetailBean;
    }
}
