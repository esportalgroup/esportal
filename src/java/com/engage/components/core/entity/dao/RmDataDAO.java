/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.RmDataApplicantDetail;
import com.engage.components.core.entity.bean.RmDataApplication;
import com.engage.components.core.entity.bean.RmLogUid;
import java.util.List;

/**
 *
 * @author dattatrayt
 */
public interface RmDataDAO {

    public RmDataApplication getRmDataApplcation(Long applicationId);

    public RmDataApplication setRmDataApplcation(RmDataApplication rmDataApplcation);

    public List<RmDataApplicantDetail> getRmDataApplicantDetail(Long applicationId);

    public RmDataApplicantDetail getRmDataApplicantDetailByApplicantsID(Long applicationId, Long applicantId);

    public RmDataApplicantDetail setRmDataApplicantDetail(RmDataApplicantDetail rmDataApplicantDetail);

    public List<RmDataApplicantDetail> getRmDataApplicantDetailByApplicationID(Long applicationId);
    
   public RmLogUid getRmLogUid(Long applicationId, Long applicantId, String typeOfReq);
   
   public RmLogUid setRmLogUid(RmLogUid rmLogUid);
    public List<RmDataApplicantDetail> getRMDetais(Long applicationId);
}
