/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsMstrAccount;
import com.engage.components.core.entity.bean.EsMstrApfProject;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author VEN00288
 */
@Repository
public class BuilderDetailDaoImpl implements  BuilderDetailDao{

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
    
    @Override
    @Transactional
    public EsMstrAccount getBuilderAccountsDetails(String builderC) {
        
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsMstrAccount.class);
        cr.add(Restrictions.eq("builderC", builderC));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsMstrAccount> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            System.out.println(lst.get(0));
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public EsMstrApfProject getEsMstrApfProjectInfo(String builderC) {
           
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(EsMstrApfProject.class);
        cr.add(Restrictions.eq("builderId", builderC));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<EsMstrApfProject> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
             System.out.println(lst.get(0));
            return lst.get(0);
        }
    }
    
}
