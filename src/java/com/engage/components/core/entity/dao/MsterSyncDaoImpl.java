/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.EsMpngBuilderSchemeMappingC;
import com.engage.components.core.entity.bean.EsMstrAccount;
import com.engage.components.core.entity.bean.EsMstrApfDisbursementSchedule;
import com.engage.components.core.entity.bean.EsMstrApfFlatConfiguration;
import com.engage.components.core.entity.bean.EsMstrApfFloorRiseChargeC;
import com.engage.components.core.entity.bean.EsMstrApfProject;
import com.engage.components.core.entity.bean.EsMstrApfPromotionC;
import com.engage.components.core.entity.bean.EsMstrApfTowerConfiguration;
import com.engage.components.core.entity.bean.EsMstrApfTowerPlcC;
import com.engage.components.core.entity.bean.EsMstrDealerBean;
import com.engage.components.core.entity.bean.EsMstrSchemeC;
import com.engage.components.core.entity.bean.EsTokenConstants;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author VEN00288
 */
@Repository
public class MsterSyncDaoImpl implements MsterSyncDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<EsTokenConstants> getEsTokenConstants(String type) {

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EsTokenConstants.class);
        criteria.add(Restrictions.eq("type", type));
        List<EsTokenConstants> EsTokenConstantsList = criteria.list();
        if (EsTokenConstantsList.isEmpty()) {
            return null;
        } else {
            return EsTokenConstantsList;
        }

    }

    @Override
    @Transactional
    public String syncDataToLocal(JSONArray array) {

        System.out.println("Inside Dao syncDataToLocal" + array.length());
        int batchSize = 50;
        EsMstrDealerBean dealerData = null;

        try {

            List<EsMstrDealerBean> esDealerDataLst = sessionFactory.getCurrentSession().createCriteria(EsMstrDealerBean.class).list();
            System.out.println("esDealerDataLst" + esDealerDataLst.size());

            if (!esDealerDataLst.isEmpty()) {
                System.out.println("Inside If ---");
                for (int i = 0; i < esDealerDataLst.size(); i++) {
                    dealerData = esDealerDataLst.get(i);
                    dealerData = setDataTOBean(dealerData, array);
                    sessionFactory.getCurrentSession().save(dealerData);

                    if (i % batchSize == 0 && i > 0) {

                        System.out.println("Inside batchSize for >>>>>>>>>");
                        sessionFactory.getCurrentSession().flush();
                        sessionFactory.getCurrentSession().clear();
                    }
                }

            } else {
                System.out.println("Inside helper else");
                for (int i = 0; i < array.length(); i++) {
                    dealerData = new EsMstrDealerBean();
                    dealerData = setDataTOBean(dealerData, array);
                    sessionFactory.getCurrentSession().save(dealerData);

                    if (i % batchSize == 0 && i > 0) {

                        System.out.println("Inside batchSize for >>>>>>>>>");
                        sessionFactory.getCurrentSession().flush();
                        sessionFactory.getCurrentSession().clear();
                    }
                }

            }

            return "Success";

        } catch (Exception e) {

            e.printStackTrace();
            return "fails";
        }

    }

    private String checkNull(Object data) {
        if (data == null) {
            return null;
        } else {
            return data.toString();
        }
    }

    private EsMstrDealerBean setDataTOBean(EsMstrDealerBean dealerData, JSONArray array) {
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject dataObj = (JSONObject) array.get(i);
                dealerData.setDealerId(dataObj.getString("Id"));
                dealerData.setName(dataObj.getString("Name"));
                dealerData.setEmployerCategory(checkNull(dataObj.get("Employer_Category__c")));
                dealerData.setEmployerId(checkNull(dataObj.get("Employer_Id__c")));
                dealerData.setEmployerName(checkNull(dataObj.get("Employer_Name__c")));
                dealerData.setEmployerRanking(checkNull(dataObj.get("Employer_Ranking__c")));
                dealerData.setEmployerRating(checkNull(dataObj.get("Employer_Rating__c")));
                dealerData.setRelationship(checkNull(dataObj.get("Relationship__c")));
                dealerData.setOccupation(checkNull(dataObj.get("Occupation__c")));
                dealerData.setMaritalStatus(checkNull(dataObj.get("Marital_Status__c")));
                dealerData.setPreferedLanguage(checkNull(dataObj.get("Prefered_Language__c")));
                dealerData.setIndustry(checkNull(dataObj.get("Industry")));
                dealerData.setAnnualRevenue(checkNull(dataObj.get("AnnualRevenue")));
                dealerData.setNumberOfEmployees(checkNull(dataObj.get("NumberOfEmployees")));
                dealerData.setBillingState(checkNull(dataObj.get("BillingState")));
                dealerData.setShippingState(checkNull(dataObj.get("ShippingState")));
                dealerData.setCreateDate(DateUtility.getCurrentDate());
                dealerData.setCreateBy("SYSTEM");
                dealerData.setModDate(DateUtility.getCurrentDate());
                dealerData.setModBy("SYSTEM");
                dealerData.setIsActive('Y');
                dealerData.setValidTill(null);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dealerData;
    }

    @Override
    @Transactional
    public String setFrameworkMstrLogin(FrameworkMstrLogin loginMstr) {
        try {
            sessionFactory.getCurrentSession().save(loginMstr);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESDealerMstr(EsMstrDealerBean dealerData) {
        try {
            sessionFactory.getCurrentSession().save(dealerData);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESAccountMstr(EsMstrAccount mstrAccount) {
        try {
            sessionFactory.getCurrentSession().save(mstrAccount);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESApfMstr(EsMstrApfProject apfProject) {
        try {
            sessionFactory.getCurrentSession().save(apfProject);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESTowerConfigMstr(EsMstrApfTowerConfiguration twrConfig) {
        try {
            sessionFactory.getCurrentSession().save(twrConfig);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESFlatConfigMstr(EsMstrApfFlatConfiguration flatConfig) {
        try {
            sessionFactory.getCurrentSession().save(flatConfig);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESFloorRiseChargeC(EsMstrApfFloorRiseChargeC floorRiseCharge) {
        try {
            sessionFactory.getCurrentSession().save(floorRiseCharge);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESapfTowerPlc(EsMstrApfTowerPlcC aptTwrPlc) {
        try {
            sessionFactory.getCurrentSession().save(aptTwrPlc);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setESPromotions(EsMstrApfPromotionC promotion) {
        try {
            sessionFactory.getCurrentSession().save(promotion);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setEsMstrApfDisbursementSchedule(EsMstrApfDisbursementSchedule disburseSch) {
        try {
            sessionFactory.getCurrentSession().save(disburseSch);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setEsSchemeMstr(EsMstrSchemeC scheme) {
        try {
            sessionFactory.getCurrentSession().save(scheme);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String setEsSMpngBulScheme(EsMpngBuilderSchemeMappingC schemeMpng) {
        try {
            sessionFactory.getCurrentSession().save(schemeMpng);
            return "Success";
        } catch (Exception e) {
            return "fails";
        }
    }

}
