/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsDealerData;
import com.engage.components.core.entity.bean.EsLoanDetailBean;

/**
 *
 * @author VEN00288
 */
public abstract interface DealerDataPushDAO {

    public void saveDelarDataLog(EsDealerData esDealerData);


    public String saveAsset(EsAssetDetailBean assetDetails);

    public String saveLoanData(EsLoanDetailBean loanDetails);

    public String saveApplicantsData(EsApplicantsDetailBean applicantsData);

    public EsDealerData getReqLog(String applicationId);
    
}
