/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsLoanDetailBean;

/**
 *
 * @author Shrikant Katakdhond
 */

public interface LoanAssetDao {
    
    public EsApplicantsDetailBean getApplicantsDetailsByAppId(String applicationId);
    
    public EsAssetDetailBean getAssetDetailByAppId(String applicationId);    
    
    public EsLoanDetailBean getLoanDetailByAppId(String applicationId);

    public String saveApplicantDetails(EsApplicantsDetailBean applicantDetails);

    public String saveLoanDetails(EsLoanDetailBean loanDetails);

    public String saveAssetDetails(EsAssetDetailBean assetDetails);

    public EsInvoiceDocBean getInvoiceDetailsByAppId(String applicationId);

    public String saveInvoiceDocDetails(EsInvoiceDocBean invoiceDoc);
    
}
