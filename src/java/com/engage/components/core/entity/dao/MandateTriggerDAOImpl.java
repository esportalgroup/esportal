/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

/**
 *
 * @author DattatrayT
 */
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.MandateTriggerDataApplicantsDetails;
import com.engage.components.core.entity.bean.MandateTriggerDataApplication;
import com.engage.components.core.entity.bean.MandateTriggerDataDocument;
import com.engage.components.core.entity.bean.MandateTriggerRequestLog;
import com.engage.components.core.entity.bean.MandateTriggerTokenUser;
import com.engage.components.core.entity.bean.MandateTriggerTokenRequestLog;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class MandateTriggerDAOImpl implements MandateTriggerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
    
    @Override
    @Transactional
    public MandateTriggerTokenUser getMandateTriggerTokenUser(String userId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerTokenUser.class);
        cr.add(Restrictions.eq("userId", userId));
//        cr.add(Restrictions.eq("isActive", FrameworkAppConstants.CONSTANT_YES));
        List<MandateTriggerTokenUser> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public MandateTriggerTokenRequestLog setMandateTriggerTokenRequestLog(MandateTriggerTokenRequestLog log) {
        sessionFactory.getCurrentSession().saveOrUpdate(log);
        return log;
    }

    @Override
    @Transactional
    public MandateTriggerTokenUser setMandateTriggerTokenUser(MandateTriggerTokenUser mandateTriggerTokenUser) {
        sessionFactory.getCurrentSession().saveOrUpdate(mandateTriggerTokenUser);
        return mandateTriggerTokenUser;
    }

    @Override
    @Transactional
    public MandateTriggerRequestLog setMandateTriggerRequestLog(MandateTriggerRequestLog mandateTriggerRequestLog) {
        sessionFactory.getCurrentSession().saveOrUpdate(mandateTriggerRequestLog);
        return mandateTriggerRequestLog;
    }

    @Override
    @Transactional
    public MandateTriggerDataApplication getMandateTriggerDataApplication(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerDataApplication.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        cr.add(Restrictions.eq("isLatest", FrameworkAppConstants.CONSTANT_YES));
        cr.addOrder(Order.desc("modDate"));
        List<MandateTriggerDataApplication> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public MandateTriggerDataApplication setMandateTriggerDataApplication(MandateTriggerDataApplication mandateTriggerDataApplication) {
        sessionFactory.getCurrentSession().saveOrUpdate(mandateTriggerDataApplication);
        return mandateTriggerDataApplication;
    }

    @Override
    @Transactional
    public MandateTriggerDataDocument setMandateTriggerDataDocument(MandateTriggerDataDocument mandateTriggerDataDocument) {
        sessionFactory.getCurrentSession().saveOrUpdate(mandateTriggerDataDocument);
        return mandateTriggerDataDocument;
    }

    @Override
    @Transactional
    public List<MandateTriggerDataDocument> getMandateTriggerDataDocument(Long refid) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerDataDocument.class)
                .createAlias("mandateTriggerDataApplication", "mandateTriggerDataApplication");
        cr.add(Restrictions.eq("mandateTriggerDataApplication.refid", refid));
        List<MandateTriggerDataDocument> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public MandateTriggerDataApplication getMandateTriggerDataApplicationByRefid(Long refid) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerDataApplication.class);
        cr.add(Restrictions.eq("refid", refid));
        List<MandateTriggerDataApplication> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public List<MandateTriggerDataApplicantsDetails> getMandateTriggerDataApplicantsDetails(Long refid) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerDataApplicantsDetails.class)
                .createAlias("mandateTriggerDataApplication", "mandateTriggerDataApplication");
        cr.add(Restrictions.eq("mandateTriggerDataApplication.refid", refid));
        List<MandateTriggerDataApplicantsDetails> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }

    @Override
    @Transactional
    public MandateTriggerDataApplicantsDetails setMandateTriggerDataApplicantsDetails(MandateTriggerDataApplicantsDetails mandateTriggerDataApplicantsDetails) {
        sessionFactory.getCurrentSession().saveOrUpdate(mandateTriggerDataApplicantsDetails);
        return mandateTriggerDataApplicantsDetails;
    }

    @Override
    @Transactional
    public MandateTriggerRequestLog getMandateTriggerRequestLog(Long id) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerRequestLog.class);
        cr.add(Restrictions.eq("logId", id));
        cr.addOrder(Order.desc("modDate"));
        List<MandateTriggerRequestLog> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public MandateTriggerDataDocument getMandateTriggerDataDocumentById(Long id, Long refid) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerDataDocument.class)
                .createAlias("mandateTriggerDataApplication", "mandateTriggerDataApplication");
        cr.add(Restrictions.eq("mandateTriggerDataApplication.refid", refid));
        cr.add(Restrictions.eq("documentId", id));
        List<MandateTriggerDataDocument> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public List<MandateTriggerDataApplication> getMandateTriggerDataApplicationLst(String applicationId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(MandateTriggerDataApplication.class);
        cr.add(Restrictions.eq("applicationId", applicationId));
        cr.add(Restrictions.eq("isLatest", FrameworkAppConstants.CONSTANT_YES));
        List<MandateTriggerDataApplication> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst;
        }
    }
    
}
