package com.engage.components.core.entity.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.*;

@Repository
public class SingleSingOnDAOImpl implements SingleSignOnDAO {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public FrameworkMstrSingleSignonTbl getCurrentSession(String userId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(FrameworkMstrSingleSignonTbl.class);
        cr.add(Restrictions.eq("userId", userId));
        List<FrameworkMstrSingleSignonTbl> lst = cr.list();
        if (lst.isEmpty()) {
            return null;
        } else {
            return lst.get(0);
        }
    }

    @Override
    @Transactional
    public void saveCurrentSession(FrameworkMstrSingleSignonTbl fmsst) {
        sessionFactory.getCurrentSession().saveOrUpdate(fmsst);
//       SangamLogSingleSignonTbl sangamLogSingleSignonTbl= new SangamLogSingleSignonTbl();
//       sangamLogSingleSignonTbl.setCreTime(fmsst.getCreTime());
//       sangamLogSingleSignonTbl.setHostName(fmsst.getHostName());
//       sangamLogSingleSignonTbl.setIpAddress(fmsst.getIpAddress());
//       sangamLogSingleSignonTbl.setSessionId(fmsst.getSessionId());
//       sangamLogSingleSignonTbl.setUserAgent(fmsst.getUserAgent());
//       sangamLogSingleSignonTbl.setUserId(fmsst.getUserId());
//       sessionFactory.getCurrentSession().save(sangamLogSingleSignonTbl);
    }
}