/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.RcuTriggerApplicant;
import com.engage.components.core.entity.bean.RcuTriggerConstants;
import com.engage.components.core.entity.bean.RcuTriggerDocument;
import com.engage.components.core.entity.bean.RcuTriggerLog;
import com.engage.components.core.entity.bean.RcuTriggerLosLog;
import com.engage.components.core.entity.bean.RcuTriggerStatus;
import com.engage.components.core.entity.bean.RcuTriggerToken;
import java.util.List;

/**
 *
 * @author SurajK
 */
public interface RCUTriggerDAO {
    
    public RcuTriggerLog setSangamRcuTriggerLog(RcuTriggerLog sangamRcuTriggerLog);
    
    public RcuTriggerLog getSangamRcuTriggerLogById(Long Id);
    
    public RcuTriggerStatus getSangamRcuTriggerStatus(String applicationId);
    
    public void setSangamRcuTriggerStatus(RcuTriggerStatus sangamRcuTriggerStatus);
    
    public RcuTriggerApplicant getSangamRcuTriggerApplicant(String applicationId,String applicantId);
    
    public List<RcuTriggerApplicant> getSangamRcuTriggerApplicantByApplication(String applicationId);
    
    public void setSangamRcuTriggerApplicant(RcuTriggerApplicant sangamRcuTriggerApplicant);
    
    public RcuTriggerDocument getSangamRcuTriggerDocument(String applicationId);
    
    public List<RcuTriggerDocument> getSangamRcuTriggerDocumentByApplicant(String applicationId,String applicantId);
    
    public void setSangamRcuTriggerDocument(RcuTriggerDocument sangamRcuTriggerDocument);
    
    public RcuTriggerDocument getDocumentById(Long docId);
    
    public List<RcuTriggerConstants> getRcuConstants(String type);
    
    public void setToken(RcuTriggerToken sangamRcuTriggerToken);
    
    public RcuTriggerToken getToken();
    
    public void setLosRequest(RcuTriggerLosLog sangamRcuTriggerLosLog);
    
}
