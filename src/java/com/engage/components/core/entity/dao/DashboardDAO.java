package com.engage.components.core.entity.dao;

import java.util.List;
import com.engage.components.core.entity.bean.*;
import com.engage.components.esportal.vo.BuilderDashBoardVO;
import com.engage.components.esportal.vo.DealerDashbordVO;
import com.engage.components.esportal.vo.DealerStatusDashboardVO;
import com.engage.components.esportal.vo.McmDashBoardVO;
import com.engage.components.esportal.vo.OpsDashBoardVO;
import com.engage.components.esportal.vo.RCUDashboardVO;
import com.engage.components.esportal.vo.RMDashboardVO;

/**
 *
 * @author Shrikant Katakdhond
 */

public abstract interface DashboardDAO {
    public List<SangamMstrSelectCriteria> getSelectCriterias(String forDashboard);
    public List<DealerStatusDashboardVO> getDealerDashbord(String userId,Long fromRowNum,Long toRowNum,Long profileID);
    public List<OpsDashBoardVO> getOpsDashbord(String supplierId, Long fromRowNum, Long toRowNum, Character isPending);
    public List<OpsDashBoardVO> getOpsDashbordSearch(String supplierId, Long fromRowNum, Long toRowNum, Character isPending, String searchParameter, String searchParameterValue);
    public List<OpsDashBoardVO> getOpsDashbordByApplicationId(String supplierId, Long fromRowNum, Long toRowNum, Character isPending, String ApplicationId);
    public List<McmDashBoardVO> getMcmDashboard(String supplierId, Long fromRowNum, Long toRowNum,String type,String localityId,String villageName,String mcCode);
    
    public List<McmDashBoardVO> getMcmDashbordSearch(Long fromRowNum, Long toRowNum, String mcCode, String searchParameter, String searchParameterValue);
    
    public List<BuilderDashBoardVO> getBuilderDashbord(String userId, Long fromRowNum, Long toRowNum, Long profileID);
    public List<RCUDashboardVO> getRCUTriggerApplications(String userId,Long fromRowNum,Long toRowNum);

    public List<RMDashboardVO> getRMDashboard(String userId, Long fromRowNum, Long toRowNum);
    public EsApplicantsDetailBean getApplicantsDetailsByAppId(String applicationId);
}
