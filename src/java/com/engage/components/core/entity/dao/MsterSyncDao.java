/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.EsMpngBuilderSchemeMappingC;
import com.engage.components.core.entity.bean.EsMstrAccount;
import com.engage.components.core.entity.bean.EsMstrApfDisbursementSchedule;
import com.engage.components.core.entity.bean.EsMstrApfFlatConfiguration;
import com.engage.components.core.entity.bean.EsMstrApfFloorRiseChargeC;
import com.engage.components.core.entity.bean.EsMstrApfProject;
import com.engage.components.core.entity.bean.EsMstrApfPromotionC;
import com.engage.components.core.entity.bean.EsMstrApfTowerConfiguration;
import com.engage.components.core.entity.bean.EsMstrApfTowerPlcC;
import com.engage.components.core.entity.bean.EsMstrDealerBean;
import com.engage.components.core.entity.bean.EsMstrSchemeC;
import com.engage.components.core.entity.bean.EsTokenConstants;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author VEN00288
 */
public interface MsterSyncDao {

    public List<EsTokenConstants> getEsTokenConstants(String type);

    public String syncDataToLocal(JSONArray array);

    public String setFrameworkMstrLogin(FrameworkMstrLogin loginMstr);

    public String setESDealerMstr(EsMstrDealerBean dealerData);

    public String setESAccountMstr(EsMstrAccount mstrAccount);

    public String setESApfMstr(EsMstrApfProject apfProject);

    public String setESTowerConfigMstr(EsMstrApfTowerConfiguration twrConfig);

    public String setESFlatConfigMstr(EsMstrApfFlatConfiguration flatConfig);

    public String setESFloorRiseChargeC(EsMstrApfFloorRiseChargeC floorRiseCharge);

    public String setESapfTowerPlc(EsMstrApfTowerPlcC aptTwrPlc);

    public String setESPromotions(EsMstrApfPromotionC promotion);

    public String setEsMstrApfDisbursementSchedule(EsMstrApfDisbursementSchedule disburseSch);

    public String setEsSchemeMstr(EsMstrSchemeC scheme);

    public String setEsSMpngBulScheme(EsMpngBuilderSchemeMappingC schemeMpng);
    
}
