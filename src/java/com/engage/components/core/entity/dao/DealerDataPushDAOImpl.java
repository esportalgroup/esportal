/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsDealerData;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author VEN00288
 */
@Repository
public class DealerDataPushDAOImpl implements DealerDataPushDAO{

    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Override
    @Transactional
    public void saveDelarDataLog(EsDealerData esDealerData) {
      sessionFactory.getCurrentSession().save(esDealerData);
    }

   

    @Override
    @Transactional
    public String saveAsset(EsAssetDetailBean assetDetails) {
        
        try
        {
            sessionFactory.getCurrentSession().save(assetDetails);  
             return "Success";
        }catch(Exception e)
        {
            return "fails";
        }
     
    }

    @Override
    @Transactional
    public String saveLoanData(EsLoanDetailBean loanDetails) {
         try
        {
            sessionFactory.getCurrentSession().save(loanDetails);  
             return "Success";
        }catch(Exception e)
        {
            return "fails";
        }
    }

    @Override
    @Transactional
    public String saveApplicantsData(EsApplicantsDetailBean applicantsData) {
         try
        {
             sessionFactory.getCurrentSession().save(applicantsData);  
             return "Success";
        }catch(Exception e)
        {
            return "fails";
        }
    }

    @Override
    @Transactional
    public EsDealerData getReqLog(String applicationId) {
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(EsDealerData.class);
        criteria.add(Restrictions.eq("applicationId",applicationId));
        List<EsDealerData> lst = criteria.list();
        System.err.println("lst"+lst.size());
        if(lst.isEmpty()){
            return null;
        }else{
            return lst.get(0);
        }
        
        
    }

     
}
