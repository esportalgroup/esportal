/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.FrameworkMstrLogin;

/**
 *
 * @author Shrikant Katakdhond
 */
public interface PasswordResetDAO {
    
    public FrameworkMstrLogin getFrameworkMstrLoginByUserName(String userName);
    
    public FrameworkMstrLogin getFrameworkMstrLoginByUserId(Long userId);
    
    public void setFrameworkMstrLogin(FrameworkMstrLogin frameworkMstrLogin);
    
}
