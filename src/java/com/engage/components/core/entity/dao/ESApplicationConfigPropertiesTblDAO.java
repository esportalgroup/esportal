/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.ESApplicationConfigPropertiesTbl;
import java.util.List;

/**
 *
 * @author cmssjava
 */
public interface ESApplicationConfigPropertiesTblDAO {
    List<ESApplicationConfigPropertiesTbl> getProperty();
}
