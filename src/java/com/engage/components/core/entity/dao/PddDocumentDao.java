/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsLogDocumentDownload;
import com.engage.components.core.entity.bean.EsLogDocumentUpload;
import com.engage.components.core.entity.bean.EsPddDocumentDetail;

/**
 *
 * @author Shrikant Katakdhond
 */
public interface PddDocumentDao {
    public EsPddDocumentDetail getEsPddDocumentDetail(String applicationId);
    
    public EsApplicantsDetailBean getApplicantsDetailByApplicantionID(String applicationId);
    
    public void setEsPddDocumentDetail(EsPddDocumentDetail documentDetail);
    
    public void setEsLogDocumentUpload(EsLogDocumentUpload documentUpload);
    
    public void setEsLogDocumentDownload(EsLogDocumentDownload documentDownload);

    public EsInvoiceDocBean getEsInvoiceDocDetail(String applicationId);

    public void setEsInvoiceDocDetail(EsInvoiceDocBean documentDetail);

    public EsApplicantsDetailBean setEsApplicantsDetailBean(EsApplicantsDetailBean esApplicantsDetailBean);
}
