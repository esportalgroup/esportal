/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.dao;

import com.engage.components.core.entity.bean.MCMLogAccountActivate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Shrikant Katakdhond
 */
@Repository
public class MCMAccountValidateDaoImpl implements MCMAccountValidateDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    @Transactional
    public void setMCMLogAccountActivate(MCMLogAccountActivate logActivate){
        sessionFactory.getCurrentSession().saveOrUpdate(logActivate);
    }

}
