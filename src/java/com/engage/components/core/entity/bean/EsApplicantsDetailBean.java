/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author VEN00288
 */
@Entity
@Table(name="es_applicants_details"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class EsApplicantsDetailBean {
    
    private Long applicantDetailsId;
    private String applicationId;
    private String applicantId;
    private String applicantName;
     private String CoapplicantName;
    private String supplierName;
    private String supplierId;
    private String dsaName;
    private String dsaId;
    private String flsName;
    private String flsId;
    private Date modDate;
    private Date creDate;
    private String status;
    private String id;
    private String applicationDate;
    private String pddStatus;
    
    

    
    public EsApplicantsDetailBean() {
    }

    public EsApplicantsDetailBean(Long applicantDetailsId, String applicationId, String applicantId, String applicantName, String supplierName, String supplierId, String dsaName, String dsaId, String flsName, String flsId, Date modDate, Date creDate, String pddStatus) {
        this.applicantDetailsId = applicantDetailsId;
        this.applicationId = applicationId;
        this.applicantId = applicantId;
        this.applicantName = applicantName;
        this.supplierName = supplierName;
        this.supplierId = supplierId;
        this.dsaName = dsaName;
        this.dsaId = dsaId;
        this.flsName = flsName;
        this.flsId = flsId;
        this.modDate = modDate;
        this.creDate = creDate;
        this.pddStatus = pddStatus;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "applicants_detail_id", unique = true, nullable = false)
    public Long getApplicantDetailsId() {
        return applicantDetailsId;
    }

    public void setApplicantDetailsId(Long applicantDetailsId) {
        this.applicantDetailsId = applicantDetailsId;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

   @Column(name = "applicant_Id")
   public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    @Column(name = "applicant_name")
   public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    @Column(name = "supplier_name")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    @Column(name = "supplier_id")
    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    @Column(name = "dsa_name")
    public String getDsaName() {
        return dsaName;
    }

    public void setDsaName(String dsaName) {
        this.dsaName = dsaName;
    }

    @Column(name = "dsa_id")
    public String getDsaId() {
        return dsaId;
    }

    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }
  
    
    

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Column(name = "fls_name")
    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    @Column(name = "fls_id")
    public String getFlsId() {
        return flsId;
    }

    public void setFlsId(String flsId) {
        this.flsId = flsId;
    }

     @Column(name = "co_applicant_name")
    public String getCoapplicantName() {
        return CoapplicantName;
    }

    public void setCoapplicantName(String CoapplicantName) {
        this.CoapplicantName = CoapplicantName;
    }

     @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

      @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
      @Column(name = "application_date")
    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }  

    @Column(name = "pdd_status")
    public String getPddStatus() {
        return pddStatus;
    }

    public void setPddStatus(String pddStatus) {
        this.pddStatus = pddStatus;
    }
     
}
