/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ven00288
 */
@Entity
@Table(name = "mcm_repayments_summeries_details", schema = "dbo", catalog = "ES_Portal"
)
public class RepaymentSummeriesBean {
    
    private long rePaymentSummeriesId;
    private String installmentDate;
    private String installmentAmount;
    private String noOfInstallment;
    private String openingBalance;
    private String interestAmount;
    private String principleAmount;
    private String closingAmount;
    private String applicationId;
    private String losId;
    private Date createDate;
    private Date modDate;
    private String creBy;
    private String modBy;

    public RepaymentSummeriesBean() {
    }

    public RepaymentSummeriesBean(String installmentDate, String installmentAmount, String noOfInstallment, String openingBalance, String interestAmount, String principleAmount, String closingAmount, String applicationId, String losId, Date createDate, Date modDate, String creBy, String modBy) {
        this.installmentDate = installmentDate;
        this.installmentAmount = installmentAmount;
        this.noOfInstallment = noOfInstallment;
        this.openingBalance = openingBalance;
        this.interestAmount = interestAmount;
        this.principleAmount = principleAmount;
        this.closingAmount = closingAmount;
        this.applicationId = applicationId;
        this.losId = losId;
        this.createDate = createDate;
        this.modDate = modDate;
        this.creBy = creBy;
        this.modBy = modBy;
    }

    
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "repayment_summiers_id", unique = true, nullable = false)
    public long getRePaymentSummeriesId() {
        return rePaymentSummeriesId;
    }

    public void setRePaymentSummeriesId(long rePaymentSummeriesId) {
        this.rePaymentSummeriesId = rePaymentSummeriesId;
    }
    
    
    
    @Column(name = "installment_date")
    public String getInstallmentDate() {
        return installmentDate;
    }

    public void setInstallmentDate(String installmentDate) {
        this.installmentDate = installmentDate;
    }

    @Column(name = "installment_amount")
    public String getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(String installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    @Column(name = "no_of_installment")
    public String getNoOfInstallment() {
        return noOfInstallment;
    }

    public void setNoOfInstallment(String noOfInstallment) {
        this.noOfInstallment = noOfInstallment;
    }

    @Column(name = "opening_balance")
    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }

    @Column(name = "interest_amount")
    public String getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(String interestAmount) {
        this.interestAmount = interestAmount;
    }

    @Column(name = "principle_amount")
    public String getPrincipleAmount() {
        return principleAmount;
    }

    public void setPrincipleAmount(String principleAmount) {
        this.principleAmount = principleAmount;
    }

    @Column(name = "closing_amount")
    public String getClosingAmount() {
        return closingAmount;
    }

    public void setClosingAmount(String closingAmount) {
        this.closingAmount = closingAmount;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "los_id")
    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }
    
      
      
    
    
    
    
    
}
