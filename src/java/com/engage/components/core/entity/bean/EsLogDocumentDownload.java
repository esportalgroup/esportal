/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Shrikant Katakdhond
 */

@Entity
@Table(name = "es_log_document_download", schema = "dbo", catalog = "ES_Portal")
public class EsLogDocumentDownload implements java.io.Serializable{
    
    private long logId;
    private String applicationId;
    private String request;
    private String response;
    private Date requestTime;
    private Date responseTime;

    public EsLogDocumentDownload() {
    }

    public EsLogDocumentDownload(long logId, String applicationId, String request, String response, Date requestTime, Date responseTime) {
        this.logId = logId;
        this.applicationId = applicationId;
        this.request = request;
        this.response = response;
        this.requestTime = requestTime;
        this.responseTime = responseTime;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "LOG_ID", unique = true, nullable = false)
    public long getLogId() {
        return this.logId;
    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

    @Column(name = "APPLICATION_ID")
    public String getApplicationId() {
        return this.applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "REQUEST")
    public String getRequest() {
        return this.request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Column(name = "RESPONSE")
    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REQUEST_TIME", length = 23)
    public Date getRequestTime() {
        return this.requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESPONSE_TIME", length = 23)
    public Date getResponseTime() {
        return this.responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    
}
