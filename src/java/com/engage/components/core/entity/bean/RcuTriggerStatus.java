/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author SurajK
 */
@Entity
@Table(name="rcu_trigger_status"
    ,schema="dbo"
    ,catalog="ES_Portal"
    , uniqueConstraints = @UniqueConstraint(columnNames={"application_id"})
)
public class RcuTriggerStatus  implements java.io.Serializable {


     private long rcuId;
     private String applicationId;
     private String flsName;
     private String flsId;
     private String rcuVendorName;
     private String rcuVendorId;
     private String city;
     private String product;
     private String productCategory;
     private String rcuRemark;
     private String rcuStatus;
     private String rcuModBy;
     private Date rcuModDate;
     private String rcuOutcomeStatus;
     private Character isSubmited;
     private Date submitedDate;

    public RcuTriggerStatus() {
    }

	
    public RcuTriggerStatus(long rcuId) {
        this.rcuId = rcuId;
    }

    public RcuTriggerStatus(long rcuId, String applicationId, String flsName, String flsId, String rcuVendorName, String rcuVendorId, String city, String product, String productCategory, String rcuRemark, String rcuStatus, String rcuModBy, Date rcuModDate, String rcuOutcomeStatus, Character isSubmited, Date submitedDate) {
        this.rcuId = rcuId;
        this.applicationId = applicationId;
        this.flsName = flsName;
        this.flsId = flsId;
        this.rcuVendorName = rcuVendorName;
        this.rcuVendorId = rcuVendorId;
        this.city = city;
        this.product = product;
        this.productCategory = productCategory;
        this.rcuRemark = rcuRemark;
        this.rcuStatus = rcuStatus;
        this.rcuModBy = rcuModBy;
        this.rcuModDate = rcuModDate;
        this.rcuOutcomeStatus = rcuOutcomeStatus;
        this.isSubmited = isSubmited;
        this.submitedDate = submitedDate;
    }
    
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="rcu_id", unique=true, nullable=false)
    public long getRcuId() {
        return this.rcuId;
    }
    
    public void setRcuId(long rcuId) {
        this.rcuId = rcuId;
    }

    
    @Column(name="application_id", length=20)
    public String getApplicationId() {
        return this.applicationId;
    }
    
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    
    @Column(name="rcu_remark")
    public String getRcuRemark() {
        return this.rcuRemark;
    }
    
    public void setRcuRemark(String rcuRemark) {
        this.rcuRemark = rcuRemark;
    }

    
    @Column(name="rcu_status", length=100)
    public String getRcuStatus() {
        return this.rcuStatus;
    }
    
    public void setRcuStatus(String rcuStatus) {
        this.rcuStatus = rcuStatus;
    }

    @Column(name="rcu_mod_by", length=50)
    public String getRcuModBy() {
        return this.rcuModBy;
    }
    
    public void setRcuModBy(String rcuModBy) {
        this.rcuModBy = rcuModBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="rcu_mod_date", length=23)
    public Date getRcuModDate() {
        return this.rcuModDate;
    }
    
    public void setRcuModDate(Date rcuModDate) {
        this.rcuModDate = rcuModDate;
    }

    
    @Column(name="rcu_outcome_status", length=50)
    public String getRcuOutcomeStatus() {
        return this.rcuOutcomeStatus;
    }
    
    public void setRcuOutcomeStatus(String rcuOutcomeStatus) {
        this.rcuOutcomeStatus = rcuOutcomeStatus;
    }

    
    @Column(name="is_submited", length=1)
    public Character getIsSubmited() {
        return this.isSubmited;
    }
    
    public void setIsSubmited(Character isSubmited) {
        this.isSubmited = isSubmited;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="submited_date", length=23)
    public Date getSubmitedDate() {
        return this.submitedDate;
    }
    
    public void setSubmitedDate(Date submitedDate) {
        this.submitedDate = submitedDate;
    }

    @Column(name="fls_name", length=50)
    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    @Column(name="fls_id", length=50)
    public String getFlsId() {
        return flsId;
    }

    public void setFlsId(String flsId) {
        this.flsId = flsId;
    }

    @Column(name="rcu_vendor_name", length=50)
    public String getRcuVendorName() {
        return rcuVendorName;
    }

    public void setRcuVendorName(String rcuVendorName) {
        this.rcuVendorName = rcuVendorName;
    }

    @Column(name="rcu_vendor_id", length=50)
    public String getRcuVendorId() {
        return rcuVendorId;
    }

    public void setRcuVendorId(String rcuVendorId) {
        this.rcuVendorId = rcuVendorId;
    }

    @Column(name="city", length=100)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name="product", length=20)
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Column(name="product_category", length=20)
    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }
}


