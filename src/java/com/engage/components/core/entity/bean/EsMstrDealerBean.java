/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author VEN00288
 */
@Entity
@Table(name = "es_mstr_dealer", schema = "dbo", catalog = "ES_Portal"
)
public class EsMstrDealerBean implements Serializable {

    private Long esMstrDlrId;
    private String dealerId;
    private String name;
    private String employerCategory;
    private String employerId;
    private String employerRanking;
    private String employerRating;
    private String employerName;
    private String relationship;
    private String occupation;
    private String maritalStatus;
    private String preferedLanguage;
    private String industry;
    private String annualRevenue;
    private String numberOfEmployees;
    private String billingState;
    private String shippingState;
    private Date createDate;
    private String createBy;
    private Date modDate;
    private String modBy;
    private Character isActive;
    private Date validTill;

    public EsMstrDealerBean() {
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getEsMstrDlrId() {
        return esMstrDlrId;
    }

    public void setEsMstrDlrId(Long esMstrDlrId) {
        this.esMstrDlrId = esMstrDlrId;
    }

    @Column(name = "dealer_id ")
    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "employer_category__c")
    public String getEmployerCategory() {
        return employerCategory;
    }

    public void setEmployerCategory(String employerCategory) {
        this.employerCategory = employerCategory;
    }

    @Column(name = "employer_id__c")
    public String getEmployerId() {
        return employerId;
    }

    public void setEmployerId(String employerId) {
        this.employerId = employerId;
    }

    @Column(name = "employer_ranking__c")
    public String getEmployerRanking() {
        return employerRanking;
    }

    public void setEmployerRanking(String employerRanking) {
        this.employerRanking = employerRanking;
    }

    @Column(name = "employer_rating__c")
    public String getEmployerRating() {
        return employerRating;
    }

    public void setEmployerRating(String employerRating) {
        this.employerRating = employerRating;
    }

    @Column(name = "employer_name__c")
    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    @Column(name = "relationship__c")
    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    @Column(name = "occupation__c")
    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Column(name = "marital_status__c")
    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Column(name = "prefered_language__c ")
    public String getPreferedLanguage() {
        return preferedLanguage;
    }

    public void setPreferedLanguage(String preferedLanguage) {
        this.preferedLanguage = preferedLanguage;
    }

    @Column(name = "industry")
    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    @Column(name = "annual_revenue")
    public String getAnnualRevenue() {
        return annualRevenue;
    }

    public void setAnnualRevenue(String annualRevenue) {
        this.annualRevenue = annualRevenue;
    }

    @Column(name = "number_of_employees")
    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    @Column(name = "billing_state")
    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    @Column(name = "shipping_state")
    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "create_by")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date")
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Column(name = "is_active")
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "valid_till")
    public Date getValidTill() {
        return validTill;
    }

    public void setValidTill(Date validTill) {
        this.validTill = validTill;
    }

}
