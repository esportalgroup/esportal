/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author VEN00288
 */

@Entity
@Table(name = "es_mcm_applications_detail", schema = "dbo", catalog = "ES_Portal"
)
public class McmApplicationDetails 
{
    private String mcmApplicationId;
    private String applicationId;
    private String genesisLoanAmount;
    private String genesisTerm;
          private String maxEMIAmount;
          private String procFee;
          private String  cliAmount;
          private String netDisbursementAmt;
          private String appraisalflag;
          private String modeOfDisbursement;
          private String bankAccountNumber;
          private String ifscCode;
          private String losId;
          private String flsName;
          private String flsCode;
           private String adharNumber;
          private String mobileNumber;
          private Character isAccountValid;
           private Character isEsigned;
private String accountValidationStatus;
private String accountValidationMessage;
private String groupId;
private String status;
    public McmApplicationDetails() {
    }

    public McmApplicationDetails(String applicationId, String genesisLoanAmount, String genesisTerm, String maxEMIAmount, String procFee, String cliAmount, String netDisbursementAmt, String appraisalflag, String modeOfDisbursement, String bankAccountNumber, String ifscCode) {
        this.applicationId = applicationId;
        this.genesisLoanAmount = genesisLoanAmount;
        this.genesisTerm = genesisTerm;
        this.maxEMIAmount = maxEMIAmount;
        this.procFee = procFee;
        this.cliAmount = cliAmount;
        this.netDisbursementAmt = netDisbursementAmt;
        this.appraisalflag = appraisalflag;
        this.modeOfDisbursement = modeOfDisbursement;
        this.bankAccountNumber = bankAccountNumber;
        this.ifscCode = ifscCode;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "es_mcm_application_id", unique = true, nullable = false)
    public String getMcmApplicationId() {
        return mcmApplicationId;
    }

    public void setMcmApplicationId(String mcmApplicationId) {
        this.mcmApplicationId = mcmApplicationId;
    }
          
    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "genesis__Loan_Amount")
    public String getGenesisLoanAmount() {
        return genesisLoanAmount;
    }

    public void setGenesisLoanAmount(String genesisLoanAmount) {
        this.genesisLoanAmount = genesisLoanAmount;
    }

    @Column(name = "genesis__term")
    public String getGenesisTerm() {
        return genesisTerm;
    }

    public void setGenesisTerm(String genesisTerm) {
        this.genesisTerm = genesisTerm;
    }

    @Column(name = "max_emi_amount")
    public String getMaxEMIAmount() {
        return maxEMIAmount;
    }

    public void setMaxEMIAmount(String maxEMIAmount) {
        this.maxEMIAmount = maxEMIAmount;
    }

    @Column(name = "proc_fee")
    public String getProcFee() {
        return procFee;
    }

    public void setProcFee(String procFee) {
        this.procFee = procFee;
    }

    @Column(name = "cli_amount")
    public String getCliAmount() {
        return cliAmount;
    }

    public void setCliAmount(String cliAmount) {
        this.cliAmount = cliAmount;
    }

    @Column(name = "net_disbursement_amount")
    public String getNetDisbursementAmt() {
        return netDisbursementAmt;
    }

    public void setNetDisbursementAmt(String netDisbursementAmt) {
        this.netDisbursementAmt = netDisbursementAmt;
    }

    @Column(name = "appraisal_flag")
    public String getAppraisalflag() {
        return appraisalflag;
    }

    public void setAppraisalflag(String appraisalflag) {
        this.appraisalflag = appraisalflag;
    }

    @Column(name = "mod_of_disbursement")
    public String getModeOfDisbursement() {
        return modeOfDisbursement;
    }

    public void setModeOfDisbursement(String modeOfDisbursement) {
        this.modeOfDisbursement = modeOfDisbursement;
    }

    @Column(name = "bank_account_number")
    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Column(name = "ifsc_code")
    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    @Column(name = "account_validation_status")
    public String getAccountValidationStatus() {
        return accountValidationStatus;
    }

    
    public void setAccountValidationStatus(String accountValidationStatus) {
        this.accountValidationStatus = accountValidationStatus;
    }

    @Column(name = "groupId")
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Column(name = "losId")
    public String getLosId() {
        return losId;
    }

    
    public void setLosId(String losId) {
        this.losId = losId;
    }

     @Column(name = "fls_name")
    public String getFlsName() {
        return flsName;
    }

    public void setFlsName(String flsName) {
        this.flsName = flsName;
    }

    @Column(name = "fls_code")
    public String getFlsCode() {
        return flsCode;
    }

    public void setFlsCode(String flsCode) {
        this.flsCode = flsCode;
    }

    @Column(name = "mobile_number")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Column(name = "is_account_validated")
    public Character getIsAccountValid() {
        return isAccountValid;
    }

    public void setIsAccountValid(Character isAccountValid) {
        this.isAccountValid = isAccountValid;
    }

    
    @Column(name = "account_validated_message")
    public String getAccountValidationMessage() {
        return accountValidationMessage;
    }

    public void setAccountValidationMessage(String accountValidationMessage) {
        this.accountValidationMessage = accountValidationMessage;
    }

     @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

     @Column(name = "adhar_number")
    public String getAdharNumber() {
        return adharNumber;
    }

    public void setAdharNumber(String adharNumber) {
        this.adharNumber = adharNumber;
    }

     @Column(name = "is_esigned")
    public Character getIsEsigned() {
        return isEsigned;
    }

    public void setIsEsigned(Character isEsigned) {
        this.isEsigned = isEsigned;
    }
    
    
    
    
}
    
    
    
    
    
    

