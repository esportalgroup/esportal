/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

/**
 *
 * @author ven00253
 */
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "mandate_trigger_data_applicants_details", schema = "dbo", catalog = "ES_Portal"
)
public class MandateTriggerDataApplicantsDetails implements java.io.Serializable {
    Long applicant_id;
    private MandateTriggerDataApplication mandateTriggerDataApplication;
    String id;
    String name;
    String isFinancial;
    String isPrimary;
    private String createBy;
    private Date createDate;
    private String modBy;
    private Date modDate;

    public MandateTriggerDataApplicantsDetails() {
    }

    public MandateTriggerDataApplicantsDetails(Long applicant_id, MandateTriggerDataApplication mandateTriggerDataApplication, String id, String name, String isFinancial, String isPrimary, String createBy, Date createDate, String modBy, Date modDate) {
        this.applicant_id = applicant_id;
        this.mandateTriggerDataApplication = mandateTriggerDataApplication;
        this.id = id;
        this.name = name;
        this.isFinancial=isFinancial;
        this.isPrimary=isPrimary;
        this.createBy = createBy;
        this.createDate = createDate;
        this.modBy = modBy;
        this.modDate = modDate;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "applicant_id", unique = true, nullable = false)
    public Long getApplicant_id() {
        return applicant_id;
    }

    public void setApplicant_id(Long applicant_id) {
        this.applicant_id = applicant_id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "refid", nullable = false)
    public MandateTriggerDataApplication getMandateTriggerDataApplication() {
        return mandateTriggerDataApplication;
    }

    public void setMandateTriggerDataApplication(MandateTriggerDataApplication mandateTriggerDataApplication) {
        this.mandateTriggerDataApplication = mandateTriggerDataApplication;
    }

    @Column(name = "id", length = 100)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "is_financial", length = 5)
    public String getIsFinancial() {
        return isFinancial;
    }

    public void setIsFinancial(String isFinancial) {
        this.isFinancial = isFinancial;
    }

    @Column(name = "is_primary", length = 5)
    public String getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(String isPrimary) {
        this.isPrimary = isPrimary;
    }

    @Column(name = "create_by", length = 100)
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", length = 23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "mod_by", length = 100)
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
    
}
