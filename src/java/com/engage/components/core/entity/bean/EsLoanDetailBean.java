/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author VEN00288
 */
@Entity
@Table(name = "es_dealer_loan_details", schema = "dbo", catalog = "ES_Portal"
)
public class EsLoanDetailBean {

    private Long loanDetailId;
    private String applicationId;

    private String marginMoney;
    private String processingFee;
    private String ECSCharges;
    private String docCharges;
    private String stampDuty;
    private String cliPremium;
    private String loanAmount;
    private String disbuursemenrAmount;
    private String currentDate;
//    private String applicationDate;
    private String invoiceNumber;
    private String marginMoney_DocID;
    private String invoice_DocID;
    private Date modDate;
    private Date creDate;

    public EsLoanDetailBean() {
    }

    public EsLoanDetailBean(Long loanDetailId, String applicationId, String marginMoney, String processingFee, String ECSCharges, String docCharges, String stampDuty, String cliPremium, String loanAmount, String disbuursemenrAmount, String currentDate, Date modDate, Date creDate) {
        this.loanDetailId = loanDetailId;
        this.applicationId = applicationId;
        this.marginMoney = marginMoney;
        this.processingFee = processingFee;
        this.ECSCharges = ECSCharges;
        this.docCharges = docCharges;
        this.stampDuty = stampDuty;
        this.cliPremium = cliPremium;
        this.loanAmount = loanAmount;
        this.disbuursemenrAmount = disbuursemenrAmount;
        this.currentDate = currentDate;
        this.modDate = modDate;
        this.creDate = creDate;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "loan_detail_id", unique = true, nullable = false)
    public Long getLoanDetailId() {
        return loanDetailId;
    }

    public void setLoanDetailId(Long loanDetailId) {
        this.loanDetailId = loanDetailId;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "margin_money")
    public String getMarginMoney() {
        return marginMoney;
    }

    public void setMarginMoney(String marginMoney) {
        this.marginMoney = marginMoney;
    }

    @Column(name = "proccessing_fee")
    public String getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(String processingFee) {
        this.processingFee = processingFee;
    }

    @Column(name = "ecs_charges")
    public String getECSCharges() {
        return ECSCharges;
    }

    public void setECSCharges(String ECSCharges) {
        this.ECSCharges = ECSCharges;
    }

    @Column(name = "doc_charges")
    public String getDocCharges() {
        return docCharges;
    }

    public void setDocCharges(String docCharges) {
        this.docCharges = docCharges;
    }

    @Column(name = "stamp_duty")
    public String getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(String stampDuty) {
        this.stampDuty = stampDuty;
    }

    @Column(name = "loan_amount")
    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Column(name = "disbursement_amount")
    public String getDisbuursemenrAmount() {
        return disbuursemenrAmount;
    }

    public void setDisbuursemenrAmount(String disbuursemenrAmount) {
        this.disbuursemenrAmount = disbuursemenrAmount;
    }

    @Column(name = "curr_Date")
    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Column(name = "cli_premium")
    public String getCliPremium() {
        return cliPremium;
    }

    public void setCliPremium(String cliPremium) {
        this.cliPremium = cliPremium;
    }

//    @Column(name = "application_date")
//    public String getApplicationDate() {
//        return applicationDate;
//    }
//
//    public void setApplicationDate(String applicationDate) {
//        this.applicationDate = applicationDate;
//    }

     @Column(name = "invoice_number")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Column(name = "margin_money_docId")
    public String getMarginMoney_DocID() {
        return marginMoney_DocID;
    }

    public void setMarginMoney_DocID(String marginMoney_DocID) {
        this.marginMoney_DocID = marginMoney_DocID;
    }

    @Column(name = "invoice_DocId")
    public String getInvoice_DocID() {
        return invoice_DocID;
    }

    public void setInvoice_DocID(String invoice_DocID) {
        this.invoice_DocID = invoice_DocID;
    }

  
    
    

}
