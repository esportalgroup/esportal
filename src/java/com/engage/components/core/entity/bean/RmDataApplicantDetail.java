/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;
// Generated Nov 4, 2017 12:10:22 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * RmDataApplicantDetail generated by dattatrayt
 */
@Entity
@Table(name = "rm_data_applicant_detail", schema = "dbo", catalog = "ES_Portal"
)
public class RmDataApplicantDetail implements java.io.Serializable {

    private Long applicantId;
    private RmDataApplication rmDataApplication;
    private String applicantName;
    private String email;
    private String uid;
    private String pan;
    private Character otpSend;
    private Date otpSendTime;
    private Character emailSend;
    private Date emailSendTime;
    private Character uidVerified;
    private Character isActive;
    private String creBy;
    private Date creDate;
    private String modBy;
    private Date modDate;
    private String status;
     private String otpTxtId;
     private Date uidVerifiedTime;

    public RmDataApplicantDetail() {
    }

    public RmDataApplicantDetail(Long applicantId) {
        this.applicantId = applicantId;
    }

    public RmDataApplicantDetail(Long applicantId, RmDataApplication rmDataApplication, String applicantName, String email, String uid, String pan, Character otpSend, Date otpSendTime, Character emailSend, Date emailSendTime, Character uidVerified, Character isActive, String creBy, Date creDate, String modBy, Date modDate, String status, String otpTxtId, Date uidVerifiedTime) {
        this.applicantId = applicantId;
        this.rmDataApplication = rmDataApplication;
        this.applicantName = applicantName;
        this.email = email;
        this.uid = uid;
        this.pan = pan;
        this.otpSend = otpSend;
        this.otpSendTime = otpSendTime;
        this.emailSend = emailSend;
        this.emailSendTime = emailSendTime;
        this.uidVerified = uidVerified;
        this.isActive = isActive;
        this.creBy = creBy;
        this.creDate = creDate;
        this.modBy = modBy;
        this.modDate = modDate;
        this.status = status;
       this.otpTxtId=otpTxtId;
       this.uidVerifiedTime=uidVerifiedTime;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "applicant_id", unique = true, nullable = false)
    public Long getApplicantId() {
        return this.applicantId;
    }

    public void setApplicantId(Long applicantId) {
        this.applicantId = applicantId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    public RmDataApplication getRmDataApplication() {
        return this.rmDataApplication;
    }

    public void setRmDataApplication(RmDataApplication rmDataApplication) {
        this.rmDataApplication = rmDataApplication;
    }

    @Column(name = "applicant_name", length = 200)
    public String getApplicantName() {
        return this.applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    @Column(name = "email", length = 200)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "uid", length = 12)
    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Column(name = "otp_send", length = 1)
    public Character getOtpSend() {
        return this.otpSend;
    }

    public void setOtpSend(Character otpSend) {
        this.otpSend = otpSend;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "otp_send_time", length = 27)
    public Date getOtpSendTime() {
        return this.otpSendTime;
    }

    public void setOtpSendTime(Date otpSendTime) {
        this.otpSendTime = otpSendTime;
    }

    @Column(name = "uid_verified", length = 1)
    public Character getUidVerified() {
        return this.uidVerified;
    }

    public void setUidVerified(Character uidVerified) {
        this.uidVerified = uidVerified;
    }

    @Column(name = "is_active", length = 1)
    public Character getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    @Column(name = "cre_by", length = 50)
    public String getCreBy() {
        return this.creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 27)
    public Date getCreDate() {
        return this.creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Column(name = "mod_by", length = 50)
    public String getModBy() {
        return this.modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 27)
    public Date getModDate() {
        return this.modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "status", length = 2)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "is_email_send", length = 2)
    public Character getEmailSend() {
        return emailSend;
    }

    public void setEmailSend(Character emailSend) {
        this.emailSend = emailSend;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "email_send_time", length = 27)
    public Date getEmailSendTime() {
        return emailSendTime;
    }

    public void setEmailSendTime(Date emailSendTime) {
        this.emailSendTime = emailSendTime;
    }

    @Column(name="otp_txt_id", length=100)
    public String getOtpTxtId() {
        return otpTxtId;
    }

    public void setOtpTxtId(String otpTxtId) {
        this.otpTxtId = otpTxtId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="uid_verified_time", length=27)
    public Date getUidVerifiedTime() {
        return uidVerifiedTime;
    }

    public void setUidVerifiedTime(Date uidVerifiedTime) {
        this.uidVerifiedTime = uidVerifiedTime;
    }

    @Column(name="pan", length=50)
    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

}
