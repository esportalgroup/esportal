/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;

/**
 *
 * @author VEN00624
 */
public class EsMcmDisbursementDocDetail {

    private Long EsMcmDisbursementDocDetailId;
    private String applicationId;
    private String groupId;
    private String isMemmberPresent;
    private String preferedLang;
    private Date creationDate;
    private Date modifiedDate;
    private String creBy;
    private String modBy;

    public EsMcmDisbursementDocDetail(Long EsMcmDisbursementDocDetailId, String applicationId, String groupId, String isMemmberPresent, String preferedLang, Date creationDate, Date modifiedDate, String creBy, String modBy) {
        this.EsMcmDisbursementDocDetailId = EsMcmDisbursementDocDetailId;
        this.applicationId = applicationId;
        this.groupId = groupId;
        this.isMemmberPresent = isMemmberPresent;
        this.preferedLang = preferedLang;
        this.creationDate = creationDate;
        this.modifiedDate = modifiedDate;
        this.creBy = creBy;
        this.modBy = modBy;
    }

    public Long getEsMcmDisbursementDocDetailId() {
        return EsMcmDisbursementDocDetailId;
    }

    public void setEsMcmDisbursementDocDetailId(Long EsMcmDisbursementDocDetailId) {
        this.EsMcmDisbursementDocDetailId = EsMcmDisbursementDocDetailId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getIsMemmberPresent() {
        return isMemmberPresent;
    }

    public void setIsMemmberPresent(String isMemmberPresent) {
        this.isMemmberPresent = isMemmberPresent;
    }

    public String getPreferedLang() {
        return preferedLang;
    }

    public void setPreferedLang(String preferedLang) {
        this.preferedLang = preferedLang;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

}
