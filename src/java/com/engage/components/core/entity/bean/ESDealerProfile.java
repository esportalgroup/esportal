/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Shrikant Katakdhond
 */
@Entity
@Table(name = "es_dealer_profile_details", schema = "dbo", catalog = "ES_Portal"
)
public class ESDealerProfile implements Serializable {

    private Long Id;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;
    private String addressType;
    private String city;
    private String country;
    private String state;
    private String stdIsd;
    private String phone;
    private String Account;
    private String bankName;
    private String branchName;
    private String location;
    private String bankAccountNumber;
    private String paymentMode;
    private String bankAccountName;
    private String supplierId;
    private String description;
    private String salesStNumber;
    private String centralStNumber;
    private String incomeTaxNumber;
    private String supplierDealerFlag;
    private String internalFlag;
    private String dealerCommission;
    private String vatAccountNumber;
    private String aggregateLimit;
    private String osRcAmt;
    private String active;
    private String validFrom;
    private String validTo;
    private String panId;
    private String gstNumber;
    private String registeredUnderGst;
    private String exemptionFlag;
    private String exemptionPeriodFrom;
    private String exemptionPeriodTo;
    private String recordTypeId;
    private String contactPerson;

    public ESDealerProfile() {
    }

    public ESDealerProfile(Long Id, String name, String addressLine1, String addressLine2, String addressLine3, String addressLine4, String addressType, String city, String country, String state, String stdIsd, String phone, String Account, String bankName, String branchName, String location, String bankAccountNumber, String paymentMode, String bankAccountName, String supplierId, String description, String salesStNumber, String centralStNumber, String incomeTaxNumber, String supplierDealerFlag, String internalFlag, String dealerCommission, String vatAccountNumber, String aggregateLimit, String osRcAmt, String active, String validFrom, String validTo, String panId, String gstNumber, String registeredUnderGst, String exemptionFlag, String exemptionPeriodFrom, String exemptionPeriodTo, String recordTypeId, String contactPerson) {
        this.Id = Id;
        this.name = name;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.addressLine4 = addressLine4;
        this.addressType = addressType;
        this.city = city;
        this.country = country;
        this.state = state;
        this.stdIsd = stdIsd;
        this.phone = phone;
        this.Account = Account;
        this.bankName = bankName;
        this.branchName = branchName;
        this.location = location;
        this.bankAccountNumber = bankAccountNumber;
        this.paymentMode = paymentMode;
        this.bankAccountName = bankAccountName;
        this.supplierId = supplierId;
        this.description = description;
        this.salesStNumber = salesStNumber;
        this.centralStNumber = centralStNumber;
        this.incomeTaxNumber = incomeTaxNumber;
        this.supplierDealerFlag = supplierDealerFlag;
        this.internalFlag = internalFlag;
        this.dealerCommission = dealerCommission;
        this.vatAccountNumber = vatAccountNumber;
        this.aggregateLimit = aggregateLimit;
        this.osRcAmt = osRcAmt;
        this.active = active;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.panId = panId;
        this.gstNumber = gstNumber;
        this.registeredUnderGst = registeredUnderGst;
        this.exemptionFlag = exemptionFlag;
        this.exemptionPeriodFrom = exemptionPeriodFrom;
        this.exemptionPeriodTo = exemptionPeriodTo;
        this.recordTypeId = recordTypeId;
        this.contactPerson = contactPerson;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "profile_id", unique = true, nullable = false)
    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    @Column(name = "address_line_1")
    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Column(name = "address_line_2")
    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Column(name = "address_line_3")
    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    @Column(name = "address_line_4")
    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    @Column(name = "address_type")
    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "stdIsd")
    public String getStdIsd() {
        return stdIsd;
    }

    public void setStdIsd(String stdIsd) {
        this.stdIsd = stdIsd;
    }

    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "account")
    public String getAccount() {
        return Account;
    }

    public void setAccount(String Account) {
        this.Account = Account;
    }

    @Column(name = "bank_name")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Column(name = "branch_name")
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "bank_account_number")
    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Column(name = "payment_mode")
    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @Column(name = "bank_account_name")
    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    @Column(name = "supplier_id")
    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "sales_st_number")
    public String getSalesStNumber() {
        return salesStNumber;
    }

    public void setSalesStNumber(String salesStNumber) {
        this.salesStNumber = salesStNumber;
    }

    @Column(name = "central_st_number")
    public String getCentralStNumber() {
        return centralStNumber;
    }

    public void setCentralStNumber(String centralStNumber) {
        this.centralStNumber = centralStNumber;
    }

    @Column(name = "income_tax_number")
    public String getIncomeTaxNumber() {
        return incomeTaxNumber;
    }

    public void setIncomeTaxNumber(String incomeTaxNumber) {
        this.incomeTaxNumber = incomeTaxNumber;
    }

    @Column(name = "supplier_dealer_flag")
    public String getSupplierDealerFlag() {
        return supplierDealerFlag;
    }

    public void setSupplierDealerFlag(String supplierDealerFlag) {
        this.supplierDealerFlag = supplierDealerFlag;
    }

    @Column(name = "internal_flag")
    public String getInternalFlag() {
        return internalFlag;
    }

    public void setInternalFlag(String internalFlag) {
        this.internalFlag = internalFlag;
    }

    @Column(name = "dealer_commission")
    public String getDealerCommission() {
        return dealerCommission;
    }

    public void setDealerCommission(String dealerCommission) {
        this.dealerCommission = dealerCommission;
    }

    @Column(name = "vat_account_number")
    public String getVatAccountNumber() {
        return vatAccountNumber;
    }

    public void setVatAccountNumber(String vatAccountNumber) {
        this.vatAccountNumber = vatAccountNumber;
    }

    @Column(name = "aggregate_limit")
    public String getAggregateLimit() {
        return aggregateLimit;
    }

    public void setAggregateLimit(String aggregateLimit) {
        this.aggregateLimit = aggregateLimit;
    }

    @Column(name = "os_rc_amt")
    public String getOsRcAmt() {
        return osRcAmt;
    }

    public void setOsRcAmt(String osRcAmt) {
        this.osRcAmt = osRcAmt;
    }

    @Column(name = "active")
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Column(name = "valid_from")
    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    @Column(name = "valid_to")
    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    @Column(name = "pan_id")
    public String getPanId() {
        return panId;
    }

    public void setPanId(String panId) {
        this.panId = panId;
    }

    @Column(name = "gst_number")
    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    @Column(name = "registered_under_gst")
    public String getRegisteredUnderGst() {
        return registeredUnderGst;
    }

    public void setRegisteredUnderGst(String registeredUnderGst) {
        this.registeredUnderGst = registeredUnderGst;
    }

    @Column(name = "exemption_flag")
    public String getExemptionFlag() {
        return exemptionFlag;
    }

    public void setExemptionFlag(String exemptionFlag) {
        this.exemptionFlag = exemptionFlag;
    }

    @Column(name = "exemption_period_from")
    public String getExemptionPeriodFrom() {
        return exemptionPeriodFrom;
    }

    public void setExemptionPeriodFrom(String exemptionPeriodFrom) {
        this.exemptionPeriodFrom = exemptionPeriodFrom;
    }

    @Column(name = "exemption_period_to")
    public String getExemptionPeriodTo() {
        return exemptionPeriodTo;
    }

    public void setExemptionPeriodTo(String exemptionPeriodTo) {
        this.exemptionPeriodTo = exemptionPeriodTo;
    }

    @Column(name = "record_type_id")
    public String getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(String recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    @Column(name = "contact_person")
    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
