/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author VEN00288
 */
@Entity
@Table(name = "es_mcm_applicants_detail", schema = "dbo", catalog = "ES_Portal"
)
public class McmApplicantsDetails {
    
    private Long mcmApplicantId;
    private String applicantId;
    private String applicationId;
    private String applicantName;
    private String realationWithApplicant;
    private String groupId;
     private String losId;

    public McmApplicantsDetails() {
    }

    public McmApplicantsDetails(String applicantId, String applicationId, String applicantName, String realationWithApplicant) {
        this.applicantId = applicantId;
        this.applicationId = applicationId;
        this.applicantName = applicantName;
        this.realationWithApplicant = realationWithApplicant;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "es_mcm_applicant_id", unique = true, nullable = false)
    public Long getMcmApplicantId() {
        return mcmApplicantId;
    }

    public void setMcmApplicantId(Long mcmApplicantId) {
        this.mcmApplicantId = mcmApplicantId;
    }
    
    
    @Column(name = "applicant_id")
    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "application_name")
    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    @Column(name = "realation_with_applicant")
    public String getRealationWithApplicant() {
        return realationWithApplicant;
    }

    public void setRealationWithApplicant(String realationWithApplicant) {
        this.realationWithApplicant = realationWithApplicant;
    }
    
    
@Column(name = "groupId")
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }    

    @Column(name = "los_id")
    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }
    
    
}
