/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ven00288
 */
@Entity
@Table(name = "mcm_repayment_other_details", schema = "dbo", catalog = "ES_Portal"
)
public class RepaymentSummeriesOthersDetails {

    private long otherDetailsId;
    private String laonAmount;
    private String lpfAmount;
    private String insurancePremium;
    private Date createDate;
    private Date modDate;
    private String creBy;
    private String modBy;

    public RepaymentSummeriesOthersDetails() {
    }

    public RepaymentSummeriesOthersDetails(long otherDetailsId, String laonAmount, String lpfAmount, String insurancePremium, Date createDate, Date modDate, String creBy, String modBy) {
        this.otherDetailsId = otherDetailsId;
        this.laonAmount = laonAmount;
        this.lpfAmount = lpfAmount;
        this.insurancePremium = insurancePremium;
        this.createDate = createDate;
        this.modDate = modDate;
        this.creBy = creBy;
        this.modBy = modBy;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "mcm_other_details_id", unique = true, nullable = false)
    public long getOtherDetailsId() {
        return otherDetailsId;
    }

    public void setOtherDetailsId(long otherDetailsId) {
        this.otherDetailsId = otherDetailsId;
    }

    @Column(name = "loan_amount")
    public String getLaonAmount() {
        return laonAmount;
    }

    public void setLaonAmount(String laonAmount) {
        this.laonAmount = laonAmount;
    }

    @Column(name = "apf_amount")
    public String getLpfAmount() {
        return lpfAmount;
    }

    public void setLpfAmount(String lpfAmount) {
        this.lpfAmount = lpfAmount;
    }

    @Column(name = "insurance_premium")
    public String getInsurancePremium() {
        return insurancePremium;
    }

    public void setInsurancePremium(String insurancePremium) {
        this.insurancePremium = insurancePremium;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

}
