/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Shrikant Katakdhond
 */
@Entity
@Table(name = "es_mcm_group_detail", schema = "dbo", catalog = "ES_Portal"
)
public class McmGroupDetail implements Serializable{

    private Long grpId;
    private String groupId;
    private String groupName;
    private String branchId;
    private String villageId;
    private String localityId;
    private String stateId;
    private String districtId;
    private String cityId;
    private String villageName;
    private String mcCode;
    private String floId;
    private String floName;
    private Date sanctionDate;
    private String createBy;
    private Date createDate;
    private Date modDate;
    private String modBy;
    private Character isActive;
    private String collectionDate;
    private String collectionTime;
    private String mcmAccNo;
    private String mcmAccBankName;
    private String mcmAccBranchName;
    private String status;
    

    public McmGroupDetail() {
    }

    public McmGroupDetail(Long grpId, String groupId, String groupName, String branchId, String villageId, String localityId, String stateId, String districtId, String cityId, String floId, String floName, Date sanctionDate, String createBy, Date createDate, Date modDate, String modBy, Character isActive) {
        this.grpId = grpId;
        this.groupId = groupId;
        this.groupName = groupName;
        this.branchId = branchId;
        this.villageId = villageId;
        this.localityId = localityId;
        this.stateId = stateId;
        this.districtId = districtId;
        this.cityId = cityId;
        this.floId = floId;
        this.floName = floName;
        this.sanctionDate = sanctionDate;
        this.createBy = createBy;
        this.createDate = createDate;
        this.modDate = modDate;
        this.modBy = modBy;
        this.isActive = isActive;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "grp_id", unique = true, nullable = false)
    public Long getGrpId() {
        return grpId;
    }

    public void setGrpId(Long grpId) {
        this.grpId = grpId;
    }

    @Column(name = "group_id ")
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Column(name = "group_name")
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Column(name = "branch_id")
    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    @Column(name = "village_id")
    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    @Column(name = "locality_id")
    public String getLocalityId() {
        return localityId;
    }

    public void setLocalityId(String localityId) {
        this.localityId = localityId;
    }

    @Column(name = "state_id")
    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    @Column(name = "district_id")
    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    @Column(name = "city_id")
    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    @Column(name = "flo_id")
    public String getFloId() {
        return floId;
    }

    public void setFloId(String floId) {
        this.floId = floId;
    }

    @Column(name = "flo_name")
    public String getFloName() {
        return floName;
    }

    public void setFloName(String floName) {
        this.floName = floName;
    }

    @Column(name = "mc_code")
    public String getMcCode() {
        return mcCode;
    }

    public void setMcCode(String mcCode) {
        this.mcCode = mcCode;
    }
    
    
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sanction_date")
    public Date getSanctionDate() {
        return sanctionDate;
    }

    public void setSanctionDate(Date sanctionDate) {
        this.sanctionDate = sanctionDate;
    }

    @Column(name = "create_by")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date")
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Column(name = "is_active", length = 1)
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    @Column(name = "village_name")
    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

//    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "collection_date")
    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    @Column(name = "collection_time",length = 100)
    public String getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(String collectionTime) {
        this.collectionTime = collectionTime;
    }

    @Column(name = "mcm_acc_no",length = 100)
    public String getMcmAccNo() {
        return mcmAccNo;
    }

    public void setMcmAccNo(String mcmAccNo) {
        this.mcmAccNo = mcmAccNo;
    }

    @Column(name = "mcm_acc_bank_name",length = 100)
    public String getMcmAccBankName() {
        return mcmAccBankName;
    }

    public void setMcmAccBankName(String mcmAccBankName) {
        this.mcmAccBankName = mcmAccBankName;
    }

    @Column(name = "mcm_acc_branch_name",length = 100)
    public String getMcmAccBranchName() {
        return mcmAccBranchName;
    }

    public void setMcmAccBranchName(String mcmAccBranchName) {
        this.mcmAccBranchName = mcmAccBranchName;
    }

    @Column(name = "status",length = 100)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
