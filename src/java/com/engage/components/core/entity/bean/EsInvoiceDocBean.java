/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ven00288
 */
@Entity
@Table(name = "es_dealer_invoice_doc_details", schema = "dbo", catalog = "ES_Portal"
)
public class EsInvoiceDocBean {

    private Long invoiceDocId;
    private String applicationId;
    private String invoiceNumber;
    private Date invoiceDate;
    private String engineNumber;
    private String chassisNumber;
    private String invoiceDocType;
    private String invoiceDocName;
    private String invoiceDocUuid;
    private Date creDate;
    private Date modDate;
    private String creBy;
    private String modBy;
    private Character isActive;

    public EsInvoiceDocBean() {
    }

    public EsInvoiceDocBean(Long invoiceDocId, String applicationId, String invoiceNumber, Date invoiceDate, String engineNumber, String chassisNumber, String invoiceDocType, String invoiceDocName, String invoiceDocUuid, Date creDate, Date modDate) {
        this.invoiceDocId = invoiceDocId;
        this.applicationId = applicationId;
        this.invoiceNumber = invoiceNumber;
        this.invoiceDate = invoiceDate;
        this.engineNumber = engineNumber;
        this.chassisNumber = chassisNumber;
        this.invoiceDocType = invoiceDocType;
        this.invoiceDocName = invoiceDocName;
        this.invoiceDocUuid = invoiceDocUuid;
        this.creDate = creDate;
        this.modDate = modDate;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "invoice_doc_id", unique = true, nullable = false)
    public Long getInvoiceDocId() {
        return invoiceDocId;
    }

    public void setInvoiceDocId(Long invoiceDocId) {
        this.invoiceDocId = invoiceDocId;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "invoice_number")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "invoice_date", length = 23)
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Column(name = "engine_number")
    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    @Column(name = "chassis_number")
    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    @Column(name = "invoice_doc_type")
    public String getInvoiceDocType() {
        return invoiceDocType;
    }

    public void setInvoiceDocType(String invoiceDocType) {
        this.invoiceDocType = invoiceDocType;
    }

    @Column(name = "invoice_doc_name")
    public String getInvoiceDocName() {
        return invoiceDocName;
    }

    public void setInvoiceDocName(String invoiceDocName) {
        this.invoiceDocName = invoiceDocName;
    }

    @Column(name = "invoice_doc_uuid")
    public String getInvoiceDocUuid() {
        return invoiceDocUuid;
    }

    public void setInvoiceDocUuid(String invoiceDocUuid) {
        this.invoiceDocUuid = invoiceDocUuid;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

     @Column(name = "is_active")
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }
    
    

}
