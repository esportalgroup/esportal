/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

/**
 *
 * @author dattatrayt
 */
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="mcm_trigger_request_log"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class McmTriggerRequestLog implements java.io.Serializable {
    
    private Long logId;
    private String applicationId;
     private String request;
     private String response;
     private String creId;
     private Date createDate;
     private String modId;
     private Date modDate;

    public McmTriggerRequestLog() {
    }

    public McmTriggerRequestLog(Long logId, String applicationId, String request, String response, String creId, Date createDate, String modId, Date modDate) {
        this.logId = logId;
        this.applicationId=applicationId;
        this.request = request;
        this.response = response;
        this.creId = creId;
        this.createDate = createDate;
        this.modId = modId;
        this.modDate = modDate;
    }
     
    @Id 
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="log_id", unique=true, nullable=false)
    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    @Column(name="application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name="request")
    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Column(name="response")
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Column(name="cre_id", length=50)
    public String getCreId() {
        return creId;
    }

    public void setCreId(String creId) {
        this.creId = creId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name="mod_id", length=50)
    public String getModId() {
        return modId;
    }

    public void setModId(String modId) {
        this.modId = modId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="mod_date", length=23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
     

}
