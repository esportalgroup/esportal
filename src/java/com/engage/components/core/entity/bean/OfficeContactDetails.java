/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ven00288
 */
@Entity
@Table(name = "mcm_office_contact_details", schema = "dbo", catalog = "ES_Portal"
)
public class OfficeContactDetails {
    
    
    private long officeCotactId;
    private String mcmName;
    private String mcmMobileNumber;
    private String associateName;
    private String associateMobileNumber;
    private String applicationId;
    private String losId;
    private Date createDate;
    private Date modDate;
    private String creBy;
    private String modBy;

    public OfficeContactDetails() {
    }

    public OfficeContactDetails(long officeCotactId, String mcmName, String mcmMobileNumber, String associateName, String associateMobileNumber, String applicationId, String losId, Date createDate, Date modDate, String creBy, String modBy) {
        this.officeCotactId = officeCotactId;
        this.mcmName = mcmName;
        this.mcmMobileNumber = mcmMobileNumber;
        this.associateName = associateName;
        this.associateMobileNumber = associateMobileNumber;
        this.applicationId = applicationId;
        this.losId = losId;
        this.createDate = createDate;
        this.modDate = modDate;
        this.creBy = creBy;
        this.modBy = modBy;
    }

    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "mcm_office_contact_id", unique = true, nullable = false)
    public long getOfficeCotactId() {
        return officeCotactId;
    }

    public void setOfficeCotactId(long officeCotactId) {
        this.officeCotactId = officeCotactId;
    }

    @Column(name = "mcm_name")
    public String getMcmName() {
        return mcmName;
    }

    public void setMcmName(String mcmName) {
        this.mcmName = mcmName;
    }

    @Column(name = "mcm_mobile_no")
    public String getMcmMobileNumber() {
        return mcmMobileNumber;
    }

    public void setMcmMobileNumber(String mcmMobileNumber) {
        this.mcmMobileNumber = mcmMobileNumber;
    }

    @Column(name = "associate_name")
    public String getAssociateName() {
        return associateName;
    }

    public void setAssociateName(String associateName) {
        this.associateName = associateName;
    }

    @Column(name = "associate_mobile_no")
    public String getAssociateMobileNumber() {
        return associateMobileNumber;
    }

    public void setAssociateMobileNumber(String associateMobileNumber) {
        this.associateMobileNumber = associateMobileNumber;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "los_id")
    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }
    
}
