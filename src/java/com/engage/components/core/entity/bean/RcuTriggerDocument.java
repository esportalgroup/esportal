/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SurajK
 */
@Entity
@Table(name = "rcu_trigger_document", schema = "dbo", catalog = "ES_Portal"
)
public class RcuTriggerDocument implements java.io.Serializable {

    private Long id;
    private String applicationId;
    private String applicantId;
    private String docId;
    private String docName;
    private String childDocId;
    private String childDocName;
    private String docCheckType;
    private Date createdDate;
    private String rcuModBy;
    private Date rcuModDate;
    public Boolean screeningCompleted;
    public String pickForSampling;
    public String samplingStatus;
    public Long docVersion;
    public String uuid;
    public String fileName;

    public RcuTriggerDocument() {
    }

    public RcuTriggerDocument(Long id) {
        this.id = id;
    }

    public RcuTriggerDocument(Long id, String applicationId, String applicantId, String docId, String docName, String childDocId, String childDocName, Long docVersion, String docCheckType, Date createdDate, String rcuModBy, Date rcuModDate, Boolean screeningCompleted, String pickForSampling, String samplingStatus,String uuid,String fileName) {
        this.id = id;
        this.applicationId = applicationId;
        this.applicantId = applicantId;
        this.docId = docId;
        this.docName = docName;
        this.childDocId = childDocId;
        this.childDocName = childDocName;
        this.docVersion = docVersion;
        this.docCheckType = docCheckType;
        this.createdDate = createdDate;
        this.rcuModBy = rcuModBy;
        this.rcuModDate = rcuModDate;
        this.screeningCompleted = screeningCompleted;
        this.pickForSampling = pickForSampling;
        this.samplingStatus = samplingStatus;
        this.uuid=uuid;
        this.fileName=fileName;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "ID", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "APPLICATION_ID")
    public String getApplicationId() {
        return this.applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "APPLICANT_ID")
    public String getApplicantId() {
        return this.applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    @Column(name = "DOC_ID")
    public String getDocId() {
        return this.docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    @Column(name = "DOC_NAME", length = 200)
    public String getDocName() {
        return this.docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    @Column(name = "CHILD_DOC_ID")
    public String getChildDocId() {
        return this.childDocId;
    }

    public void setChildDocId(String childDocId) {
        this.childDocId = childDocId;
    }

    @Column(name = "CHILD_DOC_NAME", length = 200)
    public String getChildDocName() {
        return this.childDocName;
    }

    public void setChildDocName(String childDocName) {
        this.childDocName = childDocName;
    }

    @Column(name = "DOC_VERSION")
    public Long getDocVersion() {
        return docVersion;
    }

    public void setDocVersion(Long docVersion) {
        this.docVersion = docVersion;
    }
    
    @Column(name = "doc_check_type", length = 5)
    public String getDocCheckType() {
        return docCheckType;
    }

    public void setDocCheckType(String docCheckType) {
        this.docCheckType = docCheckType;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATED_DATE", length = 10)
    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "RCU_MOD_BY", length = 50)
    public String getRcuModBy() {
        return this.rcuModBy;
    }

    public void setRcuModBy(String rcuModBy) {
        this.rcuModBy = rcuModBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RCU_MOD_DATE", length = 23)
    public Date getRcuModDate() {
        return this.rcuModDate;
    }

    public void setRcuModDate(Date rcuModDate) {
        this.rcuModDate = rcuModDate;
    }

    @Column(name="SCREENING_COMPLETED")
    public Boolean getScreeningCompleted() {
        return screeningCompleted;
    }

    public void setScreeningCompleted(Boolean screeningCompleted) {
        this.screeningCompleted = screeningCompleted;
    }

    @Column(name = "PICK_FOR_SAMPLING", length = 50)
    public String getPickForSampling() {
        return pickForSampling;
    }

    public void setPickForSampling(String pickForSampling) {
        this.pickForSampling = pickForSampling;
    }

    @Column(name = "SAMPLING_STATUS", length = 50)
    public String getSamplingStatus() {
        return samplingStatus;
    }

    public void setSamplingStatus(String samplingStatus) {
        this.samplingStatus = samplingStatus;
    }
    
    @Column(name = "UUID", length = 10)
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Column(name = "FILE_NAME", length = 100)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    
   
}
