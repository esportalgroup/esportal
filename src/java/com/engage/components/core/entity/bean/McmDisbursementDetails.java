/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ven00253
 */
@Entity
@Table(name = "es_mcm_disbursement_detail", schema = "dbo", catalog = "ES_Portal"
)
public class McmDisbursementDetails {
    
  private Long mcmDisbursementId;
  private String groupId;
  private String mcCode;
  private String adharNumber;
  private String applicationId;

  private String status;
  private Date creDate;
  private Date modDate;
  private String creBy;
  private String modBy;

  
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "es_mcm_application_disbursement_id", unique = true, nullable = false)
    public Long getMcmDisbursementId() {
        return mcmDisbursementId;
    }

    public void setMcmDisbursementId(Long mcmDisbursementId) {
        this.mcmDisbursementId = mcmDisbursementId;
    }

    @Column(name = "group_id")
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Column(name = "mc_code")
    public String getMcCode() {
        return mcCode;
    }

    public void setMcCode(String mcCode) {
        this.mcCode = mcCode;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    
    @Column(name = "adhar_number")
    public String getAdharNumber() {
        return adharNumber;
    }

    public void setAdharNumber(String adharNumber) {
        this.adharNumber = adharNumber;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date")
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date")
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }
  
  
    
}
