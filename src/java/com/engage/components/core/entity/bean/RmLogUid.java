/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dattatrayt
 */
@Entity
@Table(name="rm_log_uid"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class RmLogUid implements java.io.Serializable {
    
     private Long logId;
     private RmDataApplication rmDataApplication;
     private RmDataApplicantDetail rmDataApplicantDetail;
     private String request;
     private String response;
     private String modId;
     private Date modDate;
     private String typeOfReq;

    public RmLogUid() {
    }

	
    public RmLogUid(Long logId) {
        this.logId = logId;
    }
    public RmLogUid(Long logId, RmDataApplication rmDataApplication, RmDataApplicantDetail rmDataApplicantDetail, String request, String response, String modId, Date modDate, String typeOfReq) {
       this.logId = logId;
       this.rmDataApplication = rmDataApplication;
       this.rmDataApplicantDetail = rmDataApplicantDetail;
       this.request = request;
       this.response = response;
       this.modId = modId;
       this.modDate = modDate;
       this.typeOfReq = typeOfReq;
    }
   
     @Id 
    @GeneratedValue(strategy = IDENTITY)

    
    @Column(name="log_id", unique=true, nullable=false)
    public Long getLogId() {
        return this.logId;
    }
    
    public void setLogId(Long logId) {
        this.logId = logId;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="application_id")
    public RmDataApplication getRmDataApplication() {
        return this.rmDataApplication;
    }
    
    public void setRmDataApplication(RmDataApplication rmDataApplication) {
        this.rmDataApplication = rmDataApplication;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="applicant_id")
    public RmDataApplicantDetail getRmDataApplicantDetail() {
        return rmDataApplicantDetail;
    }

    public void setRmDataApplicantDetail(RmDataApplicantDetail rmDataApplicantDetail) {
        this.rmDataApplicantDetail = rmDataApplicantDetail;
    }

    
    @Column(name="request")
    public String getRequest() {
        return this.request;
    }
    
    public void setRequest(String request) {
        this.request = request;
    }

    
    @Column(name="response")
    public String getResponse() {
        return this.response;
    }
    
    public void setResponse(String response) {
        this.response = response;
    }

    
    @Column(name="mod_id", length=50)
    public String getModId() {
        return this.modId;
    }
    
    public void setModId(String modId) {
        this.modId = modId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="mod_date", length=23)
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    
    @Column(name="type_of_req", length=50)
    public String getTypeOfReq() {
        return this.typeOfReq;
    }
    
    public void setTypeOfReq(String typeOfReq) {
        this.typeOfReq = typeOfReq;
    }




}
