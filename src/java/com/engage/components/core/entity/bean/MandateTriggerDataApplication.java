/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

/**
 *
 * @author DattatrayT
 */
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name="mandate_trigger_data_application"
    ,schema="dbo"
    ,catalog="ES_Portal")
public class MandateTriggerDataApplication implements java.io.Serializable {
    private Long refid;
    private String applicationId;
    private String losId;
    private String Status;
    private String SanctionDate;
    private String PropertyAgreement;
    private String ProductType;
    private String PhysicalMandateEMI;
    private String PhysicalMandateBranchname;
    private String PhysicalMandateBankname;
    private String PhysicalMandateBankACnumber;
    private String PDCIFSCcode;
    private String PDCBranchname;
    private String PDCBankname;
    private String PDCBankACnumber;
    private String InstallmentDate;
    private String FLSName;
    private String FLSId;
    private String CreditVerificationDate;
    private String Builder_Name;
    private String BranchName;
    private String BranchId;
    private String APFProject;
    private String Amount;
    private String createBy;
    private Date createDate;
    private String modBy;
    private Date modDate;
    private String action;
    private Date submitDate;
    private String losStatus;
    private String lockBy;
    private String remark;
    private String physicalMandateApplicationNumber;
    private Character isLatest;
    private Date deactiveDate;
    private Date lockDate;

    public MandateTriggerDataApplication() {
    }

    public MandateTriggerDataApplication(Long refid, String losId, String applicationId, String Status, String SanctionDate, String PropertyAgreement, String ProductType, String PhysicalMandateEMI, String PhysicalMandateBranchname, String PhysicalMandateBankname, String PhysicalMandateBankACnumber, String PDCIFSCcode, String PDCBranchname, String PDCBankname, String PDCBankACnumber, String InstallmentDate, String FLSName, String FLSId, String CreditVerificationDate, String Builder_Name, String BranchName, String BranchId, String APFProject, String Amount, String createBy, Date createDate, String modBy, Date modDate, String action, Date submitDate,String losStatus, String lockBy, String remark, String physicalMandateApplicationNumber, Character isLatest) {
        this.refid = refid;
        this.losId=losId;
        this.applicationId = applicationId;
        this.Status = Status;
        this.SanctionDate = SanctionDate;
        this.PropertyAgreement = PropertyAgreement;
        this.ProductType = ProductType;
        this.PhysicalMandateEMI = PhysicalMandateEMI;
        this.PhysicalMandateBranchname = PhysicalMandateBranchname;
        this.PhysicalMandateBankname = PhysicalMandateBankname;
        this.PhysicalMandateBankACnumber = PhysicalMandateBankACnumber;
        this.PDCIFSCcode = PDCIFSCcode;
        this.PDCBranchname = PDCBranchname;
        this.PDCBankname = PDCBankname;
        this.PDCBankACnumber = PDCBankACnumber;
        this.InstallmentDate = InstallmentDate;
        this.FLSName = FLSName;
        this.FLSId = FLSId;
        this.CreditVerificationDate = CreditVerificationDate;
        this.Builder_Name = Builder_Name;
        this.BranchName = BranchName;
        this.BranchId = BranchId;
        this.APFProject = APFProject;
        this.Amount = Amount;
        this.createBy = createBy;
        this.createDate = createDate;
        this.modBy = modBy;
        this.modDate = modDate;
        this.action = action;
        this.submitDate = submitDate;
        this.losStatus = losStatus;
        this.lockBy=lockBy;
        this.remark=remark;
        this.physicalMandateApplicationNumber=physicalMandateApplicationNumber;
        this.isLatest=isLatest;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "refid", unique = true, nullable = false)
    public Long getRefid() {
        return refid;
    }

    public void setRefid(Long refid) {
        this.refid = refid;
    }

    @Column(name = "application_id", length = 100)
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "los_id", length = 100)
    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    @Column(name = "status", length = 100)
    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    @Column(name = "sanction_date", length = 100)
    public String getSanctionDate() {
        return SanctionDate;
    }

    public void setSanctionDate(String SanctionDate) {
        this.SanctionDate = SanctionDate;
    }

    @Column(name = "property_agreement", length = 100)
    public String getPropertyAgreement() {
        return PropertyAgreement;
    }

    public void setPropertyAgreement(String PropertyAgreement) {
        this.PropertyAgreement = PropertyAgreement;
    }

    @Column(name = "product_type", length = 100)
    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String ProductType) {
        this.ProductType = ProductType;
    }

    @Column(name = "physical_mandate_emi", length = 100)
    public String getPhysicalMandateEMI() {
        return PhysicalMandateEMI;
    }

    public void setPhysicalMandateEMI(String PhysicalMandateEMI) {
        this.PhysicalMandateEMI = PhysicalMandateEMI;
    }

    @Column(name = "physical_mandate_branch_name", length = 100)
    public String getPhysicalMandateBranchname() {
        return PhysicalMandateBranchname;
    }

    public void setPhysicalMandateBranchname(String PhysicalMandateBranchname) {
        this.PhysicalMandateBranchname = PhysicalMandateBranchname;
    }

    @Column(name = "physical_mandate_bank_name", length = 100)
    public String getPhysicalMandateBankname() {
        return PhysicalMandateBankname;
    }

    public void setPhysicalMandateBankname(String PhysicalMandateBankname) {
        this.PhysicalMandateBankname = PhysicalMandateBankname;
    }

    @Column(name = "physical_mandate_bank_AC_number", length = 100)
    public String getPhysicalMandateBankACnumber() {
        return PhysicalMandateBankACnumber;
    }

    public void setPhysicalMandateBankACnumber(String PhysicalMandateBankACnumber) {
        this.PhysicalMandateBankACnumber = PhysicalMandateBankACnumber;
    }

    @Column(name = "PDC_IFSC_code", length = 100)
    public String getPDCIFSCcode() {
        return PDCIFSCcode;
    }

    public void setPDCIFSCcode(String PDCIFSCcode) {
        this.PDCIFSCcode = PDCIFSCcode;
    }

    @Column(name = "PDC_branch_name", length = 100)
    public String getPDCBranchname() {
        return PDCBranchname;
    }

    public void setPDCBranchname(String PDCBranchname) {
        this.PDCBranchname = PDCBranchname;
    }

    @Column(name = "PDC_bank_name", length = 100)
    public String getPDCBankname() {
        return PDCBankname;
    }

    public void setPDCBankname(String PDCBankname) {
        this.PDCBankname = PDCBankname;
    }

    @Column(name = "PDC_bank_AC_number", length = 100)
    public String getPDCBankACnumber() {
        return PDCBankACnumber;
    }

    public void setPDCBankACnumber(String PDCBankACnumber) {
        this.PDCBankACnumber = PDCBankACnumber;
    }

    @Column(name = "installment_date", length = 100)
    public String getInstallmentDate() {
        return InstallmentDate;
    }

    public void setInstallmentDate(String InstallmentDate) {
        this.InstallmentDate = InstallmentDate;
    }

    @Column(name = "FLS_name", length = 100)
    public String getFLSName() {
        return FLSName;
    }

    public void setFLSName(String FLSName) {
        this.FLSName = FLSName;
    }

    @Column(name = "FLS_id", length = 100)
    public String getFLSId() {
        return FLSId;
    }

    public void setFLSId(String FLSId) {
        this.FLSId = FLSId;
    }

    @Column(name = "credit_verification_date", length = 100)
    public String getCreditVerificationDate() {
        return CreditVerificationDate;
    }

    public void setCreditVerificationDate(String CreditVerificationDate) {
        this.CreditVerificationDate = CreditVerificationDate;
    }

    @Column(name = "builder_name", length = 100)
    public String getBuilder_Name() {
        return Builder_Name;
    }

    public void setBuilder_Name(String Builder_Name) {
        this.Builder_Name = Builder_Name;
    }

    @Column(name = "branch_name", length = 100)
    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String BranchName) {
        this.BranchName = BranchName;
    }

    @Column(name = "branch_id", length = 100)
    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String BranchId) {
        this.BranchId = BranchId;
    }

    @Column(name = "APF_project", length = 100)
    public String getAPFProject() {
        return APFProject;
    }

    public void setAPFProject(String APFProject) {
        this.APFProject = APFProject;
    }

    @Column(name = "amount", length = 100)
    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    @Column(name = "create_by", length = 100)
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", length = 23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "mod_by", length = 100)
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "action", length = 100)
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "submit_date", length = 23)
    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    @Column(name = "los_tatus", length = 100)
    public String getLosStatus() {
        return losStatus;
    }

    public void setLosStatus(String losStatus) {
        this.losStatus = losStatus;
    }

    @Column(name = "lock_by", length = 100)
    public String getLockBy() {
        return lockBy;
    }

    public void setLockBy(String lockBy) {
        this.lockBy = lockBy;
    }

    @Column(name = "remark", length = 100)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(name = "physical_mandate_application_number", length = 100)
    public String getPhysicalMandateApplicationNumber() {
        return physicalMandateApplicationNumber;
    }

    public void setPhysicalMandateApplicationNumber(String physicalMandateApplicationNumber) {
        this.physicalMandateApplicationNumber = physicalMandateApplicationNumber;
    }

    @Column(name = "is_latest", length = 1)
    public Character getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(Character isLatest) {
        this.isLatest = isLatest;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deactive_date", length = 23)
    public Date getDeactiveDate() {
        return deactiveDate;
    }

    public void setDeactiveDate(Date deactiveDate) {
        this.deactiveDate = deactiveDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lock_date", length = 23)
    public Date getLockDate() {
        return lockDate;
    }

    public void setLockDate(Date lockDate) {
        this.lockDate = lockDate;
    }
    
}
