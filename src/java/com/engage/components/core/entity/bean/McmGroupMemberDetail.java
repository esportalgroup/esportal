/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Shrikant Katakdhond
 */
@Entity
@Table(name = "es_mcm_group_member_detail", schema = "dbo", catalog = "ES_Portal"
)
public class McmGroupMemberDetail implements Serializable {

    private Long id;
    private McmGroupDetail groupDetail;
    private String memberId;
    private String memberName;
    private String loanAmount;
    private String loanTenure;
    private String emi;
    private String procFee;
    private String cliAmount;
    private String netDisbursementAmt;
    private String appraisalflag;
    private String modeOfDisbursement;
    private String bankAccountNumber;
    private String ifscCode;
    private String accountValidationStatus;
    private Long validAccountWSCount;
    private String createBy;
    private Date createDate;
    private Date modDate;
    private String modBy;
    private Character isActive;

    public McmGroupMemberDetail() {
    }

    public McmGroupMemberDetail(Long id, McmGroupDetail groupDetail, String memberId, String memberName, String loanAmount, String loanTenure, String emi, String procFee, String cliAmount, String netDisbursementAmt, String appraisalflag, String modeOfDisbursement, String bankAccountNumber, String ifscCode, String accountValidationStatus, Long validAccountWSCount, String createBy, Date createDate, Date modDate, String modBy, Character isActive) {
        this.id = id;
        this.groupDetail = groupDetail;
        this.memberId = memberId;
        this.memberName = memberName;
        this.loanAmount = loanAmount;
        this.loanTenure = loanTenure;
        this.emi = emi;
        this.procFee = procFee;
        this.cliAmount = cliAmount;
        this.netDisbursementAmt = netDisbursementAmt;
        this.appraisalflag = appraisalflag;
        this.modeOfDisbursement = modeOfDisbursement;
        this.bankAccountNumber = bankAccountNumber;
        this.ifscCode = ifscCode;
        this.accountValidationStatus = accountValidationStatus;
        this.validAccountWSCount = validAccountWSCount;
        this.createBy = createBy;
        this.createDate = createDate;
        this.modDate = modDate;
        this.modBy = modBy;
        this.isActive = isActive;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "mbr_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grp_id")
    public McmGroupDetail getGroupDetail() {
        return groupDetail;
    }

    public void setGroupDetail(McmGroupDetail groupDetail) {
        this.groupDetail = groupDetail;
    }

    @Column(name = "member_id")
    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    @Column(name = "member_name")
    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    @Column(name = "loan_amount")
    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Column(name = "loan_tenure")
    public String getLoanTenure() {
        return loanTenure;
    }

    public void setLoanTenure(String loanTenure) {
        this.loanTenure = loanTenure;
    }

    @Column(name = "emi")
    public String getEmi() {
        return emi;
    }

    public void setEmi(String emi) {
        this.emi = emi;
    }

    @Column(name = "proc_fee")
    public String getProcFee() {
        return procFee;
    }

    public void setProcFee(String procFee) {
        this.procFee = procFee;
    }

    @Column(name = "cli_amount")
    public String getCliAmount() {
        return cliAmount;
    }

    public void setCliAmount(String cliAmount) {
        this.cliAmount = cliAmount;
    }

    @Column(name = "net_disbursement_amount")
    public String getNetDisbursementAmt() {
        return netDisbursementAmt;
    }

    public void setNetDisbursementAmt(String netDisbursementAmt) {
        this.netDisbursementAmt = netDisbursementAmt;
    }

    @Column(name = "appraisal_flag")
    public String getAppraisalflag() {
        return appraisalflag;
    }

    public void setAppraisalflag(String appraisalflag) {
        this.appraisalflag = appraisalflag;
    }

    @Column(name = "mode_Of_disbursement")
    public String getModeOfDisbursement() {
        return modeOfDisbursement;
    }

    public void setModeOfDisbursement(String modeOfDisbursement) {
        this.modeOfDisbursement = modeOfDisbursement;
    }

    @Column(name = "account_number")
    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Column(name = "ifsc_code")
    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    @Column(name = "account_valid_status")
    public String getAccountValidationStatus() {
        return accountValidationStatus;
    }

    public void setAccountValidationStatus(String accountValidationStatus) {
        this.accountValidationStatus = accountValidationStatus;
    }

    @Column(name = "valid_account_ws_count")
    public Long getValidAccountWSCount() {
        return validAccountWSCount;
    }

    public void setValidAccountWSCount(Long validAccountWSCount) {
        this.validAccountWSCount = validAccountWSCount;
    }

    @Column(name = "create_by")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date")
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Column(name = "is_active", length = 1)
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }
    
    
}
