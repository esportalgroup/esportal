/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author VEN00288
 */
@Entity
@Table(name = "es_dealer_asset_details", schema = "dbo", catalog = "ES_Portal"
)
public class EsAssetDetailBean {

    private Long assetDetaisId;
    private String applicationId;
    private String assetManufacturer;
    private String assetMake;
    private String assetModel;
    private String assetCost;
    private String assetCostOnboardingPrice;
    private String assetCostTax;
    private String assetCostIns;
    private String assetCostESR;
//    private String engineNumber;
//    private String chassisNumber;

    private Date modDate;
    private Date creDate;

    public EsAssetDetailBean() {
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "asset_detail_id", unique = true, nullable = false)
    public Long getAssetDetaisId() {
        return assetDetaisId;
    }

    public void setAssetDetaisId(Long assetDetaisId) {
        this.assetDetaisId = assetDetaisId;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "asset_manufacturer")
    public String getAssetManufacturer() {
        return assetManufacturer;
    }

    public void setAssetManufacturer(String assetManufacturer) {
        this.assetManufacturer = assetManufacturer;
    }

    @Column(name = "asset_make")
    public String getAssetMake() {
        return assetMake;
    }

    public void setAssetMake(String assetMake) {
        this.assetMake = assetMake;
    }

    @Column(name = "asset_model")
    public String getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(String assetModel) {
        this.assetModel = assetModel;
    }

    @Column(name = "asset_cost")
    public String getAssetCost() {
        return assetCost;
    }

    public void setAssetCost(String assetCost) {
        this.assetCost = assetCost;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Column(name = "assetCost_Onboarding_price")
    public String getAssetCostOnboardingPrice() {
        return assetCostOnboardingPrice;
    }

    
    public void setAssetCostOnboardingPrice(String assetCostOnboardingPrice) {
        this.assetCostOnboardingPrice = assetCostOnboardingPrice;
    }

    @Column(name = "assetCost_cost_tax")
    public String getAssetCostTax() {
        return assetCostTax;
    }

    public void setAssetCostTax(String assetCostTax) {
        this.assetCostTax = assetCostTax;
    }

    @Column(name = "assetCost_cost_ins")
    public String getAssetCostIns() {
        return assetCostIns;
    }

    public void setAssetCostIns(String assetCostIns) {
        this.assetCostIns = assetCostIns;
    }

    @Column(name = "assetCost_cost_esr")
    public String getAssetCostESR() {
        return assetCostESR;
    }

    public void setAssetCostESR(String assetCostESR) {
        this.assetCostESR = assetCostESR;
    }

//    @Column(name = "engine_number")
//    public String getEngineNumber() {
//        return engineNumber;
//    }
//
//    public void setEngineNumber(String engineNumber) {
//        this.engineNumber = engineNumber;
//    }

//    @Column(name = "chassis_number")
//    public String getChassisNumber() {
//        return chassisNumber;
//    }
//
//    public void setChassisNumber(String chassisNumber) {
//        this.chassisNumber = chassisNumber;
//    }
    
    

}
