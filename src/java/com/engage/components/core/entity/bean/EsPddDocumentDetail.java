/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Shrikant Katakdhond
 */
@Entity
@Table(name = "es_pdd_document_detail", schema = "dbo", catalog = "ES_Portal"
)
public class EsPddDocumentDetail implements java.io.Serializable {

    private Long pddDocId;
    private String applicationId;
    private String invoiceNumber;
    private Date invoiceDate;
    private String engineNumber;
    private String chassisNumber;
    private String invoiceHypothicationStatus;
    private String invoiceDocType;
    private String invoiceDocName;
    private String invoiceDocUuid;
    private String registrationNumber;
    private Date registrationDate;
    private String registrationHypothicationStatus;
    private String registrationDocType;
    private String registrationDocName;
    private String registrationDocUuid;
    private String marginMoneyDocType;
    private String marginMoneyDocName;
    private String marginMoneyDocUuid;
    private Character isActive;
    private Date modDate;
    private String modBy;
    private Date creDate;
    private String creBy;

    public EsPddDocumentDetail() {
    }

    public EsPddDocumentDetail(Long pddDocId, String applicationId, String invoiceNumber, Date invoiceDate, String engineNumber, String chassisNumber, String invoiceHypothicationStatus, String invoiceDocType, String invoiceDocName, String invoiceDocUuid, String registrationNumber, Date registrationDate, String registrationHypothicationStatus, String registrationDocType, String registrationDocName, String registrationDocUuid, String marginMoneyDocType, String marginMoneyDocName, String marginMoneyDocUuid, Character isActive, Date modDate, String modBy, Date creDate, String creBy) {
        this.pddDocId = pddDocId;
        this.applicationId = applicationId;
        this.invoiceNumber = invoiceNumber;
        this.invoiceDate = invoiceDate;
        this.engineNumber = engineNumber;
        this.chassisNumber = chassisNumber;
        this.invoiceHypothicationStatus = invoiceHypothicationStatus;
        this.invoiceDocType = invoiceDocType;
        this.invoiceDocName = invoiceDocName;
        this.invoiceDocUuid = invoiceDocUuid;
        this.registrationNumber = registrationNumber;
        this.registrationDate = registrationDate;
        this.registrationHypothicationStatus = registrationHypothicationStatus;
        this.registrationDocType = registrationDocType;
        this.registrationDocName = registrationDocName;
        this.registrationDocUuid = registrationDocUuid;
        this.marginMoneyDocType = marginMoneyDocType;
        this.marginMoneyDocName = marginMoneyDocName;
        this.marginMoneyDocUuid = marginMoneyDocUuid;
        this.isActive = isActive;
        this.modDate = modDate;
        this.modBy = modBy;
        this.creDate = creDate;
        this.creBy = creBy;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "pdd_doc_id", unique = true, nullable = false)
    public Long getPddDocId() {
        return pddDocId;
    }

    public void setPddDocId(Long pddDocId) {
        this.pddDocId = pddDocId;
    }

    @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "invoice_no")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "invoice_date", length = 23)
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Column(name = "engine_no")
    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    @Column(name = "chassis_no")
    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    @Column(name = "invoice_hypothication_status")
    public String getInvoiceHypothicationStatus() {
        return invoiceHypothicationStatus;
    }

    public void setInvoiceHypothicationStatus(String invoiceHypothicationStatus) {
        this.invoiceHypothicationStatus = invoiceHypothicationStatus;
    }

    @Column(name = "invoice_doc_uuid")
    public String getInvoiceDocUuid() {
        return invoiceDocUuid;
    }

    public void setInvoiceDocUuid(String invoiceDocUuid) {
        this.invoiceDocUuid = invoiceDocUuid;
    }

    @Column(name = "registration_no")
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date", length = 23)
    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Column(name = "registration_hypothication_status")
    public String getRegistrationHypothicationStatus() {
        return registrationHypothicationStatus;
    }

    public void setRegistrationHypothicationStatus(String registrationHypothicationStatus) {
        this.registrationHypothicationStatus = registrationHypothicationStatus;
    }

    @Column(name = "registration_doc_uuid")
    public String getRegistrationDocUuid() {
        return registrationDocUuid;
    }

    public void setRegistrationDocUuid(String registrationDocUuid) {
        this.registrationDocUuid = registrationDocUuid;
    }

    @Column(name = "margin_money_doc_uuid")
    public String getMarginMoneyDocUuid() {
        return marginMoneyDocUuid;
    }

    public void setMarginMoneyDocUuid(String marginMoneyDocUuid) {
        this.marginMoneyDocUuid = marginMoneyDocUuid;
    }

    @Column(name = "is_active")
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "invoice_doc_type")
    public String getInvoiceDocType() {
        return invoiceDocType;
    }

    public void setInvoiceDocType(String invoiceDocType) {
        this.invoiceDocType = invoiceDocType;
    }

    @Column(name = "invoice_doc_name")
    public String getInvoiceDocName() {
        return invoiceDocName;
    }

    public void setInvoiceDocName(String invoiceDocName) {
        this.invoiceDocName = invoiceDocName;
    }

    @Column(name = "reg_doc_type")
    public String getRegistrationDocType() {
        return registrationDocType;
    }

    public void setRegistrationDocType(String registrationDocType) {
        this.registrationDocType = registrationDocType;
    }

    @Column(name = "reg_doc_name")
    public String getRegistrationDocName() {
        return registrationDocName;
    }

    public void setRegistrationDocName(String registrationDocName) {
        this.registrationDocName = registrationDocName;
    }

    @Column(name = "margin_money_doc_type")
    public String getMarginMoneyDocType() {
        return marginMoneyDocType;
    }

    public void setMarginMoneyDocType(String marginMoneyDocType) {
        this.marginMoneyDocType = marginMoneyDocType;
    }

    @Column(name = "margin_money_doc_name")
    public String getMarginMoneyDocName() {
        return marginMoneyDocName;
    }

    public void setMarginMoneyDocName(String marginMoneyDocName) {
        this.marginMoneyDocName = marginMoneyDocName;
    }

    
}
