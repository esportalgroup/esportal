/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

/**
 *
 * @author DattatrayT
 */
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="mandate_trigger_token_user"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class MandateTriggerTokenUser {
    private Long id;
    private String userId;
    private String password;
    private Character isActive;

    public MandateTriggerTokenUser() {
    }

    public MandateTriggerTokenUser(Long id, String userId, String password, Character isActive) {
        this.id = id;
        this.userId = userId;
        this.password = password;
        this.isActive = isActive;
    }
    
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Id 
    @Column(name="user_id", unique=true, nullable=false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name="password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name="is_active")
    public Character getIsAcive() {
        return isActive;
    }

    public void setIsAcive(Character isActive) {
        this.isActive = isActive;
    }
    
}
