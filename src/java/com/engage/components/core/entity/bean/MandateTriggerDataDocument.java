/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

/**
 *
 * @author DattatrayT
 */
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "mandate_trigger_data_document", schema = "dbo", catalog = "ES_Portal"
)
public class MandateTriggerDataDocument implements java.io.Serializable {

    private Long documentId;
    private MandateTriggerDataApplication mandateTriggerDataApplication;
    private String uuid;
    private String losId;
    private String docRef;
    private String docName;
    private String DocCategoryName;
    private String ApplicantID;
    private String reason;
    private String opsAction;
    private String remark;
    private String description;
    private String createBy;
    private Date createDate;
    private String modBy;
    private Date modDate;
    private String Q1;
    private String Q2;
    private String Q3;
    private String Q4;
    private String Q5;
    private String Q6;
    private String Q7;
    private String Q8;
    private String Q9;
    private String Q10;
    private String Q11;
    private String Q12;
    private String Q13;
    private String Q14;
    private String Q15;
    private String Q16;
    private String Q17;
    private String Q18;
    private String Q19;
    private String Q20;
    private String Q21;
    private String Q22;
    private String Q23;
    private String Q24;
    private String Q25;    
    private String docRemark;
    private String losDocStatus;

    public MandateTriggerDataDocument() {
    }

    public MandateTriggerDataDocument(Long documentId, MandateTriggerDataApplication mandateTriggerDataApplication, String uuid, String losId, String docRef, String docName, String DocCategoryName, String ApplicantID, String reason, String opsAction, String remark, String description, String createBy, Date createDate, String modBy, Date modDate, String docRemark) {
        this.documentId = documentId;
        this.mandateTriggerDataApplication = mandateTriggerDataApplication;
        this.uuid = uuid;
        this.losId = losId;
        this.docRef = docRef;
        this.docName = docName;
        this.DocCategoryName = DocCategoryName;
        this.ApplicantID = ApplicantID;
        this.reason = reason;
        this.opsAction = opsAction;
        this.remark = remark;
        this.description = description;
        this.createBy = createBy;
        this.createDate = createDate;
        this.modBy = modBy;
        this.modDate = modDate;
        this.docRemark=docRemark;
    }

    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "document_id", unique = true, nullable = false)
    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "refid", nullable = false)
    public MandateTriggerDataApplication getMandateTriggerDataApplication() {
        return mandateTriggerDataApplication;
    }

    public void setMandateTriggerDataApplication(MandateTriggerDataApplication mandateTriggerDataApplication) {
        this.mandateTriggerDataApplication = mandateTriggerDataApplication;
    }
    
    @Column(name = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Column(name = "los_id", length = 100)
    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    @Column(name = "doc_ref", length = 100)
    public String getDocRef() {
        return docRef;
    }

    public void setDocRef(String docRef) {
        this.docRef = docRef;
    }

    @Column(name = "doc_name", length = 100)
    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    @Column(name = "doc_category_name", length = 100)
    public String getDocCategoryName() {
        return DocCategoryName;
    }

    public void setDocCategoryName(String DocCategoryName) {
        this.DocCategoryName = DocCategoryName;
    }

    @Column(name = "applicant_id", length = 100)
    public String getApplicantID() {
        return ApplicantID;
    }

    public void setApplicantID(String ApplicantID) {
        this.ApplicantID = ApplicantID;
    }

    @Column(name = "reason")
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Column(name = "ops_action", length = 100)
    public String getOpsAction() {
        return opsAction;
    }

    public void setOpsAction(String opsAction) {
        this.opsAction = opsAction;
    }

    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(name = "description", length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "create_by")
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", length = 23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "Q1", length = 10)
    public String getQ1() {
        return Q1;
    }

    public void setQ1(String Q1) {
        this.Q1 = Q1;
    }

    @Column(name = "Q2", length = 10)
    public String getQ2() {
        return Q2;
    }

    public void setQ2(String Q2) {
        this.Q2 = Q2;
    }

    @Column(name = "Q3", length = 10)
    public String getQ3() {
        return Q3;
    }

    public void setQ3(String Q3) {
        this.Q3 = Q3;
    }

    @Column(name = "Q4", length = 10)
    public String getQ4() {
        return Q4;
    }

    public void setQ4(String Q4) {
        this.Q4 = Q4;
    }

    @Column(name = "Q5", length = 10)
    public String getQ5() {
        return Q5;
    }

    public void setQ5(String Q5) {
        this.Q5 = Q5;
    }

    @Column(name = "Q6", length = 10)
    public String getQ6() {
        return Q6;
    }

    public void setQ6(String Q6) {
        this.Q6 = Q6;
    }

    @Column(name = "Q7", length = 10)
    public String getQ7() {
        return Q7;
    }

    public void setQ7(String Q7) {
        this.Q7 = Q7;
    }

    @Column(name = "Q8", length = 10)
    public String getQ8() {
        return Q8;
    }

    public void setQ8(String Q8) {
        this.Q8 = Q8;
    }

    @Column(name = "Q9", length = 10)
    public String getQ9() {
        return Q9;
    }

    public void setQ9(String Q9) {
        this.Q9 = Q9;
    }

    @Column(name = "Q10", length = 10)
    public String getQ10() {
        return Q10;
    }

    public void setQ10(String Q10) {
        this.Q10 = Q10;
    }

    @Column(name = "Q11", length = 10)
    public String getQ11() {
        return Q11;
    }

    public void setQ11(String Q11) {
        this.Q11 = Q11;
    }

    @Column(name = "Q12", length = 10)
    public String getQ12() {
        return Q12;
    }

    public void setQ12(String Q12) {
        this.Q12 = Q12;
    }

    @Column(name = "Q13", length = 10)
    public String getQ13() {
        return Q13;
    }

    public void setQ13(String Q13) {
        this.Q13 = Q13;
    }

    @Column(name = "Q14", length = 10)
    public String getQ14() {
        return Q14;
    }

    public void setQ14(String Q14) {
        this.Q14 = Q14;
    }

    @Column(name = "Q15", length = 10)
    public String getQ15() {
        return Q15;
    }

    public void setQ15(String Q15) {
        this.Q15 = Q15;
    }

    @Column(name = "Q16", length = 10)
    public String getQ16() {
        return Q16;
    }

    public void setQ16(String Q16) {
        this.Q16 = Q16;
    }

    @Column(name = "Q17", length = 10)
    public String getQ17() {
        return Q17;
    }

    public void setQ17(String Q17) {
        this.Q17 = Q17;
    }

    @Column(name = "Q18", length = 10)
    public String getQ18() {
        return Q18;
    }

    public void setQ18(String Q18) {
        this.Q18 = Q18;
    }

    @Column(name = "Q19", length = 10)
    public String getQ19() {
        return Q19;
    }

    public void setQ19(String Q19) {
        this.Q19 = Q19;
    }

    @Column(name = "Q20", length = 10)
    public String getQ20() {
        return Q20;
    }

    public void setQ20(String Q20) {
        this.Q20 = Q20;
    }

    @Column(name = "Q21", length = 10)
    public String getQ21() {
        return Q21;
    }

    public void setQ21(String Q21) {
        this.Q21 = Q21;
    }

    @Column(name = "Q22", length = 10)
    public String getQ22() {
        return Q22;
    }

    public void setQ22(String Q22) {
        this.Q22 = Q22;
    }

    @Column(name = "Q23", length = 10)
    public String getQ23() {
        return Q23;
    }

    public void setQ23(String Q23) {
        this.Q23 = Q23;
    }

    @Column(name = "Q24", length = 10)
    public String getQ24() {
        return Q24;
    }

    public void setQ24(String Q24) {
        this.Q24 = Q24;
    }

    @Column(name = "Q25", length = 10)
    public String getQ25() {
        return Q25;
    }

    public void setQ25(String Q25) {
        this.Q25 = Q25;
    }

    @Column(name = "doc_remark")
    public String getDocRemark() {
        return docRemark;
    }

    public void setDocRemark(String docRemark) {
        this.docRemark = docRemark;
    }

    @Column(name = "los_doc_status", length = 50)
    public String getLosDocStatus() {
        return losDocStatus;
    }

    public void setLosDocStatus(String losDocStatus) {
        this.losDocStatus = losDocStatus;
    }

}
