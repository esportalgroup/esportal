/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author SurajK
 */
@Entity
@Table(name="rcu_trigger_status_log"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class RcuTriggerStatusLog  implements java.io.Serializable {


     private long rcuLogId;
     private String applicationId;
     private String rcuRemark;
     private String rcuStatus;
     private String rcuModBy;
     private Date rcuModDate;
     private String rcuOutcomeStatus;
     private Character isSubmited;
     private Date submitedDate;

    public RcuTriggerStatusLog() {
    }

	
    public RcuTriggerStatusLog(long rcuLogId) {
        this.rcuLogId = rcuLogId;
    }
    public RcuTriggerStatusLog(long rcuLogId,String applicationId, String rcuRemark, String rcuStatus, String rcuModBy, Date rcuModDate, String rcuOutcomeStatus, Character isSubmited, Date submitedDate) {
       this.rcuLogId = rcuLogId;
       this.applicationId = applicationId;
       this.rcuRemark = rcuRemark;
       this.rcuStatus = rcuStatus;
       this.rcuModBy = rcuModBy;
       this.rcuModDate = rcuModDate;
       this.rcuOutcomeStatus = rcuOutcomeStatus;
       this.isSubmited = isSubmited;
       this.submitedDate = submitedDate;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="rcu_log_id", unique=true, nullable=false)
    public long getRcuLogId() {
        return this.rcuLogId;
    }
    
    public void setRcuLogId(long rcuLogId) {
        this.rcuLogId = rcuLogId;
    }

    @Column(name="application_id")
    public String getApplicationId() {
        return this.applicationId;
    }
    
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    
    @Column(name="rcu_remark")
    public String getRcuRemark() {
        return this.rcuRemark;
    }
    
    public void setRcuRemark(String rcuRemark) {
        this.rcuRemark = rcuRemark;
    }

    
    @Column(name="rcu_status", length=100)
    public String getRcuStatus() {
        return this.rcuStatus;
    }
    
    public void setRcuStatus(String rcuStatus) {
        this.rcuStatus = rcuStatus;
    }

    @Column(name="rcu_mod_by", length=50)
    public String getRcuModBy() {
        return this.rcuModBy;
    }
    
    public void setRcuModBy(String rcuModBy) {
        this.rcuModBy = rcuModBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="rcu_mod_date", length=23)
    public Date getRcuModDate() {
        return this.rcuModDate;
    }
    
    public void setRcuModDate(Date rcuModDate) {
        this.rcuModDate = rcuModDate;
    }

    
    @Column(name="rcu_outcome_status", length=50)
    public String getRcuOutcomeStatus() {
        return this.rcuOutcomeStatus;
    }
    
    public void setRcuOutcomeStatus(String rcuOutcomeStatus) {
        this.rcuOutcomeStatus = rcuOutcomeStatus;
    }

    
    @Column(name="is_submited", length=1)
    public Character getIsSubmited() {
        return this.isSubmited;
    }
    
    public void setIsSubmited(Character isSubmited) {
        this.isSubmited = isSubmited;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="submited_date", length=23)
    public Date getSubmitedDate() {
        return this.submitedDate;
    }
    
    public void setSubmitedDate(Date submitedDate) {
        this.submitedDate = submitedDate;
    }

}


