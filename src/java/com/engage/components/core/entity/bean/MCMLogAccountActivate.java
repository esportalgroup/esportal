/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Shrikant Katakdhond
 */
@Entity
@Table(name = "es_mcm_log_account_validate", schema = "dbo", catalog = "ES_Portal"
)
public class MCMLogAccountActivate implements Serializable{
    
    private long logId;
     private String request;
     private String response;
     private String modId;
     private Date modDate;

    public MCMLogAccountActivate() {
    }

     
    public MCMLogAccountActivate(long logId, String request, String response, String modId, Date modDate) {
        this.logId = logId;
        this.request = request;
        this.response = response;
        this.modId = modId;
        this.modDate = modDate;
    }
     
     @Id 
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="log_id", unique=true, nullable=false)
    public long getLogId() {
        return logId;
    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

    @Column(name="request")
    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Column(name="response")
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Column(name="mod_id", length=50)
    public String getModId() {
        return modId;
    }

    public void setModId(String modId) {
        this.modId = modId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="mod_date", length=23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
}
