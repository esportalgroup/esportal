/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
/**
 *
 * @author SurajK
 */
@Entity
@Table(name="rcu_trigger_applicant"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class RcuTriggerApplicant  implements java.io.Serializable {


     private long id;
     private String applicationId;
     private String applicantId;
     private String applicantName;
     private String applicantType;
     private Date createdDate;
     private String rcuModBy;
     private Date rcuModDate;
     private String currentAddress;

    public RcuTriggerApplicant() {
    }

	
    public RcuTriggerApplicant(long id) {
        this.id = id;
    }
    public RcuTriggerApplicant(long id, String applicationId, String applicantId, String applicantName, String applicantType, Date createdDate, String rcuModBy, Date rcuModDate) {
       this.id = id;
       this.applicationId = applicationId;
       this.applicantId = applicantId;
       this.applicantName = applicantName;
       this.applicantType = applicantType;
       this.createdDate = createdDate;
       this.rcuModBy = rcuModBy;
       this.rcuModDate = rcuModDate;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="ID", unique=true, nullable=false)
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    
    @Column(name="APPLICATION_ID", length=20)
    public String getApplicationId() {
        return this.applicationId;
    }
    
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    
    @Column(name="APPLICANT_ID", length=50)
    public String getApplicantId() {
        return this.applicantId;
    }
    
    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    
    @Column(name="APPLICANT_NAME", length=100)
    public String getApplicantName() {
        return this.applicantName;
    }
    
    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    
    @Column(name="APPLICANT_TYPE", length=20)
    public String getApplicantType() {
        return this.applicantType;
    }
    
    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    
    @Temporal(TemporalType.DATE)
    @Column(name="CREATED_DATE", length=10)
    public Date getCreatedDate() {
        return this.createdDate;
    }
    
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    
    @Column(name="RCU_MOD_BY", length=50)
    public String getRcuModBy() {
        return this.rcuModBy;
    }
    
    public void setRcuModBy(String rcuModBy) {
        this.rcuModBy = rcuModBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="RCU_MOD_DATE", length=23)
    public Date getRcuModDate() {
        return this.rcuModDate;
    }
    
    public void setRcuModDate(Date rcuModDate) {
        this.rcuModDate = rcuModDate;
    }

    @Column(name="CURRENT_ADDRESS", length=500)
    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }




}


