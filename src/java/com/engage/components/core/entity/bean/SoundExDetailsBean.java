/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ven00288
 */
@Entity
@Table(name = "es_soundex_detail", schema = "dbo", catalog = "ES_Portal"
)
public class SoundExDetailsBean {

    private String soundexId;
    private String fuzzy_score1_mapping_percent;
    private String fuzzy_score2_mapping_percent;
    private String referenceId;
    private Character isActive;
    private Date modDate;
    private String modBy;
    private Date creDate;
    private String creBy;

    public SoundExDetailsBean() {
    }

    public SoundExDetailsBean(String soundexId, String fuzzy_score1_mapping_percent, String fuzzy_score2_mapping_percent, Character isActive, Date modDate, String modBy, Date creDate, String creBy) {
        this.soundexId = soundexId;
        this.fuzzy_score1_mapping_percent = fuzzy_score1_mapping_percent;
        this.fuzzy_score2_mapping_percent = fuzzy_score2_mapping_percent;
        this.isActive = isActive;
        this.modDate = modDate;
        this.modBy = modBy;
        this.creDate = creDate;
        this.creBy = creBy;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "soundex_id", unique = true, nullable = false)
    public String getSoundexId() {
        return soundexId;
    }

    public void setSoundexId(String soundexId) {
        this.soundexId = soundexId;
    }

    @Column(name = "fuzzy_score1_mapping_percent")
    public String getFuzzy_score1_mapping_percent() {
        return fuzzy_score1_mapping_percent;
    }

    public void setFuzzy_score1_mapping_percent(String fuzzy_score1_mapping_percent) {
        this.fuzzy_score1_mapping_percent = fuzzy_score1_mapping_percent;
    }

    @Column(name = "fuzzy_score2_mapping_percent")
    public String getFuzzy_score2_mapping_percent() {
        return fuzzy_score2_mapping_percent;
    }

    public void setFuzzy_score2_mapping_percent(String fuzzy_score2_mapping_percent) {
        this.fuzzy_score2_mapping_percent = fuzzy_score2_mapping_percent;
    }

    @Column(name = "reference_id")
    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @Column(name = "is_active")
    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

}
