/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author VEN00288
 */

@Entity
@Table(name="es_token_config_parameters"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class EsTokenConstants {
    
    
     private long constantId;
     private String propertyName;
     private String properyValue;
     private Date createdDate;
     private String status;
     private String type;
     private Date  creDate;
     private Date  modDate;

    public EsTokenConstants() {
    }

    public EsTokenConstants(long constantId, String propertyName, String properyValue, Date createdDate, String status, String type, Date creDate, Date modDate) {
        this.constantId = constantId;
        this.propertyName = propertyName;
        this.properyValue = properyValue;
        this.createdDate = createdDate;
        this.status = status;
        this.type = type;
        this.creDate = creDate;
        this.modDate = modDate;
    }
     
     
     
     
    @Id 
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="CONSTANT_ID", unique=true, nullable=false)
    public long getConstantId() {
        return this.constantId;
    }
    
    public void setConstantId(long constantId) {
        this.constantId = constantId;
    }

    
    @Column(name="PROPERTY_NAME", unique=true, length=100)
    public String getPropertyName() {
        return this.propertyName;
    }
    
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    
    @Column(name="PROPERY_VALUE", length=250)
    public String getProperyValue() {
        return this.properyValue;
    }
    
    public void setProperyValue(String properyValue) {
        this.properyValue = properyValue;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED_DATE", length=23)
    public Date getCreatedDate() {
        return this.createdDate;
    }
    
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    
    @Column(name="STATUS", length=5)
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name="TYPE", length=20)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    
     @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

     
}
