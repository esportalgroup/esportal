package com.engage.components.core.entity.bean;
// Generated Oct 27, 2017 11:53:22 AM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * EsMstrApfPromotionC generated by hbm2java
 */
@Entity
@Table(name="es_mstr_apf_promotion__c"
    ,schema="dbo"
    ,catalog="ES_Portal"
)
public class EsMstrApfPromotionC  implements java.io.Serializable {


     private long esMstrApfPromotionId;
     private String apfCodeC;
     private String apfProjectC;
     private String promotionCodeC;
     private Date creDate;
     private Long creId;
     private Date modDate;
     private Long modId;
     private String attributeId;

    public EsMstrApfPromotionC() {
    }

	
    public EsMstrApfPromotionC(long esMstrApfPromotionId) {
        this.esMstrApfPromotionId = esMstrApfPromotionId;
    }
    public EsMstrApfPromotionC(long esMstrApfPromotionId, String apfCodeC, String apfProjectC, String promotionCodeC, Date creDate, Long creId, Date modDate, Long modId, String attributeId) {
       this.esMstrApfPromotionId = esMstrApfPromotionId;
       this.apfCodeC = apfCodeC;
       this.apfProjectC = apfProjectC;
       this.promotionCodeC = promotionCodeC;
       this.creDate = creDate;
       this.creId = creId;
       this.modDate = modDate;
       this.modId = modId;
       this.attributeId = attributeId;
    }
   
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="ES_MSTR_APF_PROMOTION_Id", unique=true, nullable=false)
    public long getEsMstrApfPromotionId() {
        return this.esMstrApfPromotionId;
    }
    
    public void setEsMstrApfPromotionId(long esMstrApfPromotionId) {
        this.esMstrApfPromotionId = esMstrApfPromotionId;
    }

    
    @Column(name="APF_Code__c", length=100)
    public String getApfCodeC() {
        return this.apfCodeC;
    }
    
    public void setApfCodeC(String apfCodeC) {
        this.apfCodeC = apfCodeC;
    }

    
    @Column(name="APF_Project__c", length=100)
    public String getApfProjectC() {
        return this.apfProjectC;
    }
    
    public void setApfProjectC(String apfProjectC) {
        this.apfProjectC = apfProjectC;
    }

    
    @Column(name="Promotion_Code__c", length=100)
    public String getPromotionCodeC() {
        return this.promotionCodeC;
    }
    
    public void setPromotionCodeC(String promotionCodeC) {
        this.promotionCodeC = promotionCodeC;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="cre_date", length=27)
    public Date getCreDate() {
        return this.creDate;
    }
    
    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    
    @Column(name="cre_id")
    public Long getCreId() {
        return this.creId;
    }
    
    public void setCreId(Long creId) {
        this.creId = creId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="mod_date", length=27)
    public Date getModDate() {
        return this.modDate;
    }
    
    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    
    @Column(name="mod_id")
    public Long getModId() {
        return this.modId;
    }
    
    public void setModId(Long modId) {
        this.modId = modId;
    }

    
    @Column(name="attribute_id", length=100)
    public String getAttributeId() {
        return this.attributeId;
    }
    
    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }




}


