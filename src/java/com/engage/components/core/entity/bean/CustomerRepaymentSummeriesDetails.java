/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ven00288
 */
@Entity
@Table(name = "mcm_customer_details", schema = "dbo", catalog = "ES_Portal"
)
public class CustomerRepaymentSummeriesDetails {

    private long cutomerDetailId;
    private String clientId;
    private String accountId;
    private String customerName;
    private String spousesName;
    private String fatherName;
    private String address;
    private String groupId;
    private String territory;
    private String villageName;
    private String disbursementDate;
    private String loanAmount;
    private String payableIntDays;
    private String tenure;
    private String rateOfInterest;
    private String loanCycle;
    private String collectionTime;
    private String applicationId;
    private String losId;
    private Date createDate;
    private Date modDate;
    private String creBy;
    private String modBy;

    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "mcm_customer_id", unique = true, nullable = false)
    public long getCutomerDetailId() {
        return cutomerDetailId;
    }

    public void setCutomerDetailId(long cutomerDetailId) {
        this.cutomerDetailId = cutomerDetailId;
    }

    @Column(name = "client_id")
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Column(name = "account_id")
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Column(name = "customer_name")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name = "spouse_name")
    public String getSpousesName() {
        return spousesName;
    }

    public void setSpousesName(String spousesName) {
        this.spousesName = spousesName;
    }

    @Column(name = "father_name")
    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "group_id")
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Column(name = "territory")
    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    @Column(name = "village_name")
    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    @Column(name = "disbursement_date")
    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    @Column(name = "loan_amount")
    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Column(name = "payable_int_days")
    public String getPayableIntDays() {
        return payableIntDays;
    }

    public void setPayableIntDays(String payableIntDays) {
        this.payableIntDays = payableIntDays;
    }

    @Column(name = "tenure")
    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    @Column(name = "rate_of_interest")
    public String getRateOfInterest() {
        return rateOfInterest;
    }

    public void setRateOfInterest(String rateOfInterest) {
        this.rateOfInterest = rateOfInterest;
    }

    @Column(name = "loan_cycle")
    public String getLoanCycle() {
        return loanCycle;
    }

    public void setLoanCycle(String loanCycle) {
        this.loanCycle = loanCycle;
    }

    @Column(name = "collection_time")
    public String getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(String collectionTime) {
        this.collectionTime = collectionTime;
    }

     @Column(name = "application_id")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Column(name = "los_id")
    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date", length = 23)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date", length = 23)
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

}
