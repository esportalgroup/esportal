/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.entity.bean;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author VEN00288
 */
@Entity
@Table(name = "es_mcm_code_mstr", schema = "dbo", catalog = "ES_Portal"
)
public class McCodeMstr {
    
    private Long esMcCodeId;
    private String mcCode;
    private String floId;
    private String floName;
    private String creBy;
    private String modBy;
    private Date creDate;
    private Date modDate;

    public McCodeMstr() {
    }

    public McCodeMstr(Long esMcCodeId, String mcCode, String floId, String floName, String creBy, String modBy, Date creDate, Date modDate) {
        this.esMcCodeId = esMcCodeId;
        this.mcCode = mcCode;
        this.floId = floId;
        this.floName = floName;
        this.creBy = creBy;
        this.modBy = modBy;
        this.creDate = creDate;
        this.modDate = modDate;
    }

    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "es_mc_code_id", unique = true, nullable = false)
    public Long getEsMcCodeId() {
        return esMcCodeId;
    }

    public void setEsMcCodeId(Long esMcCodeId) {
        this.esMcCodeId = esMcCodeId;
    }

    @Column(name = "mc_code")
    public String getMcCode() {
        return mcCode;
    }

    public void setMcCode(String mcCode) {
        this.mcCode = mcCode;
    }

    @Column(name = "flo_id")
    public String getFloId() {
        return floId;
    }

    public void setFloId(String floId) {
        this.floId = floId;
    }

    @Column(name = "flo_name")
    public String getFloName() {
        return floName;
    }

    public void setFloName(String floName) {
        this.floName = floName;
    }

    @Column(name = "cre_by")
    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    @Column(name = "mod_by")
    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cre_date")
    public Date getCreDate() {
        return creDate;
    }

    public void setCreDate(Date creDate) {
        this.creDate = creDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "mod_date")
    public Date getModDate() {
        return modDate;
    }

    public void setModDate(Date modDate) {
        this.modDate = modDate;
    }
    
    
    
    
}
