package com.engage.components.core.helper;

import com.cmss.engage.framework.util.AESAlgoClass;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.cmss.engage.security.LDAPAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import com.engage.components.core.entity.dao.LoginDAO;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

@Service
public class LoginHelper {

    @Autowired
    LoginDAO loginDAO;

    @Autowired
    MsterSyncHelper mstrSyncHelper;

    @Autowired
    PasswordResetHelper passwordResetHelper;


    public FrameworkMstrLogin validateUser(String userName, String password, Long userType,HttpServletRequest req) throws CustomNonFatalException, Exception {

        FrameworkMstrLogin frameworkMstrLogin = loginDAO.getFrameworkMstrLogin(userName);
        
//        System.out.println("frameworkMstrLogin ::::::::"+frameworkMstrLogin.getUserName()+frameworkMstrLogin.getUserPassword());
        String decryptedPass = null;
        HashMap<String, String> response=null;
        boolean flag=false;
        //---------get Profile Data
        
        if (frameworkMstrLogin == null) {
            
            throw new CustomNonFatalException("User Not Found", "User Not Found");
            
//            if(role.equalsIgnoreCase("DLR"))
//            {
//                 response = mstrSyncHelper.getSyncMstrLogin(userName,role);
//            }else if(role.equalsIgnoreCase("BLD"))
//            {
//                 response = mstrSyncHelper.getBuilderSyncMstrLogin(userName,role);
//            }else
//            {
//               throw new CustomNonFatalException("Incorrect Username/Password/Role", "Incorrect Username/Password/Role"); 
//            }
//            
//
//            if (response.get("message").equals("Success")) {
//
//                sendMailToUser(userName);
//
//                throw new CustomNonFatalException("User Name And User Id Created Send to Your E-mail", "User Name And User Id Created Send to Your E-mail");
//
//            } else {
//                throw new CustomNonFatalException("Invalid UserName", "Invalid UserName");
//            }

        }
        
//        with LDAP
//        else if(role.equalsIgnoreCase("OPS") || role.equalsIgnoreCase("RCU") || role.equalsIgnoreCase("RM") ){
//        else if(role.equalsIgnoreCase("EMP")){
        else if(frameworkMstrLogin.getRole().trim().equalsIgnoreCase("OPS") || frameworkMstrLogin.getRole().trim().equalsIgnoreCase("RCU") || frameworkMstrLogin.getRole().trim().equalsIgnoreCase("RM") ||  frameworkMstrLogin.getRole().trim().equalsIgnoreCase("MCM")){// 
//            String msg = "ok";
            String msg = LDAPAuth.doLDAPAuthentication(userName, password, "LDAP://10.3.1.43:389/DC=lntfinsvcs");
            if (!msg.equalsIgnoreCase("ok")) {
                throw new CustomNonFatalException("Authentication Failed", "Incorrect Username/Password\n or\nPassword may have got expired\n\nPlease contact ID management\n");
            }
        }
        //without LDAP
        else{
            if (frameworkMstrLogin.getUserPassword() != null) {
                decryptedPass = AESAlgoClass.decrypt(frameworkMstrLogin.getUserPassword());
                if (!decryptedPass.equals(password)) {
                    throw new CustomNonFatalException("Invalid Password", "Invalid Password");
                }
            } else if (!frameworkMstrLogin.getUserType().equals(userType)) {
                throw new CustomNonFatalException("Invalid User Type", "Invalid User Type");
            }
//            
//            if(role.equalsIgnoreCase("DLR"))
//            {
//            mstrSyncHelper.getSyncProfileDetailLogin(userName,role,req);
//            }
        }
        
//        else if (frameworkMstrLogin.getUserPassword() != null) {
//          decryptedPass = AESAlgoClass.decrypt(frameworkMstrLogin.getUserPassword());
//                if (!decryptedPass.equals(password)) {
//                    throw new CustomNonFatalException("Invalid Password", "Invalid Password");
//                } 
//        }
//         else if (!frameworkMstrLogin.getUserType().equals(userType)) {
//            throw new CustomNonFatalException("Invalid User Type", "Invalid User Type");
//        }
            return frameworkMstrLogin;
    }

    private String sendMailToUser(String userName) throws CustomNonFatalException {
        FrameworkMstrLogin frameworkMstrLogin = loginDAO.getFrameworkMstrLogin(userName);
        boolean isMailSent = false;
        if (frameworkMstrLogin != null) {
            try {
                //                if(frameworkMstrLogin.getEmailId() !=null && !frameworkMstrLogin.getEmailId().equals("")){
//                String maskedEmail = frameworkMstrLogin.getEmailId().replaceAll("(?<=.{3}).(?=[^@]*?.@)", "*");

//--------sending Email
//                    String RECIPIENT = frameworkMstrLogin.getEmailId();
                
                String[] to = {"SagarGawakar@ltfs.com"}; // list of recipient email addresses  {RECIPIENT};
                String subject = "Creaate Password";
                String body = "Dear User" // + frameworkMstrLogin.getDisplayName()
                        + "\n Your Password has been reset Succsesfully.\n"
                        + "please use Following details for login.\n"
                        + "User Name : " + frameworkMstrLogin.getUserName()
                        + "\nPassword : " + AESAlgoClass.decrypt(frameworkMstrLogin.getUserPassword());

//                System.out.println("Decrypted password :::::::" + AESAlgoClass.decrypt(frameworkMstrLogin.getUserPassword()));

                isMailSent = passwordResetHelper.sendMail(to, subject, body);

//                else {
//                    throw new CustomNonFatalException("Email Id Not Link With Your User Name.", "Email Id Not Link With Your User Name.");
//                }
            } catch (Exception ex) {
                Logger.getLogger(LoginHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            throw new CustomNonFatalException("User not found", "User not found");
        }

        if (isMailSent) {
            return "Success";
        } else {
            return "Fails";
        }
    }
    
    public String setFrameworkMstrLogin(FrameworkMstrLogin frameworkMstrLogin, String userName) {
        return loginDAO.saveFrameworkMstrLogin(frameworkMstrLogin,userName);
        
    }

}
