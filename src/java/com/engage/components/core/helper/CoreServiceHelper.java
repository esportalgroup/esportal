package com.engage.components.core.helper;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import com.engage.components.core.entity.bean.Module;
import com.engage.components.core.entity.dao.CoreComponentDAO;

@Service
public class CoreServiceHelper {

	@Autowired
	CoreComponentDAO core_dao;
	
	
	public List getAllModules(){
		return core_dao.getModules();
	}
	
}
