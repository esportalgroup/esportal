/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsLogDocumentDownload;
import com.engage.components.core.entity.bean.EsLogDocumentUpload;
import com.engage.components.core.entity.bean.EsPddDocumentDetail;
import com.engage.components.core.entity.dao.PddDocumentDao;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.PddDocDetailVo;
import com.engage.components.esportal.vo.PddDocInvoiceDetailVo;
import com.engage.components.esportal.vo.PddDocMarginMoneyDetailVo;
import com.engage.components.esportal.vo.PddDocRegistrationDetailVo;
import com.engage.components.esportal.vo.uploadDocumentDetailWS;
import com.engage.framework.util.log4j.CustomLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Shrikant Katakdhond
 */
@Service
public class PddDocumentHelper {

    @Autowired
    PddDocumentDao documentDao;

    public PddDocDetailVo getPddDocumentDetail(String applicationId) {
        EsPddDocumentDetail documentDetail = getEsPddDocumentDetail(applicationId);

        PddDocDetailVo detailVo = new PddDocDetailVo();

        //---------------Set Application Details
        EsApplicantsDetailBean applicantsDetailBean = getApplicantsDetailByApplicantionID(applicationId);
        if (applicantsDetailBean != null) {
            detailVo.setApplicationId(applicantsDetailBean.getApplicationId());
            detailVo.setCustomerName(applicantsDetailBean.getApplicantName());
        }
        //--------------Set Invoice Details

        if (documentDetail != null) {

            PddDocInvoiceDetailVo docInvoiceDetailVo = new PddDocInvoiceDetailVo();
            if (documentDetail.getInvoiceNumber() != null) {
                docInvoiceDetailVo.setChassisNumber(documentDetail.getChassisNumber());
                docInvoiceDetailVo.setEngineNumber(documentDetail.getEngineNumber());
                docInvoiceDetailVo.setInvoiceDate(DateUtility.parseDateToString(documentDetail.getInvoiceDate(), "yyyy-MM-dd"));
                docInvoiceDetailVo.setInvoiceDocUuid(documentDetail.getInvoiceDocUuid());
                docInvoiceDetailVo.setHypothicationStatus(documentDetail.getInvoiceHypothicationStatus());
                docInvoiceDetailVo.setInvoiceNumber(documentDetail.getInvoiceNumber());
            } else {
                docInvoiceDetailVo.setHypothicationStatus("0");
            }
            detailVo.setInvoiceDetailVo(docInvoiceDetailVo);

            //----------Set Registration Detail
            PddDocRegistrationDetailVo docRegistrationDetailVo = new PddDocRegistrationDetailVo();
            if (documentDetail.getRegistrationNumber() != null) {
                docRegistrationDetailVo.setRegistrationDate(DateUtility.parseDateToString(documentDetail.getRegistrationDate(), "yyyy-MM-dd"));
                docRegistrationDetailVo.setRegistrationDocUuid(documentDetail.getRegistrationDocUuid());
                docRegistrationDetailVo.setHypothicationStatus(documentDetail.getRegistrationHypothicationStatus());
                docRegistrationDetailVo.setRegistrationNumber(documentDetail.getRegistrationNumber());
            } else {
                docRegistrationDetailVo.setHypothicationStatus("0");
            }
            detailVo.setDocRegistrationDetailVo(docRegistrationDetailVo);

            //------------Set Margin Money Detail
            PddDocMarginMoneyDetailVo marginMoneyDetailVo = new PddDocMarginMoneyDetailVo();
            if (documentDetail.getMarginMoneyDocUuid() != null) {
                marginMoneyDetailVo.setMarginMoneyDocUuid(documentDetail.getMarginMoneyDocUuid());
            }
            detailVo.setMarginMoneyDetailVo(marginMoneyDetailVo);
        }

        return detailVo;
    }

    public EsPddDocumentDetail getEsPddDocumentDetail(String applicationId) {
        EsPddDocumentDetail documentDetail = documentDao.getEsPddDocumentDetail(applicationId);
        return documentDetail;
    }

    public EsApplicantsDetailBean getApplicantsDetailByApplicantionID(String applicationId) {
        EsApplicantsDetailBean applicantsDetailBean = documentDao.getApplicantsDetailByApplicantionID(applicationId);
        return applicantsDetailBean;
    }

    public String getDocumentFromMiddelware(String docUuid, String applicationId) throws JSONException, CustomNonFatalException, JsonProcessingException {
        //This wasn't working with bean.So hardcoded String.
        String requestData = "{\"docDetail\": {\"uuid\": \"" + docUuid + "\"}}";

        RestTemplate restTemplate = new RestTemplate();
        EsLogDocumentDownload documentDownload = new EsLogDocumentDownload();
        documentDownload.setApplicationId(applicationId);
        documentDownload.setRequest(requestData);
        documentDownload.setRequestTime(DateUtility.getCurrentDate());
        String reqUrl = ApplicationPropertyManager.getProperty(FrameworkAppConstants.DOCUMENT_DOWNLOAD_URL);
//        String reqUrl = FrameworkAppConstants.DOCUMENT_DOWNLOAD_URL;

        String op = "";
        try {
            op = restTemplate.postForObject(reqUrl, requestData, String.class);
            documentDownload.setResponse(op);
            documentDownload.setResponseTime(DateUtility.getCurrentDate());
        } catch (Exception e) {
            throw new CustomNonFatalException("Service Unavilable.Please try after some time.", "Service Unavilable.Please try after some time.");
        } finally {
            setEsLogDocumentDownload(documentDownload);
        }

        return op;
    }

    public void setEsPddDocumentDetail(EsPddDocumentDetail documentDetail) {
        documentDao.setEsPddDocumentDetail(documentDetail);
    }

    public void setEsLogDocumentUpload(EsLogDocumentUpload documentUpload) {
        documentDao.setEsLogDocumentUpload(documentUpload);
    }

    public void setEsLogDocumentDownload(EsLogDocumentDownload documentDownload) {
        documentDao.setEsLogDocumentDownload(documentDownload);
    }

    public String documentUploadToMiddelware(uploadDocumentDetailWS detailWS) throws CustomNonFatalException, JsonProcessingException {

        String op = "";
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity(detailWS, headers);

        String HLUrlAdd = ApplicationPropertyManager.getProperty(FrameworkAppConstants.DOCUMENT_UPLOAD_URL);
//String HLUrlAdd = "http://10.3.2.38:7800/DMS_UPLOAD_DOCUMENT";

        ObjectMapper mapper = new ObjectMapper();
        CustomLogger.info("PDD Document Upload request :: " + mapper.writeValueAsString(request));

        EsLogDocumentUpload documentUpload = new EsLogDocumentUpload();
        try {
            documentUpload.setApplicationId(detailWS.getApplicationId());
            documentUpload.setRequest(mapper.writeValueAsString(request));
            documentUpload.setRequestTime(DateUtility.getCurrentDate());

            op = restTemplate.postForObject(HLUrlAdd, request, String.class);

            documentUpload.setResponse(op);
            documentUpload.setResponseTime(DateUtility.getCurrentDate());
        } catch (Exception e) {
            CustomLogger.error("Save PDD Document Upload Response", e);
        } finally {
            setEsLogDocumentUpload(documentUpload);
        }
        return op;
    }

    public EsInvoiceDocBean getEsInvoiceDocDetail(String applicationId) {
        EsInvoiceDocBean documentDetail = documentDao.getEsInvoiceDocDetail(applicationId);
        return documentDetail;
    }

    public void setEsInvoiceDocDetail(EsInvoiceDocBean documentDetail) {
        documentDao.setEsInvoiceDocDetail(documentDetail);
    }
    
    public EsApplicantsDetailBean setEsApplicantsDetailBean(EsApplicantsDetailBean esApplicantsDetailBean){
        return documentDao.setEsApplicantsDetailBean(esApplicantsDetailBean);
    }
}
