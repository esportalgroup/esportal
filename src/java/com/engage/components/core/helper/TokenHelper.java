/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.engage.components.core.entity.dao.LoginDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.*;
import java.util.Date;
import com.engage.components.core.entity.bean.EsTokenUser;
import com.engage.components.core.entity.bean.EsTokenRequestLog;
import com.engage.components.core.entity.dao.TokenDAO;

/**
 *
 * @author DattatrayT
 */
@Service
public class TokenHelper {
    
    final String token_id="d3d3LmxudGZpbnN2Y3MuY29t", token_issuer="esportal_user", token_subject = "esportal_jwt";
    final long token_ttlMillis=1800000;
    
    
    @Autowired
    TokenDAO tokenDAO;

    public String createJWT() {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("MTVGbG9vcixMJlRmaW5hbmNlLFJ1cGFzb2xpdGFpcmUsTUJQLE1haGFwZQ==");
        System.out.println("apiKeySecretBytes : " + apiKeySecretBytes);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(token_id)
                .setIssuedAt(now)
                .setSubject(token_subject)
                .setIssuer(token_issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (token_ttlMillis >= 0) {
            long expMillis = nowMillis + token_ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public void parseJWT(String jwt) throws CustomNonFatalException,JwtException {
        try {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary("MTVGbG9vcixMJlRmaW5hbmNlLFJ1cGFzb2xpdGFpcmUsTUJQLE1haGFwZQ=="))
                .parseClaimsJws(jwt).getBody();
//        System.out.println("ID: " + claims.getId());
//        System.out.println("Subject: " + claims.getSubject());
//        System.out.println("Issuer: " + claims.getIssuer());
//        System.out.println("Expiration: " + claims.getExpiration());
        
        if(!claims.getId().equals(token_id)){
            throw new CustomNonFatalException("Invalid token id","Invalid token id");
        }
        if(!claims.getIssuer().equals(token_issuer)){
            throw new CustomNonFatalException("Invalid token issuer","Invalid token issuer");
        }
        if(!claims.getSubject().equals(token_subject)){
            throw new CustomNonFatalException("Invalid token subject","Invalid token subject");
        }
        } catch (Exception e) {
            if(e.getMessage()!= null && e.getMessage().toString().trim().contains("JWT expired")){
                throw new CustomNonFatalException("JWT expired","JWT expired");
            }else{
                throw new CustomNonFatalException(e.getMessage(),e.getMessage());
            }
        }
        
    }
    
    public EsTokenUser getEsTokenUser(String userId){
        EsTokenUser obj=tokenDAO.getEsTokenUser(userId);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public EsTokenRequestLog setEsTokenRequestLog(EsTokenRequestLog esTokenRequestLog){
        return tokenDAO.setEsTokenRequestLog(esTokenRequestLog);
    }
}
