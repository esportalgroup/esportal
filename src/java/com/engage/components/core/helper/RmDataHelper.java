/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.RmDataApplicantDetail;
import com.engage.components.core.entity.bean.RmDataApplication;
import com.engage.components.core.entity.dao.RmDataDAO;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.uid.vo.UidSendOTPVO;
import com.engage.components.esportal.uid.vo.UidVerifyOTPVO;
import com.engage.framework.util.log4j.CustomLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import com.cmss.engage.framework.util.AESAlgoClass;
import static com.cmss.engage.framework.util.AESAlgoClass.decrypt;
import com.engage.components.esportal.vo.RMApplicationDetailVO;
import com.engage.components.esportal.vo.RMDetailVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import javax.servlet.ServletContext;
/**
 *
 * @author dattatrayt
 */
@Service
public class RmDataHelper {

//	final private String HOST = "10.3.2.23";
    final private String HOST = "masw.ltfs.com";

    @Autowired
    RmDataDAO rmDataDAO;

    @Autowired
    ServletContext context;

    public RmDataApplication getRmDataApplcation(Long applicationId) {
        RmDataApplication obj = rmDataDAO.getRmDataApplcation(applicationId);
        if (obj != null) {
            return obj;
        } else {
            return null;
        }
    }

    public RmDataApplication setRmDataApplcation(RmDataApplication rmDataApplcation) {
        return rmDataDAO.setRmDataApplcation(rmDataApplcation);
    }

    public List<RmDataApplicantDetail> getRmDataApplicantDetail(Long applicationId) {
        List<RmDataApplicantDetail> obj = rmDataDAO.getRmDataApplicantDetail(applicationId);
        if (obj != null) {
            return obj;
        } else {
            return null;
        }
    }

    public RmDataApplicantDetail getRmDataApplicantDetailByApplicantsID(Long applicationId, Long applicantId) {
        RmDataApplicantDetail obj = rmDataDAO.getRmDataApplicantDetailByApplicantsID(applicationId, applicantId);
        if (obj != null) {
            return obj;
        } else {
            return null;
        }
    }

    public List<RmDataApplicantDetail> getRmDataApplicantDetailByApplicationID(Long applicationId) {
        List<RmDataApplicantDetail> obj = rmDataDAO.getRmDataApplicantDetailByApplicationID(applicationId);
        if (obj != null) {
            return obj;
        } else {
            return null;
        }
    }

    public RmDataApplicantDetail setRmDataApplicantDetail(RmDataApplicantDetail rmDataApplicantDetail) {
        return rmDataDAO.setRmDataApplicantDetail(rmDataApplicantDetail);
    }

    public boolean sendEmail(Long applicationId) throws Exception {
        List<RmDataApplicantDetail> lstApplicantsDetail = getRmDataApplicantDetailByApplicationID(applicationId);
        if (lstApplicantsDetail != null) {

            for (RmDataApplicantDetail applicantDetail : lstApplicantsDetail) {

                //--------sending Email
                String RECIPIENT = applicantDetail.getEmail();
                String[] to = {RECIPIENT}; // list of recipient email addresses
                String subject = "Link Aadhar across L&T Financial Services";
                String body = "https://sangamsalesuat2.ltferp.com/ESPortal/RM/veryfiUid.json?applicationId=" + URLEncoder.encode(AESAlgoClass.encrypt(applicantDetail.getRmDataApplication().getApplicationId().toString()), "UTF-8") + "&applicantId=" + URLEncoder.encode(AESAlgoClass.encrypt(applicantDetail.getApplicantId().toString()), "UTF-8");
//                String body = "Dear " + applicantDetail.getApplicantName()
//                        + "\n Please Click below link to Verify Your Aadhar Number.\n"
//                        + "https://sangamsalesuat2.ltferp.com/ESPortal/RM/veryfiUid.json?applicationId=" + (applicantDetail.getRmDataApplication().getApplicationId().toString()) + "&applicantId=" + (applicantDetail.getApplicantId().toString()) + "";
//                
                boolean isMailSent = sendMail(to, subject, body);
                if (isMailSent) {
                    applicantDetail.setEmailSend(FrameworkAppConstants.CONSTANT_YES);
                    applicantDetail.setEmailSendTime(DateUtility.getCurrentDate());
                    applicantDetail.setOtpSend(FrameworkAppConstants.CONSTANT_NO);
                    applicantDetail.setUidVerified(FrameworkAppConstants.CONSTANT_NO);
                    applicantDetail = setRmDataApplicantDetail(applicantDetail);

                }
            }
            return true;
        } else {
            return false;
        }

    }

    public boolean sendMail(String[] to, String subject, String body) throws Exception {
        String login_id, from, pass;
        login_id = ApplicationPropertyManager.getProperty(FrameworkAppConstants.EMAIL_LOGIN_ID);
        pass = decrypt(ApplicationPropertyManager.getProperty(FrameworkAppConstants.EMAIL_PASSWORD));
        from = ApplicationPropertyManager.getProperty(FrameworkAppConstants.EMAIL_ID);

        Properties props = System.getProperties();
        String host = "POLTFMSEXJMBX01.LNTFINSVCS.COM";

        props.put("mail.smtp.starttls.enable", "true");

        props.put("mail.smtp.ssl.trust", host);
//    props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.user", login_id);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "25");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {

            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
////-------------------------------
            MimeMultipart multipart = new MimeMultipart("related");

            // first part  (the html)
            BodyPart messageBodyPart = new MimeBodyPart();
            String htmlText = "<div style=\"\"><img src=\"cid:image\"><br><br>Click Here: <a href='" + body + "'>" + body + "<br></div>";
            messageBodyPart.setContent(htmlText, "text/html");

            // add it
            multipart.addBodyPart(messageBodyPart);

            // second part (the image)
            messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(context.getRealPath("/images/linkAadhar.jpg"));
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<image>");

            // add it
            multipart.addBodyPart(messageBodyPart);

            // put everything together
            message.setContent(multipart);
////------------------

            message.setSubject(subject);
//            message.setText(body);
//        message.setContent(mp);

            Transport transport = session.getTransport("smtp");

//        transport.connect(host, from, pass);
            transport.connect(host, login_id, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            return true;
        } catch (Exception e) {
            CustomLogger.error("send mail", e);
            return false;
        }
    }

    public RMApplicationDetailVO getRMData(Long applicationId) {

        RMApplicationDetailVO detail = null;
        List<RMDetailVO> lstDetailVO = null;
        RMDetailVO detailVO = null;

        RmDataApplication rmDataApplcation = getRmDataApplcation(applicationId);
        if (rmDataApplcation != null) {
            detail = new RMApplicationDetailVO();

            detail.setApplicationId(rmDataApplcation.getApplicationId());
            detail.setLanNumber(rmDataApplcation.getLanNumber());

            List<RmDataApplicantDetail> lstApplicants = getRmDataApplicantDetailByApplicationID(applicationId);
            if (lstApplicants != null) {
                lstDetailVO = new ArrayList<RMDetailVO>();
                for (RmDataApplicantDetail applicantDetail : lstApplicants) {

                    detailVO = new RMDetailVO();

                    detailVO.setApplicantId(applicantDetail.getApplicantId());
                    detailVO.setApplicantName(applicantDetail.getApplicantName());
                    detailVO.setEmail(applicantDetail.getEmail());
                    // detailVO.setPan(applicantDetail.getPan());
                    RmDataApplication application = applicantDetail.getRmDataApplication();
                    detailVO.setCustomerName(application.getCustomerName());
                    detailVO.setPan(application.getPan());

                    lstDetailVO.add(detailVO);
                }
                detail.setLstApplicants(lstDetailVO);
            }

        }
        return detail;
    }

    public String sendOtp(Long applicationId, Long applicantId, String uid) throws CustomNonFatalException {
        String op = "";
        String url = ApplicationPropertyManager.getProperty("UID_API_OTP_URL");
        String headerContentType = ApplicationPropertyManager.getProperty("UID_API_OTP_HEADER_Content-Type");
        String headerLendtoken = ApplicationPropertyManager.getProperty("UID_API_OTP_HEADER_lendToken");

        UidSendOTPVO uidSendOTPVO = new UidSendOTPVO();
        uidSendOTPVO.setAadhaar_number(uid);

//       RestTemplate restTemplate = new RestTemplate();
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Content-Type", headerContentType);
//        headers.add("lendToken", headerLendtoken);
//
//        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
//        
//        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//        op = restTemplate.postForObject(url, request, String.class);
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("lendToken", headerLendtoken);
        HttpEntity request = new HttpEntity(uidSendOTPVO, headers);

        /*ti print json*/
        ObjectMapper mapper = new ObjectMapper();

//       try {
//           CustomLogger.info("UID OTP request :: " + mapper.writeValueAsString(request));
//
//           RmLogUid rmLogUid = new RmLogUid();
//           rmLogUid.setTypeOfReq(FrameworkAppConstants.UID_DOCUMENT_TYPE_OTP_REQUEST);
//           rmLogUid.setRmDataApplication(new RmDataApplication(applicationId));
//           rmLogUid.setRmDataApplicantDetail(new RmDataApplicantDetail(applicantId));
//           rmLogUid.setRequest(mapper.writeValueAsString(request));
////            rmLogUid.setModId(userId.toString());
//           rmLogUid.setModDate(DateUtility.getCurrentDate());
//           rmLogUid = rmDataDAO.setRmLogUid(rmLogUid);
//       } catch (Exception e) {
////           e.printStackTrace();
//           CustomLogger.error("Save UID Request", e);
//       }
        try {
            SSLUtilities.trustAllHostnames();
            SSLUtilities.trustAllHttpsCertificates();
            op = restTemplate.postForObject(url, request, String.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomNonFatalException("Fail to send request", "Fail to send request");
        }

//       try {
//           CustomLogger.info("UID OTP response :: " + op);
//
//           RmLogUid rmLogUid = rmDataDAO.getRmLogUid(applicationId, applicantId, FrameworkAppConstants.UID_DOCUMENT_TYPE_OTP_REQUEST);
//           if (rmLogUid != null) {
//               rmLogUid.setResponse(op);
////            rmLogUid.setModId(userId.toString());
//               rmLogUid.setModDate(DateUtility.getCurrentDate());
//               rmDataDAO.setRmLogUid(rmLogUid);
//           }
//       } catch (Exception e) {
////           e.printStackTrace();
//           CustomLogger.error("Save UID Request", e);
//       }
        return op;
    }

    public String verifyOtp(Long applicationId, Long applicantId, String uid, String otp, String otpTxtId) throws CustomNonFatalException {
        String op = "";
        String url = ApplicationPropertyManager.getProperty("UID_API_VERIFY_OTP_URL");
        String headerContentType = ApplicationPropertyManager.getProperty("UID_API_OTP_HEADER_Content-Type");
        String headerLendtoken = ApplicationPropertyManager.getProperty("UID_API_OTP_HEADER_lendToken");

        UidVerifyOTPVO uidVerifyOTPVO = new UidVerifyOTPVO();
        uidVerifyOTPVO.setAadhaar_number(uid);
        uidVerifyOTPVO.setConsent("Y");
        uidVerifyOTPVO.setOtp(otp);
        uidVerifyOTPVO.setOtp_tx_id(otpTxtId);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("lendToken", headerLendtoken);
        HttpEntity request = new HttpEntity(uidVerifyOTPVO, headers);

        /*ti print json*/
        ObjectMapper mapper = new ObjectMapper();

//       try {
//           CustomLogger.info("UID OTP request :: " + mapper.writeValueAsString(request));
//
//           RmLogUid rmLogUid = new RmLogUid();
//           rmLogUid.setTypeOfReq(FrameworkAppConstants.UID_DOCUMENT_TYPE_OTP_VERIFY);
//           rmLogUid.setRmDataApplication(new RmDataApplication(applicationId));
//           rmLogUid.setRmDataApplicantDetail(new RmDataApplicantDetail(applicantId));
//           rmLogUid.setRequest(mapper.writeValueAsString(request));
////            rmLogUid.setModId(userId.toString());
//           rmLogUid.setModDate(DateUtility.getCurrentDate());
//           rmLogUid = rmDataDAO.setRmLogUid(rmLogUid);
//       } catch (Exception e) {
////           e.printStackTrace();
//           CustomLogger.error("Save UID Request", e);
//       }
        try {
            op = restTemplate.postForObject(url, request, String.class);
        } catch (Exception e) {
//           e.printStackTrace();
            throw new CustomNonFatalException("Fail to send request", "Fail to send request");
        }

//       try {
//           CustomLogger.info("UID OTP request :: " + mapper.writeValueAsString(request));
//
//           RmLogUid rmLogUid = rmDataDAO.getRmLogUid(applicationId, applicantId, FrameworkAppConstants.UID_DOCUMENT_TYPE_OTP_VERIFY);
//           if (rmLogUid != null) {
//               rmLogUid.setResponse(op);
////            rmLogUid.setModId(userId.toString());
//               rmLogUid.setModDate(DateUtility.getCurrentDate());
//               rmDataDAO.setRmLogUid(rmLogUid);
//           }
//       } catch (Exception e) {
////           e.printStackTrace();
//           CustomLogger.error("Save UID Request", e);
//       }
        return op;
    }

    public List<RmDataApplicantDetail> getRmDataApplicant(Long applicationId) {
        return rmDataDAO.getRMDetais(applicationId);
    }
}
