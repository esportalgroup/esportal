/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.engage.components.core.entity.bean.ESDealerProfile;
import com.engage.components.core.entity.dao.ProfileDetailsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Shrikant Katakdhond
 */
@Service
public class ProfileDetailsHelper {
    
    @Autowired
    ProfileDetailsDao profileDetailsDao;
    
    
    public ESDealerProfile getDealerProfileBYDealerId(String userName) {
        ESDealerProfile dealerProfile = profileDetailsDao.getDealerProfileBYDealerId(userName);
        return dealerProfile;
    }
}
