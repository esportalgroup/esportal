/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.AESAlgoClass;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsMstrAccount;
import com.engage.components.core.entity.bean.EsMstrApfDisbursementSchedule;
import com.engage.components.core.entity.bean.EsMstrApfFlatConfiguration;
import com.engage.components.core.entity.bean.EsMstrApfFloorRiseChargeC;
import com.engage.components.core.entity.bean.EsMstrApfProject;
import com.engage.components.core.entity.bean.EsMstrApfPromotionC;
import com.engage.components.core.entity.bean.EsMstrApfTowerConfiguration;
import com.engage.components.core.entity.bean.EsMpngBuilderSchemeMappingC;
import com.engage.components.core.entity.bean.EsMstrApfTowerPlcC;
import com.engage.components.core.entity.bean.EsMstrDealerBean;
import com.engage.components.core.entity.bean.EsMstrSchemeC;
import com.engage.components.core.entity.bean.EsTokenConstants;
import com.engage.components.core.entity.dao.MsterSyncDao;
import com.engage.framework.util.log4j.CustomLogger;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import com.engage.components.core.util.excepton.CustomFatalException;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.ProfileDetailsVO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.springframework.web.client.HttpStatusCodeException;

/**
 *
 * @author VEN00288
 */
@Service
public class MsterSyncHelper {

    @Autowired
    MsterSyncDao mstrSyncDao;

    @Autowired
    PasswordResetHelper passwordResetHelper;

    public String getAuthToken() {

        List<EsTokenConstants> esTokenConstants = mstrSyncDao.getEsTokenConstants(FrameworkAppConstants.TOKEN_TYPE);

//        System.out.println("esTokenConstants :::::::::" + esTokenConstants);
        String username = "appintegrationuser@ltfs.com", password = "Ltfs@12345", client_id = "3MVG9d8..z.hDcPJo6ow52N5c1z.ywS97.LcIRcHbxbtrSNPTxidiBJseHWqIgMd118KVg8tZo3a._YbcyFKC", client_secret = "4575468882273673935", grant_type = "password",
                token_url = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9d8..z.hDcPJo6ow52N5c1z.ywS97.LcIRcHbxbtrSNPTxidiBJseHWqIgMd118KVg8tZo3a._YbcyFKC&client_secret=4575468882273673935&username=appintegrationuser@ltfs.com&password=Ltfs@12345&redirect_uri=https://test.salesforce.com/services/oauth2/callback";

        if (esTokenConstants != null) {
            for (EsTokenConstants tokenConstant : esTokenConstants) {
                if (tokenConstant.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_USERNAME)) {
                    username = tokenConstant.getProperyValue();
                }

                if (tokenConstant.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_PASSWORD)) {
                    password = tokenConstant.getProperyValue();
                }

                if (tokenConstant.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_CLIENTID)) {
                    client_id = tokenConstant.getProperyValue();
                }

                if (tokenConstant.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_CLIENT_SECRET)) {
                    client_secret = tokenConstant.getProperyValue();
                }

                if (tokenConstant.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_GRANT_TYPE)) {
                    grant_type = tokenConstant.getProperyValue();
                }

                if (tokenConstant.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_TOKEN_URL)) {
                    token_url = tokenConstant.getProperyValue();
                }
            }
        }
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add(FrameworkAppConstants.TOKEN_USERNAME, username);
        map.add(FrameworkAppConstants.TOKEN_PASSWORD, password);
        map.add(FrameworkAppConstants.TOKEN_CLIENTID, client_id);
        map.add(FrameworkAppConstants.TOKEN_CLIENT_SECRET, client_secret);
        map.add(FrameworkAppConstants.TOKEN_GRANT_TYPE, grant_type);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        String op = "", token = "";
        try {
            op = restTemplate.postForObject(token_url, request, String.class);

//            System.out.println("Token Obj:::::::::::;!!" + op);
            JSONObject tokenResponse = new JSONObject(op);
            if (tokenResponse.has("access_token")) {
                token = tokenResponse.get("access_token").toString();
//                System.out.println("Token :::::::::::;!!" + token);

            }
        } catch (RestClientException e) {
            CustomLogger.error("Token Generation Error", e);
        } catch (JSONException e) {
            CustomLogger.error("Token Generation Error", e);
        }

        return token;
    }

    public String synsMstrs(String token) {
//        System.out.println("token @@@@@" + token);
        String uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Id+,+Name+,+Employer_Category__c+,+Employer_Id__c+,+Employer_Ranking__c+,+Employer_Rating__c+,+Employer_Name__c+,+Relationship__c+,+Occupation__c+,+Marital_Status__c+,+Prefered_Language__c+,+Industry+,+AnnualRevenue+,+NumberOfEmployees+,+BillingState+,+ShippingState+from+Account+WHERE+RecordType.Name='Supplier'";
        try {

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("authorization", "OAuth " + token);
//            headers.add("content-type", "application/json; charset=UTF-8");
//            headers.add("accept-encoding", "gzip, deflate");
//            headers.add("accept-language", "en-US,en;q=0.8");
//            headers.add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");

            HttpEntity entity = new HttpEntity(headers);

            ResponseEntity result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.GET, entity, String.class);

            JSONObject myObject = new JSONObject(result.getBody().toString());

//            System.out.println("myObject >>>>>>>>>" + myObject);
            org.json.JSONArray array = myObject.getJSONArray("records");

//            System.out.println("array >>>>>>>>>" + array + array.length());
            String response = mstrSyncDao.syncDataToLocal(array);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }

    HashMap<String, String> getSyncMstrLogin(String id, String role) throws CustomNonFatalException {
        String uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Id+,+Name+,+Employer_Category__c+,+Employer_Id__c+,+Employer_Ranking__c+,+Employer_Rating__c+,+Employer_Name__c+,+Relationship__c+,+Occupation__c+,+Marital_Status__c+,+Prefered_Language__c+,+Industry+,+AnnualRevenue+,+NumberOfEmployees+,+BillingState+,+ShippingState+from+Account+WHERE+Id=".concat("'") + id.concat("'") + "+and+RecordType.Name='Supplier'";
        String password = "";
        String resMessage = "";
        HashMap<String, String> resMap = null;
        FrameworkMstrLogin loginMstr = null;
        JSONObject obj = null;
        JSONObject dataObj = null;
        String response = null;
        ResponseEntity result = null;
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("authorization", "OAuth " + getAuthToken());

            HttpEntity entity = new HttpEntity(headers);

            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.GET, entity, String.class);

            obj = new JSONObject(result.getBody().toString());
            org.json.JSONArray array = obj.getJSONArray("records");
//            System.out.println("obj" + obj);
            dataObj = (JSONObject) array.get(0);
//            System.out.println("dataObj>>>>>>>>>>>>>>>" + dataObj);

            resMap = new HashMap<String, String>();
            if (null != dataObj && dataObj.length() != 0) {
                password = passwordResetHelper.generateRandomPassword();
                resMap.put("password", password);
                loginMstr = new FrameworkMstrLogin();
                loginMstr.setUserName(dataObj.get("Id").toString());
//                System.out.println("Encrypted password" + AESAlgoClass.encrypt(password));
                loginMstr.setUserPassword(AESAlgoClass.encrypt(password));
                loginMstr.setCreTime(DateUtility.getCurrentDate());
                loginMstr.setModTime(DateUtility.getCurrentDate());
                loginMstr.setIsActive('Y');
                loginMstr.setUserProfile(Long.parseLong("1"));
                loginMstr.setRole(role);
                loginMstr.setUserType(Long.parseLong("1"));

                response = mstrSyncDao.setFrameworkMstrLogin(loginMstr);

                EsMstrDealerBean dealerData = new EsMstrDealerBean();

                if (response.equals("Success")) {
                    dealerData.setDealerId(dataObj.get("Id").toString());
                    dealerData.setName(dataObj.get("Name").toString());
                    dealerData.setEmployerCategory(checkNull(dataObj.get("Employer_Category__c").toString()));
                    dealerData.setEmployerId(checkNull(dataObj.get("Employer_Id__c").toString()));
                    dealerData.setEmployerName(checkNull(dataObj.get("Employer_Name__c").toString()));
                    dealerData.setEmployerRanking(checkNull(dataObj.get("Employer_Ranking__c").toString()));
                    dealerData.setEmployerRating(checkNull(dataObj.get("Employer_Rating__c").toString()));
                    dealerData.setRelationship(checkNull(dataObj.get("Relationship__c").toString()));
                    dealerData.setOccupation(checkNull(dataObj.get("Occupation__c").toString()));
                    dealerData.setMaritalStatus(checkNull(dataObj.get("Marital_Status__c").toString()));
                    dealerData.setPreferedLanguage(checkNull(dataObj.get("Prefered_Language__c").toString()));
                    dealerData.setIndustry(checkNull(dataObj.get("Industry").toString()));
                    dealerData.setAnnualRevenue(checkNull(dataObj.get("AnnualRevenue").toString()));
                    dealerData.setNumberOfEmployees(checkNull(dataObj.get("NumberOfEmployees").toString()));
                    dealerData.setBillingState(checkNull(dataObj.get("BillingState").toString()));
                    dealerData.setShippingState(checkNull(dataObj.get("ShippingState").toString()));
                    dealerData.setCreateDate(DateUtility.getCurrentDate());
                    dealerData.setCreateBy("SYSTEM");
                    dealerData.setModDate(DateUtility.getCurrentDate());
                    dealerData.setModBy("SYSTEM");
                    dealerData.setIsActive('Y');
                    dealerData.setValidTill(null);

                    resMessage = mstrSyncDao.setESDealerMstr(dealerData);
                }

            } else {
                throw new CustomNonFatalException("InValid User Name ", "InValid User Name");
            }

            resMap.put("message", "Success");
        } catch (HttpStatusCodeException e) {
//            System.out.println("statusCode>>>>>>>>>>>>>>>" + e.getMessage());
            e.printStackTrace();
            throw new CustomNonFatalException("InValid User Name", "InValid User Name");
        } catch (Exception e) {
            resMap.put("message", "Fails");
            e.printStackTrace();
        }
        return resMap;
    }

    public void getSyncProfileDetailLogin(String id, String role, HttpServletRequest req) throws CustomNonFatalException {

         JSONObject addressDataObj = null;
        JSONObject addressObj = null;
        JSONObject bankDataObj = null;
        JSONObject bankObj = null;
        JSONObject dealerDataObj = null;
        JSONObject dealerObj = null;
        JSONObject attributeObj = null;
        JSONObject attributeUrlDataObj=null;
        ResponseEntity result = null;
        UriComponentsBuilder builder = null;
        ProfileDetailsVO detailsVO = new ProfileDetailsVO();
        String attributeUrl = null;
        RestTemplate restTemplate = null;
        HttpHeaders headers = null;
        HttpEntity entity = null;
        String domain = "https://oneltfs-los.my.salesforce.com";
        //----------------Address details
        String uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Supplier_Id__c+,+Description+,+Sales_ST_Number__c+,+Central_ST_Number__c+,+Income_Tax_Number__c+,+Supplier_Dealer_Flag__c+,+Internal_Flag__c+,+Dealer_Commission__c+,+VAT_Account_Number__c+,+Aggregate_Limit__c+,+OS_RC_AMT__c+,+Active__c+,+Valid_From__c+,+Valid_To__c+,+PAN_ID__c+,+GST_Number__c+,+Registered_Under_GST__c+,+Exemption_Flag__c+,+Exemption_Period_From__c+,+Exemption_Period_To__c+,+RecordTypeId+from+Account+WHERE+Supplier_Id__c=".concat("'") + id.concat("'");
        try{
            
             builder = UriComponentsBuilder.fromUriString(uri);

            restTemplate = new RestTemplate();

            headers = new HttpHeaders();
            headers.add("authorization", "OAuth " + getAuthToken());

//            System.out.println("URL >>>>>>>>  " + builder.buildAndExpand().toUri());
            entity = new HttpEntity(headers);

            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.GET, entity, String.class);

            addressDataObj = new JSONObject(result.getBody().toString());
            
             if (addressDataObj.has("totalSize") && !addressDataObj.get("totalSize").toString().equals("0")) {
                JSONArray addressArray = addressDataObj.getJSONArray("records");

                if (addressArray != null && addressArray.length() > 0) {
                    addressObj = (JSONObject) addressArray.get(0);

                    if (null != addressObj && addressObj.length() != 0) {

                        attributeObj = (JSONObject) addressObj.get("attributes");

                        attributeUrl = attributeObj.getString("url");

                        attributeUrl = domain.concat(attributeUrl);

                      //  System.out.println("attributeUrl >>>>>>>>  " + attributeUrl);

                        builder = UriComponentsBuilder.fromUriString(attributeUrl);

                        restTemplate = new RestTemplate();

                        headers = new HttpHeaders();
                        headers.add("authorization", "OAuth " + getAuthToken());

                        System.out.println("attributeURL >>>>>>>>  " + builder.buildAndExpand().toUri());
                        
                        entity = new HttpEntity(headers);

                        result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.GET, entity, String.class);
                        
                        //System.out.println("attributeURL result>>>>>>>>  " +result);
                        
                        attributeUrlDataObj=new JSONObject(result.getBody().toString());
                        
                       // System.out.println("attributeUrlDataObj>>>>>>>>  " +attributeUrlDataObj);  
                        
                       detailsVO.setAddress_Line1(checkNull(attributeUrlDataObj.get("Address_Line1__c").toString()));
                        detailsVO.setAddress_Line2(checkNull(attributeUrlDataObj.get("Address_Line2__c").toString()));
                        detailsVO.setAddress_Line3(checkNull(attributeUrlDataObj.get("Address_Line3__c").toString()));
//                        detailsVO.setAddress_Line4(checkNull(attributeUrlDataObj.get("Address_Line4__c").toString()));
//                        detailsVO.setAddress_Type(checkNull(attributeUrlDataObj.get("Address_Type__c").toString()));
                        detailsVO.setCity(checkNull(attributeUrlDataObj.get("City__c").toString()));
                        detailsVO.setCountry(checkNull(attributeUrlDataObj.get("Country__c").toString()));
                        detailsVO.setState(checkNull(attributeUrlDataObj.get("State__c").toString()));
                        detailsVO.setSTD_ISD(checkNull(attributeUrlDataObj.get("STD_ISD__c").toString()));
//                        detailsVO.setPhone(checkNull(attributeUrlDataObj.get("Phone__c").toString()));

//                        detailsVO.setClcommon_Account(checkNull(attributeUrlDataObj.get("clcommon__Account__c").toString()));
//                        detailsVO.setClcommon_Bank_Name(checkNull(attributeUrlDataObj.get("clcommon__Bank_Name__c").toString()));
//                        detailsVO.setBranch_Name(checkNull(attributeUrlDataObj.get("Branch_Name__c").toString()));
//                        detailsVO.setLocation(checkNull(attributeUrlDataObj.get("Location__c").toString()));
//                        detailsVO.setClcommon_Bank_Account_Number(checkNull(attributeUrlDataObj.get("clcommon__Bank_Account_Number__c").toString()));
//                        detailsVO.setPayment_Mode(checkNull(attributeUrlDataObj.get("Payment_Mode__c").toString()));
//                        detailsVO.setClcommon_Bank_Account_Name(checkNull(attributeUrlDataObj.get("clcommon__Bank_Account_Name__c").toString()));

                        detailsVO.setSupplier_Id(checkNull(attributeUrlDataObj.get("Supplier_Id__c").toString()));
                        detailsVO.setDescription(checkNull(attributeUrlDataObj.get("Description").toString()));
                        detailsVO.setSales_ST_Number(checkNull(attributeUrlDataObj.get("Sales_ST_Number__c").toString()));
                        detailsVO.setCentral_ST_Number(checkNull(attributeUrlDataObj.get("Central_ST_Number__c").toString()));
                        detailsVO.setIncome_Tax_Number(checkNull(attributeUrlDataObj.get("Income_Tax_Number__c").toString()));
                        detailsVO.setSupplier_Dealer_Flag(checkNull(attributeUrlDataObj.get("Supplier_Dealer_Flag__c").toString()));
                        detailsVO.setInternal_Flag(checkNull(attributeUrlDataObj.get("Internal_Flag__c").toString()));
                        detailsVO.setDealer_Commission(checkNull(attributeUrlDataObj.get("Dealer_Commission__c").toString()));
                        detailsVO.setVAT_Account_Number(checkNull(attributeUrlDataObj.get("VAT_Account_Number__c").toString()));
                        detailsVO.setAggregate_Limit(checkNull(attributeUrlDataObj.get("Aggregate_Limit__c").toString()));
                        detailsVO.setOS_RC_AMT(checkNull(attributeUrlDataObj.get("OS_RC_AMT__c").toString()));
                        detailsVO.setActive(checkNull(attributeUrlDataObj.get("Active__c").toString()));
                        detailsVO.setValid_From(checkNull(attributeUrlDataObj.get("Valid_From__c").toString()));
                        detailsVO.setValid_To(checkNull(attributeUrlDataObj.get("Valid_To__c").toString()));
                        detailsVO.setPAN_ID(checkNull(attributeUrlDataObj.get("PAN_ID__c").toString()));
                        detailsVO.setGST_Number(checkNull(attributeUrlDataObj.get("GST_Number__c").toString()));
                        detailsVO.setRegistered_Under_GST(checkNull(attributeUrlDataObj.get("Registered_Under_GST__c").toString()));
                        detailsVO.setExemption_Flag(checkNull(attributeUrlDataObj.get("Exemption_Flag__c").toString()));
                        detailsVO.setExemption_Period_From(checkNull(attributeUrlDataObj.get("Exemption_Period_From__c").toString()));
                        detailsVO.setExemption_Period_To(checkNull(attributeUrlDataObj.get("Exemption_Period_To__c").toString()));
                        detailsVO.setRecordTypeId(checkNull(attributeUrlDataObj.get("RecordTypeId").toString()));
                        
                        req.getSession().setAttribute("profileDetails", detailsVO);
                        
                    }
//            else {
//                throw new CustomNonFatalException("InValid User Name ", "InValid User Name");
//            }
                }
            }
             
        } catch (HttpStatusCodeException e) {
            e.printStackTrace();
            throw new CustomNonFatalException("InValid User Name", "InValid User Name");
        } catch (Exception e) {
            e.printStackTrace();
        }
//        return resMap;
    }

    private String checkNull(Object data) {
        if (data == null) {
            return null;
        } else {
            return data.toString();
        }
    }

    HashMap<String, String> getBuilderSyncMstrLogin(String id, String role) throws CustomNonFatalException {

        String uri = null;
        String password = "";
        String resMessage = "";
        HashMap<String, String> resMap = null;
        FrameworkMstrLogin loginMstr = null;
        JSONObject obj = null;
        JSONObject dataObj = null;
        JSONObject attributeObj = null;
        String response = "";
        String builderID = null;
        String apfProjectC = null;
        String towerC = null;
        String schemeC = null;
        ResponseEntity result = null;
        org.json.JSONArray array = null;
        org.json.JSONArray apfProjectArray = null;
        List<String> attrHoldLst = new ArrayList<String>();
        List<String> apfProjectCLst = new ArrayList<String>();
        List<String> schemeIdcMpngLst = new ArrayList<String>();
        org.json.JSONArray apfFlatConfigArray = null;
        org.json.JSONArray apfFloorArray = null;
        org.json.JSONArray apfTwrPlcArray = null;
        org.json.JSONArray apfDisburseSchArray = null;
        org.json.JSONArray apfPromotionArray = null;
        org.json.JSONArray bulSchemeMpngArray = null;
        org.json.JSONArray apfSchemeArray = null;
        EsMstrAccount mstrAccount = null;
        EsMstrSchemeC scheme = null;
        EsMstrApfProject apfProject = null;
        EsMstrApfTowerConfiguration twrConfig = null;
        EsMpngBuilderSchemeMappingC schemeMpng = null;
        EsMstrApfFloorRiseChargeC floorRiseCharge = null;
        EsMstrApfTowerPlcC aptTwrPlc = null;
        try {

            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Builder_Id__c+,+Name+,+Builder_Code__c+,+APF_Status__c+,+Builder_Contact__c+,+Description+from+Account+WHERE+RecordType.Name='Builder'+and+Account.Builder_Code__c=".concat("'") + id.concat("'");
//            System.out.println("BUL uri" + uri);
            obj = callExternalWScallExternalWS(uri);
            array = obj.getJSONArray("records");
//            System.out.println("obj" + obj);
            dataObj = (JSONObject) array.get(0);
//            System.out.println("dataObj>>>>>>>>>>>>>>>" + dataObj);

            resMap = new HashMap<String, String>();
            if (array != null && array.length() != 0) {
                if (null != dataObj && dataObj.length() != 0) {

                    attributeObj = dataObj.getJSONObject("attributes");

                    builderID = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                    System.out.println("buldAttributUrl>>>>>>>>>>>>>>>" + builderID);

                    password = passwordResetHelper.generateRandomPassword();
                    resMap.put("password", password);
                    loginMstr = new FrameworkMstrLogin();
                    loginMstr.setUserName(dataObj.get("Builder_Code__c").toString());
//                    System.out.println("Encrypted password" + AESAlgoClass.encrypt(password));
                    loginMstr.setUserPassword(AESAlgoClass.encrypt(password));
                    loginMstr.setCreTime(DateUtility.getCurrentDate());
                    loginMstr.setModTime(DateUtility.getCurrentDate());
                    loginMstr.setIsActive('Y');
                    loginMstr.setUserProfile(Long.parseLong("2"));
                    loginMstr.setRole(role);
                    loginMstr.setUserType(Long.parseLong("2"));

                    response = mstrSyncDao.setFrameworkMstrLogin(loginMstr);

                }

                mstrAccount = new EsMstrAccount();

                if (response.equals("Success")) {
                    response = "";
                    mstrAccount.setBuilderC(builderID);
                    mstrAccount.setBuilderIdC(dataObj.get("Builder_Id__c").toString());
                    mstrAccount.setAccountName(dataObj.get("Name").toString());
                    mstrAccount.setBuilderCode(dataObj.get("Builder_Code__c").toString());
                    mstrAccount.setBuilderContact(dataObj.get("Builder_Contact__c").toString());
                    mstrAccount.setApfStatus(dataObj.get("APF_Status__c").toString());
                    mstrAccount.setDescription(dataObj.get("Description").toString());
                    mstrAccount.setAttributeId(builderID);
                    mstrAccount.setCreDate(DateUtility.getCurrentDate());
                    mstrAccount.setModDate(DateUtility.getCurrentDate());
//                    mstrAccount.setIsActive('Y');

                    response = mstrSyncDao.setESAccountMstr(mstrAccount);

                    uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Address_Line1__c+,+Builder__c+,+APF_Code__c+,+Address_Line2__c+,+Address_Line3__c+,+City__c+,+Country__c+,+Current_Construction_Stage__c+,+Landmark_If_Any__c+,+Latitude__c+,+Longitude__c+,+Number_Of_Towers__c+,+PinCode__c+,+Project_Name__c+,+State__c+,+Technical_Status__c+,+Technical_Valuation_Date__c+from+APF_Project__c+WHERE+APF_Project__c.Builder__c=".concat("'") + builderID.concat("'");
                    JSONObject apfProjectobj = callExternalWScallExternalWS(uri);
//                    System.out.println("apfProjectobj" + apfProjectobj);
                    apfProjectArray = apfProjectobj.getJSONArray("records");
                    if (apfProjectArray != null && apfProjectArray.length() != 0) {
                        for (int i = 0; i < apfProjectArray.length(); i++) {
                            dataObj = (JSONObject) apfProjectArray.get(i);
//                            System.out.println("dataObj apf>>>>>>>>>>>>>>>" + dataObj);

                            attributeObj = dataObj.getJSONObject("attributes");
                            apfProjectC = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
                            apfProjectCLst.add(apfProjectC);
//                            System.out.println("apfProjectCLst>>>>>>>>>>>>>>>" + apfProjectCLst.get(i));

                            apfProject = new EsMstrApfProject();
                            apfProject.setAddressLine1(dataObj.get("Address_Line1__c").toString());
                            apfProject.setAddressLine2(dataObj.get("Address_Line2__c").toString());
                            apfProject.setAddressLine3(dataObj.get("Address_Line3__c").toString());
                            apfProject.setBuilderId(dataObj.get("Builder__c").toString()); //builder_c
                            apfProject.setApfCode(dataObj.get("APF_Code__c").toString());
                            apfProject.setCity(dataObj.get("City__c").toString());
                            apfProject.setCountry(dataObj.get("Country__c").toString());
                            apfProject.setCurrentConstructionStage(dataObj.get("Current_Construction_Stage__c").toString());
                            apfProject.setLandmark(dataObj.get("Landmark_If_Any__c").toString());
                            apfProject.setLatitude(dataObj.get("Latitude__c").toString());
                            apfProject.setLongitude(dataObj.get("Longitude__c").toString());
                            apfProject.setNumberOfTowers(dataObj.get("Number_Of_Towers__c").toString());
                            apfProject.setPinCode(dataObj.get("PinCode__c").toString());
                            apfProject.setProjectName(dataObj.get("Project_Name__c").toString());
                            apfProject.setState(dataObj.get("State__c").toString());
                            apfProject.setTechnicalStatus(dataObj.get("Technical_Status__c").toString());
                            apfProject.setTechnicalValuationDate(dataObj.get("Technical_Valuation_Date__c").toString());
                            apfProject.setApfProjectC(apfProjectC);
                            apfProject.setCreDate(DateUtility.getCurrentDate());
                            apfProject.setModDate(DateUtility.getCurrentDate());
                            apfProject.setAttributeId(apfProjectC);
                            response = mstrSyncDao.setESApfMstr(apfProject);

                        }

                        for (int g = 0; g < apfProjectCLst.size(); g++) {
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+APF_Code__c+,+APF_Project__c+,+Current_Construction_Stage__c+,+Parking_Charges_Per_Car_Park__c+,+Parking_Type__c+,+Technical_status__c+,+Technical_valuation_date__c+,+Tower_Code__c+,+Tower_Name__c+from+APF_Tower_Configuration__c+WHERE+APF_Tower_Configuration__c.APF_Project__c=".concat("'") + apfProjectCLst.get(g).concat("'");
                            JSONObject apfTwrConfig = callExternalWScallExternalWS(uri);
//                            System.out.println("apfProjectobj" + apfProjectobj);
                            array = apfTwrConfig.getJSONArray("records");
                            if (array != null && array.length() != 0) {
                                for (int j = 0; j < array.length(); j++) {

                                    dataObj = (JSONObject) array.get(j);
//                                    System.out.println("dataObj apf>>>>>>>>>>>>>>>" + dataObj);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    towerC = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
                                    attrHoldLst.add(towerC);
//                                    System.out.println("towerCntr====>>>>>>>>>>>>>>>" + j + "tc" + towerC + "Array" + attrHoldLst.get(j));

                                    twrConfig = new EsMstrApfTowerConfiguration();
                                    twrConfig.setApfCode(dataObj.get("APF_Code__c").toString());
                                    twrConfig.setApfProjectC(apfProjectC);
                                    twrConfig.setCurrentConstructionStage(dataObj.get("Current_Construction_Stage__c").toString());
                                    twrConfig.setParkingChargesPerCarPark(dataObj.get("Parking_Charges_Per_Car_Park__c").toString());
                                    twrConfig.setParkingType(dataObj.get("Parking_Type__c").toString());
                                    twrConfig.setTechnicalStatus(dataObj.get("Technical_status__c").toString());
                                    twrConfig.setTechnicalValuationDate(dataObj.get("Technical_valuation_date__c").toString());
                                    twrConfig.setTowerCode(dataObj.get("Tower_Code__c").toString());
                                    twrConfig.setTowerName(dataObj.get("Tower_Name__c").toString());
                                    twrConfig.setCreDate(DateUtility.getCurrentDate());
                                    twrConfig.setModDate(DateUtility.getCurrentDate());
                                    twrConfig.setAttributeId(towerC);
                                    response = mstrSyncDao.setESTowerConfigMstr(twrConfig);

                                }

                            }
                        }

                        for (int z = 0; z < attrHoldLst.size(); z++) {
//                            System.out.println("array ele for flat config :::::::" + attrHoldLst.size() + attrHoldLst.get(z));
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Base_Price_per_sq_m__c+,+Base_Price_Area__c+,+Carpet_Area_sq_m__c+,+Flat_Configuration__c+,+Super_Built_up_Area_sq_m__c+,+Tower__c+,+Tower_Code__c+from+APF_Flat_Configuration__c+WHERE+APF_Flat_Configuration__c.Tower__c=".concat("'") + attrHoldLst.get(z).concat("'");
                            JSONObject apfFlatConfig = callExternalWScallExternalWS(uri);
                            apfFlatConfigArray = apfFlatConfig.getJSONArray("records");
                            if (apfFlatConfigArray != null && apfFlatConfigArray.length() != 0) {
                                for (int k = 0; k < apfFlatConfigArray.length(); k++) {
                                    dataObj = (JSONObject) apfFlatConfigArray.get(k);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    String flatAtt = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                                    System.out.println("flatAttUrlCntr>>>>>>>>>>>>>>>" + k + "flatAtt" + flatAtt);

                                    EsMstrApfFlatConfiguration flatConfig = new EsMstrApfFlatConfiguration();
                                    flatConfig.setBasePriceArea(dataObj.get("Base_Price_Area__c").toString());
                                    flatConfig.setBasePricePerSqM(dataObj.get("Base_Price_per_sq_m__c").toString());
                                    flatConfig.setCarpetAreaSqM(dataObj.get("Carpet_Area_sq_m__c").toString());
                                    flatConfig.setFlatConfiguration(dataObj.get("Flat_Configuration__c").toString());
                                    flatConfig.setSuperBuiltUpAreaSqM(dataObj.get("Super_Built_up_Area_sq_m__c").toString());
                                    flatConfig.setTowerC(dataObj.get("Tower__c").toString());//
                                    flatConfig.setTowerCodeC(dataObj.get("Tower_Code__c").toString());
                                    flatConfig.setCreDate(DateUtility.getCurrentDate());
                                    flatConfig.setModDate(DateUtility.getCurrentDate());
                                    flatConfig.setAttributeId(flatAtt);

                                    response = mstrSyncDao.setESFlatConfigMstr(flatConfig);
                                }
                            }

                        }

                        for (int w = 0; w < attrHoldLst.size(); w++) {
//                            System.out.println("array ele for floor rise  :::::::" + attrHoldLst.size() + attrHoldLst.get(w));
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+APF_Tower_Configuration__c+,+End_Floor__c+,+Floor_Rise_Charge_Per_Sq_M__c+,+Start_Floor__c+,+Tower_Code__c+from+APF_Floor_Rise_Charge__c+WHERE+APF_Floor_Rise_Charge__c.APF_Tower_Configuration__c=".concat("'") + attrHoldLst.get(w).concat("'");
                            JSONObject apfFloorData = callExternalWScallExternalWS(uri);
//                            System.out.println("apfFloorData" + apfFloorData);
                            apfFloorArray = apfFloorData.getJSONArray("records");
                            if (apfFloorArray != null && apfFloorArray.length() != 0) {

                                for (int m = 0; m < apfFloorArray.length(); m++) {
                                    dataObj = (JSONObject) apfFloorArray.get(m);
//                                    System.out.println("dataObj apf>>>>>>>>>>>>>>>" + dataObj);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    String floorRiseAtt = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                                    System.out.println("floorRiseAttCntr>>>>>>>>>>>>>>>" + m + "floorRiseAtt" + floorRiseAtt);

                                    floorRiseCharge = new EsMstrApfFloorRiseChargeC();
                                    floorRiseCharge.setApfTowerConfigurationC(dataObj.get("APF_Tower_Configuration__c").toString());//Tower_c
                                    floorRiseCharge.setEndFloorC(dataObj.get("End_Floor__c").toString());
                                    floorRiseCharge.setFloorRiseChargePerSqMC(dataObj.get("Floor_Rise_Charge_Per_Sq_M__c").toString());
                                    floorRiseCharge.setStartFloorC(dataObj.get("Start_Floor__c").toString());
                                    floorRiseCharge.setTowerCodeC(dataObj.get("Tower_Code__c").toString());
                                    floorRiseCharge.setCreDate(DateUtility.getCurrentDate());
                                    floorRiseCharge.setModDate(DateUtility.getCurrentDate());
                                    floorRiseCharge.setAttributeId(floorRiseAtt);
                                    response = mstrSyncDao.setESFloorRiseChargeC(floorRiseCharge);

                                }
                            }
                        }

                        for (int l = 0; l < attrHoldLst.size(); l++) {
//                            System.out.println("array ele for twr plc   :::::::" + attrHoldLst.size() + attrHoldLst.get(l));
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+APF_Tower_Configuration__c+,+Multiple_PLC_Applicable__c+,+PLC_Cost_Overall__c+,+PLC_Cost_Per_Sq_M__c+,+PLC_Tag__c+,+Tower_Code__c+,+Tower_Name__c+,+Units__c+from+APF_Tower_PLC__c+WHERE+APF_Tower_PLC__c.APF_Tower_Configuration__c=".concat("'") + attrHoldLst.get(l).concat("'");
                            JSONObject apfTwrPlc = callExternalWScallExternalWS(uri);
//                            System.out.println("apfTwrPlc" + apfTwrPlc);

                            apfTwrPlcArray = apfTwrPlc.getJSONArray("records");
                            if (apfTwrPlcArray != null && apfTwrPlcArray.length() != 0) {
                                for (int n = 0; n < apfTwrPlcArray.length(); n++) {
                                    dataObj = (JSONObject) apfTwrPlcArray.get(n);
//                                    System.out.println("dataObj apf>>>>>>>>>>>>>>>" + dataObj);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    String apfTwrPLCAtt = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                                    System.out.println("apfTwrPLCAttCntr>>>>>>>>>>>>>>>" + n + "apfTwrPLCAtt" + apfTwrPLCAtt);

                                    aptTwrPlc = new EsMstrApfTowerPlcC();
                                    aptTwrPlc.setApfTowerConfigurationC(dataObj.get("APF_Tower_Configuration__c").toString());
                                    aptTwrPlc.setMultiplePlcApplicableC(dataObj.get("Multiple_PLC_Applicable__c").toString());
                                    aptTwrPlc.setPlcCostPerSqMC(dataObj.get("PLC_Cost_Per_Sq_M__c").toString());
                                    aptTwrPlc.setPlcCostOverallC(dataObj.get("PLC_Cost_Overall__c").toString());
                                    aptTwrPlc.setPlcTagC(dataObj.get("PLC_Tag__c").toString());
                                    aptTwrPlc.setTowerCodeC(dataObj.get("Tower_Code__c").toString());
                                    aptTwrPlc.setTowerNameC(dataObj.get("Tower_Name__c").toString());
                                    aptTwrPlc.setUnitsC(dataObj.get("Units__c").toString());
                                    aptTwrPlc.setAttributeId(apfTwrPLCAtt);
                                    response = mstrSyncDao.setESapfTowerPlc(aptTwrPlc);
                                }
                            }

                        }

                        for (int m = 0; m < attrHoldLst.size(); m++) {
//                            System.out.println("array ele for disburseSch  :::::::" + attrHoldLst.size() + attrHoldLst.get(m));
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Additional_Comments__c+,+APF_Tower_Configuration__c+,+Construction_Stage__c+,+Customer_Contribution__c+,+L_T_Contribution__c+from+APF_Disbursement_Schedule__c+WHERE+APF_Disbursement_Schedule__c.APF_Tower_Configuration__c=".concat("'") + attrHoldLst.get(m).concat("'");
                            JSONObject apfDisburseSch = callExternalWScallExternalWS(uri);
//                            System.out.println("apfDisburseSch" + apfDisburseSch);
                            apfDisburseSchArray = apfDisburseSch.getJSONArray("records");
                            if (apfDisburseSchArray != null && apfDisburseSchArray.length() != 0) {
                                for (int b = 0; b < apfDisburseSchArray.length(); b++) {

                                    dataObj = (JSONObject) apfDisburseSchArray.get(b);
//                                    System.out.println("dataObj EsMstrApfPromotionC>>>>>>>>>>>>>>>" + dataObj);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    String disburseSchAtt = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                                    System.out.println("disburseSchAttCntr>>>>>>>>>>>>>>>" + b + "disburseSchAtt" + disburseSchAtt);

                                    EsMstrApfDisbursementSchedule disburseSch = new EsMstrApfDisbursementSchedule();
                                    disburseSch.setApfTowerConfigurationC(dataObj.get("APF_Tower_Configuration__c").toString());
                                    disburseSch.setAdditionalCommentsC(dataObj.get("Additional_Comments__c").toString());
                                    disburseSch.setConstructionStageC(dataObj.get("Construction_Stage__c").toString());
                                    disburseSch.setCustomerContributionC(dataObj.get("Customer_Contribution__c").toString());
                                    disburseSch.setLTContributionC(dataObj.get("L_T_Contribution__c").toString());
                                    disburseSch.setCreDate(DateUtility.getCurrentDate());
                                    disburseSch.setModDate(DateUtility.getCurrentDate());
                                    disburseSch.setAttributeId(disburseSchAtt);
                                    response = mstrSyncDao.setEsMstrApfDisbursementSchedule(disburseSch);
                                }
                            }
                        }

                        for (int k = 0; k < apfProjectCLst.size(); k++) {
//                            System.out.println("apfProjectCLst In promotions>>>>>>>>>>>>>>>" + apfProjectCLst.get(k));
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+APF_Code__c+,+APF_Project__c+,+Promotion_Code__c+from+APF_Promotion__c+WHERE+APF_Promotion__c.APF_Project__c=".concat("'") + apfProjectCLst.get(k).concat("'");
                            JSONObject apfPromotion = callExternalWScallExternalWS(uri);
//                            System.out.println("apfPromotion" + apfPromotion);
                            apfPromotionArray = apfPromotion.getJSONArray("records");
                            if (apfPromotionArray != null && apfPromotionArray.length() != 0) {
                                for (int a = 0; a < apfPromotionArray.length(); a++) {
                                    dataObj = (JSONObject) apfPromotionArray.get(a);
//                                    System.out.println("dataObj EsMstrApfPromotionC>>>>>>>>>>>>>>>" + dataObj);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    String promotionAtt = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                                    System.out.println("promotionAtt>>>>>>>>>>>>>>>" + promotionAtt);
                                    EsMstrApfPromotionC promotion = new EsMstrApfPromotionC();
                                    promotion.setApfCodeC(dataObj.get("PLC_Cost_Overall__c").toString());
                                    promotion.setApfProjectC(apfProjectC);
                                    promotion.setPromotionCodeC(dataObj.get("Promotion_Code__c").toString());
                                    promotion.setCreDate(DateUtility.getCurrentDate());
                                    promotion.setModDate(DateUtility.getCurrentDate());
                                    promotion.setAttributeId(promotionAtt);
                                    response = mstrSyncDao.setESPromotions(promotion);
                                }
                            }
                        }

                        for (int v = 0; v < apfProjectCLst.size(); v++) {
//                            System.out.println("apfProjectCLst In bulSchemeMpng>>>>>>>>>>>>>>>" + apfProjectCLst.get(v));
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+APF_Project__c+,+Builder__c+,+Builder_Id__c+,+Scheme__c+,+Scheme_Id__c+,+Tower_Code__c+from+Builder_Scheme_Mapping__c+WHERE+Builder_Scheme_Mapping__c.Builder__c=".concat("'") + builderID.concat("'") + "and Builder_Scheme_Mapping__c.APF_Project__c=".concat("'") + apfProjectCLst.get(v).concat("'");
                            JSONObject bulSchemeMpng = callExternalWScallExternalWS(uri);
//                            System.out.println("bulSchemeMpng" + bulSchemeMpng);
                            bulSchemeMpngArray = bulSchemeMpng.getJSONArray("records");
                            if (bulSchemeMpngArray != null && bulSchemeMpngArray.length() != 0) {

                                for (int d = 0; d < bulSchemeMpngArray.length(); d++) {
                                    dataObj = (JSONObject) bulSchemeMpngArray.get(d);
//                                    System.out.println("dataObj bulSchemeMpng>>>>>>>>>>>>>>>" + dataObj);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    String bulMpngC = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                                    System.out.println("bulMpngC>>>>>>>>>>>>>>>" + bulMpngC);
                                    schemeMpng = new EsMpngBuilderSchemeMappingC();
                                    schemeMpng.setBuilderC(dataObj.get("Builder__c").toString());
                                    schemeMpng.setBuilderIdC(mstrAccount.getBuilderIdC());
                                    schemeMpng.setApfProjectC(dataObj.get("APF_Project__c").toString());
                                    schemeMpng.setAttributeId(bulMpngC);
                                    schemeMpng.setSchemeC(dataObj.get("Scheme__c").toString());
                                    schemeMpng.setSchemeIdC(dataObj.get("Scheme_Id__c").toString());
                                    schemeMpng.setTowerCodeC(dataObj.get("Tower_Code__c").toString());//Tower_Code__c
                                    if (schemeIdcMpngLst.isEmpty()) {
                                        schemeIdcMpngLst.add(dataObj.get("Scheme_Id__c").toString());
                                    } else {
                                        for (int y = 0; y < schemeIdcMpngLst.size(); y++) {
                                            if (!dataObj.get("Scheme_Id__c").toString().equals(schemeIdcMpngLst.get(y))) {
                                                schemeIdcMpngLst.add(dataObj.get("Scheme_Id__c").toString());
                                            }
                                        }
                                    }

                                    response = mstrSyncDao.setEsSMpngBulScheme(schemeMpng);

                                }
                            }
                        }

                        for (int w = 0; w < schemeIdcMpngLst.size(); w++) {
//                            System.out.println("schemeIdcMpngLst In bulSchemeMpng>>>>>>>>>>>>>>>" + schemeIdcMpngLst.get(w));
                            uri = "https://oneltfs-los.my.salesforce.com/services/data/v39.0/query?q=SELECT+Accrual_On_Billing__c+,+Accrual_On_Monthend__c+,+Accrual_Reversal__c+,+Adjust_Excess_Int__c+,+Amount_Exposure__c+,+Capitalize_Interset__c+,+Charge_Int_During_Moratorium_Grace_Perid__c+,+CurrencyID__c+,+Days_Per_Year__c+,+Default_Tenure_For_This_Offering__c+,+Floating_Rate_Flag__c+,+Foreclosure_Locking_Period__c+,+Grace_Period__c+,+Impact_On_Tenure__c+,+Installment_Mode__c+,+Installment_Plan__c+,+Installment_Rounding_Method__c+,+Installment_Rounding_Till_Decimal__c+,+Interest_Rate__c+,+Interest_Rounding_Method__c+,+Interest_Rounding_Till_Decimal__c+,+Interest_Type__c\n"
                                    + "+,+Maximum_Amount_Finance_In_This_Offering__c+,+Maximum_Interest_Rate__c+,+Max_Tenure_For_This_Offering__c+,+Method_Of_Computing_Days_In_A_Month__c+,+Minimum_Amount_Finance_In_This_Offering__c+,+Minimum_Interest_Rate__c+,+Minimum_Number_Of_Advance_Installments__c+,+Min_Tenure_In_Months_For_This_Offering__c+,+Moratorium_Allowed__c+,+Number_Of_Installment__c+,+Payment_Mode__c+,+PDC_Flag__c+,+PLR_Type__c+,+Prepayment_Penalty_Rate__c+,+Product_Code__c+,+Repayment_Frequency__c\n"
                                    + "+,+Repricing_Frequency__c+,+Scheme_Code__c+,+Scheme_Description__c+,+Scheme_End_Date__c+,+Scheme_Group_Name__c+,+Scheme_ID__c+,+Scheme_Start_Date__c+,+Valume_Exposure__c+from+Scheme__c+WHERE+Scheme__c.Scheme_ID__c=" + schemeIdcMpngLst.get(w);// Integer.parseInt(schemeMpng.getSchemeIdC());
                            JSONObject apfScheme = callExternalWScallExternalWS(uri);
//                            System.out.println("apfScheme" + apfScheme);
                            apfSchemeArray = apfScheme.getJSONArray("records");
                            if (apfSchemeArray != null && apfSchemeArray.length() != 0) {
                                for (int c = 0; c < apfSchemeArray.length(); c++) {

                                    dataObj = (JSONObject) apfSchemeArray.get(c);
//                                    System.out.println("dataObj EsMstrApfPromotionC>>>>>>>>>>>>>>>" + dataObj);

                                    attributeObj = dataObj.getJSONObject("attributes");
                                    schemeC = attributeObj.get("url").toString().substring(attributeObj.get("url").toString().lastIndexOf("/") + 1).trim();
//                                    System.out.println("schemeC>>>>>>>>>>>>>>>" + schemeC);
                                    scheme = new EsMstrSchemeC();
                                    scheme.setAccrualOnBillingC(dataObj.get("Accrual_On_Billing__c").toString());
                                    scheme.setAccrualOnMonthendC(dataObj.get("Accrual_On_Monthend__c").toString());
                                    scheme.setAccrualReversalC(dataObj.get("Accrual_Reversal__c").toString());
                                    scheme.setAdjustExcessIntC(dataObj.get("Adjust_Excess_Int__c").toString());
                                    scheme.setAmountExposureC(dataObj.get("Amount_Exposure__c").toString());
                                    scheme.setAttributeId(schemeC);
                                    scheme.setCapitalizeIntersetC(dataObj.get("Capitalize_Interset__c").toString());
                                    scheme.setChargeIntDuringMoratoriumGracePeridC(dataObj.get("Charge_Int_During_Moratorium_Grace_Perid__c").toString());
                                    scheme.setCurrencyIdC(dataObj.get("CurrencyID__c").toString());
                                    scheme.setDaysPerYearC(dataObj.get("Days_Per_Year__c").toString());
                                    scheme.setDefaultTenureForThisOfferingC(dataObj.get("Default_Tenure_For_This_Offering__c").toString());
                                    scheme.setFloatingRateFlagC(dataObj.get("Floating_Rate_Flag__c").toString());
                                    scheme.setForeclosureLockingPeriodC(dataObj.get("Foreclosure_Locking_Period__c").toString());
                                    scheme.setGracePeriodC(dataObj.get("Grace_Period__c").toString());
                                    scheme.setImpactOnTenureC(dataObj.get("Impact_On_Tenure__c").toString());
                                    scheme.setInstallmentModeC(dataObj.get("Installment_Mode__c").toString());
                                    scheme.setInstallmentPlanC(dataObj.get("Installment_Plan__c").toString());
                                    scheme.setInstallmentRoundingMethodC(dataObj.get("Installment_Rounding_Method__c").toString());
                                    scheme.setInstallmentRoundingTillDecimalC(dataObj.get("Installment_Rounding_Till_Decimal__c").toString());
                                    scheme.setInterestRateC(dataObj.get("Interest_Rate__c").toString());
                                    scheme.setInterestRoundingMethodC(dataObj.get("Interest_Rounding_Method__c").toString());
                                    scheme.setInterestRoundingTillDecimalC(dataObj.get("Interest_Rounding_Till_Decimal__c").toString());
                                    scheme.setInterestTypeC(dataObj.get("Interest_Type__c").toString());
                                    scheme.setMaxTenureForThisOfferingC(dataObj.get("Max_Tenure_For_This_Offering__c").toString());
                                    scheme.setMaximumAmountFinanceInThisOfferingC(dataObj.get("Maximum_Amount_Finance_In_This_Offering__c").toString());
                                    scheme.setMaximumInterestRateC(dataObj.get("Maximum_Interest_Rate__c").toString());
                                    scheme.setMethodOfComputingDaysInAMonthC(dataObj.get("Method_Of_Computing_Days_In_A_Month__c").toString());
                                    scheme.setMinTenureInMonthsForThisOfferingC(dataObj.get("Min_Tenure_In_Months_For_This_Offering__c").toString());
                                    scheme.setMinimumAmountFinanceInThisOfferingC(dataObj.get("Minimum_Amount_Finance_In_This_Offering__c").toString());
                                    scheme.setMinimumInterestRateC(dataObj.get("Minimum_Interest_Rate__c").toString());
                                    scheme.setMinimumNumberOfAdvanceInstallmentsC(dataObj.get("Minimum_Number_Of_Advance_Installments__c").toString());
                                    scheme.setMoratoriumAllowedC(dataObj.get("Moratorium_Allowed__c").toString());
                                    scheme.setNumberOfInstallmentC(dataObj.get("Number_Of_Installment__c").toString());
                                    scheme.setPdcFlagC(dataObj.get("PDC_Flag__c").toString());
                                    scheme.setPlrTypeC(dataObj.get("PLR_Type__c").toString());
                                    scheme.setPaymentModeC(dataObj.get("Payment_Mode__c").toString());
                                    scheme.setPrepaymentPenaltyRateC(dataObj.get("Prepayment_Penalty_Rate__c").toString());
                                    scheme.setProductCodeC(dataObj.get("Product_Code__c").toString());
                                    scheme.setRepaymentFrequencyC(dataObj.get("Repayment_Frequency__c").toString());
                                    scheme.setRepricingFrequencyC(dataObj.get("Repricing_Frequency__c").toString());
                                    scheme.setSchemeCodeC(dataObj.get("Scheme_Code__c").toString());
                                    scheme.setSchemeDescriptionC(dataObj.get("Scheme_Description__c").toString());
                                    scheme.setSchemeEndDateC(dataObj.get("Scheme_End_Date__c").toString());
                                    scheme.setSchemeGroupNameC(dataObj.get("Scheme_Group_Name__c").toString());
                                    scheme.setSchemeIdC(dataObj.get("Scheme_ID__c").toString());
                                    scheme.setSchemeC(schemeC);
                                    scheme.setSchemeStartDateC(dataObj.get("Scheme_Start_Date__c").toString());
                                    scheme.setValumeExposureC(dataObj.get("Valume_Exposure__c").toString());
                                    scheme.setAttributeId(schemeC);
                                    response = mstrSyncDao.setEsSchemeMstr(scheme);
                                }
                            }

                        }

                    }

                }

            } else {
                throw new CustomNonFatalException("InValid User Name ", "InValid User Name");
            }

            resMap.put("message", response);
        } catch (HttpStatusCodeException e) {
//            System.out.println("statusCode>>>>>>>>>>>>>>>" + e.getMessage());
            e.printStackTrace();
            throw new CustomNonFatalException("InValid User Name", "InValid User Name");
        } catch (Exception e) {
            resMap.put("message", "Fails");
            e.printStackTrace();
        }
        return resMap;

    }

    private JSONObject callExternalWScallExternalWS(String uri) throws CustomNonFatalException {

        JSONObject obj = null;
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("authorization", "OAuth " + getAuthToken());

            HttpEntity entity = new HttpEntity(headers);

            ResponseEntity result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.GET, entity, String.class);

            obj = new JSONObject(result.getBody().toString());
        } catch (HttpStatusCodeException e) {
//            System.out.println("statusCode>>>>>>>>>>>>>>>" + e.getMessage());
            e.printStackTrace();
            throw new CustomNonFatalException("InValid User Name", "InValid User Name");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }

}
