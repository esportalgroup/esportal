/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.ESLogCRMSearchWS;
import com.engage.components.core.entity.bean.EsLogDocumentUpload;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.crm.vo.CRMSearchReq;
import com.engage.components.esportal.vo.uploadDocumentDetailWS;
import com.engage.framework.util.log4j.CustomLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Shrikant Katakdhond
 */
@Service
public class CRMHelper {
    
    public String sendCrmSearchData(CRMSearchReq cRMSearchReq) throws CustomNonFatalException, JsonProcessingException {

        String op = "";
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity(cRMSearchReq, headers);

//        String HLUrlAdd = ApplicationPropertyManager.getProperty(FrameworkAppConstants.DOCUMENT_UPLOAD_URL);
          String HLUrlAdd = "http://10.3.2.38:7801/CUSTOMER_SEARCH_CRM";

        ObjectMapper mapper = new ObjectMapper();
        CustomLogger.info("Send CRM Search Data Request :: " + mapper.writeValueAsString(request));

        ESLogCRMSearchWS cRMSearchWS=new ESLogCRMSearchWS();
        try {
//            cRMSearchWS.setApplicationId(detailWS.getApplicationId());
//            cRMSearchWS.setRequest(mapper.writeValueAsString(request));
//            cRMSearchWS.setRequestTime(DateUtility.getCurrentDate());

            op = restTemplate.postForObject(HLUrlAdd, request, String.class);

//            cRMSearchWS.setResponse(op);
//            cRMSearchWS.setResponseTime(DateUtility.getCurrentDate());
        } catch (Exception e) {
            CustomLogger.error("Customer Search WS Response", e);
        } finally {
            
        }
        return op;
    }
    
}
