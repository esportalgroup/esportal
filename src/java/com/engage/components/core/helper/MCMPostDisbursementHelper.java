/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.McmApplicantsDetails;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmDisbursementDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import com.engage.components.core.entity.dao.MCMPostDisbursementDao;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.mcm.vo.McmApplicantsVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupFormationDocDisbursementVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupFormationVO;
import com.engage.components.esportal.vo.McmGroupDetailVO;
import com.engage.components.esportal.vo.McmGroupMemberVo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author ven00253
 */
@Service
public class MCMPostDisbursementHelper {

    
    @Autowired
    MCMPostDisbursementDao disbursementDao;
    
    public String callGroupFormationService(mcmRequestGroupFormationDocDisbursementVO groupFormationVo) {
        
      String uri = "http://10.3.2.38:7801/LOS_SERVICE";
      ResponseEntity result = null;
      JSONObject obj = null;
      String response="";
     try
     {
        
           ObjectMapper mapper = new ObjectMapper();

              System.out.println("Pre request :: " + mapper.writeValueAsString(groupFormationVo));

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("urltype", "GroupFormation");
            HttpEntity entity = new HttpEntity(groupFormationVo,headers);

            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, String.class);
            obj = new JSONObject(result.getBody().toString());
            String state=obj.get("status").toString();
            
            System.out.println(" Response :: " + result.getBody().toString());
            
            if(state.equalsIgnoreCase("SUCCESS"))
            {
                response="success";
            }else
            {
            response="fails";
            }
     
     }catch(Exception e)
     {
         response="fails";
         e.printStackTrace();
     
     }
    return response;
    }

 
    public McmDisbursementDetails getMCMAppDisbursementDetails(String applicationId) {
       
        McmDisbursementDetails disbursementDetails=disbursementDao.getMCMAppDisbursementDetails(applicationId);
        return disbursementDetails;
    }

    public String initiateEsign(McmDisbursementDetails disbursementDetail) {
        
        String response=disbursementDao.saveInitiateEsignDetails(disbursementDetail);
        
        return response;
    }
 
}
