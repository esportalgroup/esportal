/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.engage.components.core.entity.bean.EsMstrAccount;
import com.engage.components.core.entity.bean.EsMstrApfProject;
import com.engage.components.core.entity.dao.BuilderDetailDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author VEN00288
 */
@Service
public class BuilderDetailHelper {
    
    @Autowired
    BuilderDetailDao builderDetailDao;

    public EsMstrAccount getBuilderAccountsDetails(String builderC) {
     EsMstrAccount accountBean = builderDetailDao.getBuilderAccountsDetails(builderC);
        return accountBean;
    }

    public EsMstrApfProject getEsMstrApfProjectInfo(String builderC) {
         EsMstrApfProject apfProject = builderDetailDao.getEsMstrApfProjectInfo(builderC);
        return apfProject;
    }
    
}
