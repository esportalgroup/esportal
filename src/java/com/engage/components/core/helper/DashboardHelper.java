package com.engage.components.core.helper;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.engage.components.core.entity.dao.DashboardDAO;
import com.engage.components.core.entity.bean.SangamMstrSelectCriteria;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.BuilderDashBoardVO;
import com.engage.components.esportal.vo.DealearPendingReq;
import com.engage.components.esportal.vo.DealerPendingData;
import com.engage.components.esportal.vo.DealerStatusDashboardVO;
import com.engage.components.esportal.vo.McmDashBoardVO;
import com.engage.components.esportal.vo.OpsDashBoardVO;
import com.engage.components.esportal.vo.PendindStatusDashBoardVO;
import com.engage.components.esportal.vo.RCUDashboardVO;
import com.engage.components.esportal.vo.RMDashboardVO;
import com.engage.session.beans.UserInfoBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Shrikant Katakdhond
 */
@Service
public class DashboardHelper {

    @Autowired
    DashboardDAO dashboardDAO;

    @Autowired
    UserInfoBean userInfoBean;

    public List<SangamMstrSelectCriteria> getSelectCriterias(String forDashboard) {
        return dashboardDAO.getSelectCriterias(forDashboard);
    }

    public List<DealerStatusDashboardVO> getDealerDashbord(String userId, Long fromRowNum, Long toRowNum, Long profileID) {
        List<DealerStatusDashboardVO> lst = dashboardDAO.getDealerDashbord(userId, fromRowNum, toRowNum, profileID);
        if (lst != null) {
            return lst;
        } else {
            return null;
        }
    }

    public List<OpsDashBoardVO> getOpsDashbord(String supplierId, Long fromRowNum, Long toRowNum, Character isPending) {
        return dashboardDAO.getOpsDashbord(supplierId, fromRowNum, toRowNum, isPending);
    }
    
    public List<OpsDashBoardVO> getOpsDashbordByApplicationId(String supplierId, Long fromRowNum, Long toRowNum, Character isPending, String ApplicationId){
        return dashboardDAO.getOpsDashbordByApplicationId(supplierId, fromRowNum, toRowNum, isPending, ApplicationId);
    }
    
    public List<OpsDashBoardVO> getOpsDashbordSearch(String supplierId, Long fromRowNum, Long toRowNum, Character isPending, String searchParameter, String searchParameterValue) {
        return dashboardDAO.getOpsDashbordSearch(supplierId, fromRowNum, toRowNum, isPending, searchParameter, searchParameterValue);
    }

    public List<McmDashBoardVO> getMcmDashboard(String supplierId, Long fromRowNum, Long toRowNum, String type, String localityId, String villageName,String mcCode) {
        return dashboardDAO.getMcmDashboard(supplierId, fromRowNum, toRowNum, type, localityId, villageName,mcCode);
    }
    
  
    public List<McmDashBoardVO> getMcmDashbordSearch(Long fromRowNum, Long toRowNum, String mcCode, String searchParameter, String searchParameterValue) {
        return dashboardDAO.getMcmDashbordSearch(fromRowNum, toRowNum, mcCode,searchParameter, searchParameterValue);
    }

    public List<BuilderDashBoardVO> getBuilderDashbord(String userId, Long fromRowNum, Long toRowNum, Long profileID) {

        List<BuilderDashBoardVO> lst = dashboardDAO.getBuilderDashbord(userId, fromRowNum, toRowNum, profileID);
        if (lst != null) {
            return lst;
        } else {
            return null;
        }
    }

    public List<RCUDashboardVO> getRCUTriggerApplications(String userId, Long fromRowNum, Long toRowNum) {
        return dashboardDAO.getRCUTriggerApplications(userId, fromRowNum, toRowNum);
    }

    public List<RMDashboardVO> getRMDashboard(String userId, Long fromRowNum, Long toRowNum) {
        return dashboardDAO.getRMDashboard(userId, fromRowNum, toRowNum);
    }

    List<PendindStatusDashBoardVO> getDlrPendingRecordsFromUrl(HttpServletRequest hrs, DealearPendingReq pendingReq) throws CustomNonFatalException{
        List<PendindStatusDashBoardVO> lstDashboard = null;
        PendindStatusDashBoardVO dashBoardData = null;
        DealerPendingData data = null;
        String uri = "http://10.3.2.38:7800/LOS_SERVICE";
        List<DealerPendingData> lstData = new ArrayList<DealerPendingData>();
        JSONObject obj = null;
        JSONObject dataObj = null;
        ResponseEntity result = null;
        try {
            //System.out.println("Inside Try >>>>>>>>>");
            ObjectMapper mapper = new ObjectMapper();

            //System.out.println("Pre request :: " + mapper.writeValueAsString(pendingReq));
            lstDashboard = new ArrayList<PendindStatusDashBoardVO>();

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("urltype", "getApplications");

            HttpEntity entity = new HttpEntity(pendingReq, headers);

            //System.out.println(" Call WS  >>>>>>>>>");

            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, String.class);
//            System.out.println(" after Call WS  >>>>>>>>>"+result);
            obj = new JSONObject(result.getBody().toString());

//            System.out.println(" WS O/P  obj >>>>>>>>>" + obj);
            org.json.JSONArray array = obj.getJSONArray("Content");
            //System.out.println("obj" + obj);
            int dataLength = array.length();

                for (int j = 0; j < array.length(); j++) {
                    
//                      System.out.println("Inside For :: " +j);
                   
                    dataObj = (JSONObject) array.get(j);
                    dashBoardData = new PendindStatusDashBoardVO();
                    dashBoardData.setApplicationId(checkNull(dataObj.get("Application_ID")));
                    dashBoardData.setApplicatName(checkNull(dataObj.get("Applicant_Name")));
//                    System.out.println("Application Date ::::"+checkNull(dataObj.get("Application_Date")).substring(checkNull(dataObj.get("Application_Date")).lastIndexOf(" ")).trim());
                    dashBoardData.setApplicationDate(checkNull(dataObj.get("Application_Date")).substring(0,checkNull(dataObj.get("Application_Date")).lastIndexOf(" ")).trim());
                    dashBoardData.setDsaName(checkNull(dataObj.get("DSA_Name")));
                    dashBoardData.setFlsName(checkNull(dataObj.get("FLS_Name")));
                    dashBoardData.setProduct(FrameworkAppConstants.PRODUCT_CONS);//"TW"
//                    dashBoardData.setStatus(FrameworkAppConstants.PENDING_STATUS);
                    if(!checkNull(dataObj.get("Invoice_DocID")).equals("")){
                        dashBoardData.setStatus(FrameworkAppConstants.DLR_PENDING_STATUS);
                    }else{
                        dashBoardData.setStatus(FrameworkAppConstants.DLR_SANCTIONED_STATUS);
                    }


                    dashBoardData.setProgress("1");
                    dashBoardData.setCaseCount(dataLength);
                    dashBoardData.setSupplier_ID(checkNull(dataObj.get("Supplier_ID")));
                     dashBoardData.setSupplier_Name(checkNull(dataObj.get("Supplier_Name")));
                     dashBoardData.setStampDuty(checkNull(dataObj.get("StampDuty")));
                     dashBoardData.setProcessing_Fees(checkNull(dataObj.get("Processing_Fees")));
                     dashBoardData.setMarginMoney_DocID(checkNull(dataObj.get("MarginMoney_DocID")));
                     dashBoardData.setMargin_Money(checkNull(dataObj.get("Margin_Money")));
                     dashBoardData.setLoan_Amount(checkNull(dataObj.get("Loan_Amount")));
                     dashBoardData.setId(checkNull(dataObj.get("Id")));
                     dashBoardData.setInvoiceNumber(checkNull(dataObj.get("InvoiceNumber")));
                     dashBoardData.setInvoice_DocID(checkNull(dataObj.get("Invoice_DocID")));
                     dashBoardData.setFLS_ID(checkNull(dataObj.get("FLS_ID")));
                     dashBoardData.setFLS_Name(checkNull(dataObj.get("FLS_Name")));
                     dashBoardData.setECS_charges(checkNull(dataObj.get("ECS_charges")));
                     dashBoardData.setEngineNumber(checkNull(dataObj.get("EngineNumber")));
                     dashBoardData.setDSA_ID(checkNull(dataObj.get("DSA_ID")));
                     dashBoardData.setDSA_Name(checkNull(dataObj.get("DSA_Name")));
                     dashBoardData.setDoc_Charges(checkNull(dataObj.get("Doc_Charges")));
                     dashBoardData.setCLI_Premium(checkNull(dataObj.get("CLI_Premium")));
                     dashBoardData.setChassisNumber(checkNull(dataObj.get("ChassisNumber")));
                     dashBoardData.setCo_Applicant_Name(checkNull(dataObj.get("Co_Applicant_Name")));
                     dashBoardData.setApplicant_Name(checkNull(dataObj.get("Applicant_Name")));
                     dashBoardData.setApplication_Date(checkNull(dataObj.get("Application_Date")));
                     dashBoardData.setApplication_ID(checkNull(dataObj.get("Application_ID")));
                     dashBoardData.setAssetCost_ESR(checkNull(dataObj.get("AssetCost_ESR")));
                     dashBoardData.setAssetCost_Ins(checkNull(dataObj.get("AssetCost_Ins")));
                     dashBoardData.setAssetCost_OnboardingPrice(checkNull(dataObj.get("AssetCost_OnboardingPrice")));
                     dashBoardData.setAssetCost_Tax(checkNull(dataObj.get("AssetCost_Tax")));
                     dashBoardData.setAssetModel(checkNull(dataObj.get("AssetModel")));
                     dashBoardData.setAsset_Make(checkNull(dataObj.get("Asset_Make")));
                     dashBoardData.setAsset_Manufacturer(checkNull(dataObj.get("Asset_Manufacturer")));
                     dashBoardData.setAdvEmi(checkNull(""));
                     
                    
                     
                     dashBoardData.setOther_charges((checkNullCharges(dataObj.get("ECS_charges"))+checkNullCharges(dataObj.get("Doc_Charges"))+checkNullCharges(dataObj.get("StampDuty")))+checkNullCharges(dataObj.get("Processing_Fees")));
                     dashBoardData.setPaymentToDlr(checkNullCharges(dataObj.get("CLI_Premium"))+checkNullCharges(dataObj.get("Margin_Money"))+checkNullCharges(dataObj.get("ECS_charges"))+checkNullCharges(dataObj.get("Doc_Charges"))+checkNullCharges(dataObj.get("StampDuty"))+checkNullCharges(dataObj.get("Processing_Fees")));
                     
//                     System.out.println("other Charges"+dashBoardData.getOther_charges());
                      lstDashboard.add(dashBoardData);
                    
                    
                    //
                     data=new DealerPendingData();
                     data.setSupplier_ID(checkNull(dataObj.get("Supplier_ID")));
                     data.setSupplier_Name(checkNull(dataObj.get("Supplier_Name")));
                     data.setStampDuty(checkNull(dataObj.get("StampDuty")));
                     data.setProcessing_Fees(checkNull(dataObj.get("Processing_Fees")));
                     data.setMarginMoney_DocID(checkNull(dataObj.get("MarginMoney_DocID")));
                     data.setMargin_Money(checkNull(dataObj.get("Margin_Money")));
                     data.setLoan_Amount(checkNull(dataObj.get("Loan_Amount")));
                     data.setId(checkNull(dataObj.get("Id")));
                     data.setInvoiceNumber(checkNull(dataObj.get("InvoiceNumber")));
                     data.setInvoice_DocID(checkNull(dataObj.get("Invoice_DocID")));
                     data.setFLS_ID(checkNull(dataObj.get("FLS_ID")));
                     data.setFLS_Name(checkNull(dataObj.get("FLS_Name")));
                     data.setECS_charges(checkNull(dataObj.get("ECS_charges")));
                     data.setEngineNumber(checkNull(dataObj.get("EngineNumber")));
                     data.setDSA_ID(checkNull(dataObj.get("DSA_ID")));
                     data.setDSA_Name(checkNull(dataObj.get("DSA_Name")));
                     data.setDoc_Charges(checkNull(dataObj.get("Doc_Charges")));
                     data.setCLI_Premium(checkNull(dataObj.get("CLI_Premium")));
                     data.setChassisNumber(checkNull(dataObj.get("ChassisNumber")));
                     data.setCo_Applicant_Name(checkNull(dataObj.get("Co_Applicant_Name")));
                     data.setApplicant_Name(checkNull(dataObj.get("Applicant_Name")));
                     data.setApplication_Date(checkNull(dataObj.get("Application_Date")));
                     data.setApplication_ID(checkNull(dataObj.get("Application_ID")));
                     data.setAssetCost_ESR(checkNull(dataObj.get("AssetCost_ESR")));
                     data.setAssetCost_Ins(checkNull(dataObj.get("AssetCost_Ins")));
                     data.setAssetCost_OnboardingPrice(checkNull(dataObj.get("AssetCost_OnboardingPrice")));
                     data.setAssetCost_Tax(checkNull(dataObj.get("AssetCost_Tax")));
                     data.setAssetModel(checkNull(dataObj.get("AssetModel")));
                     data.setAsset_Make(checkNull(dataObj.get("Asset_Make")));
                     data.setAsset_Manufacturer(checkNull(dataObj.get("Asset_Manufacturer")));
                      lstData.add(data);

            }
            hrs.getSession().setAttribute("dataLst", lstDashboard);
            hrs.getSession().setAttribute("dealerPendingLastHitDate", DateUtility.getCurrentDate().getTime());

            //System.out.println("Set Data in  Session ::>>>>>>>>>>>>>> ");
        } catch (HttpStatusCodeException e) {
//            System.out.println("statusCode>>>>>>>>>>>>>>>" + e.getMessage());
            throw new CustomNonFatalException("Data Not Found", "Data Not Found");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDashboard;
    }
    
    public List<PendindStatusDashBoardVO> getLstPendindStatusDashBoard(HttpServletRequest hrs, String toString, Long fromRowNum, Long toRowNum, Long profileID, DealearPendingReq pendingReq) throws CustomNonFatalException {
        //System.out.println(" Call WS getLstPendindStatusDashBoard  >>>>>>>>>");

        List<PendindStatusDashBoardVO> lstDashboard = null;
        EsApplicantsDetailBean esApplicantsDetailBean = null;

        Boolean allow = true;
        Long lastHitDate = null;
        if (hrs.getSession().getAttribute("dealerPendingLastHitDate") != null) {
//            System.out.println("dealerPendingLastHitDate : " + hrs.getSession().getAttribute("dealerPendingLastHitDate").toString());
            lastHitDate = Long.parseLong(hrs.getSession().getAttribute("dealerPendingLastHitDate").toString());
        }
//        System.out.println("lastHitDate : " + lastHitDate);
        Date currentTime = DateUtility.getCurrentDate();
        if (lastHitDate != null) {
            Long diff = currentTime.getTime() - lastHitDate;
            diff = diff / (60 * 1000) % 60;
            System.out.println("diff : " + diff);
            if (diff < 15L) {
                allow = false;
            }
        }

        if (allow == true) {
//            System.out.println("from url");
            lstDashboard = getDlrPendingRecordsFromUrl(hrs, pendingReq);
        } else {
//            System.out.println("from session");
            lstDashboard = (List<PendindStatusDashBoardVO>) hrs.getSession().getAttribute("dataLst");
//            System.out.println("from session 2............");
            if (lstDashboard != null) {
//                System.out.println("Inside Not Null");
                for (int i = 0; i < lstDashboard.size(); i++) {
                    esApplicantsDetailBean = new EsApplicantsDetailBean();
                    esApplicantsDetailBean = dashboardDAO.getApplicantsDetailsByAppId(lstDashboard.get(i).getApplicationId());
                    if (esApplicantsDetailBean != null) {
//                        System.out.println("Removing .... "+i);
                        lstDashboard.remove(i);
                    }
                }
                if(lstDashboard == null || lstDashboard.size() <= 0){
//                    System.out.println("Lst Found Null");
                    lstDashboard = getDlrPendingRecordsFromUrl(hrs, pendingReq);
                }
            }else{
//                System.out.println("Inside Null.....................");
                lstDashboard = getDlrPendingRecordsFromUrl(hrs, pendingReq);
            }
        }
        return lstDashboard;
    }
    
    private String checkNull(Object data) {
        if (data.equals(null)) {
            return "";
        } else {
            return data.toString();
        }
    }

    
    private Float checkNullCharges(Object data) {
        if (data.equals(null)) {
            return 0f;
        } else {
            return Float.valueOf(data.toString());
        }
    }

    public String getLstLoanAndStatusDashBoardFromMW(HttpServletRequest hrs, String toString, Long fromRowNum, Long toRowNum, Long profileID, String userName) throws CustomNonFatalException {

        String op = "";

        String uri = ApplicationPropertyManager.getProperty("BUILDER_LOAN_STATUS_URL");

        ResponseEntity result = null;

        try {
            ObjectMapper mapper = new ObjectMapper();

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("urltype", "builderDealerStatus");

            String requestData = "{\"Content\": {\"builderCode\": \"" + userName + "\"}}";
            HttpEntity entity = new HttpEntity(requestData, headers);
            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, String.class);

            op = result.getBody().toString();

        } catch (Exception e) {
            throw new CustomNonFatalException("Service Unavilable.Please try after some time.", "Service Unavilable.Please try after some time.");
        }

        return op;
    }

    public String getPipelineDashBoardFromMW(HttpServletRequest hrs, String toString, Long fromRowNum, Long toRowNum, Long profileID, String userName) throws CustomNonFatalException {
        String op = "";

        String uri = ApplicationPropertyManager.getProperty("");

        ResponseEntity result = null;

        try {
            ObjectMapper mapper = new ObjectMapper();

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("urltype", "");

            String requestData = "{\"Content\": {\"builderCode\": \"" + userName + "\"}}";
            HttpEntity entity = new HttpEntity(requestData, headers);
            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, String.class);

            op = result.getBody().toString();

        } catch (Exception e) {
            throw new CustomNonFatalException("Service Unavilable.Please try after some time.", "Service Unavilable.Please try after some time.");
        }

        return op;
    }

}
