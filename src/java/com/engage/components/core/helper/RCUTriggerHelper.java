/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.RcuTriggerApplicant;
import com.engage.components.core.entity.bean.RcuTriggerConstants;
import com.engage.components.core.entity.bean.RcuTriggerDocument;
import com.engage.components.core.entity.bean.RcuTriggerLog;
import com.engage.components.core.entity.bean.RcuTriggerLosLog;
import com.engage.components.core.entity.bean.RcuTriggerStatus;
import com.engage.components.core.entity.bean.RcuTriggerToken;
import com.engage.components.core.entity.dao.RCUTriggerDAO;
import com.engage.components.core.util.excepton.CustomNonFatalException;

import com.engage.components.esportal.vo.LosRequestVO;

import com.engage.framework.util.log4j.CustomLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author SurajK
 */
@Service
public class RCUTriggerHelper {

    @Autowired
    RCUTriggerDAO rCUTriggerDAO;
    public int counter = 0;

    public RcuTriggerLog setSangamRcuTriggerLog(RcuTriggerLog sangamRcuTriggerLog) {
        return rCUTriggerDAO.setSangamRcuTriggerLog(sangamRcuTriggerLog);
    }

    public RcuTriggerLog getSangamRcuTriggerLogById(Long Id) {
        return rCUTriggerDAO.getSangamRcuTriggerLogById(Id);
    }

    public RcuTriggerStatus getSangamRcuTriggerStatus(String applicationId) {
        return rCUTriggerDAO.getSangamRcuTriggerStatus(applicationId);
    }

    public void setSangamRcuTriggerStatus(RcuTriggerStatus sangamRcuTriggerStatus) {
        rCUTriggerDAO.setSangamRcuTriggerStatus(sangamRcuTriggerStatus);
    }

    public RcuTriggerApplicant getSangamRcuTriggerApplicant(String applicationId, String applicantId) {
        return rCUTriggerDAO.getSangamRcuTriggerApplicant(applicationId, applicantId);
    }

    public List<RcuTriggerApplicant> getSangamRcuTriggerApplicantByApplication(String applicationId) {
        return rCUTriggerDAO.getSangamRcuTriggerApplicantByApplication(applicationId);
    }

    public void setSangamRcuTriggerApplicant(RcuTriggerApplicant sangamRcuTriggerApplicant) {
        rCUTriggerDAO.setSangamRcuTriggerApplicant(sangamRcuTriggerApplicant);
    }

    public RcuTriggerDocument getSangamRcuTriggerDocument(String applicationId) {
        return rCUTriggerDAO.getSangamRcuTriggerDocument(applicationId);
    }

    public List<RcuTriggerDocument> getSangamRcuTriggerDocumentByApplicant(String applicationId, String applicantId) {
        return rCUTriggerDAO.getSangamRcuTriggerDocumentByApplicant(applicationId, applicantId);
    }

    public void setSangamRcuTriggerDocument(RcuTriggerDocument sangamRcuTriggerDocument) {
        rCUTriggerDAO.setSangamRcuTriggerDocument(sangamRcuTriggerDocument);
    }

    public RcuTriggerDocument getDocumentById(Long docId) {
        return rCUTriggerDAO.getDocumentById(docId);
    }

    public String getRCUWSToken() throws JSONException, CustomNonFatalException, JsonProcessingException {
        List<RcuTriggerConstants> sangamRcuTriggerConstants = rCUTriggerDAO.getRcuConstants(FrameworkAppConstants.TOKEN_TYPE);
        String username = "", password = "", client_id = "", client_secret = "", grant_type = "", token_url = "";
        if (sangamRcuTriggerConstants != null) {
            for (RcuTriggerConstants rs : sangamRcuTriggerConstants) {
                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_USERNAME)) {
                    username = rs.getProperyValue();
                }

                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_PASSWORD)) {
                    password = rs.getProperyValue();
                }

                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_CLIENTID)) {
                    client_id = rs.getProperyValue();
                }

                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_CLIENT_SECRET)) {
                    client_secret = rs.getProperyValue();
                }

                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_GRANT_TYPE)) {
                    grant_type = rs.getProperyValue();
                }

                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.TOKEN_TOKEN_URL)) {
                    token_url = rs.getProperyValue();
                }
            }
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add(FrameworkAppConstants.TOKEN_USERNAME, username);
        map.add(FrameworkAppConstants.TOKEN_PASSWORD, password);
        map.add(FrameworkAppConstants.TOKEN_CLIENTID, client_id);
        map.add(FrameworkAppConstants.TOKEN_CLIENT_SECRET, client_secret);
        map.add(FrameworkAppConstants.TOKEN_GRANT_TYPE, grant_type);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ObjectMapper mapper = new ObjectMapper();
        String op = "", token = "";
        try {
            op = restTemplate.postForObject(token_url, request, String.class);
            RcuTriggerToken sangamRcuTriggerToken = new RcuTriggerToken();
            sangamRcuTriggerToken.setCreatedDate(DateUtility.getCurrentDate());
            sangamRcuTriggerToken.setResponse(op);
            JSONObject tokenResponse = new JSONObject(op);
            if (tokenResponse.has("access_token")) {
                token = tokenResponse.get("access_token").toString();
                sangamRcuTriggerToken.setToken(token);
            }
            rCUTriggerDAO.setToken(sangamRcuTriggerToken);
        } catch (RestClientException e) {
            CustomLogger.error("Token Generation Error", e);
        } catch (JSONException e) {
            CustomLogger.error("Token Generation Error", e);
        }
        return token;
    }

    public ResponseEntity updateLos(LosRequestVO losRequestVO, String applicationId, String userId) throws JSONException, CustomNonFatalException, JsonProcessingException {
        ResponseEntity responseEntity = null;
        RcuTriggerToken sangamRcuTriggerToken = rCUTriggerDAO.getToken();
        String token = "", losUrl = "";
        if (sangamRcuTriggerToken != null) {
            token = sangamRcuTriggerToken.getToken();
        } else {
            token = getRCUWSToken();
        }

        List<RcuTriggerConstants> sangamRcuTriggerConstants = rCUTriggerDAO.getRcuConstants(FrameworkAppConstants.RCU_TRIGGER_TYPE_REQUEST);
        if (sangamRcuTriggerConstants != null) {
            for (RcuTriggerConstants rs : sangamRcuTriggerConstants) {
                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.RCU_TRIGGER_TOKEN_WS_URL)) {
                    losUrl = rs.getProperyValue();
                }
            }
        }
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("authorization", "OAuth " + token);
        headers.add("content-type", "application/json; charset=UTF-8");
        headers.add("accept", "application/json");
        HttpEntity request = new HttpEntity(losRequestVO, headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(losUrl + applicationId)
                .queryParam("_HttpMethod", "PATCH");

        ObjectMapper mapper = new ObjectMapper();

        RcuTriggerLosLog sangamRcuTriggerLosLog = new RcuTriggerLosLog();
        sangamRcuTriggerLosLog.setApplicationId(applicationId);
        sangamRcuTriggerLosLog.setCreatedBy(userId);
        sangamRcuTriggerLosLog.setCreatedDate(DateUtility.getCurrentDate());
        sangamRcuTriggerLosLog.setRequest(mapper.writeValueAsString(request));

        try {
            counter++;
            responseEntity = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, request, String.class);
            sangamRcuTriggerLosLog.setResponse("");
            rCUTriggerDAO.setLosRequest(sangamRcuTriggerLosLog);
        } catch (HttpClientErrorException e1) {
            sangamRcuTriggerLosLog.setResponse(e1.getMessage());
            rCUTriggerDAO.setLosRequest(sangamRcuTriggerLosLog);
            if (e1.getMessage().equalsIgnoreCase(FrameworkAppConstants.RCU_TRIGGER_UNAUTHORIZED)) {
                if (counter < 2) {
                    getRCUWSToken();
                    updateLos(losRequestVO, applicationId, userId);
                }
            } else if (e1.getMessage().equalsIgnoreCase(FrameworkAppConstants.RCU_TRIGGER_NOT_FOUND)) {
                throw new CustomNonFatalException("Application not found in LOS", "Application not found in LOS");
            } else {
                throw new CustomNonFatalException(e1.getMessage(), e1.getMessage());
            }
            CustomLogger.error("LOS WS Error", e1);
        } catch (Exception e) {
            sangamRcuTriggerLosLog.setResponse(e.getMessage());
            rCUTriggerDAO.setLosRequest(sangamRcuTriggerLosLog);
            CustomLogger.error("LOS WS Error", e);
        }
        return responseEntity;
    }

    public String getDocumentFromMiddelware(String docUuid,  String applicationId) throws JSONException, CustomNonFatalException, JsonProcessingException {
        //This wasn't working with bean.So hardcoded String.
        String requestData = "{\"docDetail\": {\"uuid\": \"" + docUuid + "\"}}";

        RestTemplate restTemplate = new RestTemplate();
        RcuTriggerLog sangamRcuTriggerLog = new RcuTriggerLog();
        sangamRcuTriggerLog.setApplicationId(applicationId);
        sangamRcuTriggerLog.setRequest(requestData);
        sangamRcuTriggerLog.setRequestTime(DateUtility.getCurrentDate());
        sangamRcuTriggerLog.setReqType(FrameworkAppConstants.RCU_TRIGGER_LOG_DOCUMENT);
        List<RcuTriggerConstants> sangamRcuTriggerConstants = rCUTriggerDAO.getRcuConstants(FrameworkAppConstants.RCU_TRIGGER_DOCUMENT);
        String reqUrl = "";
        if (sangamRcuTriggerConstants != null) {
            reqUrl = sangamRcuTriggerConstants.get(0).getProperyValue();
        }
        String op = "";
        try {
            op = restTemplate.postForObject(reqUrl, requestData, String.class);
            sangamRcuTriggerLog.setResponse(op);
            sangamRcuTriggerLog.setResponseTime(DateUtility.getCurrentDate());
        } catch (Exception e) {
            throw new CustomNonFatalException("Service Unavilable.Please try after some time.", "Service Unavilable.Please try after some time.");
        } finally {
            setSangamRcuTriggerLog(sangamRcuTriggerLog);
        }

        return op;
    }

    public String updateStatusMiddleWare(LosRequestVO losRequestVO, String applicationId, String userId) throws JSONException, CustomNonFatalException, JsonProcessingException{
       String updateUrl="";
        List<RcuTriggerConstants> sangamRcuTriggerConstants = rCUTriggerDAO.getRcuConstants(FrameworkAppConstants.RCU_TRIGGER_TYPE_REQUEST);
        if (sangamRcuTriggerConstants != null) {
            for (RcuTriggerConstants rs : sangamRcuTriggerConstants) {
                if (rs.getPropertyName().equalsIgnoreCase(FrameworkAppConstants.RCU_TRIGGER_TOKEN_MIDDLEWARE_URL)) {
                    updateUrl = rs.getProperyValue();
                }
            }
        }
        
        
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Application_id", applicationId);
        HttpEntity request = new HttpEntity(losRequestVO, headers);
        String op = "";
        ObjectMapper mapper = new ObjectMapper();
        RcuTriggerLosLog sangamRcuTriggerLosLog = new RcuTriggerLosLog();
        sangamRcuTriggerLosLog.setApplicationId(applicationId);
        sangamRcuTriggerLosLog.setCreatedBy(userId);
        sangamRcuTriggerLosLog.setCreatedDate(DateUtility.getCurrentDate());
        sangamRcuTriggerLosLog.setRequest(mapper.writeValueAsString(request));
        
        try {
            op = restTemplate.postForObject(updateUrl, request, String.class);
            sangamRcuTriggerLosLog.setResponse(op);
            rCUTriggerDAO.setLosRequest(sangamRcuTriggerLosLog);
        } catch (Exception e) {
            sangamRcuTriggerLosLog.setResponse(e.getMessage());
            rCUTriggerDAO.setLosRequest(sangamRcuTriggerLosLog);  
            throw new CustomNonFatalException(e.getMessage(), e.getMessage());
        }
        
        return op;
    }
}
