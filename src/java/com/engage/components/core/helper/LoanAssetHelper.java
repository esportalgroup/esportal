/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import com.engage.components.core.entity.dao.LoanAssetDao;
import com.engage.components.esportal.vo.DealerApproveRejVO;
import com.engage.components.esportal.vo.DealerPendingData;
import com.engage.session.beans.UserInfoBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Shrikant Katakdhond
 */
@Service
public class LoanAssetHelper {

    @Autowired
    LoanAssetDao assetDao;

    @Autowired
    UserInfoBean userInfoBean;

    public EsApplicantsDetailBean getApplicantsDetailsByAppId(String applicationId) {
        EsApplicantsDetailBean applicantsDetailBean = assetDao.getApplicantsDetailsByAppId(applicationId);
        return applicantsDetailBean;
    }

    public EsAssetDetailBean getAssetDetailByAppId(String applicationId) {
        EsAssetDetailBean assetDetailBean = assetDao.getAssetDetailByAppId(applicationId);
        return assetDetailBean;
    }

    public EsLoanDetailBean getLoanDetailByAppId(String applicationId) {
        EsLoanDetailBean loanDetailBean = assetDao.getLoanDetailByAppId(applicationId);
        return loanDetailBean;
    }

    public DealerPendingData getPendingDataFromSession(String applicationId, HttpSession session) {
        DealerPendingData returnData=new DealerPendingData();

        try {
            HashMap<String, List<DealerPendingData>> datamap = (HashMap<String, List<DealerPendingData>>) session.getAttribute("pendingData");
            List<DealerPendingData> dataLst = (List<DealerPendingData>) session.getAttribute("dataLst");
            //System.out.println("DATA dataLst::::::" + dataLst);

            for (int k = 0; k < dataLst.size(); k++) {
                 DealerPendingData data = dataLst.get(k);
                //System.out.println("DATA ::::::" + data.getApplication_ID() + "" + data.getId());
                if (applicationId.equals(data.getApplication_ID())) {
                    //System.out.println("Inside IF ::::::");
                        returnData=data;
                   

                } else {
                    //System.out.println("no record Found ::::::");
                }

            }

        } catch (Exception e) {
            e.printStackTrace();

           
        }
        
        //System.out.println("returnData::::::"+returnData.getApplication_ID());
       return returnData; 
    }

    public String setApplicantDetails(EsApplicantsDetailBean applicantDetails) {
        String response=assetDao.saveApplicantDetails(applicantDetails);
        return response;    }

    public String setLoanDetails(EsLoanDetailBean loanDetails) {
        String response=assetDao.saveLoanDetails(loanDetails);
        return response;
    }

    public String setAssetDetails(EsAssetDetailBean assetDetails) {
        String response=assetDao.saveAssetDetails(assetDetails);
        return response;
    }

    public String approveAndRejectDEtailsFromWS(DealerApproveRejVO  delReq) {
     String uri = "http://10.3.2.38:7800/LOS_SERVICE";
      ResponseEntity result = null;
      JSONObject obj = null;
      String response="";
     try
     {
        
           ObjectMapper mapper = new ObjectMapper();

              System.out.println("Pre request :: " + mapper.writeValueAsString(delReq));

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("urltype", "updateDealerStatus");

            HttpEntity entity = new HttpEntity(delReq, headers);

              System.out.println(" Call WS  >>>>>>>>>");
            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, String.class);
              System.out.println(" after Call WS  >>>>>>>>>");
            obj = new JSONObject(result.getBody().toString());
            System.out.println(" after Call WS  >>>>>>>>>"+obj);
            String state=obj.get("status").toString();
            
            if(state.equalsIgnoreCase("SUCCESS"))
            {
                response="success";
            }else
            {
            response="fails";
            }
     
     }catch(Exception e)
     {
         response="fails";
         e.printStackTrace();
     
     }
    return response;
    }

    public EsInvoiceDocBean getInvoiceDocDetailsByAppId(String applicationId) {
         EsInvoiceDocBean invoiceDetail = assetDao.getInvoiceDetailsByAppId(applicationId);
        return invoiceDetail;
    }

    public String setInvoiceDocDetails(EsInvoiceDocBean invoiceDoc) {
         String response=assetDao.saveInvoiceDocDetails(invoiceDoc);
        return response; 
    }
}
