/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

/**
 *
 * @author dattatrayt
 */
import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.MCMGroupFormationLog;
import com.engage.components.core.entity.bean.McCodeMstr;
import com.engage.components.core.entity.bean.McmApplicantsDetails;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmGroupMemberDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import com.engage.components.core.entity.bean.McmTriggerRequestLog;
import com.engage.components.core.entity.bean.SoundExDetailsBean;
import com.engage.components.core.entity.dao.McmTriggerDAO;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.mcm.vo.McmApplicantsVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupFormationVO;
//import com.engage.components.esportal.vo.FusyServiceReqVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import com.engage.components.esportal.vo.FusyServiceReqVO;
import com.engage.components.esportal.vo.McmGroupDetailVO;
import com.engage.components.esportal.vo.McmGroupMemberVo;
import com.engage.components.esportal.vo.fusyServiceReqVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class McmTriggerHelper {

    @Autowired
    McmTriggerDAO mcmTriggerDAO;

    public McmTriggerRequestLog setMcmTriggerRequestLog(McmTriggerRequestLog mcmTriggerRequestLog) {
        McmTriggerRequestLog obj = mcmTriggerDAO.setMcmTriggerRequestLog(mcmTriggerRequestLog);
        if (obj != null) {
            return obj;
        } else {
            return null;
        }
    }

    public McmGroupMemberDetail setMcmGroupMemberDetail(McmGroupMemberDetail memberDetail) {
        McmGroupMemberDetail detail = mcmTriggerDAO.setMcmGroupMemberDetail(memberDetail);
        if (detail != null) {
            return detail;
        } else {
            return null;
        }
    }

    public McmGroupDetail setMcmGroupDetail(McmGroupDetail groupDetail) {
        McmGroupDetail detail = mcmTriggerDAO.setMcmGroupDetail(groupDetail);
        if (detail != null) {
            return detail;
        } else {
            return null;
        }
    }

    public McmGroupDetailVO getMcmGroupDetail(String groupId) throws CustomNonFatalException {

        McmGroupDetailVO groupDetailVo = null;
        McmGroupDetail groupDetail = getMcmGroupDetailByGroupId(groupId);
        System.out.println("groupDetail >>>>>>>>"+groupDetail);
        if (groupDetail != null) {

            groupDetailVo = new McmGroupDetailVO();

            groupDetailVo.setBranchId(checkNull(groupDetail.getBranchId()));
            groupDetailVo.setCityId(checkNull(groupDetail.getCityId()));
            groupDetailVo.setDistrictId(checkNull(groupDetail.getDistrictId()));
            groupDetailVo.setFloId(checkNull(groupDetail.getFloId()));
            groupDetailVo.setFloName(checkNull(groupDetail.getFloName()));
            groupDetailVo.setGroupId(checkNull(groupDetail.getGroupId().toString()));
            groupDetailVo.setGroupName(checkNull(groupDetail.getGroupName()));
            groupDetailVo.setGrpId(checkNull(groupDetail.getGrpId().toString()));
            groupDetailVo.setIsActive(checkNull(groupDetail.getIsActive().toString()));
            groupDetailVo.setLocalityId(checkNull(groupDetail.getLocalityId()));
            groupDetailVo.setMcCode(checkNull(groupDetail.getMcCode()));
            groupDetailVo.setVillageName(checkNull(groupDetail.getVillageName()));
            groupDetailVo.setStatus(checkNull(groupDetail.getStatus()));
            if(groupDetail.getSanctionDate()==null)
            {
                groupDetailVo.setSanctionDate("");
            }else
            {
              groupDetailVo.setSanctionDate(DateUtility.parseDateToString(groupDetail.getSanctionDate(), "yyyy-MM-dd"));  
            }
            
            groupDetailVo.setStateId(groupDetail.getStateId());
            groupDetailVo.setVillageId(groupDetail.getVillageId());
            List<McmGroupMemberVo> lstGroupMemberVo;

            
            List<McmApplicationDetails> groupMemberDetails=mcmTriggerDAO.getMcmApplicationDetailsByGroupId(groupId);

              if(groupMemberDetails==null)
                    {                     
                        throw new CustomNonFatalException("There Are No Applications In in Group Id  "+groupDetail.getGroupId(), "There Are No Applications In in Group Id  "+groupDetail.getGroupId());

                    }
              
            System.out.println("groupMemberDetails >>>>>>>>"+groupMemberDetails.size());
            if (groupMemberDetails != null) {

                            System.out.println("Set Data >>>>>>>>");
                lstGroupMemberVo = new ArrayList<McmGroupMemberVo>();
                McmGroupMemberVo memberVo;
                for (McmApplicationDetails detail : groupMemberDetails) {

                     System.out.println("Set Data >>>>>>>>"+detail.getApplicationId());
                    memberVo = new McmGroupMemberVo();

                    memberVo.setAccountValidationStatus(checkNull(detail.getAccountValidationStatus()));
                    memberVo.setAppraisalflag(checkNull(detail.getAppraisalflag()));
                    memberVo.setBankAccountNumber(checkNull(detail.getBankAccountNumber()));
                    memberVo.setCliAmount(checkNull(detail.getCliAmount()));
                    memberVo.setEmi(detail.getMaxEMIAmount());
//                    memberVo.setGrpId(detail.getGroupDetail().getGrpId().toString());
                    memberVo.setIfscCode(checkNull(detail.getIfscCode()));
                    memberVo.setLoanAmount(checkNull(detail.getGenesisLoanAmount()));
                    memberVo.setLoanTenure(checkNull(detail.getGenesisTerm()));
                   
                    memberVo.setModeOfDisbursement(checkNull(detail.getModeOfDisbursement()));
                    memberVo.setNetDisbursementAmt(checkNull(detail.getNetDisbursementAmt()));
                    memberVo.setProcFee(checkNull(detail.getProcFee()));
                    memberVo.setFlsCode(checkNull(detail.getFlsCode()));
                    memberVo.setFlsName(checkNull(detail.getFlsName()));
                    memberVo.setMobileNo(checkNull(detail.getMobileNumber()));
                    memberVo.setIsAccountValid(checkNull(detail.getIsAccountValid()));
                    memberVo.setAdharNumber(checkNull(detail.getAdharNumber()));
                    List<McmApplicantsDetails> applicantDetails=mcmTriggerDAO.getMcmApplicantsDetailsByGroupId(groupId,detail.getApplicationId());
                    
                    if(applicantDetails==null)
                    {                     
                        throw new CustomNonFatalException("Applicnts Not Found", "Application Not Found");

                    }
                   System.out.println(">>>>>>>>>applicantDetails"+applicantDetails);
                    McmApplicantsVO applicant=null; 
                    List<McmApplicantsVO> applicantLst=new ArrayList<McmApplicantsVO>();
                    
                    for (McmApplicantsDetails applicantData : applicantDetails) 
                     {
                         applicant=new McmApplicantsVO();
                         System.out.println(">>>>>>>>>Rel"+applicantData.getRealationWithApplicant());
//                         if(applicantData.getRealationWithApplicant()==null || applicantData.getRealationWithApplicant().equals(""))
//                         {
//                         memberVo.setPrimaryApplicantId(checkNull(applicantData.getApplicantId().toString()));
//                         memberVo.setPrimaryApplicant(checkNull(applicantData.getApplicantName()));
//                         }else
//                         {
//                         memberVo.setCoApplicant(checkNull(applicantData.getApplicantName()));
//                         memberVo.setCoApplicantId(checkNull(applicantData.getApplicantId().toString()));
//                         memberVo.setCoRelation(checkNull(applicantData.getRealationWithApplicant()));
//                         }
                         
                         
                         
                         memberVo.setApplicationId(checkNull(applicantData.getApplicationId().toString()));                         applicant.setApplicantId(applicantData.getApplicantId().toString());
                         applicant.setApplicantName(checkNull(applicantData.getApplicantName()));
                         applicant.setRealationWithApplicant(checkNull(applicantData.getRealationWithApplicant()));
                         applicant.setGroupId(checkNull(applicantData.getGroupId()));
                         applicant.setApplicationId(checkNull(applicantData.getApplicationId().toString()));
                         applicantLst.add(applicant);
                     }
                    memberVo.setApplicants(applicantLst);

                    lstGroupMemberVo.add(memberVo);
                }

                groupDetailVo.setLstGroupMember(lstGroupMemberVo);
            }
        }
        return groupDetailVo;
    }

    public McmGroupDetail getMcmGroupDetailByGroupId(String groupId) {
        McmGroupDetail detail = mcmTriggerDAO.getMcmGroupDetailByGroupId(groupId);
        if (detail != null) {
            return detail;
        } else {
            return null;
        }
    }

    public List<McmGroupMemberDetail> getMcmGroupMemberDetailByGroupId(Long groupId) {
        List<McmGroupMemberDetail> listMembers = mcmTriggerDAO.getMcmGroupMemberDetailByGroupId(groupId);
        if (listMembers != null) {
            return listMembers;
        } else {
            return null;
        }
    }

    public String callSoundexService(fusyServiceReqVO fusyReq) {
        
        String uri = "http://10.3.2.38:7800/LTFS/LOS/SOUNDEX";
        ResponseEntity result = null;
        JSONObject obj = null;
        JSONObject dataObj = null;
        SoundExDetailsBean details=null;
        String response=null;
        try
        {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();
            HttpEntity entity = new HttpEntity(fusyReq);

            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, String.class);

            //System.out.println("Result ::: >>>"+result);
                    
            obj = new JSONObject(result.getBody().toString());
            org.json.JSONArray array = obj.getJSONArray("Score");
            
            for(int l=0;l<array.length();l++)
            {
                details=new SoundExDetailsBean();
                dataObj=(JSONObject) array.get(l);
                JSONObject score1Obj=dataObj.getJSONObject("Fuzzy_Score1");
                JSONObject score2Obj=dataObj.getJSONObject("Fuzzy_Score2");
                details.setReferenceId(dataObj.get("ReferenceID").toString());
                details.setFuzzy_score1_mapping_percent(score1Obj.get("MappingPercent").toString());
                details.setFuzzy_score2_mapping_percent(score2Obj.get("MappingPercent").toString());
                details.setCreDate(DateUtility.getCurrentDate());
                details.setModDate(DateUtility.getCurrentDate());
                details.setIsActive('Y');
                
            }
             response=mcmTriggerDAO.saveSoundexDetails(details);
             
            
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return response;
    }

    public McmApplicationDetails getMcmApplicationDetailsDetail(String appId) {
        McmApplicationDetails appDetails=mcmTriggerDAO.getMcmApplicationDetailsDetail(appId);
        return appDetails;
    }

    public void setMcmApplicationDetailsDetail(McmApplicationDetails appdetails) {
        mcmTriggerDAO.saveMcmApplicationDetailsDetail(appdetails);  
    }

    public void setMcmApplicantDetail(McmApplicantsDetails applicantDetails) {
        mcmTriggerDAO.saveMcmApplicantDetail(applicantDetails);
    }

    public McmApplicantsDetails getMcmApplicantDetail(String applicantId, String appId) {
        McmApplicantsDetails applicantDetails=mcmTriggerDAO.getMcmApplicantDetail(applicantId,appId);
        return applicantDetails;
    }

    public McmGroupDetail getMcmGroupDetails(String groupId) {
        
        McmGroupDetail mcmDetails=mcmTriggerDAO.getMcmGroupDetails(groupId);
        return mcmDetails;
    }

    public McCodeMstr getMcmCodeMstr(String mcmCode) {
        McCodeMstr mcmCodeDetails=mcmTriggerDAO.getMcmMstrDetails(mcmCode);
        return mcmCodeDetails;
    }

    public String callGroupFormationService(mcmRequestGroupFormationVO groupFormationVo) {
        
      String uri = "http://10.3.2.38:7801/LOS_SERVICE";
      ResponseEntity result = null;
      JSONObject obj = null;
      String response="";
      
      MCMGroupFormationLog log=null;
     try
     {
         log=new MCMGroupFormationLog();
        
           ObjectMapper mapper = new ObjectMapper();

           System.out.println("Pre request :: " + mapper.writeValueAsString(groupFormationVo));
           
           log.setRequest(mapper.writeValueAsString(groupFormationVo));
            log.setModDate(DateUtility.getCurrentDate());

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("urltype", "GroupFormation");
            HttpEntity entity = new HttpEntity(groupFormationVo,headers);

            result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, entity, String.class);
            obj = new JSONObject(result.getBody().toString());
            
            log.setResponse(result.getBody().toString());
            
            String status=obj.get("status").toString();
            
            if(status.equalsIgnoreCase("SUCCESS"))
            {
                response="success";
            }else
            {
            response="fails";
            }
     
     }catch(Exception e)
     {
         response="fails";
         e.printStackTrace();
     
     }
     finally{
         mcmTriggerDAO.setMCMGroupFormationLog(log);
     }
    return response;
    }
 private String checkNull(Object data) {
        if (data==null) {
            return "";
        } else {
            return data.toString();
        }
    }    
 
    public List<McmMpngDisbursementAccount> getMcmMpngDisbursementAccount(String mcCode){
        List<McmMpngDisbursementAccount> obj = mcmTriggerDAO.getMcmMpngDisbursementAccount(mcCode);
        if (obj != null) {
            return obj;
        } else {
            return null;
        }
    };

    public List<McmGroupMemberVo> getaddMembersDetail(String mcCode, String groupId) throws CustomNonFatalException {
        
            List<McmGroupMemberVo> lstGroupMemberVo;
//
////            List<McmGroupMemberDetail> groupMemberDetails = mcmTriggerDAO.getMcmGroupMemberDetailByGroupId(groupId);
//            
            List<McmApplicationDetails> groupMemberDetails=mcmTriggerDAO.getMcmApplicationDetailsFilterByGroupId(groupId,mcCode);

            System.out.println("groupMemberDetails >>>>>>>>"+groupMemberDetails);
            if (groupMemberDetails != null) {

                            System.out.println("Set Data >>>>>>>>");
                lstGroupMemberVo = new ArrayList<McmGroupMemberVo>();
                McmGroupMemberVo memberVo;
                for (McmApplicationDetails detail : groupMemberDetails) {

                    memberVo = new McmGroupMemberVo();
                    memberVo.setGroupId(checkNull(detail.getGroupId()));
                    memberVo.setAccountValidationStatus(checkNull(detail.getAccountValidationStatus()));
                    memberVo.setAppraisalflag(checkNull(detail.getAppraisalflag()));
                    memberVo.setBankAccountNumber(checkNull(detail.getBankAccountNumber()));
                    memberVo.setCliAmount(checkNull(detail.getCliAmount()));
                    memberVo.setEmi(detail.getMaxEMIAmount());
//                    memberVo.setGrpId(detail.getGroupDetail().getGrpId().toString());
                    memberVo.setIfscCode(checkNull(detail.getIfscCode()));
                    memberVo.setLoanAmount(detail.getGenesisLoanAmount());
                    memberVo.setLoanTenure(detail.getGenesisTerm());
                   
                    memberVo.setModeOfDisbursement(detail.getModeOfDisbursement());
                    memberVo.setNetDisbursementAmt(checkNull(detail.getNetDisbursementAmt()));
                    memberVo.setProcFee(detail.getProcFee());
                    
                    List<McmApplicantsDetails> applicantDetails=mcmTriggerDAO.getMcmApplicantsDetailsByGroupId(groupId,detail.getApplicationId());
                    
                    if(applicantDetails!=null)
                    {  
                   System.out.println(">>>>>>>>>applicantDetails"+applicantDetails);
                    McmApplicantsVO applicant=null; 
                    List<McmApplicantsVO> applicantLst=new ArrayList<McmApplicantsVO>();
                    
                    for (McmApplicantsDetails applicantData : applicantDetails) 
                     {
                         applicant=new McmApplicantsVO();
                         System.out.println(">>>>>>>>>Rel"+applicantData.getRealationWithApplicant());
                         if(applicantData.getRealationWithApplicant()==null || applicantData.getRealationWithApplicant().equals(""))
//                         {
//                         memberVo.setPrimaryApplicantId(applicantData.getApplicantId().toString());
//                         memberVo.setPrimaryApplicant(applicantData.getApplicantName());
//                         }else
//                         {
//                         memberVo.setCoApplicant(applicantData.getApplicantName());
//                         memberVo.setCoApplicantId(applicantData.getApplicantId().toString());
//                         memberVo.setCoRelation(applicantData.getRealationWithApplicant());
//                         }
                         
                         
                         
                         memberVo.setApplicationId(applicantData.getApplicationId().toString());
                         applicant.setApplicantId(applicantData.getApplicantId().toString());
                         applicant.setApplicantName(applicantData.getApplicantName());
                         applicant.setRealationWithApplicant(applicantData.getRealationWithApplicant());
                         applicant.setGroupId(applicantData.getGroupId());
                         applicant.setApplicationId(applicantData.getApplicationId().toString());
                         applicantLst.add(applicant);
                     }
                    memberVo.setApplicants(applicantLst);

                    lstGroupMemberVo.add(memberVo);
                    }
                }
            }else{
                throw new CustomNonFatalException("No eligible applications exists","No eligible applications exists");
            }
        return lstGroupMemberVo;
        
    }

    public McmGroupDetail getMcmGroupDetailBtGroupId(String groupId) {
        return mcmTriggerDAO.getMcmGroupDetailBtGroupId(groupId);
    }

    public void updateMCMGroupDetail(McmGroupDetail groupDetails) {
        mcmTriggerDAO.updateMCMGroupDetail(groupDetails);
    }

    public McmApplicationDetails getMcmApplicationByGroupId(String groupId, String appId) {
        return mcmTriggerDAO.getMcmApplicationByGroupId(groupId,appId);
    }

    public void updateMCMApplicationDetails(McmApplicationDetails applications) {
        mcmTriggerDAO.updateMCMApplicationDetails(applications);
    }

    public McmApplicantsDetails getMcmApplicantByGroupId(String groupId, String applicantId) {
        return mcmTriggerDAO.getMcmApplicantByGroupId(groupId,applicantId);
    }

    public void upDateMCMApplicantDetails(McmApplicantsDetails applicants) {
        mcmTriggerDAO.upDateMCMApplicantDetails(applicants);
    }

    public McmMpngDisbursementAccount getAccountDetailsByAccountNo(String bankAccountNo) {
            return mcmTriggerDAO.getAccountDetailsByAccountNo(bankAccountNo);
    }

    public List<McmApplicantsDetails> getApplicantDetails(String groupId) {
         return mcmTriggerDAO.getApplicantDetails(groupId);
    }

    public List<McmApplicationDetails> getApplicationsByGroupId(String groupId) {
        
         return mcmTriggerDAO.getApplicatonsDetails(groupId);
    }

    
    
}
