/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import static com.cmss.engage.framework.util.AESAlgoClass.decrypt;
import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import com.engage.components.core.entity.dao.PasswordResetDAO;
import com.engage.framework.util.log4j.CustomLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Properties;
import java.util.Random;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Shrikant Katakdhond
 */
@Service
public class PasswordResetHelper {

    @Autowired
    PasswordResetDAO resetDAO;

    public FrameworkMstrLogin getFrameworkMstrLoginByUserName(String userName) {
        FrameworkMstrLogin mstrLogin = resetDAO.getFrameworkMstrLoginByUserName(userName);
        return mstrLogin;
    }

    public FrameworkMstrLogin getFrameworkMstrLoginByUserId(Long userId) {
        FrameworkMstrLogin mstrLogin = resetDAO.getFrameworkMstrLoginByUserId(userId);
        return mstrLogin;
    }

    public boolean sendMail(String[] to, String subject, String body) throws Exception {
        String login_id, from, pass;
        login_id = ApplicationPropertyManager.getProperty(FrameworkAppConstants.EMAIL_LOGIN_ID);
        pass = decrypt(ApplicationPropertyManager.getProperty(FrameworkAppConstants.EMAIL_PASSWORD));
        from = ApplicationPropertyManager.getProperty(FrameworkAppConstants.EMAIL_ID);

        Properties props = System.getProperties();
        String host = "POLTFMSEXJMBX01.LNTFINSVCS.COM";

        props.put("mail.smtp.starttls.enable", "true");

        props.put("mail.smtp.ssl.trust", host);
//    props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.user", login_id);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "25");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {

            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
////-------------------------------
//        BodyPart messageBodyPart = new MimeBodyPart();
//
//         // Now set the actual message
//         messageBodyPart.setText("Dear Datta,\n kute gela\n"
//                 + "aksjbkas"
//                 + "alscnbla");
//
//         // Create a multipar message
//         Multipart multipart = new MimeMultipart();
//
//         // Set text message part
//         multipart.addBodyPart(messageBodyPart);
//
////         // Part two is attachment
////         messageBodyPart = new MimeBodyPart();
////         String filename = "D:\\subquery1.pdf";
////         DataSource source = new FileDataSource(filename);
////         messageBodyPart.setDataHandler(new DataHandler(source));
////         messageBodyPart.setFileName(filename);
////         multipart.addBodyPart(messageBodyPart);
//
//         // Send the complete message parts
//         message.setContent(multipart);
//         
////------------------

            message.setSubject(subject);
            message.setText(body);
//        message.setContent(mp);

            Transport transport = session.getTransport("smtp");

//        transport.connect(host, from, pass);
            transport.connect(host, login_id, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            return true;
        } catch (Exception e) {
            CustomLogger.error("send mail", e);
            return false;
        }
    }

    public void setFrameworkMstrLogin(FrameworkMstrLogin frameworkMstrLogin) {
        resetDAO.setFrameworkMstrLogin(frameworkMstrLogin);
    }

    public String generateRandomPassword() {
        String aToZ = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890@#";
        Random rand = new Random();
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            int randIndex = rand.nextInt(aToZ.length());
            res.append(aToZ.charAt(randIndex));
        }
        return res.toString();
    }
}
