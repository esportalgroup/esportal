/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

/**
 *
 * @author DattatrayT
 */
import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsLogDocumentDownload;
import com.engage.components.core.entity.bean.MandateTriggerDataApplicantsDetails;
import com.engage.components.core.entity.bean.MandateTriggerDataApplication;
import com.engage.components.core.entity.bean.MandateTriggerDataDocument;
import com.engage.components.core.entity.bean.MandateTriggerRequestLog;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.*;
import java.util.Date;
import com.engage.components.core.entity.dao.MandateTriggerDAO;
import com.engage.components.core.entity.bean.MandateTriggerTokenUser;
import com.engage.components.core.entity.bean.MandateTriggerTokenRequestLog;
import com.engage.components.core.entity.bean.RmLogUid;
import com.engage.components.core.entity.dao.RmDataDAO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import com.engage.components.esportal.vo.OpsApplicationVO;
import com.engage.components.esportal.vo.OpsDocumentVO;
import com.engage.components.esportal.vo.OpsUpdateContentVO;
import com.engage.components.esportal.vo.OpsUpdateReqVO;
import com.engage.components.esportal.vo.MandateDocVo;
import com.engage.framework.util.log4j.CustomLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;


@Service
public class MandateTriggerHelper {
    @Autowired
    MandateTriggerDAO mandateTriggerDAO;
    
    final String token_id="d3d3LmxudGZpbnN2Y3MuY29t", token_issuer="esportal_user", token_subject = "esportal_jwt";
    final long token_ttlMillis=1800000;
    
    public MandateTriggerTokenUser getMandateTriggerTokenUser(String userId){
        MandateTriggerTokenUser obj=mandateTriggerDAO.getMandateTriggerTokenUser(userId);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public MandateTriggerTokenRequestLog setMandateTriggerTokenRequestLog(MandateTriggerTokenRequestLog log){
        return mandateTriggerDAO.setMandateTriggerTokenRequestLog(log);
    }
    
    public MandateTriggerTokenUser setMandateTriggerTokenUser(MandateTriggerTokenUser mandateTriggerTokenUser) {
        return mandateTriggerDAO.setMandateTriggerTokenUser(mandateTriggerTokenUser);
    }
    
    
    public String createJWT() {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("MTVGbG9vcixMJlRmaW5hbmNlLFJ1cGFzb2xpdGFpcmUsTUJQLE1haGFwZQ==");
        System.out.println("apiKeySecretBytes : " + apiKeySecretBytes);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(token_id)
                .setIssuedAt(now)
                .setSubject(token_subject)
                .setIssuer(token_issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (token_ttlMillis >= 0) {
            long expMillis = nowMillis + token_ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public void parseJWT(String jwt) throws CustomNonFatalException,JwtException {

        try{
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary("MTVGbG9vcixMJlRmaW5hbmNlLFJ1cGFzb2xpdGFpcmUsTUJQLE1haGFwZQ=="))
                .parseClaimsJws(jwt).getBody();
//        System.out.println("ID: " + claims.getId());
//        System.out.println("Subject: " + claims.getSubject());
//        System.out.println("Issuer: " + claims.getIssuer());
//        System.out.println("Expiration: " + claims.getExpiration());
        
        if(!claims.getId().equals(token_id)){
            throw new CustomNonFatalException("Invalid token id","Invalid token id");
        }
        if(!claims.getIssuer().equals(token_issuer)){
            throw new CustomNonFatalException("Invalid token issuer","Invalid token issuer");
        }
        if(!claims.getSubject().equals(token_subject)){
            throw new CustomNonFatalException("Invalid token subject","Invalid token subject");
        }
        } catch (Exception e) {
            if(e.getMessage()!= null && e.getMessage().toString().trim().contains("JWT expired")){
                throw new CustomNonFatalException("JWT expired","JWT expired");
            }else{
                throw new CustomNonFatalException(e.getMessage(),e.getMessage());
            }
        }
    }
    
    public MandateTriggerRequestLog setMandateTriggerRequestLog(MandateTriggerRequestLog mandateTriggerRequestLog) {
        return mandateTriggerDAO.setMandateTriggerRequestLog(mandateTriggerRequestLog);
    }
    
    public MandateTriggerDataApplication getMandateTriggerDataApplication(String applicationId){
        MandateTriggerDataApplication obj=mandateTriggerDAO.getMandateTriggerDataApplication(applicationId);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public MandateTriggerDataApplication setMandateTriggerDataApplication(MandateTriggerDataApplication mandateTriggerDataApplication){
        return mandateTriggerDAO.setMandateTriggerDataApplication(mandateTriggerDataApplication);
    }
    
    public MandateTriggerDataDocument setMandateTriggerDataDocument(MandateTriggerDataDocument mandateTriggerDataDocument){
        return mandateTriggerDAO.setMandateTriggerDataDocument(mandateTriggerDataDocument);
    }
    
    public List<MandateTriggerDataDocument> getMandateTriggerDataDocument(Long refid) {
        List<MandateTriggerDataDocument> obj=mandateTriggerDAO.getMandateTriggerDataDocument(refid);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public MandateTriggerDataApplication getMandateTriggerDataApplicationByRefid(Long refid) {
        MandateTriggerDataApplication obj=mandateTriggerDAO.getMandateTriggerDataApplicationByRefid(refid);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public List<MandateTriggerDataApplicantsDetails> getMandateTriggerDataApplicantsDetails(Long refid){
        List<MandateTriggerDataApplicantsDetails> obj=mandateTriggerDAO.getMandateTriggerDataApplicantsDetails(refid);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public MandateTriggerDataApplicantsDetails setMandateTriggerDataApplicantsDetails(MandateTriggerDataApplicantsDetails mandateTriggerDataApplicantsDetails){
        return mandateTriggerDAO.setMandateTriggerDataApplicantsDetails(mandateTriggerDataApplicantsDetails);
    }
    
    public MandateTriggerRequestLog getMandateTriggerRequestLog(Long id){
        MandateTriggerRequestLog obj=mandateTriggerDAO.getMandateTriggerRequestLog(id);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public String updateOPS(OpsUpdateReqVO opsUpdateReqVO) throws CustomNonFatalException {
        String op = null;
        String url = ApplicationPropertyManager.getProperty("MIDDLE_WARE_UPDATE_URL").trim();
        String urltype = ApplicationPropertyManager.getProperty("MIDDLE_OPS_UPDATE_URL_TYPE").trim();

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("urltype", urltype);
        HttpEntity request = new HttpEntity(opsUpdateReqVO, headers);
        /*ti print json*/
        ObjectMapper mapper = new ObjectMapper();

        try {
            CustomLogger.info("OPS Update request :: " + mapper.writeValueAsString(request));

//            RmLogUid rmLogUid = new RmLogUid();
//            rmLogUid.setTypeOfReq(FrameworkAppConstants.UID_DOCUMENT_TYPE_OTP_REQUEST);
////            rmLogUid.setRmDataApplication(new RmDataApplication(applicationId));
////            rmLogUid.setRmDataApplicantDetail(new RmDataApplicantDetail(applicantId));
//            rmLogUid.setRequest(mapper.writeValueAsString(request));
////            rmLogUid.setModId(userId.toString());
//            rmLogUid.setModDate(DateUtility.getCurrentDate());
//            rmLogUid = rmDataDAO.setRmLogUid(rmLogUid);
        } catch (Exception e) {
//           e.printStackTrace();
            CustomLogger.error("Save OPS Update Request", e);
        }

        try {
            op = restTemplate.postForObject(url, request, String.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomNonFatalException("Fail to send request", "Fail to send request");
        }

        try {
            CustomLogger.info("OPS Update response :: " + op);

//            RmLogUid rmLogUid = rmDataDAO.getRmLogUid(applicationId, 0l, FrameworkAppConstants.UID_DOCUMENT_TYPE_OTP_REQUEST);
//            if (rmLogUid != null) {
//                rmLogUid.setResponse(op);
////            rmLogUid.setModId(userId.toString());
//                rmLogUid.setModDate(DateUtility.getCurrentDate());
//                rmDataDAO.setRmLogUid(rmLogUid);
//            }
        } catch (Exception e) {
//           e.printStackTrace();
            CustomLogger.error("Save OPS Update Response", e);
        }

        return op;
    }
    
    public MandateTriggerDataDocument getMandateTriggerDataDocumentById(Long id, Long refid) {
        MandateTriggerDataDocument obj=mandateTriggerDAO.getMandateTriggerDataDocumentById(id, refid);
        if(obj != null){
            return obj;
        }else{
            return null;
        }
    }
    
    public String getDocumentFromMiddelware(String docUuid, String applicationId) throws CustomNonFatalException {
        //This wasn't working with bean.So hardcoded String.
        String requestData = "{\"docDetail\": {\"uuid\": \"" + docUuid + "\"}}";
//        String requestData = "{\"docDetail\": {\"uuid\": \"" + "15" + "\"}}";

        RestTemplate restTemplate = new RestTemplate();
        EsLogDocumentDownload documentDownload = new EsLogDocumentDownload();
        documentDownload.setApplicationId(applicationId);
        documentDownload.setRequest(requestData);
        documentDownload.setRequestTime(DateUtility.getCurrentDate());
        String reqUrl = ApplicationPropertyManager.getProperty(FrameworkAppConstants.DOCUMENT_DOWNLOAD_URL);
//        String reqUrl = FrameworkAppConstants.DOCUMENT_DOWNLOAD_URL;

        String op = "";
        try {
            op = restTemplate.postForObject(reqUrl, requestData, String.class);
            documentDownload.setResponse(op);
            documentDownload.setResponseTime(DateUtility.getCurrentDate());
        } catch (Exception e) {
            throw new CustomNonFatalException("Service Unavilable.Please try after some time.", "Service Unavilable.Please try after some time.");
        } finally {
//            setEsLogDocumentDownload(documentDownload);
        }

        return op;
    }
    
    public List<MandateDocVo> getDocument(Long refid) throws CustomNonFatalException, JSONException{
        List<MandateDocVo> lstDoc = new ArrayList<MandateDocVo>();
        MandateDocVo mandateDocVo=null;
        
        List<MandateTriggerDataDocument> lstMandateTriggerDataDocument = mandateTriggerDAO.getMandateTriggerDataDocument(refid);
        if(lstMandateTriggerDataDocument == null){
            throw new CustomNonFatalException("Document Not Found","Document Not Found");
        }
        for(MandateTriggerDataDocument mandateTriggerDataDocument : lstMandateTriggerDataDocument){
            mandateDocVo = new MandateDocVo();
            mandateDocVo.setDocumentId(mandateTriggerDataDocument.getDocumentId());
            mandateDocVo.setRefid(refid);
            mandateDocVo.setApplicantID(mandateTriggerDataDocument.getApplicantID());
            mandateDocVo.setDescription(mandateTriggerDataDocument.getDescription());
            mandateDocVo.setDocCategoryName(mandateTriggerDataDocument.getDocCategoryName());
            mandateDocVo.setDocName(mandateTriggerDataDocument.getDocName());
            mandateDocVo.setDocRef(mandateTriggerDataDocument.getDocRef());
            mandateDocVo.setLosId(mandateTriggerDataDocument.getLosId());
            mandateDocVo.setOpsAction(mandateTriggerDataDocument.getOpsAction());
            mandateDocVo.setLosDocStatus(mandateTriggerDataDocument.getLosDocStatus());
            mandateDocVo.setQ1(mandateTriggerDataDocument.getQ1());
            mandateDocVo.setQ2(mandateTriggerDataDocument.getQ2());
            mandateDocVo.setQ3(mandateTriggerDataDocument.getQ3());
            mandateDocVo.setQ4(mandateTriggerDataDocument.getQ4());
            mandateDocVo.setQ5(mandateTriggerDataDocument.getQ5());
            mandateDocVo.setQ6(mandateTriggerDataDocument.getQ6());
            mandateDocVo.setQ7(mandateTriggerDataDocument.getQ7());
            mandateDocVo.setQ8(mandateTriggerDataDocument.getQ8());
            mandateDocVo.setQ9(mandateTriggerDataDocument.getQ9());
            mandateDocVo.setQ10(mandateTriggerDataDocument.getQ10());
            mandateDocVo.setQ11(mandateTriggerDataDocument.getQ11());
            mandateDocVo.setQ12(mandateTriggerDataDocument.getQ12());
            mandateDocVo.setQ13(mandateTriggerDataDocument.getQ13());
            mandateDocVo.setQ14(mandateTriggerDataDocument.getQ14());
            mandateDocVo.setQ15(mandateTriggerDataDocument.getQ15());
            mandateDocVo.setQ16(mandateTriggerDataDocument.getQ16());
            mandateDocVo.setQ17(mandateTriggerDataDocument.getQ17());
            mandateDocVo.setQ18(mandateTriggerDataDocument.getQ18());
            mandateDocVo.setQ19(mandateTriggerDataDocument.getQ19());
            mandateDocVo.setQ20(mandateTriggerDataDocument.getQ20());
            mandateDocVo.setQ21(mandateTriggerDataDocument.getQ21());
            mandateDocVo.setQ22(mandateTriggerDataDocument.getQ22());
            mandateDocVo.setQ23(mandateTriggerDataDocument.getQ23());
            mandateDocVo.setQ24(mandateTriggerDataDocument.getQ24());
            mandateDocVo.setQ25(mandateTriggerDataDocument.getQ25());
            
            String op=getDocumentFromMiddelware(mandateTriggerDataDocument.getDocRef(), mandateTriggerDataDocument.getMandateTriggerDataApplication().getRefid().toString());
            JSONObject docResp = new JSONObject(op);
            if(docResp.has("status") && docResp.get("status") != null && docResp.get("status").toString().trim().equalsIgnoreCase("1")
                  && docResp.has("data") && docResp.get("data") != null && !docResp.get("data").toString().trim().equalsIgnoreCase("null")){
                JSONObject dataJsn=docResp.getJSONObject("data");
                if(dataJsn.has("fileStream") && dataJsn.get("fileStream") != null && !dataJsn.get("fileStream").toString().trim().equalsIgnoreCase("") && !dataJsn.get("fileStream").toString().trim().equalsIgnoreCase("null")){
                    mandateDocVo.setFileStream(dataJsn.get("fileStream").toString().trim());
                }else{
                    throw new CustomNonFatalException("fileStream Not Found","fileStream Not Found");
                }
                if(dataJsn.has("ext") && dataJsn.get("ext") != null && !dataJsn.get("ext").toString().trim().equalsIgnoreCase("") && !dataJsn.get("ext").toString().trim().equalsIgnoreCase("null")){
                    mandateDocVo.setExt(dataJsn.get("ext").toString().trim());
                }else{
                    throw new CustomNonFatalException("ext Not Found","ext Not Found");
                }
                if(dataJsn.has("mimeType") && dataJsn.get("mimeType") != null && !dataJsn.get("mimeType").toString().trim().equalsIgnoreCase("") && !dataJsn.get("mimeType").toString().trim().equalsIgnoreCase("null")){
                    mandateDocVo.setMimeType(dataJsn.get("mimeType").toString().trim());
                }else{
                    throw new CustomNonFatalException("mimeType Not Found","mimeType Not Found");
                }
                if(dataJsn.has("fileName") && dataJsn.get("fileName") != null && !dataJsn.get("fileName").toString().trim().equalsIgnoreCase("") && !dataJsn.get("fileName").toString().trim().equalsIgnoreCase("null")){
                    mandateDocVo.setFileName(dataJsn.get("fileName").toString().trim());
                }
                if(dataJsn.has("parentUuid") && dataJsn.get("parentUuid") != null && !dataJsn.get("parentUuid").toString().trim().equalsIgnoreCase("") && !dataJsn.get("parentUuid").toString().trim().equalsIgnoreCase("null")){
                    mandateDocVo.setParentUuid(dataJsn.get("parentUuid").toString().trim());
                }
                if(dataJsn.has("parentFolderName") && dataJsn.get("parentFolderName") != null && !dataJsn.get("parentFolderName").toString().trim().equalsIgnoreCase("") && !dataJsn.get("parentFolderName").toString().trim().equalsIgnoreCase("null")){
                    mandateDocVo.setParentFolderName(dataJsn.get("parentFolderName").toString().trim());
                }
            }
            
            lstDoc.add(mandateDocVo);
        }
        
        return lstDoc;
    }
    
    public List<MandateTriggerDataApplication> getMandateTriggerDataApplicationLst(String applicationId) {
        List<MandateTriggerDataApplication> lst=mandateTriggerDAO.getMandateTriggerDataApplicationLst(applicationId);
        if(lst != null){
            return lst;
        }else{
            return null;
        }
    }
    
    public Boolean opsCheckSetLock(Long refid, String userId) throws CustomNonFatalException{
       MandateTriggerDataApplication mandateTriggerDataApplication = mandateTriggerDAO.getMandateTriggerDataApplicationByRefid(refid);
       if(mandateTriggerDataApplication != null){
           if(mandateTriggerDataApplication.getLockBy() == null){
               mandateTriggerDataApplication.setLockBy(userId);
               mandateTriggerDataApplication=mandateTriggerDAO.setMandateTriggerDataApplication(mandateTriggerDataApplication);
               return true;
           }else if(mandateTriggerDataApplication.getLockBy() != null && mandateTriggerDataApplication.getLockBy().trim().equalsIgnoreCase(userId)){
               return true;
           }else{
               throw new CustomNonFatalException("Application Is Locked","Application Is Locked");
           }
       }else{
           throw new CustomNonFatalException("Application Not Found","Application Not Found");
       }
    }
}
