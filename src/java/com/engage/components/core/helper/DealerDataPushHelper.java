/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsDealerData;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.engage.components.core.entity.dao.DealerDataPushDAO;

/**
 *
 * @author VEN00288
 */
@Service
public class DealerDataPushHelper {

    @Autowired 
    DealerDataPushDAO delarDataDao;
    String response;
    public void setDelarDataLog(String payload,String applicationId) {
        try
        {
            
            EsDealerData esDealerData=new EsDealerData();
        
            esDealerData.setApplicationId(applicationId);
            esDealerData.setRequest(payload);
            esDealerData.setResponse("");
//            esDealerData.setCreId("1");
//            esDealerData.setModId("123");
            esDealerData.setCreDate(DateUtility.getCurrentDate());
            esDealerData.setModDate(DateUtility.getCurrentDate());

            delarDataDao.saveDelarDataLog(esDealerData);
        }catch(Exception e)
        {
        
        }
    }

    public void setResponse(EsDealerData esDealerData) {
        try
        {
           delarDataDao.saveDelarDataLog(esDealerData);
        }catch(Exception e)
        {
        }
    }

    public  String saveAssetDetails(EsAssetDetailBean assetDetails) {
           response=delarDataDao.saveAsset(assetDetails);
            return response;
    }

    public String saveLoanDetails(EsLoanDetailBean loanDetails) {
        response=delarDataDao.saveLoanData(loanDetails);
            return response;
    }

    public String saveApplicantDetails(EsApplicantsDetailBean applicantsData) {
       response=delarDataDao.saveApplicantsData(applicantsData);
            return response;  
    }

    public EsDealerData getRequestLog(String applicationId) {
       EsDealerData reqLog=delarDataDao.getReqLog(applicationId);
       return reqLog;
    }

   
    
}
