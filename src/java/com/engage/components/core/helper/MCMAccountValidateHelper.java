/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.helper;

import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.MCMLogAccountActivate;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.dao.MCMAccountValidateDao;
import com.engage.components.core.entity.dao.McmTriggerDAO;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.mcm.vo.mcmAccountValidateVO;
import com.engage.components.esportal.mcm.vo.mcmConsumerConsumerAliasVO;
import com.engage.components.esportal.mcm.vo.mcmConsumerVO;
import com.engage.components.esportal.mcm.vo.mcmMerchantVO;
import com.engage.components.esportal.mcm.vo.mcmOrderCartItemVO;
import com.engage.components.esportal.mcm.vo.mcmOrderCartVO;
import com.engage.components.esportal.mcm.vo.mcmOrderVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentInstructionVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentMethodVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentReceiverVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentReceiverVO1;
import com.engage.components.esportal.mcm.vo.mcmPaymentSenderInstrumentAuthenticationVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentSenderInstrumentExpiryVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentSenderInstrumentHolderAddressVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentSenderInstrumentHolderVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentSenderInstrumentIssuanceVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentSenderInstrumentVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentSenderVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentVO;
import com.engage.components.esportal.mcm.vo.mcmPaymentVO1;
import com.engage.components.esportal.mcm.vo.mcmTransactionVO;
import com.engage.components.esportal.vo.FusyServiceReqDetailsVO;
import com.engage.components.esportal.vo.fusyServiceReqVO;
import com.engage.framework.util.log4j.CustomLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author VEN00288
 */
@Service
public class MCMAccountValidateHelper {

    @Autowired
    MCMAccountValidateDao validateDao;

    @Autowired
    McmTriggerDAO mcmTriggerDAO;

    @Autowired
    McmTriggerHelper mcmTriggerHelper;

    public String validateUserAccount(String netDisbursementAmt, String bankAccNo, String ifsc, String modeOfDisbursement, String applicantName, String mobileNo, String applicationId) throws CustomNonFatalException {

        MCMLogAccountActivate logActivate = null;
        ObjectMapper mapper = null;
        String response = "";
        McmApplicationDetails applicationDetails = null;
        try {
            logActivate = new MCMLogAccountActivate();

            mcmAccountValidateVO validateVO = setAccountValidateReqData();
            mcmMerchantVO merchantVo = new mcmMerchantVO();
            merchantVo.setWebhookEndpointURL("");
            merchantVo.setWebhookType("");
            merchantVo.setResponseType("");
            merchantVo.setResponseEndpointURL("");
            merchantVo.setDescription("");
            merchantVo.setIdentifier("PNY");
            merchantVo.setCardRegPGMID("");
            merchantVo.setTxnPGMID("T141514");
            merchantVo.setAspMasterId("");
            validateVO.setMerchantVO(merchantVo);

            mcmOrderVO orderVo = new mcmOrderVO();
            mcmOrderCartVO cartVo = new mcmOrderCartVO();
            List<mcmOrderCartItemVO> lstItemVo = new ArrayList<mcmOrderCartItemVO>();
            mcmOrderCartItemVO itemVo = new mcmOrderCartItemVO();
            itemVo.setDescription("");
            itemVo.setProviderIdentifier("");
            itemVo.setSurchargeOrDiscountAmount("");
            itemVo.setAmount("");
            itemVo.setSku("");
            itemVo.setReference("");
            itemVo.setIdentifier("");
            lstItemVo.add(itemVo);
            cartVo.setItemVO(lstItemVo);
            cartVo.setReference("");
            cartVo.setIdentifier("");
            cartVo.setDescription("");
            orderVo.setCartVO(cartVo);
            validateVO.setOrderVO(orderVo);

            mcmPaymentVO paymentVo = new mcmPaymentVO();
            mcmPaymentMethodVO methodVo = new mcmPaymentMethodVO();
            methodVo.setToken("");
            methodVo.setType("");
            paymentVo.setMethodVO(methodVo);

            mcmPaymentSenderVO senderVo = new mcmPaymentSenderVO();
            senderVo.setPaynimoSenderAliasToken("");
            senderVo.setPaynimoSenderName("");
            senderVo.setPaynimoSenderInstrumentToken("");
            senderVo.setPaynimoSenderInstrumentNumber("");
            senderVo.setPaynimoSendTxnID("");
            senderVo.setPaynimoSendTxnStatus("");

            mcmPaymentSenderInstrumentVO instrumentVo = new mcmPaymentSenderInstrumentVO();
            mcmPaymentSenderInstrumentExpiryVO expiryVo = new mcmPaymentSenderInstrumentExpiryVO();
            expiryVo.setYear("");
            expiryVo.setMonth("");
            expiryVo.setDateTime("");
            instrumentVo.setExpiryVO(expiryVo);
            instrumentVo.setProvider("");
            instrumentVo.setiFSC("");
            mcmPaymentSenderInstrumentHolderVO holderVo = new mcmPaymentSenderInstrumentHolderVO();
            holderVo.setName("");
            mcmPaymentSenderInstrumentHolderAddressVO addressVo = new mcmPaymentSenderInstrumentHolderAddressVO();
            addressVo.setCountry("");
            addressVo.setStreet("");
            addressVo.setState("");
            addressVo.setCity("");
            addressVo.setZipCode("");
            addressVo.setCounty("");
            holderVo.setAddressVO(addressVo);
            instrumentVo.setHolderVO(holderVo);
            instrumentVo.setBic("");
            instrumentVo.setType("");
            instrumentVo.setAction("");
            instrumentVo.setMicr("");
            instrumentVo.setVerificationCode("");
            instrumentVo.setIban("");
            instrumentVo.setProcessor("");
            mcmPaymentSenderInstrumentIssuanceVO issuanceVO = new mcmPaymentSenderInstrumentIssuanceVO();
            issuanceVO.setYear("");
            issuanceVO.setMonth("");
            issuanceVO.setDateTime("");
            instrumentVo.setIssuanceVO(issuanceVO);
            instrumentVo.setToVault("");
            instrumentVo.setAlias("");
            instrumentVo.setIdentifier("");
            instrumentVo.setToken("");
            mcmPaymentSenderInstrumentAuthenticationVO authenticationVO = new mcmPaymentSenderInstrumentAuthenticationVO();
            authenticationVO.setToken("");
            authenticationVO.setType("");
            authenticationVO.setSubType("");
            instrumentVo.setAuthenticationVO(authenticationVO);

            instrumentVo.setSubType("");
            instrumentVo.setIssuer("");
            instrumentVo.setAcquirer("");
            senderVo.setInstrumentVO(instrumentVo);
            paymentVo.setSenderVO(senderVo);

            mcmPaymentReceiverVO receiverVo = new mcmPaymentReceiverVO();
            receiverVo.setPaynimoReceiverAliasToken("");
            receiverVo.setPaynimoBeneficiaryName("");
            receiverVo.setPaynimoBeneficiaryInstrumentToken("");
            receiverVo.setPaynimoBeneficiaryInstrumentNumber("");
            receiverVo.setPaynimoSenderSourceOfFunds("");
            receiverVo.setPaynimoReceiveTxnID("");
            receiverVo.setPaynimoReceiveTxnStatus("");
            paymentVo.setReceiverVO(receiverVo);

            //
            mcmPaymentSenderInstrumentVO instrumentVo1 = new mcmPaymentSenderInstrumentVO();
            mcmPaymentSenderInstrumentExpiryVO expiryVo1 = new mcmPaymentSenderInstrumentExpiryVO();
            expiryVo1.setYear("");
            expiryVo1.setMonth("");
            expiryVo1.setDateTime("");
            instrumentVo1.setExpiryVO(expiryVo1);
            instrumentVo1.setProvider("");
            instrumentVo1.setiFSC("");
            mcmPaymentSenderInstrumentHolderVO holderVo1 = new mcmPaymentSenderInstrumentHolderVO();
            holderVo1.setName("");
            mcmPaymentSenderInstrumentHolderAddressVO addressVo1 = new mcmPaymentSenderInstrumentHolderAddressVO();
            addressVo1.setCountry("");
            addressVo1.setStreet("");
            addressVo1.setState("");
            addressVo1.setCity("");
            addressVo1.setZipCode("");
            addressVo1.setCounty("");
            holderVo1.setAddressVO(addressVo);
            instrumentVo1.setHolderVO(holderVo);
            instrumentVo1.setBic("");
            instrumentVo1.setType("");
            instrumentVo1.setAction("");
            instrumentVo1.setMicr("");
            instrumentVo1.setVerificationCode("");
            instrumentVo1.setIban("");
            instrumentVo1.setProcessor("");
            mcmPaymentSenderInstrumentIssuanceVO issuanceVO1 = new mcmPaymentSenderInstrumentIssuanceVO();
            issuanceVO1.setYear("");
            issuanceVO1.setMonth("");
            issuanceVO1.setDateTime("");
            instrumentVo1.setIssuanceVO(issuanceVO1);
            instrumentVo1.setToVault("");
            instrumentVo1.setAlias("");
            instrumentVo1.setIdentifier("");
            instrumentVo1.setToken("");
            mcmPaymentSenderInstrumentAuthenticationVO authenticationVO1 = new mcmPaymentSenderInstrumentAuthenticationVO();
            authenticationVO1.setToken("");
            authenticationVO1.setType("");
            authenticationVO1.setSubType("");
            instrumentVo1.setAuthenticationVO(authenticationVO1);

            instrumentVo1.setSubType("");
            instrumentVo1.setIssuer("");
            instrumentVo1.setAcquirer("");
            paymentVo.setInstrumentVO(instrumentVo1);

            mcmPaymentInstructionVO instructionVO = new mcmPaymentInstructionVO();
            instructionVO.setOccurrence("");
            instructionVO.setAmount("");
            instructionVO.setFrequency("");
            instructionVO.setType("");
            instructionVO.setDescription("");
            instructionVO.setAction("");
            instructionVO.setLimit("");
            instructionVO.setEndDateTime("");
            instructionVO.setIdentifier("");
            instructionVO.setReference("");
            instructionVO.setStartDateTime("");
            instructionVO.setValidity("");
            paymentVo.setInstructionVO(instructionVO);

            validateVO.setPaymentVO(paymentVo);

            mcmTransactionVO transactionVO = new mcmTransactionVO();

            transactionVO.setDeviceIdentifier("");
            transactionVO.setChannelType("WEB");
            transactionVO.setChannelName("RIB");
            transactionVO.setUtm_source("");
            transactionVO.setUtm_medium("");
            transactionVO.setUtm_term("");
            transactionVO.setUtm_content("");
            transactionVO.setUtm_campaign("");
            transactionVO.setSmsSending("");
            transactionVO.setAmount("1.00");
            transactionVO.setForced3DSCall("");
            transactionVO.setType("006");
            transactionVO.setDescription("test transaction");
            transactionVO.setCurrency("");
            transactionVO.setIdentifier("");
            transactionVO.setDateTime("");
            transactionVO.setToken("");
            transactionVO.setTxnErrorDesc("");
            transactionVO.setTxnErrorCode("");
            transactionVO.setSubType("001");
            transactionVO.setRequestType("FD");
            transactionVO.setAction("");
            transactionVO.setReference(Long.parseLong(generateRandomNumber()));

            mcmPaymentVO1 paymentVo1 = new mcmPaymentVO1();
            mcmPaymentMethodVO methodVo1 = new mcmPaymentMethodVO();
            methodVo1.setToken("");
            methodVo1.setType("");
            paymentVo1.setMethodVO(methodVo);

            mcmPaymentSenderVO senderVo1 = new mcmPaymentSenderVO();
            senderVo1.setPaynimoSenderAliasToken("");
            senderVo1.setPaynimoSenderName("");
            senderVo1.setPaynimoSenderInstrumentToken("");
            senderVo1.setPaynimoSenderInstrumentNumber("");
            senderVo1.setPaynimoSendTxnID("");
            senderVo1.setPaynimoSendTxnStatus("");

            mcmPaymentSenderInstrumentVO instrumentVo2 = new mcmPaymentSenderInstrumentVO();
            mcmPaymentSenderInstrumentExpiryVO expiryVo2 = new mcmPaymentSenderInstrumentExpiryVO();
            expiryVo2.setYear("");
            expiryVo2.setMonth("");
            expiryVo2.setDateTime("");
            instrumentVo2.setExpiryVO(expiryVo2);
            instrumentVo2.setProvider("");
            instrumentVo2.setiFSC("");

            mcmPaymentSenderInstrumentHolderVO holderVo2 = new mcmPaymentSenderInstrumentHolderVO();
            holderVo2.setName("");
            mcmPaymentSenderInstrumentHolderAddressVO addressVo2 = new mcmPaymentSenderInstrumentHolderAddressVO();
            addressVo2.setCountry("");
            addressVo2.setStreet("");
            addressVo2.setState("");
            addressVo2.setCity("");
            addressVo2.setZipCode("");
            addressVo2.setCounty("");
            holderVo2.setAddressVO(addressVo2);
            instrumentVo2.setHolderVO(holderVo2);
            instrumentVo2.setBic("");
            instrumentVo2.setType("");
            instrumentVo2.setAction("");
            instrumentVo2.setMicr("");
            instrumentVo2.setVerificationCode("");
            instrumentVo2.setIban("");
            instrumentVo2.setProcessor("");

            mcmPaymentSenderInstrumentIssuanceVO issuanceVO2 = new mcmPaymentSenderInstrumentIssuanceVO();
            issuanceVO2.setYear("");
            issuanceVO2.setMonth("");
            issuanceVO2.setDateTime("");
            instrumentVo2.setIssuanceVO(issuanceVO2);
            instrumentVo2.setToVault("");
            instrumentVo2.setAlias("");
            instrumentVo2.setIdentifier("");
            instrumentVo2.setToken("");
            mcmPaymentSenderInstrumentAuthenticationVO authenticationVO2 = new mcmPaymentSenderInstrumentAuthenticationVO();
            authenticationVO2.setToken("");
            authenticationVO2.setType("");
            authenticationVO2.setSubType("");
            instrumentVo2.setAuthenticationVO(authenticationVO2);

            instrumentVo2.setSubType("");
            instrumentVo2.setIssuer("");
            instrumentVo2.setAcquirer("");
            senderVo1.setInstrumentVO(instrumentVo2);
            paymentVo1.setSenderVO(senderVo1);

            mcmPaymentReceiverVO1 receiverVo1 = new mcmPaymentReceiverVO1();
            receiverVo1.setPaynimoReceiverAliasToken("");
            receiverVo1.setPaynimoBeneficiaryName("");
            receiverVo1.setPaynimoBeneficiaryInstrumentToken("");
            receiverVo1.setPaynimoBeneficiaryInstrumentNumber("");
            receiverVo1.setPaynimoSenderSourceOfFunds("");
            receiverVo1.setPaynimoReceiveTxnID("");
            receiverVo1.setPaynimoReceiveTxnStatus("");
            receiverVo1.setName(applicantName);//applicantName

            mcmPaymentSenderInstrumentVO instrumentVo3 = new mcmPaymentSenderInstrumentVO();
            mcmPaymentSenderInstrumentExpiryVO expiryVo3 = new mcmPaymentSenderInstrumentExpiryVO();
            expiryVo3.setYear("");
            expiryVo3.setMonth("");
            expiryVo3.setDateTime("");
            instrumentVo3.setExpiryVO(expiryVo3);
            instrumentVo3.setProvider("");
            instrumentVo3.setiFSC(ifsc);//
            mcmPaymentSenderInstrumentHolderVO holderVo3 = new mcmPaymentSenderInstrumentHolderVO();
            holderVo3.setName(applicantName);//applicantName
            mcmPaymentSenderInstrumentHolderAddressVO addressVo3 = new mcmPaymentSenderInstrumentHolderAddressVO();
            addressVo3.setCountry("");
            addressVo3.setStreet("");
            addressVo3.setState("");
            addressVo3.setCity("");
            addressVo3.setZipCode("");
            addressVo3.setCounty("");
            holderVo3.setAddressVO(addressVo3);
            instrumentVo3.setHolderVO(holderVo3);
            instrumentVo3.setBic("");
            instrumentVo3.setType("");
            instrumentVo3.setAction("");
            instrumentVo3.setMicr("");
            instrumentVo3.setVerificationCode("");
            instrumentVo3.setIban("");
            instrumentVo3.setProcessor("");
            mcmPaymentSenderInstrumentIssuanceVO issuanceVO3 = new mcmPaymentSenderInstrumentIssuanceVO();
            issuanceVO3.setYear("");
            issuanceVO3.setMonth("");
            issuanceVO3.setDateTime("");
            instrumentVo3.setIssuanceVO(issuanceVO3);

            instrumentVo3.setToVault("");
            instrumentVo3.setAlias("");
            instrumentVo3.setIdentifier(generateRandomNumber());//119601519951
            instrumentVo3.setToken("");
            mcmPaymentSenderInstrumentAuthenticationVO authenticationVO3 = new mcmPaymentSenderInstrumentAuthenticationVO();
            authenticationVO3.setToken("");
            authenticationVO3.setType("");
            authenticationVO3.setSubType("");
            instrumentVo3.setAuthenticationVO(authenticationVO3);
            instrumentVo3.setSubType("");
            instrumentVo3.setIssuer("A");
            instrumentVo3.setAcquirer("");
            receiverVo1.setInstrumentVO(instrumentVo3);
            paymentVo1.setReceiverVO(receiverVo1);

            mcmPaymentSenderInstrumentVO instrumentVo4 = new mcmPaymentSenderInstrumentVO();
            mcmPaymentSenderInstrumentExpiryVO expiryVo4 = new mcmPaymentSenderInstrumentExpiryVO();
            expiryVo4.setYear("");
            expiryVo4.setMonth("");
            expiryVo4.setDateTime("");
            instrumentVo4.setExpiryVO(expiryVo4);
            instrumentVo4.setProvider("");
            instrumentVo4.setiFSC("");
            mcmPaymentSenderInstrumentHolderVO holderVo4 = new mcmPaymentSenderInstrumentHolderVO();
            holderVo4.setName("");
            mcmPaymentSenderInstrumentHolderAddressVO addressVo4 = new mcmPaymentSenderInstrumentHolderAddressVO();
            addressVo4.setCountry("");
            addressVo4.setStreet("");
            addressVo4.setState("");
            addressVo4.setCity("");
            addressVo4.setZipCode("");
            addressVo4.setCounty("");
            holderVo4.setAddressVO(addressVo4);
            instrumentVo4.setHolderVO(holderVo4);
            instrumentVo4.setBic("");
            instrumentVo4.setType("");
            instrumentVo4.setAction("");
            instrumentVo4.setMicr("");
            instrumentVo4.setVerificationCode("");
            instrumentVo4.setIban("");
            instrumentVo4.setProcessor("");
            mcmPaymentSenderInstrumentIssuanceVO issuanceVO4 = new mcmPaymentSenderInstrumentIssuanceVO();
            issuanceVO4.setYear("");
            issuanceVO4.setMonth("");
            issuanceVO4.setDateTime("");
            instrumentVo4.setIssuanceVO(issuanceVO4);
            instrumentVo4.setToVault("");
            instrumentVo4.setAlias("");
            instrumentVo4.setIdentifier("");
            instrumentVo4.setToken("");
            mcmPaymentSenderInstrumentAuthenticationVO authenticationVO4 = new mcmPaymentSenderInstrumentAuthenticationVO();
            authenticationVO4.setToken("");
            authenticationVO4.setType("");
            authenticationVO4.setSubType("");
            instrumentVo4.setAuthenticationVO(authenticationVO4);
            instrumentVo4.setSubType("");
            instrumentVo4.setIssuer("");
            instrumentVo4.setAcquirer("");
            paymentVo1.setInstrumentVO(instrumentVo4);

            mcmPaymentInstructionVO instructionVO1 = new mcmPaymentInstructionVO();
            instructionVO1.setOccurrence("");
            instructionVO1.setAmount("");
            instructionVO1.setFrequency("");
            instructionVO1.setType("");
            instructionVO1.setDescription("");
            instructionVO1.setAction("");
            instructionVO1.setLimit("");
            instructionVO1.setEndDateTime("");
            instructionVO1.setIdentifier("");
            instructionVO1.setReference("");
            instructionVO1.setStartDateTime("");
            instructionVO1.setValidity("");

            paymentVo1.setInstructionVO(instructionVO1);

            transactionVO.setPaymentVO(paymentVo1);

            validateVO.setTransactionVO(transactionVO);

            mcmConsumerVO consumerVO = new mcmConsumerVO();
            consumerVO.setName(applicantName);//applicantName
            consumerVO.setMobileNumber(Long.parseLong(mobileNo));//9999999999L
            consumerVO.setEmailID("");
            consumerVO.setIdentifier("");
            consumerVO.setPaynimoCustRegId("");
            consumerVO.setAccouontNo(bankAccNo);
            consumerVO.setHashURL("");
            consumerVO.setInvitationCode("");
            consumerVO.setCustSecurityQuestID("");
            consumerVO.setCustSecurityAns("");
            consumerVO.setCustSecretKey("");
            consumerVO.setCustRegStatus("");
            consumerVO.setCustType("");
            consumerVO.setRoleId("");

            mcmConsumerConsumerAliasVO consumerAliasVO = new mcmConsumerConsumerAliasVO();
            List<mcmConsumerConsumerAliasVO> lstConsumerAliasVO = new ArrayList<mcmConsumerConsumerAliasVO>();
            consumerAliasVO.setCustAlias("");
            consumerAliasVO.setCustAliasId("");
            consumerAliasVO.setCustAliasSource("");
            consumerAliasVO.setCustAliasType("");
            lstConsumerAliasVO.add(consumerAliasVO);
            consumerVO.setAliasVO(lstConsumerAliasVO);
            validateVO.setConsumerVO(consumerVO);

            mapper = new ObjectMapper();
//         System.out.println(mapper.writeValueAsString(validateVO));

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            HttpEntity request = new HttpEntity(validateVO);

            //        String HLUrlAdd = ApplicationPropertyManager.getProperty(FrameworkAppConstants.MCM_ACCOUNT_VALIDATE_URL);
            String HLUrlAdd = "http://10.3.2.38:7801/LTFS_PENNY_TRANSFER_TECHPROCESS";

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(HLUrlAdd);

            logActivate.setRequest(mapper.writeValueAsString(validateVO));
            logActivate.setModDate(DateUtility.getCurrentDate());

            ResponseEntity result = restTemplate.exchange(builder.buildAndExpand().toUri(), HttpMethod.POST, request, String.class);

            System.out.println("result  >>>>" + result);

            JSONObject resObj = new JSONObject(result.getBody().toString());

            JSONObject dataObj = (JSONObject) resObj.get("transaction");

            System.out.println("Transactional Obj  >>>>" + dataObj);

            applicationDetails = mcmTriggerHelper.getMcmApplicationDetailsDetail(applicationId);

            applicationDetails.setApplicationId(applicationId);
            applicationDetails.setModeOfDisbursement(modeOfDisbursement);
            applicationDetails.setBankAccountNumber(bankAccNo);
            applicationDetails.setIfscCode(ifsc);

            if (dataObj.get("txnErrorDesc").toString().equalsIgnoreCase("Transaction Successful") || dataObj.get("txnErrorDesc").toString().equalsIgnoreCase("Transaction is already exists with success status")) {
                applicationDetails.setAccountValidationStatus(FrameworkAppConstants.VALID_STATUS);
                applicationDetails.setIsAccountValid(FrameworkAppConstants.CONSTANT_YES);
                applicationDetails.setAccountValidationMessage(dataObj.get("txnErrorDesc").toString());

//                fusyServiceReqVO fusyReq = new fusyServiceReqVO();
//                List<FusyServiceReqDetailsVO> detailsLst = new ArrayList<FusyServiceReqDetailsVO>();
//                FusyServiceReqDetailsVO details = new FusyServiceReqDetailsVO();
//
//                details.setFirstName("O P");
//                details.setLastName("SHARMA");
//                details.setFirstNamePan("OM PRAKASH");
//                details.setLastNamePan("SHARMA");
//                details.setReferenceID("123");
//                detailsLst.add(details);
//                fusyReq.setApplicantsDetails(detailsLst);
//
//                String fusyRes = mcmTriggerHelper.callSoundexService(fusyReq);

            } else {
                applicationDetails.setModeOfDisbursement("CASH");
                applicationDetails.setAccountValidationStatus(FrameworkAppConstants.IN_VALID_STATUS);
                applicationDetails.setIsAccountValid(FrameworkAppConstants.CONSTANT_YES);
                applicationDetails.setAccountValidationMessage(dataObj.get("txnErrorDesc").toString());
            }

            mcmTriggerHelper.updateMCMApplicationDetails(applicationDetails);

            logActivate.setResponse(result.getBody().toString());
//            logActivate.setModId(userId.toString());
            logActivate.setModDate(DateUtility.getCurrentDate());
            response = dataObj.get("txnErrorDesc").toString();
        } catch (HttpStatusCodeException e) {
//            System.out.println("statusCode>>>>>>>>>>>>>>>" + e.getMessage());
            e.printStackTrace();
            throw new CustomNonFatalException("Call to Account Validation Service Failed", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            response = "error while account validation";
            CustomLogger.error("Account Validate WS Call", e);
        } finally {
            validateDao.setMCMLogAccountActivate(logActivate);
        }
        return response;

    }

    public String sendAccountValidateData() {

        return "";
    }

    public mcmAccountValidateVO setAccountValidateReqData() {

        mcmAccountValidateVO validateVO = new mcmAccountValidateVO();
        return validateVO;
    }

    public String generateRandomNumber() {
        Random rand = null;
        int num = 0;
        String randomNo = null;
        try {
            rand = new Random();
            num = rand.nextInt(900000000) + 100000000;
            randomNo = Integer.toString(num);
            System.out.println("randomNo >>>>>>>>" + randomNo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return randomNo;
    }
}
