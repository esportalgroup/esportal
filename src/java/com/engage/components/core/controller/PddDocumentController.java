/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsPddDocumentDetail;
import com.engage.components.core.helper.PddDocumentHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.PddDocDetailVo;
import com.engage.components.esportal.vo.uploadDocumentDetailWS;
import com.engage.components.esportal.vo.uploadDocumentDetails;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Component
@Controller
public class PddDocumentController {

    @Autowired
    PddDocumentHelper documentHelper;

    @RequestMapping(value = "/getPddDocumentDetail", method = RequestMethod.POST)
    public ModelAndView forgot(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try {

            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("userId not entered", "userId not entered");
            }

            if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("applicationId not entered", "applicationId not entered");
            }

            String applicationId = reqJson.get("applicationId").toString();
            Long userId = Long.parseLong(reqJson.get("userId").toString());

            PddDocDetailVo detailVo = documentHelper.getPddDocumentDetail(applicationId);
            if (detailVo != null) {
                mav.addObject("status", "200");
                mav.addObject("pddDocDetail", detailVo);
            } else {
                mav.addObject("status", "400");
                mav.addObject("msg", "Document Detail Not Found.");
            }

        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("getPddDocumentDetail", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getPddDocumentDetail", e);
        }
        return mav;
    }

    @RequestMapping(value = "/invoiceUpload")
    public ModelAndView upload(@RequestParam("file") MultipartFile file,
            @RequestParam("data") String payload) throws IOException, JSONException, Exception {
        ModelAndView mav = new ModelAndView("home");
        try {
            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("Invalid applicationId", "Invalid applicationId");
            }
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid user Id", "Invalid user Id");
            }
            if (reqJson.get("docType") == null || "".equals(reqJson.get("docType").toString().trim())) {
                throw new CustomNonFatalException("Invalid docType", "Invalid docType");
            }

            String applicationId = reqJson.get("applicationId").toString();
            Long userId = Long.parseLong(reqJson.get("userId").toString());
            String docType = reqJson.get("docType").toString();
            byte[] bytes;

            bytes = file.getBytes();
            if (!file.isEmpty()) {

                StringBuilder sb = new StringBuilder();
                sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(bytes, false)));
                List<uploadDocumentDetails> lstDocumentDetails = new ArrayList<uploadDocumentDetails>();

                uploadDocumentDetails documentDetails = new uploadDocumentDetails();
                documentDetails.setApplicantId(applicationId);
                documentDetails.setDocOriginalName(reqJson.get("docOriginalName").toString());
                documentDetails.setDocType(reqJson.get("docType").toString());
                documentDetails.setExt(reqJson.get("ext").toString());
                documentDetails.setMimeType(reqJson.get("mimeType").toString());
                documentDetails.setDocumentString(sb.toString());

                lstDocumentDetails.add(documentDetails);

                uploadDocumentDetailWS detailWS = new uploadDocumentDetailWS();
                detailWS.setApplicationId(applicationId);
                detailWS.setApplicationName("TWH");
                detailWS.setDocumentDetailses(lstDocumentDetails);

                String op = documentHelper.documentUploadToMiddelware(detailWS);
                String docName = null, docUuid = null;
                if (op != null || !op.equals("")) {
                    JSONObject docResp = new JSONObject(op);
                    if (docResp.has("errorCode") && !docResp.get("errorCode").toString().trim().equals("") && docResp.get("errorCode") != null) {
                        if (docResp.get("errorCode").toString().equals("0")) {

                            JSONObject dataObj = docResp.getJSONObject("data");
                            JSONArray lstDocDetail = dataObj.getJSONArray("docDetail");
                            JSONObject docObj;
                            for (int i = 0; i < lstDocDetail.length(); i++) {
                                docObj = lstDocDetail.getJSONObject(i);
                                if (docObj.has("name")) {
                                    docName = docObj.get("name").toString();
                                }
                                if (docObj.has("uuid")) {
                                    docUuid = docObj.get("uuid").toString();
                                }

                            }

                            EsApplicantsDetailBean esApplicantsDetailBean = documentHelper.getApplicantsDetailByApplicantionID(applicationId);
                            if(esApplicantsDetailBean == null){
                                throw new CustomNonFatalException("Application Detail Not Found", "Application Detail Not Foundr");
                            }
                            
                            if (docType.equalsIgnoreCase("Invoice")) {
                                //Getting data from JSON
                                if (reqJson.get("invoiceNumber") == null || "".equals(reqJson.get("invoiceNumber").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid invoiceNumber", "Invalid invoiceNumber");
                                }
                                if (reqJson.get("invoiceDate") == null || "".equals(reqJson.get("invoiceDate").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid invoiceDate", "Invalid invoiceDate");
                                }
                                if (reqJson.get("engineNumber") == null || "".equals(reqJson.get("engineNumber").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid engineNumber", "Invalid engineNumber");
                                }
                                if (reqJson.get("chasisNumber") == null || "".equals(reqJson.get("chasisNumber").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid chasisNumber", "Invalid chasisNumber");
                                }
                                if (reqJson.get("invoiceHypothicationStatus") == null || "".equals(reqJson.get("invoiceHypothicationStatus").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid invoiceHypothicationStatus", "Invalid invoiceHypothicationStatus");
                                }

                                String invoiceNumber = reqJson.get("invoiceNumber").toString().toUpperCase();
                                String engineNumber = reqJson.get("engineNumber").toString().toUpperCase();
                                String chasisNumber = reqJson.get("chasisNumber").toString().toUpperCase();
                                String invoiceHypothicationStatus = reqJson.get("invoiceHypothicationStatus").toString();
                                EsPddDocumentDetail documentDetail = documentHelper.getEsPddDocumentDetail(applicationId);

                                if (documentDetail == null) {
                                    documentDetail = new EsPddDocumentDetail();
                                    documentDetail.setApplicationId(applicationId);
                                    documentDetail.setChassisNumber(chasisNumber);
                                    documentDetail.setEngineNumber(engineNumber);
                                    documentDetail.setInvoiceDate(DateUtility.parseStringToDate(reqJson.get("invoiceDate").toString(), "dd-MM-yyyy"));
                                    documentDetail.setCreBy(userId.toString());
                                    documentDetail.setCreDate(DateUtility.getCurrentDate());
                                    documentDetail.setInvoiceNumber(invoiceNumber);
                                    documentDetail.setInvoiceHypothicationStatus(invoiceHypothicationStatus);
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                                    documentDetail.setInvoiceDocName(docName);
                                    documentDetail.setInvoiceDocType(docType);
                                    documentDetail.setInvoiceDocUuid(docUuid);

                                } else {
                                    documentDetail.setChassisNumber(chasisNumber);
                                    documentDetail.setEngineNumber(engineNumber);
                                    documentDetail.setInvoiceDate(DateUtility.parseStringToDate(reqJson.get("invoiceDate").toString(), "dd-MM-yyyy"));
                                    documentDetail.setInvoiceNumber(invoiceNumber);
                                    documentDetail.setInvoiceHypothicationStatus(invoiceHypothicationStatus);
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setInvoiceDocName(docName);
                                    documentDetail.setInvoiceDocType(docType);
                                    documentDetail.setInvoiceDocUuid(docUuid);
                                }
                                documentHelper.setEsPddDocumentDetail(documentDetail);
                            } else if (docType.equalsIgnoreCase("Registration")) {
                                if (reqJson.get("regNo") == null || "".equals(reqJson.get("regNo").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid regNo", "Invalid regNo");
                                }
                                if (reqJson.get("registrationDate") == null || "".equals(reqJson.get("registrationDate").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid registrationDate", "Invalid registrationDate");
                                }
                                if (reqJson.get("rcHypothicationStatus") == null || "".equals(reqJson.get("rcHypothicationStatus").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid rcHypothicationStatus", "Invalid rcHypothicationStatus");
                                }

                                String regNo = reqJson.get("regNo").toString().toUpperCase();
                                String rcHypothicationStatus = reqJson.get("rcHypothicationStatus").toString();

                                EsPddDocumentDetail documentDetail = documentHelper.getEsPddDocumentDetail(applicationId);

                                if (documentDetail == null) {
                                    documentDetail = new EsPddDocumentDetail();
                                    documentDetail.setApplicationId(applicationId);
                                    documentDetail.setRegistrationNumber(regNo);
                                    documentDetail.setRegistrationDate(DateUtility.parseStringToDate(reqJson.get("registrationDate").toString(), "dd-MM-yyyy"));
                                    documentDetail.setCreBy(userId.toString());
                                    documentDetail.setCreDate(DateUtility.getCurrentDate());
                                    documentDetail.setRegistrationHypothicationStatus(rcHypothicationStatus);
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                                    documentDetail.setRegistrationDocName(docName);
                                    documentDetail.setRegistrationDocType(docType);
                                    documentDetail.setRegistrationDocUuid(docUuid);

                                } else {
                                    documentDetail.setRegistrationNumber(regNo);
                                    documentDetail.setRegistrationDate(DateUtility.parseStringToDate(reqJson.get("registrationDate").toString(), "dd-MM-yyyy"));
                                    documentDetail.setRegistrationHypothicationStatus(rcHypothicationStatus);
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setRegistrationDocName(docName);
                                    documentDetail.setRegistrationDocType(docType);
                                    documentDetail.setRegistrationDocUuid(docUuid);
                                }
                                documentHelper.setEsPddDocumentDetail(documentDetail);
                            } else if (docType.equalsIgnoreCase("Margin_Money")) {
                                EsPddDocumentDetail documentDetail = documentHelper.getEsPddDocumentDetail(applicationId);

                                if (documentDetail == null) {
                                    documentDetail = new EsPddDocumentDetail();
                                    documentDetail.setApplicationId(applicationId);
                                    documentDetail.setCreBy(userId.toString());
                                    documentDetail.setCreDate(DateUtility.getCurrentDate());
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                                    documentDetail.setMarginMoneyDocName(docName);
                                    documentDetail.setMarginMoneyDocType(docType);
                                    documentDetail.setMarginMoneyDocUuid(docUuid);

                                } else {
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setMarginMoneyDocName(docName);
                                    documentDetail.setMarginMoneyDocType(docType);
                                    documentDetail.setMarginMoneyDocUuid(docUuid);
                                }
                                documentHelper.setEsPddDocumentDetail(documentDetail);
                            }
                            esApplicantsDetailBean.setPddStatus("PDD SUBMITTED");
                            esApplicantsDetailBean = documentHelper.setEsApplicantsDetailBean(esApplicantsDetailBean);
                            mav.addObject("status", "200");
                        } else {
                            mav.addObject("status", "400");
                            mav.addObject("msg", docResp.get("errorMessage").toString());
                        }
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            mav.addObject("status", "400");
            mav.addObject("msg", ex.getMessage());
            CustomLogger.error("pddDocUpload", ex);
        } catch (IOException ioe) {
            mav.addObject("status", "400");
            mav.addObject("msg", ioe.getMessage());
            CustomLogger.error("pddDocUpload", ioe);
        } catch (CustomNonFatalException ex) {
            mav.addObject("status", "400");
            mav.addObject("msg", ex.getMessage());
            CustomLogger.error("pddDocUpload", ex);
        } catch (Exception ex) {
            mav.addObject("status", "400");
            mav.addObject("msg", ex.getMessage());
            CustomLogger.error("pddDocUpload", ex);
        }
        return mav;
    }

    @RequestMapping(value = "/pddDocDownload", method = RequestMethod.GET)
    public ModelAndView pddDocDownload(HttpServletResponse response,
            //            @RequestParam(value = "docVersion", required = true) String docVersion,
            @RequestParam(value = "uuid", required = true) String uuid,
            @RequestParam(value = "applicationId", required = true) String applicationId
    ) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        try {
//            String op = rCUTriggerHelper.getDocumentFromMiddelware(uuid, docVersion,applicationId);
            String op = documentHelper.getDocumentFromMiddelware(uuid, applicationId);

            File file;
            JSONObject dmsResponse = new JSONObject(op);
            if (dmsResponse.has("status") && dmsResponse.get("status").toString().equalsIgnoreCase("1")) {
                if (dmsResponse.has("data")) {
                    JSONObject dmsData = new JSONObject(dmsResponse.get("data").toString());
                    file = new File(dmsData.get("fileName").toString() + "." + dmsData.get("ext").toString());
                    String mimeType = URLConnection.guessContentTypeFromName(file.getName());
                    if (mimeType == null) {
                        mimeType = "application/octet-stream";
                    }
                    response.setContentType(mimeType);
                    response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
                    String base64Image = dmsData.get("fileStream").toString();
                    byte base64Byte[] = Base64.decodeBase64(base64Image);
                    response.setContentLength((int) base64Byte.length);
                    InputStream inputStream = new ByteArrayInputStream(base64Byte);
                    FileCopyUtils.copy(inputStream, response.getOutputStream());
                    inputStream.close();
                }
            }

        } catch (JSONException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("pddDocDownload", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("pddDocDownload", e);
        } catch (IOException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("pddDocDownload", e);
        }

        return mav;
    }

    @RequestMapping(value = "/invoiceDocUpload")
    public ModelAndView invoiceDocUpload(@RequestParam("file") MultipartFile file,
            @RequestParam("data") String payload) throws IOException, JSONException, Exception {
        ModelAndView mav = new ModelAndView("home");

        try {
            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("Invalid applicationId", "Invalid applicationId");
            }
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid user Id", "Invalid user Id");
            }
            if (reqJson.get("docType") == null || "".equals(reqJson.get("docType").toString().trim())) {
                throw new CustomNonFatalException("Invalid docType", "Invalid docType");
            }

            String applicationId = reqJson.get("applicationId").toString();
            Long userId = Long.parseLong(reqJson.get("userId").toString());
            String docType = reqJson.get("docType").toString();
            byte[] bytes;

            bytes = file.getBytes();

            if (!file.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(bytes, false)));
                List<uploadDocumentDetails> lstDocumentDetails = new ArrayList<uploadDocumentDetails>();

                uploadDocumentDetails documentDetails = new uploadDocumentDetails();
                documentDetails.setApplicantId(applicationId);
                documentDetails.setDocOriginalName(reqJson.get("docOriginalName").toString());
                documentDetails.setDocType(reqJson.get("docType").toString());
                documentDetails.setExt(reqJson.get("ext").toString());
                documentDetails.setMimeType(reqJson.get("mimeType").toString());
                documentDetails.setDocumentString(sb.toString());

                lstDocumentDetails.add(documentDetails);

                uploadDocumentDetailWS detailWS = new uploadDocumentDetailWS();
                detailWS.setApplicationId(applicationId);
                detailWS.setApplicationName("TWH");
                detailWS.setDocumentDetailses(lstDocumentDetails);

                String op = documentHelper.documentUploadToMiddelware(detailWS);

                String docName = null, docUuid = null;
                if (op != null || !op.equals("")) {

                    JSONObject docResp = new JSONObject(op);
                    if (docResp.has("errorCode") && !docResp.get("errorCode").toString().trim().equals("") && docResp.get("errorCode") != null) {
                        if (docResp.get("errorCode").toString().equals("0")) {

                            JSONObject dataObj = docResp.getJSONObject("data");
                            JSONArray lstDocDetail = dataObj.getJSONArray("docDetail");
                            JSONObject docObj;
                            for (int i = 0; i < lstDocDetail.length(); i++) {
                                docObj = lstDocDetail.getJSONObject(i);
                                if (docObj.has("name")) {
                                    docName = docObj.get("name").toString();
                                }
                                if (docObj.has("uuid")) {
                                    docUuid = docObj.get("uuid").toString();
                                }

                                if (reqJson.get("invoiceNumber") == null || "".equals(reqJson.get("invoiceNumber").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid invoiceNumber", "Invalid invoiceNumber");
                                }
//                                if (reqJson.get("invoiceDate") == null || "".equals(reqJson.get("invoiceDate").toString().trim())) {
//                                    throw new CustomNonFatalException("Invalid invoiceDate", "Invalid invoiceDate");
//                                }
                                if (reqJson.get("engineNumber") == null || "".equals(reqJson.get("engineNumber").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid engineNumber", "Invalid engineNumber");
                                }
                                if (reqJson.get("chasisNumber") == null || "".equals(reqJson.get("chasisNumber").toString().trim())) {
                                    throw new CustomNonFatalException("Invalid chasisNumber", "Invalid chasisNumber");
                                }

                                String invoiceNumber = reqJson.get("invoiceNumber").toString().toUpperCase();
                                String engineNumber = reqJson.get("engineNumber").toString().toUpperCase();
                                String chasisNumber = reqJson.get("chasisNumber").toString().toUpperCase();

                                EsInvoiceDocBean documentDetail = documentHelper.getEsInvoiceDocDetail(applicationId);

                                if (documentDetail == null) {
                                    documentDetail = new EsInvoiceDocBean();
                                    documentDetail.setApplicationId(applicationId);
                                    documentDetail.setChassisNumber(chasisNumber);
                                    documentDetail.setEngineNumber(engineNumber);
//                                    documentDetail.setInvoiceDate(DateUtility.parseStringToDate(reqJson.get("invoiceDate").toString(), "dd-MM-yyyy"));
                                    documentDetail.setCreBy(userId.toString());
                                    documentDetail.setCreDate(DateUtility.getCurrentDate());
                                    documentDetail.setInvoiceNumber(invoiceNumber);
//                                    documentDetail.setInvoiceHypothicationStatus(invoiceHypothicationStatus);
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                                    documentDetail.setInvoiceDocName(docName);
                                    documentDetail.setInvoiceDocType(docType);
                                    documentDetail.setInvoiceDocUuid(docUuid);

                                } else {
                                    documentDetail.setChassisNumber(chasisNumber);
                                    documentDetail.setEngineNumber(engineNumber);
//                                    documentDetail.setInvoiceDate(DateUtility.parseStringToDate(reqJson.get("invoiceDate").toString(), "dd-MM-yyyy"));
                                    documentDetail.setInvoiceNumber(invoiceNumber);
//                                    documentDetail.setInvoiceHypothicationStatus(invoiceHypothicationStatus);
                                    documentDetail.setModBy(userId.toString());
                                    documentDetail.setModDate(DateUtility.getCurrentDate());
                                    documentDetail.setInvoiceDocName(docName);
                                    documentDetail.setInvoiceDocType(docType);
                                    documentDetail.setInvoiceDocUuid(docUuid);
                                }
                                documentHelper.setEsInvoiceDocDetail(documentDetail);
                                mav.addObject("status", "200");
                                mav.addObject("docUuid", docUuid);
                                mav.addObject("docType", docType);
                                mav.addObject("docName", docName);

                            }

                        } else {
                            mav.addObject("status", "400");
                            mav.addObject("msg", docResp.get("errorMessage").toString());
                        }

                    }

                }
            }

        } catch (FileNotFoundException ex) {
            mav.addObject("status", "400");
            mav.addObject("msg", ex.getMessage());
            CustomLogger.error("invoiceDocUpload", ex);
        } catch (IOException ioe) {
            mav.addObject("status", "400");
            mav.addObject("msg", ioe.getMessage());
            CustomLogger.error("invoiceDocUpload", ioe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("invoiceDocUpload", e);
        }
        return mav;
    }

    @RequestMapping(value = "/viewInvoiceDoc", method = RequestMethod.POST)
    public ModelAndView viewInvoiceDoc(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try {

            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("userId not entered", "userId not entered");
            }

            if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("applicationId not entered", "applicationId not entered");
            }

            if (!reqJson.has("uuid") || reqJson.get("uuid") == null || "".equals(reqJson.get("uuid").toString().trim())) {
                throw new CustomNonFatalException("uuid not entered", "uuid not entered");
            }

            String applicationId = reqJson.get("applicationId").toString();
            String uuid = reqJson.get("uuid").toString();
            Long userId = Long.parseLong(reqJson.get("userId").toString());

            String op = documentHelper.getDocumentFromMiddelware(uuid, applicationId);

            File file=null;
            JSONObject dmsResponse = new JSONObject(op);
            if (dmsResponse.has("status") && dmsResponse.get("status").toString().equalsIgnoreCase("1")) {
                if (dmsResponse.has("data")) {
                    
                    JSONObject dmsData = new JSONObject();
                    dmsData = dmsResponse.getJSONObject("data");

                    HashMap<String,String> hashMap=new HashMap<String,String>();
                        
                    hashMap.put("parentUuid", dmsData.get("parentUuid").toString());
                    hashMap.put("fileName", dmsData.get("fileName").toString());
                    hashMap.put("fileStream", dmsData.get("fileStream").toString());
                    hashMap.put("parentFolderName", dmsData.get("parentFolderName").toString());
                    hashMap.put("mimeType", dmsData.get("mimeType").toString());
                    hashMap.put("ext", dmsData.get("ext").toString());
                    hashMap.put("file", "data:"+dmsData.get("mimeType").toString()+";base64,"+dmsData.get("fileStream").toString());
                    
                    mav.addObject("data", hashMap);
                    mav.addObject("status", "200");

                }
                
            } else {
                mav.addObject("status", "400");
                mav.addObject("msg", dmsResponse.get("errorMessage").toString());
            }

        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("viewInvoiceDoc", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("viewInvoiceDoc", e);
        }
        return mav;
    }
}
