package com.engage.components.core.controller;

import com.engage.components.core.entity.bean.SangamMstrSelectCriteria;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.engage.components.core.helper.DashboardHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.DealerDashbordVO;
import com.engage.components.esportal.vo.OpsDashBoardVO;
import com.engage.components.esportal.vo.BuilderDashBoardVO;
import com.engage.components.esportal.vo.BuilderStatusDashBoardVO;
import com.engage.components.esportal.vo.DealearPendingReq;
import com.engage.components.esportal.vo.DealearReqContent;
import com.engage.components.esportal.vo.DealerStatusDashboardVO;
import com.engage.components.esportal.vo.LoanDashBoardVO;
import com.engage.components.esportal.vo.McmDashBoardVO;
import com.engage.components.esportal.vo.PendindStatusDashBoardVO;
import com.engage.components.esportal.vo.PipeLineDashBoardVO;
import com.engage.components.esportal.vo.RCUDashboardVO;
import com.engage.components.esportal.vo.RMDashboardVO;
import com.engage.components.esportal.vo.builderLeadGenDashboardVO;
import com.engage.framework.util.log4j.CustomLogger;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;

/**
 *
 * @author Shrikant Katakdhond
 */
@Component
@Controller
public class DashboardController {

    
    @Autowired
    DashboardHelper dashboardHelper;

    @RequestMapping(value = "/loadSearchCriteria", method = RequestMethod.POST)
    public ModelAndView loadSearchCriteria(@RequestBody String payload) throws CustomNonFatalException, JSONException {
        ModelAndView mav = new ModelAndView("home");
        try {
//                        ////System.out.println(payload);
            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }

//            String forDashboard = FrameworkAppConstants.Pre_Sanction_Dashboard;reqJson.get("userId")
            String forDashboard = reqJson.get("forDashboard").toString();
            List<SangamMstrSelectCriteria> lstSelectCriteria = dashboardHelper.getSelectCriterias(forDashboard);

            mav.addObject("status", "200");
            mav.addObject("lstSelectCriteria", lstSelectCriteria);
        } catch (JSONException e) {
            mav.addObject("status", "400");
            CustomLogger.error("loadSearchCriteria", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", "Error : " + e.getMessage());
            CustomLogger.error("loadSearchCriteria", e);
        }
        return mav;
    }

    @RequestMapping(value = "/dealerDashboard", method = RequestMethod.POST)
    public ModelAndView saveSourcingData(@RequestBody String payload) throws CustomNonFatalException, JSONException {
        ModelAndView mav = new ModelAndView("home");
        try {
//            ////System.out.println(payload);

            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            List<DealerStatusDashboardVO> lstPostSanctionDashbordVO = dashboardHelper.getDealerDashbord(reqJson.get("userId").toString(), fromRowNum, toRowNum, profileID);

            mav.addObject("status", "200");
            mav.addObject("lstPostSanctionDashbordVO", lstPostSanctionDashbordVO);

        } catch (Exception e) {
//                    e.printStackTrace();
            CustomLogger.error("postSanctionDashboard", e);
            mav.addObject("status", "400");
            mav.addObject("msg", "Error : " + e.getMessage());
        }

        return mav;
    }

    @RequestMapping(value = "/getOpsDashboard")
    public ModelAndView getOpsDashboardData(@RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        List<OpsDashBoardVO> lstopsDashBoardVO = null;
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("isPending") == null || "".equals(reqJson.get("isPending").toString().trim())) {
                throw new CustomNonFatalException("Invalid Dashboard Flag", "Invalid Dashboard Flag");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            Character isPending = reqJson.get("isPending").toString().charAt(0);

            lstopsDashBoardVO = dashboardHelper.getOpsDashbord(reqJson.get("userId").toString(), fromRowNum, toRowNum, isPending);
            mav.addObject("status", "200");
            mav.addObject("lstopsDashBoardVO", lstopsDashBoardVO);
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getOpsDashbord", e);
        }

        return mav;

    }
    
    @RequestMapping(value = "/getOpsDashboardBySearch")
    public ModelAndView getOpsDashboardBySearch(@RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        List<OpsDashBoardVO> lstopsDashBoardVO = null;
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("isPending") == null || "".equals(reqJson.get("isPending").toString().trim())) {
                throw new CustomNonFatalException("Invalid Dashboard Flag", "Invalid Dashboard Flag");
            }
            if (reqJson.get("searchParameter") == null || "".equals(reqJson.get("searchParameter").toString().trim())) {
                throw new CustomNonFatalException("Invalid searchParameter", "Invalid searchParameter");
            }
            if (reqJson.get("searchParameterValue") == null || "".equals(reqJson.get("searchParameterValue").toString().trim())) {
                throw new CustomNonFatalException("Invalid searchParameterValue", "Invalid searchParameterValue");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            Character isPending = reqJson.get("isPending").toString().charAt(0);
            String searchParameter = reqJson.get("searchParameter").toString();
            String searchParameterValue = reqJson.get("searchParameterValue").toString();

            lstopsDashBoardVO = dashboardHelper.getOpsDashbordSearch(reqJson.get("userId").toString(), fromRowNum, toRowNum, isPending, searchParameter, searchParameterValue);
            mav.addObject("status", "200");
            mav.addObject("lstopsDashBoardVO", lstopsDashBoardVO);
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getOpsDashbord", e);
        }

        return mav;

    }
    
    @RequestMapping(value = "/getOpsDashboardByCondition")
    public ModelAndView getOpsDashboardByCondition(@RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        List<OpsDashBoardVO> lstopsDashBoardVO = null;
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("isPending") == null || "".equals(reqJson.get("isPending").toString().trim())) {
                throw new CustomNonFatalException("Invalid Dashboard Flag", "Invalid Dashboard Flag");
            }
            if (reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("Invalid applicationId", "Invalid applicationId");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            Character isPending = reqJson.get("isPending").toString().charAt(0);
            String applicationId = reqJson.get("applicationId").toString();
            
            //System.out.println(isPending+" :"+fromRowNum+" : "+toRowNum+" : "+applicationId);

            lstopsDashBoardVO = dashboardHelper.getOpsDashbordByApplicationId(reqJson.get("userId").toString(), fromRowNum, toRowNum, isPending, applicationId);
            mav.addObject("status", "200");
            mav.addObject("lstopsDashBoardVO", lstopsDashBoardVO);
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getOpsDashbord", e);
        }

        return mav;

    }
     
    @RequestMapping(value = "/getMcmPostDashboard")
    public ModelAndView getMcmDisbursementDashboard(@RequestBody String payload) throws CustomNonFatalException{
    ModelAndView mav = new ModelAndView("home");
    List<McmDashBoardVO> lstMcmDashBoardVO = null;
     String localityId=null;
        String villageName=null;
    try
    {
        JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException(" MC Code Required", " MC Code Required");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            String mcCode=reqJson.get("mcCode").toString();
            if (reqJson.get("type") == null || "".equals(reqJson.get("type").toString().trim())) {
                throw new CustomNonFatalException("Invalid page type", "Invalid page type");
            }

            String type=reqJson.get("type").toString().trim();
            //System.out.println("Type ::::::::"+type+"mcCode >>>>>>>>>>"+mcCode);
            
            lstMcmDashBoardVO = dashboardHelper.getMcmDashboard(reqJson.get("userId").toString(), fromRowNum, toRowNum,type,localityId,villageName,mcCode);
            
              if(lstMcmDashBoardVO==null)
            {
                 throw new CustomNonFatalException("Records Not Found", "Records Not Found");
            }
            
            mav.addObject("status", "200");
            mav.addObject("lstMcmDashBoardVO", lstMcmDashBoardVO);

    }catch(Exception e)
    {
        e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getMcmDashbord", e);
    
    }
    return mav;
    }
    
    @RequestMapping(value = "/getMcmDashboard")
    public ModelAndView getMcmDashboardData(@RequestBody String payload)  throws CustomNonFatalException{
        ModelAndView mav = new ModelAndView("home");
        List<McmDashBoardVO> lstMcmDashBoardVO = null;
        String localityId=null;
        String villageName=null;
        
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
//            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
//                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
//            }
            if (reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException(" MC Code Required", " MC Code Required");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            //Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            String mcCode=reqJson.get("mcCode").toString();
            
            if (reqJson.get("type") == null || "".equals(reqJson.get("type").toString().trim())) {
                throw new CustomNonFatalException("Invalid page type", "Invalid page type");
            }

            String type=reqJson.get("type").toString().trim();
            //System.out.println("Type ::::::::"+type+"mcCode >>>>>>>>>>"+mcCode);
          
            lstMcmDashBoardVO = dashboardHelper.getMcmDashboard(reqJson.get("userId").toString(), fromRowNum, toRowNum,type,localityId,villageName,mcCode);
            
            if(lstMcmDashBoardVO==null)
            {
                 throw new CustomNonFatalException("Records Not Found", "Records Not Found");
            }
            mav.addObject("status", "200");
            mav.addObject("lstMcmDashBoardVO", lstMcmDashBoardVO);
        }
//        catch(CustomNonFatalException e)
//        {
//            e.printStackTrace();
//            mav.addObject("status", "400");
//            mav.addObject("msg", e.getMessage());   
//        }
        catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getMcmDashbord", e);
        }

        return mav;

    }
    
     @RequestMapping(value = "/getMcmPrintDashboard")
    public ModelAndView getMcmPrintDashboard(@RequestBody String payload)  throws CustomNonFatalException
    {
        ModelAndView mav = new ModelAndView("home");
    List<McmDashBoardVO> lstMcmDashBoardVO = null;
     String localityId=null;
        String villageName=null;
    try
    {
        JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException(" MC Code Required", " MC Code Required");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            String mcCode=reqJson.get("mcCode").toString();
            if (reqJson.get("type") == null || "".equals(reqJson.get("type").toString().trim())) {
                throw new CustomNonFatalException("Invalid page type", "Invalid page type");
            }

            String type=reqJson.get("type").toString().trim();
            //System.out.println("Type ::::::::"+type+"mcCode >>>>>>>>>>"+mcCode);
            
            lstMcmDashBoardVO = dashboardHelper.getMcmDashboard(reqJson.get("userId").toString(), fromRowNum, toRowNum,type,localityId,villageName,mcCode);
            
              if(lstMcmDashBoardVO==null)
            {
                 throw new CustomNonFatalException("Records Not Found", "Records Not Found");
            }
            
            mav.addObject("status", "200");
            mav.addObject("lstMcmDashBoardVO", lstMcmDashBoardVO);

    }catch(Exception e)
    {
        e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getMcmPrintDashboard", e);
    
    }
    return mav;
    }
    
    @RequestMapping(value = "/getMcmDashboardBySearch")
    public ModelAndView getMcmDashboardBySearch(@RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        List<McmDashBoardVO> lstmcmDashBoardVO = null;
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
//            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
//                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
//            }
            if (reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException("Invalid Dashboard Flag", "Invalid Dashboard Flag");
            }
            if (reqJson.get("searchParameter") == null || "".equals(reqJson.get("searchParameter").toString().trim())) {
                throw new CustomNonFatalException("Invalid searchParameter", "Invalid searchParameter");
            }
            if (reqJson.get("searchParameterValue") == null || "".equals(reqJson.get("searchParameterValue").toString().trim())) {
                throw new CustomNonFatalException("Invalid searchParameterValue", "Invalid searchParameterValue");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
           // Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            String mcCode = reqJson.get("mcCode").toString();
            String searchParameter = reqJson.get("searchParameter").toString();
            String searchParameterValue = reqJson.get("searchParameterValue").toString();

            lstmcmDashBoardVO = dashboardHelper.getMcmDashbordSearch(fromRowNum, toRowNum, mcCode ,searchParameter, searchParameterValue);
            mav.addObject("status", "200");
            mav.addObject("lstmcmDashBoardVO", lstmcmDashBoardVO);
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getMcmDashbord", e);
        }

        return mav;

    }

    
    @RequestMapping(value = "/getBuilderDashboard")
    public ModelAndView getBuilderDashboardData(@RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        List<BuilderDashBoardVO> lstBuilderDashBoardVO = null;

        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            lstBuilderDashBoardVO = dashboardHelper.getBuilderDashbord(reqJson.get("userId").toString(), fromRowNum, toRowNum, profileID);

            mav.addObject("status", "200");
            mav.addObject("lstBuilderDashBoardVO", lstBuilderDashBoardVO);

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getBuilderDashboard", e);
        }

        return mav;
    }

    @RequestMapping(value = "/getRCUTriggerDashboard", method = RequestMethod.POST)
    public ModelAndView getRCUTriggerDashboard(@RequestBody String payload) throws CustomNonFatalException, JSONException {
        ModelAndView mav = new ModelAndView("home");
        try {
//            ////System.out.println(payload);

            JSONObject reqJson = new JSONObject(payload);
//            ////System.out.println("dashboard" + reqJson.get("userId"));
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            List<RCUDashboardVO> lstRCUTriggetDashboardVO = dashboardHelper.getRCUTriggerApplications(reqJson.get("userId").toString(), fromRowNum, toRowNum);

            mav.addObject("status", "200");
            mav.addObject("lstRCUTriggetDashboardVO", lstRCUTriggetDashboardVO);

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", "Error : " + e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("getRCUTriggerDashboard", e);
        }
        return mav;
    }

    @RequestMapping(value = "/getRMDashboard", method = RequestMethod.POST)
    public ModelAndView getRMDashboard(@RequestBody String payload) throws CustomNonFatalException, JSONException {
        ModelAndView mav = new ModelAndView("home");

        try {
            JSONObject reqJson = new JSONObject(payload);
//            ////System.out.println("dashboard" + reqJson.get("userId"));
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());

            List<RMDashboardVO> lstRMDashboardVO = dashboardHelper.getRMDashboard(reqJson.get("userId").toString(), fromRowNum, toRowNum);

            mav.addObject("status", "200");
            mav.addObject("lstRMDashboardVO", lstRMDashboardVO);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", "Error : " + e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("getRMDashboard", e);

        }
        return mav;
    }

    @RequestMapping(value = "/dealer/getPendingStatusDashboard")
    public ModelAndView getPendingStatusDashboard(HttpServletRequest hrs, @RequestBody String payload) {
//        //System.out.println("getPendingStatusDashboard >>>>>>>>>");
        ModelAndView mav = new ModelAndView("home");
        List<PendindStatusDashBoardVO> lstPendindStatusDashBoardVO = null;
        DealearReqContent Content = new DealearReqContent();
        DealearPendingReq pendingReq = new DealearPendingReq();
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("userName") == null || "".equals(reqJson.get("userName").toString().trim())) {
                throw new CustomNonFatalException("Invalid userName", "Invalid userName");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
//            Long userName=Long.parseLong(reqJson.get("userName").toString());
            ////System.out.println("userName :::::::::::"+userName);
//            Content.setDealerId("22720");//reqJson.get("dealerID")
            Content.setDealerId(reqJson.get("userName").toString());
            Content.setLastNoDays("1");
            Content.setLimitQ("10");
            Content.setStatus("Pending");
            Content.setProductId("TW");
            pendingReq.setContent(Content);

            lstPendindStatusDashBoardVO = dashboardHelper.getLstPendindStatusDashBoard(hrs, reqJson.get("userId").toString(), fromRowNum, toRowNum, profileID, pendingReq);

            mav.addObject("status", "200");
            mav.addObject("lstPendindStatusDashBoardVO", lstPendindStatusDashBoardVO);

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getLstPendindStatusDashBoard", e);
        }

        return mav;
    }

    @RequestMapping(value = "/builder/getLoanDashboard")
    public ModelAndView getLoanAndStatusDashboard(HttpServletRequest hrs, @RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");

        try {
            List<LoanDashBoardVO> lstLoanDashBoardVO = null;

            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("userName") == null || "".equals(reqJson.get("userName").toString().trim())) {
                throw new CustomNonFatalException("Invalid userName", "Invalid userName");
            }
            if (reqJson.get("dashBoardType") == null || "".equals(reqJson.get("dashBoardType").toString().trim())) {
                throw new CustomNonFatalException("Invalid dashBoardType", "Invalid dashBoardType");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            String userName = reqJson.get("userName").toString();
            String dashBoardType = reqJson.get("dashBoardType").toString();

//            String op = dashboardHelper.getLstLoanAndStatusDashBoardFromMW(hrs, reqJson.get("userId").toString(), fromRowNum, toRowNum, profileID, userName);
            String op = "{\n"
                    + "  \"statusCode\": 200,\n"
                    + "  \"status\": \"SUCCESS\",\n"
                    + "  \"errorMessage\": null,\n"
                    + "  \"errorCode\": \"NO_ERROR\",\n"
                    + "  \"Errors\": null,\n"
                    + "  \"Content\": [\n"
                    + "    {\n"
                    + "      \"Type\": null,\n"
                    + "      \"TowerName\": \"Aura\",\n"
                    + "      \"Status\": null,\n"
                    + "      \"SanctionedAmount\": 1800000,\n"
                    + "      \"RequestedAmount\": 1800000,\n"
                    + "      \"ProjectName\": \"NexWorld\",\n"
                    + "      \"OCRCollectedcheckbox\": null,\n"
                    + "      \"OCRAmount\": null,\n"
                    + "      \"Nooftranchesdisbursed\": null,\n"
                    + "      \"MobileNo\": \"8779851652\",\n"
                    + "      \"FlatNo\": \"2012\",\n"
                    + "      \"EmailID\": \"ajayptiwari9833@gmail.com\",\n"
                    + "      \"DisbursedAmount\": null,\n"
                    + "      \"DemandLetterUpload\": null,\n"
                    + "      \"CustomerName\": \"Ajay Kumar Tiwari\",\n"
                    + "      \"CurrentConstructionStage\": 0,\n"
                    + "      \"ConstructionStageofnexttranchedue\": null,\n"
                    + "      \"ArchitectReportUpload\": null,\n"
                    + "      \"ApplicationStage\": \"Application Form\"\n"
                    + "    }\n"
                    + "  ]\n"
                    + "}";
            JSONObject responseObj = new JSONObject(op);

            if (responseObj.has("statusCode") && responseObj.get("statusCode").toString().equals("200")) {
                List<LoanDashBoardVO> lstLaonDashboard = new ArrayList<LoanDashBoardVO>();
                List<BuilderStatusDashBoardVO> lstStatusDashboard = new ArrayList<BuilderStatusDashBoardVO>();
                LoanDashBoardVO loanDashBoardDataVo = null;
                BuilderStatusDashBoardVO statusDashBoardVo = null;
                if (responseObj.has("Content")) {
                    //------------Set Data

                    JSONArray array = responseObj.getJSONArray("Content");
                    JSONObject dataObj = null;
                    for (int j = 0; j < array.length(); j++) {

                        dataObj = (JSONObject) array.get(j);
                        //--------------Set Loan Data
                        loanDashBoardDataVo = new LoanDashBoardVO(); // dataObj to get data from Json
                        loanDashBoardDataVo.setApplicationId(dataObj.has("ApplicationId") && !dataObj.get("ApplicationId").toString().equals("null") ? dataObj.get("ApplicationId").toString() : "123");
                        loanDashBoardDataVo.setCustomerName(dataObj.has("CustomerName") && !dataObj.get("CustomerName").toString().equals("null") ? dataObj.get("CustomerName").toString() : "");
                        loanDashBoardDataVo.setProjectName(dataObj.has("ProjectName") && !dataObj.get("ProjectName").toString().equals("null") ? dataObj.get("ProjectName").toString() : "");
                        loanDashBoardDataVo.setTowerName(dataObj.has("TowerName") && !dataObj.get("TowerName").toString().equals("null") ? dataObj.get("TowerName").toString() : "");
                        loanDashBoardDataVo.setSanctionedAmt(dataObj.has("SanctionedAmount") && !dataObj.get("SanctionedAmount").toString().equals("null") ? dataObj.get("SanctionedAmount").toString() : "");
                        loanDashBoardDataVo.setDisburseAmt(dataObj.has("DisbursedAmount") && !dataObj.get("DisbursedAmount").toString().equals("null") ? dataObj.get("DisbursedAmount").toString() : "0");
                        loanDashBoardDataVo.setFlatNo(dataObj.has("FlatNo") && !dataObj.get("FlatNo").toString().equals("null") ? dataObj.get("FlatNo").toString() : "");
                        loanDashBoardDataVo.setConstStageOfNextTrancheDue(dataObj.has("ConstructionStageofnexttranchedue") && !dataObj.get("ConstructionStageofnexttranchedue").toString().equals("null") ? dataObj.get("ConstructionStageofnexttranchedue").toString() : "");
                        loanDashBoardDataVo.setNoOfTranchesDisbursed(dataObj.has("Nooftranchesdisbursed") && !dataObj.get("Nooftranchesdisbursed").toString().equals("null") ? dataObj.get("Nooftranchesdisbursed").toString() : "0");
                        loanDashBoardDataVo.setProgress("1");
                        loanDashBoardDataVo.setCaseCount(array.length() + "");

                        lstLaonDashboard.add(loanDashBoardDataVo);

                        //-------------set Status Data
                        statusDashBoardVo = new BuilderStatusDashBoardVO();
                        statusDashBoardVo.setApplicationId(dataObj.has("ApplicationId") && !dataObj.get("ApplicationId").toString().equals("null") ? dataObj.get("ApplicationId").toString() : "123");
                        statusDashBoardVo.setCustomerName(dataObj.has("CustomerName") && !dataObj.get("CustomerName").toString().equals("null") ? dataObj.get("CustomerName").toString() : "");
                        statusDashBoardVo.setProjectName(dataObj.has("ProjectName") && !dataObj.get("ProjectName").toString().equals("null") ? dataObj.get("ProjectName").toString() : "");
                        statusDashBoardVo.setTowerName(dataObj.has("TowerName") && !dataObj.get("TowerName").toString().equals("null") ? dataObj.get("TowerName").toString() : "");
                        statusDashBoardVo.setFlatNo(dataObj.has("FlatNo") && !dataObj.get("FlatNo").toString().equals("null") ? dataObj.get("FlatNo").toString() : "");
                        statusDashBoardVo.setStatus(dataObj.has("Status") && !dataObj.get("Status").toString().equals("null") ? dataObj.get("Status").toString() : "");
                        statusDashBoardVo.setRequestedAmt(dataObj.has("RequestedAmount") && !dataObj.get("RequestedAmount").toString().equals("null") ? dataObj.get("RequestedAmount").toString() : "");
                        statusDashBoardVo.setProgress("1");
                        statusDashBoardVo.setCaseCount(array.length() + "");

                        lstStatusDashboard.add(statusDashBoardVo);

                    }

                    hrs.getSession().setAttribute("LstloanDashBoardDataVo", lstLaonDashboard);
                    hrs.getSession().setAttribute("LstStatusDashBoardVo", lstStatusDashboard);

                }

                lstLoanDashBoardVO = (List<LoanDashBoardVO>) hrs.getSession().getAttribute("LstloanDashBoardDataVo");

                mav.addObject("status", "200");
                mav.addObject("lstLoanDashBoardVO", lstLoanDashBoardVO);
            } else {
                if (responseObj.has("errorMessage") && !responseObj.get("errorMessage").toString().equals("null")) {
                    mav.addObject("msg", responseObj.get("errorMessage").toString());
                } else {
                    mav.addObject("msg", "Bad Response!!!");
                }
                mav.addObject("status", "400");
            }

        } catch (JSONException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getLoanAndStatusDashboard", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getLoanAndStatusDashboard", e);
        }

        return mav;
    }

    @RequestMapping(value = "/builder/getStatusDashboard")
    public ModelAndView getStatusDashboard(HttpServletRequest hrs, HttpSession session, @RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("userName") == null || "".equals(reqJson.get("userName").toString().trim())) {
                throw new CustomNonFatalException("Invalid userName", "Invalid userName");
            }

            mav.addObject("status", "200");
            mav.addObject("lstStatusDashBoardVO", session.getAttribute("LstStatusDashBoardVo"));

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getStatusDashboard", e);
        }

        return mav;
    }

    @RequestMapping(value = "/builderLeadGenDashboard")
    public ModelAndView builderLeadGenDashboard(HttpServletRequest hrs, HttpSession session, @RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid userId", "Invalid userId");
            }

            List<builderLeadGenDashboardVO> lstBuilderLeadGenDashboardVO = new ArrayList<builderLeadGenDashboardVO>();
            builderLeadGenDashboardVO dashboardVO = new builderLeadGenDashboardVO();

            dashboardVO.setApplicationId("1234");
            dashboardVO.setCustomerName("Shrikant");
            dashboardVO.setEmailId("shrikantkatakdhond@ltfs.com");
            dashboardVO.setFlatNo("446");
            dashboardVO.setMobileNumber("9673661981");
            dashboardVO.setTowerName("Solapur");
            dashboardVO.setType("Enquiry");
            dashboardVO.setCaseCount(1);
            dashboardVO.setProjectName("LNT");
            lstBuilderLeadGenDashboardVO.add(dashboardVO);

            mav.addObject("lstBuilderLeadGenDashboardVO", lstBuilderLeadGenDashboardVO);
            mav.addObject("status", "200");
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getStatusDashboard", e);
        }

        return mav;
    }

    @RequestMapping(value = "/builder/getPipelinedashboard")
    public ModelAndView getPipelineDashboard(HttpServletRequest hrs, HttpSession session, @RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        List<PipeLineDashBoardVO> lstPipelineDashboard = new ArrayList<PipeLineDashBoardVO>();
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("fromRowNum") == null || "".equals(reqJson.get("fromRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid fromRowNum", "Invalid fromRowNum");
            }
            if (reqJson.get("toRowNum") == null || "".equals(reqJson.get("toRowNum").toString().trim())) {
                throw new CustomNonFatalException("Invalid toRowNum", "Invalid toRowNum");
            }
            if (reqJson.get("profileID") == null || "".equals(reqJson.get("profileID").toString().trim())) {
                throw new CustomNonFatalException("Invalid profileID", "Invalid profileID");
            }
            if (reqJson.get("userName") == null || "".equals(reqJson.get("userName").toString().trim())) {
                throw new CustomNonFatalException("Invalid userName", "Invalid userName");
            }

            Long fromRowNum = Long.parseLong(reqJson.get("fromRowNum").toString());
            Long toRowNum = Long.parseLong(reqJson.get("toRowNum").toString());
            Long profileID = Long.parseLong(reqJson.get("profileID").toString());
            String userName = reqJson.get("userName").toString();

//            String op = dashboardHelper.getPipelineDashBoardFromMW(hrs, reqJson.get("userId").toString(), fromRowNum, toRowNum, profileID, userName);
            String op = "{\n"
                    + "  \"statusCode\": 200,\n"
                    + "  \"status\": \"SUCCESS\",\n"
                    + "  \"errorMessage\": null,\n"
                    + "  \"errorCode\": \"NO_ERROR\",\n"
                    + "  \"Errors\": null,\n"
                    + "  \"Content\": [\n"
                    + "    {\n"
                    + "      \"Type\": null,\n"
                    + "      \"TowerName\": \"Aura\",\n"
                    + "      \"Status\": null,\n"
                    + "      \"SanctionedAmount\": 1800000,\n"
                    + "      \"RequestedAmount\": 1800000,\n"
                    + "      \"ProjectName\": \"NexWorld\",\n"
                    + "      \"OCRCollectedcheckbox\": null,\n"
                    + "      \"OCRAmount\": null,\n"
                    + "      \"Nooftranchesdisbursed\": null,\n"
                    + "      \"MobileNo\": \"8779851652\",\n"
                    + "      \"FlatNo\": \"201\",\n"
                    + "      \"EmailID\": \"ajayptiwari9833@gmail.com\",\n"
                    + "      \"DisbursedAmount\": null,\n"
                    + "      \"DemandLetterUpload\": null,\n"
                    + "      \"CustomerName\": \"Ajay Kumar Tiwari\",\n"
                    + "      \"CurrentConstructionStage\": 0,\n"
                    + "      \"ConstructionStageofnexttranchedue\": null,\n"
                    + "      \"ArchitectReportUpload\": null,\n"
                    + "      \"ApplicationStage\": \"null\"\n"
                    + "    }\n"
                    + "  ]\n"
                    + "}";
            JSONObject responseObj = new JSONObject(op);

            if (responseObj.has("statusCode") && responseObj.get("statusCode").toString().equals("200")) {

                PipeLineDashBoardVO pipelineDashBoardDataVo = null;

                if (responseObj.has("Content")) {

                    //------------Set Data
                    JSONArray array = responseObj.getJSONArray("Content");
                    JSONObject dataObj = null;
                    for (int j = 0; j < array.length(); j++) {

                        dataObj = (JSONObject) array.get(j);
                        pipelineDashBoardDataVo = new PipeLineDashBoardVO();
                        pipelineDashBoardDataVo.setApplicationId(dataObj.has("ApplicationId") && !dataObj.get("ApplicationId").toString().equals("null") ? dataObj.get("ApplicationId").toString() : "123");
                        pipelineDashBoardDataVo.setCustomerName(dataObj.has("CustomerName") && !dataObj.get("CustomerName").toString().equals("null") ? dataObj.get("CustomerName").toString() : "");
                        pipelineDashBoardDataVo.setProjectName(dataObj.has("ProjectName") && !dataObj.get("ProjectName").toString().equals("null") ? dataObj.get("ProjectName").toString() : "");
                        pipelineDashBoardDataVo.setTowerName(dataObj.has("TowerName") && !dataObj.get("TowerName").toString().equals("null") ? dataObj.get("TowerName").toString() : "");
                        pipelineDashBoardDataVo.setFlatNo(dataObj.has("FlatNo") && !dataObj.get("FlatNo").toString().equals("null") ? dataObj.get("FlatNo").toString() : "");
                        pipelineDashBoardDataVo.setApplicationStage(dataObj.has("ApplicationStage") && !dataObj.get("ApplicationStage").toString().equals("null") ? dataObj.get("ApplicationStage").toString() : "");
                        pipelineDashBoardDataVo.setProgress("1");
                        pipelineDashBoardDataVo.setCaseCount(array.length() + " ");
                        lstPipelineDashboard.add(pipelineDashBoardDataVo);

                    }

                }
            }

            mav.addObject("status", "200");
            mav.addObject("lstPipelineDashBoardVO", lstPipelineDashboard);

        } catch (JSONException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("v", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getPipelineDashboard", e);
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getPipelineDashboard", e);
        }
        return mav;

    }
}
