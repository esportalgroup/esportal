/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.cmss.engage.framework.util.AESAlgoClass;
import com.engage.components.core.entity.bean.RmDataApplication;
import com.engage.components.core.entity.bean.RmDataApplicantDetail;
import com.engage.components.core.helper.RmDataHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.RMApplicationDetailVO;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dattatrayt
 */
@Component
@Controller
public class RmDataController extends HttpServlet {

    @Autowired
    RmDataHelper rmDataHelper;

    @RequestMapping(value = "/fetchRmDataApplication", method = RequestMethod.POST)
    public ModelAndView fetchRmDataApplication(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");

        try {
            JSONObject reqJson = new JSONObject(payload);

            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("userId not found", "userId not found");
            }

            if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("applicationId not found", "applicationId not found");
            }

            Long applicationId = Long.parseLong(reqJson.get("applicationId").toString());
            Long userId = Long.parseLong(reqJson.get("userId").toString());

            RMApplicationDetailVO detail = null;
            if (applicationId != 0) {
                detail = rmDataHelper.getRMData(applicationId);

            }

            mav.addObject("data", detail);
            mav.addObject("status", "200");

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("fetchRmDataApplication", e);
        }
        return mav;
    }

    @RequestMapping(value = "/setRmDataApplication", method = RequestMethod.POST)
    public ModelAndView setRmDataApplication(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");

        try {
            JSONObject reqJson = new JSONObject(payload);

            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("userId not found", "userId not found");
            }

            if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("applicationId not found", "applicationId not found");
            }

            Long applicationId = Long.parseLong(reqJson.get("applicationId").toString());
            Long userId = Long.parseLong(reqJson.get("userId").toString());

            RmDataApplication rmDataApplcation = null;

            if (applicationId != 0) {
                rmDataApplcation = rmDataHelper.getRmDataApplcation(applicationId);

            } else {
                rmDataApplcation = new RmDataApplication();
                rmDataApplcation.setCreBy(userId.toString());
                rmDataApplcation.setCreDate(DateUtility.getCurrentDate());
                rmDataApplcation.setIsActive(FrameworkAppConstants.CONSTANT_YES);
            }

            if (reqJson.get("action").toString().equalsIgnoreCase("Submit")) {
                rmDataApplcation.setStatus(FrameworkAppConstants.RM_APPLICATION_SUBMITTED);
            } else {
                rmDataApplcation.setStatus(FrameworkAppConstants.PENDING_STATUS);
            }
            rmDataApplcation.setCustomerName(reqJson.get("customerName").toString());
            rmDataApplcation.setPan(reqJson.get("pan").toString());
//            rmDataApplcation.setLanNumber(Long.parseLong(reqJson.get("lanNumber").toString()));

            rmDataApplcation = rmDataHelper.setRmDataApplcation(rmDataApplcation);

            //---------------------
            boolean mailSend = false;
            JSONArray lstRMDetail = reqJson.getJSONArray("rmDetailArr");
            JSONObject rmDetail;
            RmDataApplicantDetail applicantDetail;
            for (int i = 0; i < lstRMDetail.length(); i++) {
                rmDetail = lstRMDetail.getJSONObject(i);
                if (rmDetail.get("applicantId").toString().equals("")) {

                    applicantDetail = new RmDataApplicantDetail();
                    applicantDetail.setCreBy(userId.toString());
                    applicantDetail.setCreDate(DateUtility.getCurrentDate());
                    applicantDetail.setStatus(FrameworkAppConstants.RM_APPLICATION_SAVE);
                    applicantDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                    applicantDetail.setRmDataApplication(rmDataApplcation);

                } else {
                    applicantDetail = rmDataHelper.getRmDataApplicantDetailByApplicantsID(applicationId, Long.parseLong(rmDetail.get("applicantId").toString()));
                }

                applicantDetail.setApplicantName(rmDetail.get("applicantName").toString());
                applicantDetail.setEmail(rmDetail.get("emailId").toString());
                applicantDetail.setPan(rmDetail.get("pan").toString());

                applicantDetail = rmDataHelper.setRmDataApplicantDetail(applicantDetail);
            }

            if (reqJson.get("action").toString().equalsIgnoreCase("Submit")) {
                mailSend = rmDataHelper.sendEmail(rmDataApplcation.getApplicationId());
                if (mailSend) {
                    mav.addObject("status", "200");
                    mav.addObject("data", rmDataApplcation);
                    mav.addObject("msg", "Application Submitted & Email Sent.");
                } else {
                    mav.addObject("status", "400");
                    mav.addObject("msg", "Error While send Email");
                }
            } else {
                mav.addObject("status", "200");
                mav.addObject("data", rmDataApplcation);
                mav.addObject("msg", "Application Saved.");
            }

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("fetchRmDataApplication", e);

        }
        return mav;
    }

    @RequestMapping(value = "/RM/veryfiUid", method = RequestMethod.GET)
    public ModelAndView veryfiUid(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "applicationId", required = true) String applicationId,
            @RequestParam(value = "applicantId", required = true) String applicantId
    ) throws IOException, CustomNonFatalException {
        ModelAndView mav = new ModelAndView("home");

        try {

            if (applicationId == null || (applicationId != null && (applicationId.equals("") || applicationId.equalsIgnoreCase("null")))) {
                throw new CustomNonFatalException("applicationId not found", "applicationId not found");
            }

//            System.out.println("applicationId : "+applicationId+" , applicantId : "+applicantId+"---------------------------------------------------");
            applicationId = AESAlgoClass.decrypt(applicationId).trim();
            applicantId = AESAlgoClass.decrypt(applicantId).trim();

            RmDataApplication rmDataApplication = rmDataHelper.getRmDataApplcation(Long.parseLong(applicationId));
            if (rmDataApplication == null) {
                throw new CustomNonFatalException("application data not found", "application data not found");
            }
            RmDataApplicantDetail rmDataApplicantDetail = rmDataHelper.getRmDataApplicantDetailByApplicantsID(Long.parseLong(applicationId), Long.parseLong(applicantId));
            if (rmDataApplicantDetail == null) {
                throw new CustomNonFatalException("applicant data not found", "applicant data not found");
            }
            if (!rmDataApplicantDetail.getEmailSend().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)
                    || rmDataApplicantDetail.getOtpSend().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)
                    || rmDataApplicantDetail.getUidVerified().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)) {
                throw new CustomNonFatalException("Invalid Link", "Invalid Link");
            }

            HttpSession session = request.getSession();
            session.setAttribute("rmApplicationId", applicationId);
            session.setAttribute("rmApplicantId", applicantId);

            mav.addObject("status", "200");
            mav.addObject("msg", "valid link");
            response.sendRedirect("/ESPortal/#/RM/verifyUid");

        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("RM/veryfiUid", e);
        }
        return mav;
    }
    
    @RequestMapping(value = "/RM/sendOtp", method = RequestMethod.POST)
    public ModelAndView sendOtp(@RequestBody String payload, HttpServletRequest request, HttpServletResponse response) throws IOException, CustomNonFatalException {
        ModelAndView mav = new ModelAndView("home");
        
        try {
            JSONObject reqJson = new JSONObject(payload);
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            
            Long applicationId;
            Long applicantId;
            String uid;
            
            HttpSession session=request.getSession();
            if(session.getAttribute("rmApplicationId") != null){
                applicationId=Long.parseLong(session.getAttribute("rmApplicationId").toString());
            }else{
                throw new CustomNonFatalException("application id not found", "application id not found");
            }
            if(session.getAttribute("rmApplicantId") != null){
                applicantId=Long.parseLong(session.getAttribute("rmApplicantId").toString());
            }else{
                throw new CustomNonFatalException("applicant id not found", "applicant id not found");
            }
            if (!reqJson.has("uid") || reqJson.get("uid") == null || "".equals(reqJson.get("uid").toString().trim())) {
                throw new CustomNonFatalException("uid not found", "uid not found");
            }
            uid=reqJson.get("uid").toString();
            
            RmDataApplication rmDataApplication=rmDataHelper.getRmDataApplcation(applicationId);
            if(rmDataApplication == null){
                throw new CustomNonFatalException("application data not found", "application data not found");
            }
            RmDataApplicantDetail rmDataApplicantDetail=rmDataHelper.getRmDataApplicantDetailByApplicantsID(applicationId, applicantId);
            if(rmDataApplicantDetail == null){
                throw new CustomNonFatalException("applicant data not found", "applicant data not found");
            }
            if(!rmDataApplicantDetail.getEmailSend().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)
                    || rmDataApplicantDetail.getOtpSend().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)
                    || rmDataApplicantDetail.getUidVerified().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)){
                throw new CustomNonFatalException("Invalid Link", "Invalid Link");
            }
//            rmDataApplicantDetail.setUid(uid);
            
            String op=rmDataHelper.sendOtp(applicationId, applicantId, uid);
            JSONObject uidResp = new JSONObject(op);
            if (uidResp.has("status") && uidResp.get("status").toString().equals("true")) {
                rmDataApplicantDetail.setOtpTxtId(uidResp.get("tx_id").toString());
                rmDataApplicantDetail.setOtpSend(FrameworkAppConstants.CONSTANT_YES);
                rmDataApplicantDetail.setOtpSendTime(DateUtility.getCurrentDate());
                rmDataApplicantDetail=rmDataHelper.setRmDataApplicantDetail(rmDataApplicantDetail);
                
                session.setAttribute("rmApplicationId", applicationId);
                session.setAttribute("rmApplicantId", applicantId);
                session.setAttribute("rmUId", uid);
                
                mav.addObject("status", "200");
                mav.addObject("msg", "OTP Sent.");
                
//                response.sendRedirect("/ESPortal/#/RM/verifyUidOTP");
            }else if (uidResp.has("status") && uidResp.has("msg") && uidResp.get("msg") != null && !uidResp.get("msg").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException(uidResp.get("msg").toString(), uidResp.get("msg").toString());
            }else{
                throw new CustomNonFatalException("Please try again", "Please try again");
            }
            
            mav.addObject("status", "200");
            mav.addObject("data", "data");
            mav.addObject("msg", "otp sent");
            
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("RM/sendOtp", e);
        }
        return mav;
    }
    
    
    @RequestMapping(value = "/RM/verifyOtp", method = RequestMethod.POST)
    public ModelAndView verifyOtp(@RequestBody String payload, HttpServletRequest request, HttpServletResponse response) throws IOException, CustomNonFatalException {
        ModelAndView mav = new ModelAndView("home");
        
        try {
            JSONObject reqJson = new JSONObject(payload);
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            
            Long applicationId;
            Long applicantId;
            String otp;
            String uid;
            
            HttpSession session=request.getSession();
            if(session.getAttribute("rmApplicationId") != null){
                applicationId=Long.parseLong(session.getAttribute("rmApplicationId").toString());
            }else{
                throw new CustomNonFatalException("application id not found", "application id not found");
            }
            if(session.getAttribute("rmApplicantId") != null){
                applicantId=Long.parseLong(session.getAttribute("rmApplicantId").toString());
            }else{
                throw new CustomNonFatalException("applicant id not found", "applicant id not found");
            }
            if(session.getAttribute("rmUId") != null){
                uid=session.getAttribute("rmUId").toString().trim();
            }else{
                throw new CustomNonFatalException("aadhar id not found", "aadhar id not found");
            }
            if (!reqJson.has("otp") || reqJson.get("otp") == null || "".equals(reqJson.get("otp").toString().trim())) {
                throw new CustomNonFatalException("otp not found", "otp not found");
            }
            otp=reqJson.get("otp").toString();
            
            RmDataApplication rmDataApplication=rmDataHelper.getRmDataApplcation(applicationId);
            if(rmDataApplication == null){
                throw new CustomNonFatalException("application data not found", "application data not found");
            }
            RmDataApplicantDetail rmDataApplicantDetail=rmDataHelper.getRmDataApplicantDetailByApplicantsID(applicationId, applicantId);
            if(rmDataApplicantDetail == null){
                throw new CustomNonFatalException("applicant data not found", "applicant data not found");
            }
            if(!rmDataApplicantDetail.getEmailSend().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)
                    || !rmDataApplicantDetail.getOtpSend().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)
                    || rmDataApplicantDetail.getUidVerified().toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)){
                throw new CustomNonFatalException("Invalid Link", "Invalid Link");
            }
            
            String op=rmDataHelper.verifyOtp(applicationId, applicantId, uid, otp, rmDataApplicantDetail.getOtpTxtId());
            JSONObject uidResp = new JSONObject(op);
            if (uidResp.has("status") && uidResp.get("status").toString().equals("true")) {
                rmDataApplicantDetail.setUidVerified(FrameworkAppConstants.CONSTANT_YES);
                rmDataApplicantDetail.setUidVerifiedTime(DateUtility.getCurrentDate());
                rmDataApplicantDetail=rmDataHelper.setRmDataApplicantDetail(rmDataApplicantDetail);
                
                //-----------Start change flag for all applicants verify UID
                List<RmDataApplicantDetail> lstApplicantDetails= rmDataHelper.getRmDataApplicant(applicationId);
                
                if(lstApplicantDetails != null){
                    
                    int cnt=0,totalApplicants=lstApplicantDetails.size();
                    
                    for(RmDataApplicantDetail applicantDetail : lstApplicantDetails){
                        if(applicantDetail.getUidVerified().equals(FrameworkAppConstants.CONSTANT_YES)){
                            cnt++;
                        }
                    }
                    if(cnt == totalApplicants){
                        RmDataApplication dataApplication=rmDataHelper.getRmDataApplcation(applicationId);
                        dataApplication.setStatus(FrameworkAppConstants.RM_APPLICATION_VERIFIED);
                        
                        dataApplication = rmDataHelper.setRmDataApplcation(rmDataApplication);
                    }
                }
                //---------------end change flag for all applicants verify UID
                mav.addObject("status", "200");
                mav.addObject("msg", uidResp.get("msg").toString());
            }else if (uidResp.has("status") && uidResp.has("msg") && uidResp.get("msg") != null && !uidResp.get("msg").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException(uidResp.get("msg").toString(), uidResp.get("msg").toString());
            }else{
                throw new CustomNonFatalException("Please try again", "Please try again");
            }
            
            mav.addObject("status", "200");
            mav.addObject("data", "data");
            
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("RM/verifyOtp", e);
        }
        return mav;
    }
}
