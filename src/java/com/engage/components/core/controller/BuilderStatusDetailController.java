/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.framework.util.log4j.CustomLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Component
@Controller
public class BuilderStatusDetailController {
    
    @RequestMapping(value = "/builderStatusDetail")
    public ModelAndView getStatusDashboard(HttpServletRequest hrs, HttpSession session, @RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");
        try {
            JSONObject reqJson = new JSONObject(payload);

//            if (reqJson.get("builderC") == null || "".equals(reqJson.get("builderC").toString().trim())) {
//                throw new CustomNonFatalException("Invalid Builder Code", "Invalid Builder Code");
//            }
            
            mav.addObject("status", "200");
            mav.addObject("lstStatusDashBoardVO", session.getAttribute("LstStatusDashBoardVo"));

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getStatusDashboard", e);
        }

        return mav;
    }
}
