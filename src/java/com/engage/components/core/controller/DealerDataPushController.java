/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.cmss.engage.framework.util.DateUtility;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsDealerData;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import com.engage.components.core.helper.DealerDataPushHelper;
import com.engage.components.core.helper.TokenHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import io.jsonwebtoken.JwtException;
import java.time.Instant;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sagar
 */
@Controller
@Component
public class DealerDataPushController {

    private static final String AUTHENTICATION_SCHEME = "Bearer";

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    DealerDataPushHelper delarDataPushHelper;

    @RequestMapping(value = "/dealer/pushloandetail", method = RequestMethod.POST)
    public ModelAndView getDelarData(@RequestHeader("Authorization") String auth, @RequestBody String payload) {
        ModelAndView mav = new ModelAndView("home");

        String token = auth.substring(AUTHENTICATION_SCHEME.length()).trim();
        String response=null;
        String applicationId=null;
        try {
            delarDataPushHelper.setDelarDataLog(payload,"unknown");
            tokenHelper.parseJWT(token);
            JSONObject applicationObj = new JSONObject(payload);

            
             if (!applicationObj.has("application_Id") || applicationObj.get("application_Id") == null || "".equals(applicationObj.get("application_Id").toString().trim())) {
                throw new CustomNonFatalException("Application Id Should Not be null or empty ", "Application Id Should Not be null or empty");
            }
              if (!applicationObj.has("applicant_Id") || applicationObj.get("applicant_Id") == null  || "".equals(applicationObj.get("applicant_Id").toString().trim())) {
                throw new CustomNonFatalException("applicant_Id  Should Not be null or empty ", " applicant_Id Should Not be null or empty");
            }
            if (!applicationObj.has("applicant_Name") || applicationObj.get("applicant_Name") == null || "".equals(applicationObj.get("applicant_Name").toString().trim())) {
                throw new CustomNonFatalException(" applicant_Name Should Not be null or empty ", " applicant_Name Should Not be null or empty");
            }
            if (!applicationObj.has("supplier_Id") || applicationObj.get("supplier_Id") == null || "".equals(applicationObj.get("supplier_Id").toString().trim())) {
                throw new CustomNonFatalException("  supplier_Id Should Not be null or empty ", " supplier_Id Should Not be null or empty");
            }
            if (!applicationObj.has("supplier_Name") || applicationObj.get("supplier_Name") == null || "".equals(applicationObj.get("supplier_Name").toString().trim())) {
                throw new CustomNonFatalException(" supplier_Name Should Not be null or empty ", " supplier_Name Should Not be null or empty");
            }
            if (!applicationObj.has("dsa_Id") || applicationObj.get("dsa_Id") == null || "".equals(applicationObj.get("dsa_Id").toString().trim())) {
                throw new CustomNonFatalException(" dsa_Id Should Not be null or empty ", " dsa_Id Should Not be null or empty");
            }
            if (!applicationObj.has("dsa_Name") || applicationObj.get("dsa_Name") == null || "".equals(applicationObj.get("dsa_Name").toString().trim())) {
                throw new CustomNonFatalException("dsa_Name  Should Not be null or empty ", " dsa_Name Should Not be null or empty");
            }
            if (!applicationObj.has("current_Date") || applicationObj.get("current_Date") == null || "".equals(applicationObj.get("current_Date").toString().trim())) {
                throw new CustomNonFatalException("  current_Date Should Not be null or empty ", " current_Date Should Not be null or empty");
            }
            if (!applicationObj.has("asset_Manufacturer") || applicationObj.get("asset_Manufacturer") == null || "".equals(applicationObj.get("asset_Manufacturer").toString().trim())) {
                throw new CustomNonFatalException("asset_Manufacturer  Should Not be null or empty ", " asset_Manufacturer Should Not be null or empty");
            }
            if (!applicationObj.has("asset_Make") || applicationObj.get("asset_Make").equals(null)|| "".equals(applicationObj.get("asset_Make").toString().trim())) {
                throw new CustomNonFatalException(" asset_Make Should Not be null or empty ", " asset_Make Should Not be null or empty");
            }
            if (!applicationObj.has("asset_Model") || applicationObj.get("asset_Model") == null || "".equals(applicationObj.get("asset_Model").toString().trim())) {
                throw new CustomNonFatalException("asset_Model  Should Not be null or empty ", " asset_Model Should Not be null or empty");
            }
            if (!applicationObj.has("asset_Cost") || applicationObj.get("asset_Cost") == null || "".equals(applicationObj.get("asset_Cost").toString().trim())) {
                throw new CustomNonFatalException("asset_Cost  Should Not be null or empty ", " asset_Cost Should Not be null or empty");
            }
            if (!applicationObj.has("margin_Money") || applicationObj.get("margin_Money") == null || "".equals(applicationObj.get("margin_Money").toString().trim())) {
                throw new CustomNonFatalException("margin_Money  Should Not be null or empty ", " margin_Money Should Not be null or empty");
            }
           
            if (!applicationObj.has("processing_Fee") || applicationObj.get("processing_Fee") == null || "".equals(applicationObj.get("processing_Fee").toString().trim())) {
                throw new CustomNonFatalException(" processing_Fee Should Not be null or empty ", " processing_Fee Should Not be null or empty");
            }
            if (!applicationObj.has("ecs_Charges") || applicationObj.get("ecs_Charges") == null || "".equals(applicationObj.get("ecs_Charges").toString().trim())) {
                throw new CustomNonFatalException(" ecs_Charges Should Not be null or empty ", " ecs_Charges Should Not be null or empty");
            }
            if (!applicationObj.has("doc_Charges") || applicationObj.get("doc_Charges") == null || "".equals(applicationObj.get("doc_Charges").toString().trim())) {
                throw new CustomNonFatalException("  doc_Charges Not be null or empty ", " doc_Charges Should Not be null or empty");
            }
            if (!applicationObj.has("stamp_Duty") || applicationObj.get("stamp_Duty") == null || "".equals(applicationObj.get("stamp_Duty").toString().trim())) {
                throw new CustomNonFatalException(" stamp_Duty Should Not be null or empty ", " stamp_Duty Should Not be null or empty");
            }
//            if (!applicationObj.has("loan_Amount") || applicationObj.get("loan_Amount") == null || "".equals(applicationObj.get("loan_Amount").toString().trim())) {
//                throw new CustomNonFatalException(" loan_Amount Should Not be null or empty ", " loan_Amount Should Not be null or empty");
//            }
//            if (!applicationObj.has("disbursement_Amount") || applicationObj.get("disbursement_Amount") == null || "".equals(applicationObj.get("disbursement_Amount").toString().trim())) {
//                throw new CustomNonFatalException(" disbursement_Amount Should Not be null or empty ", " disbursement_Amount Should Not be null or empty");
//            }
            
                applicationId=applicationObj.get("application_Id").toString();
//                delarDataPushHelper.setDelarDataLog(payload,applicationId);

                EsApplicantsDetailBean applicantsData=new EsApplicantsDetailBean();
                applicantsData.setApplicationId(applicationObj.get("application_Id").toString());
                applicantsData.setApplicantId(applicationObj.get("applicant_Id").toString());
                applicantsData.setApplicantName(applicationObj.get("applicant_Name").toString());
                applicantsData.setSupplierId(applicationObj.get("supplier_Id").toString());
                applicantsData.setSupplierName(applicationObj.get("supplier_Name").toString());
                applicantsData.setDsaId(applicationObj.get("dsa_Id").toString());
                applicantsData.setDsaName(applicationObj.get("dsa_Name").toString());
                applicantsData.setModDate(DateUtility.getCurrentDate());
                applicantsData.setCreDate(DateUtility.getCurrentDate());
                response=delarDataPushHelper.saveApplicantDetails(applicantsData);
                
                EsLoanDetailBean loanDetails=new EsLoanDetailBean();

                loanDetails.setApplicationId(applicationObj.get("application_Id").toString());
                loanDetails.setMarginMoney(applicationObj.get("margin_Money").toString());
                loanDetails.setProcessingFee(applicationObj.get("processing_Fee").toString());
                loanDetails.setECSCharges(applicationObj.get("ecs_Charges").toString());
                loanDetails.setDocCharges(applicationObj.get("doc_Charges").toString());
                loanDetails.setStampDuty(applicationObj.get("stamp_Duty").toString());
                loanDetails.setModDate(DateUtility.getCurrentDate());
                loanDetails.setCreDate(DateUtility.getCurrentDate());
                loanDetails.setCurrentDate(applicationObj.get("current_Date").toString());
//                loanDetails.setLoanAmount("10000");
//                loanDetails.setDisbuursemenrAmount("10000");
                response=delarDataPushHelper.saveLoanDetails(loanDetails);
                
                EsAssetDetailBean assetDetails=new EsAssetDetailBean();
                assetDetails.setApplicationId(applicationObj.get("application_Id").toString());
                assetDetails.setAssetManufacturer(applicationObj.get("asset_Manufacturer").toString());
                assetDetails.setAssetMake(applicationObj.get("asset_Make").toString());
                assetDetails.setAssetModel(applicationObj.get("asset_Model").toString());
                assetDetails.setAssetCost(applicationObj.get("asset_Cost").toString());
                assetDetails.setModDate(DateUtility.getCurrentDate());
                assetDetails.setCreDate(DateUtility.getCurrentDate());
                
                response=delarDataPushHelper.saveAssetDetails(assetDetails);
                
                mav.addObject("status", "200");
                mav.addObject("message",response);
               

        }catch(CustomNonFatalException e)
        {
            mav.addObject("status", "402");
            mav.addObject("message",e.getMessage());
        }catch(JwtException e)
        {
            mav.addObject("status", "400");
            mav.addObject("message",e.getMessage());
            mav.addObject("Cause", "Invalid Token");
        }catch (JSONException e) {
            mav.addObject("status", "400");
            mav.addObject("message",e.getMessage());
            e.printStackTrace();
        }
        catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("message",e.getMessage());
            e.printStackTrace();
        }
//        finally{
//            EsDealerData esDealerData=delarDataPushHelper.getRequestLog(applicationId);
//            if(esDealerData != null){
//            System.out.println("fetched appId !!!!!!!!!!"+esDealerData.getApplicationId());
//            esDealerData.setResponse(mav.getModel().toString());
//            delarDataPushHelper.setResponse(esDealerData);
//            }
//        }
        return mav;
    }
}
