/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.components.core.helper.MCMAccountValidateHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Controller
@Component
public class MCMAccountValidateController {

    @Autowired
    MCMAccountValidateHelper validateHelper;

    @RequestMapping(value = "/account/getMcmAccountValidateWS", method = RequestMethod.POST)
    public ModelAndView validateAccount(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try {
            
            System.out.println("inside validateAccount >>>>>>>>>");
            
//            if (!reqJson.has("netDisbursemantAmt") || reqJson.get("netDisbursemantAmt") == null || "".equals(reqJson.get("netDisbursemantAmt").toString().trim())) {
//                throw new CustomNonFatalException("net Disbursemant Amount Missing ", "net Disbursemant Amount Missing");
//            }
//
            if (!reqJson.has("bankAccountNo") || reqJson.get("bankAccountNo") == null || "".equals(reqJson.get("bankAccountNo").toString().trim())) {
                throw new CustomNonFatalException("Bank Account Number Missing", "Bank Account Number Missing");
            }
//            
            if (!reqJson.has("ifscCode") || reqJson.get("ifscCode") == null || "".equals(reqJson.get("ifscCode").toString().trim())) {
                throw new CustomNonFatalException("IFSC Code Missing", "IFSC Code Missing");
            }
//            
//            if (!reqJson.has("modeOfDisbursement") || reqJson.get("modeOfDisbursement") == null || "".equals(reqJson.get("modeOfDisbursement").toString().trim())) {
//                throw new CustomNonFatalException("Mode Of Disbursement Missing", "Mode Of Disbursement Missing");
//            }
//            
             if (!reqJson.has("mobileNo") || reqJson.get("mobileNo") == null || "".equals(reqJson.get("mobileNo").toString().trim())) {
                throw new CustomNonFatalException("Mobile Number Missing", "Mobile Number Missing");
            }
//             
                 if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("application Id", "application Id Missing");
            }
//            
            String netDisbursementAmt=reqJson.get("netDisbursemantAmt").toString();
            String bankAccNo=reqJson.get("bankAccountNo").toString();
            String ifsc=reqJson.get("ifscCode").toString();
            String modeOfDisbursement=reqJson.get("modeOfDisbursement").toString();
            String applicantName=reqJson.get("primaryApplicant").toString(); 
            String mobileNo=reqJson.get("mobileNo").toString(); 
            String applicationId=reqJson.get("applicationId").toString(); 
            String response = validateHelper.validateUserAccount(netDisbursementAmt,bankAccNo,ifsc,modeOfDisbursement,applicantName,mobileNo,applicationId);

            mav.addObject("msg",response);
            mav.addObject("status", "200");

        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getMcmAccountValidateWS", e);
        }
        return mav;
    }

}
