/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.cmss.engage.framework.util.AESAlgoClass;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import com.engage.components.core.helper.PasswordResetHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Controller
@Component
public class PasswordResetController {

    @Autowired
    PasswordResetHelper passwordResetHelper;

    @RequestMapping(value = "sec/forgot", method = RequestMethod.POST)
    public ModelAndView forgot(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try {

            if (!reqJson.has("userName") || reqJson.get("userName") == null || "".equals(reqJson.get("userName").toString().trim())) {
                throw new CustomNonFatalException("User Name not entered", "User Name not entered");
            }

            String userName = reqJson.get("userName").toString();
            String password = passwordResetHelper.generateRandomPassword();
            String encryptedPass = AESAlgoClass.encrypt(password);
            System.out.println("encryptedPass :::::::::" + encryptedPass);

            FrameworkMstrLogin frameworkMstrLogin;

            frameworkMstrLogin = passwordResetHelper.getFrameworkMstrLoginByUserName(userName);

            if (frameworkMstrLogin != null) {
                frameworkMstrLogin.setUserPassword(encryptedPass);
                frameworkMstrLogin.setIsResetPass(FrameworkAppConstants.CONSTANT_YES);
                if (frameworkMstrLogin.getEmailId() != null && !frameworkMstrLogin.getEmailId().equals("")) {
                    String maskedEmail = frameworkMstrLogin.getEmailId().replaceAll("(?<=.{3}).(?=[^@]*?.@)", "*");

                    //--------sending Email
                    String RECIPIENT = frameworkMstrLogin.getEmailId();
                    String[] to = {RECIPIENT}; // list of recipient email addresses
                    String subject = "LTFS user password reset";
                    String body = "Dear " + frameworkMstrLogin.getDisplayName()
                            + "\n\nYour request for a new Password has been successfully processsed.\n\n\n"
//                            + "please use Following details for login.\n"
                            + "User Name : " + frameworkMstrLogin.getUserName()
                            + "\nPassword : " + password
                            +"\n\nYou can visit https://sangamsalesuat2.ltferp.com/ESPortal to start login on our Web platform."
                            +"\n\nFor any queries or concerns, write to us at MobilityHelpdesk@ltfs.com"
                            +"\n\nBest,"
                            +"\nTeam LTFS.";
//                            +"\n\n This is a system genrated mail. please do not reply to this Email ID.";

                    boolean isMailSent = passwordResetHelper.sendMail(to, subject, body);
                    if (isMailSent) {
                        passwordResetHelper.setFrameworkMstrLogin(frameworkMstrLogin);
                        mav.addObject("status", "200");
                        mav.addObject("msg", "Your password change successfully,\n New password send to Email ID " + maskedEmail);
                    } else {
                        mav.addObject("status", "400");
                        mav.addObject("msg", "Error While sending Email.");
                    }
                } else {
                    mav.addObject("status", "400");
                    mav.addObject("msg", "Email Id Not Link With Your User Name.");
                }
            } else {
                mav.addObject("status", "400");
                mav.addObject("msg", "User Not Found.");
            }

        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("/sec/forgot", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("/sec/forgot", e);
        }
        return mav;
    }

    @RequestMapping(value = "sec/reset", method = RequestMethod.POST)
    public ModelAndView reset(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try {

            if (!reqJson.has("pass") || reqJson.get("pass") == null || "".equals(reqJson.get("pass").toString().trim())) {
                throw new CustomNonFatalException("password not entered", "password not entered");
            }
            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("password not entered", "password not entered");
            }

            Long userId = Long.parseLong(reqJson.get("userId").toString());
            String encyPass = AESAlgoClass.encrypt(reqJson.get("pass").toString());

            FrameworkMstrLogin frameworkMstrLogin;
            frameworkMstrLogin = passwordResetHelper.getFrameworkMstrLoginByUserId(userId);
            if (frameworkMstrLogin != null) {
                frameworkMstrLogin.setUserPassword(encyPass);
                frameworkMstrLogin.setIsResetPass(FrameworkAppConstants.CONSTANT_NO);

                passwordResetHelper.setFrameworkMstrLogin(frameworkMstrLogin);

                mav.addObject("status", "200");
                mav.addObject("msg", "Password Reset Success.");
            } else {
                mav.addObject("status", "400");
                mav.addObject("msg", "User Not Found.");
            }

        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("/sec/reset", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("/sec/reset", e);
        }
        return mav;
    }
}
