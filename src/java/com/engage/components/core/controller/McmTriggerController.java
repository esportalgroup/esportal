/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.McCodeMstr;
import com.engage.components.core.entity.bean.McmApplicantsDetails;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmGroupMemberDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import com.engage.components.core.entity.bean.McmTriggerRequestLog;
import com.engage.components.core.helper.McmTriggerHelper;
import com.engage.components.core.helper.TokenHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.mcm.vo.mcmApplicationVOReq;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupDetails;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupFormationVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupVO;
import com.engage.components.esportal.vo.DealerPendingData;
import com.engage.components.esportal.vo.FusyServiceReqDetailsVO;
import com.engage.components.esportal.vo.McmGroupMemberVo;
//import com.engage.components.esportal.vo.FusyServiceReqVO;
import com.engage.components.esportal.vo.McmGroupDetailVO;
import com.engage.framework.util.log4j.CustomLogger;
import io.jsonwebtoken.JwtException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dattatrayt
 */
@Controller
@Component
public class McmTriggerController {

    private static final String AUTHENTICATION_SCHEME = "Bearer";

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    McmTriggerHelper mcmTriggerHelper;

    @RequestMapping(value = "/mcm/pushdata", method = RequestMethod.POST)
    public ModelAndView getDelarData( @RequestHeader("Authorization") String auth,@RequestBody String payload) { //
        ModelAndView mav = new ModelAndView("home");

        String token = auth.substring(AUTHENTICATION_SCHEME.length()).trim();
        McmApplicationDetails appdetails = null;
        McmApplicantsDetails applicantDetails = null;
        JSONObject appObj = null;
        JSONArray applicantArray = null;
        try {
            tokenHelper.parseJWT(token);

            JSONObject applicationObj = new JSONObject(payload);
//            if (!applicationObj.has("application_Id") || applicationObj.get("application_Id") == null || "".equals(applicationObj.get("application_Id").toString().trim())) {
//                throw new CustomNonFatalException("Application Id Should Not be null or empty ", "Application Id Should Not be null or empty");
//            }
            McmTriggerRequestLog mcmTriggerRequestLog = new McmTriggerRequestLog();
            mcmTriggerRequestLog.setApplicationId("");
            mcmTriggerRequestLog.setCreateDate(DateUtility.getCurrentDate());
            mcmTriggerRequestLog.setModDate(DateUtility.getCurrentDate());
            mcmTriggerRequestLog.setRequest(payload);
            mcmTriggerRequestLog = mcmTriggerHelper.setMcmTriggerRequestLog(mcmTriggerRequestLog);

            mav.addObject("status", "200");
            mav.addObject("message", "SUCCESS");
            mav.addObject("refind", mcmTriggerRequestLog.getLogId());

            //----------------insert data in tables
//            applicationObj
            if (applicationObj.has("groups")) {

                JSONArray groupArray = applicationObj.getJSONArray("groups");

                JSONObject groupObj;
                McmGroupDetail mcmGroupDetail;
                for (int i = 0; i < groupArray.length(); i++) {
                    groupObj = groupArray.getJSONObject(i);

                    if (groupObj.has("Applications")) {
                        JSONArray appArray = groupObj.getJSONArray("Applications");
                        if (appArray.length() == 0) {
                            mcmGroupDetail = mcmTriggerHelper.getMcmGroupDetails(groupObj.get("groupId").toString());

                            if (mcmGroupDetail != null) {
                                mcmGroupDetail.setBranchId(groupObj.has("branchId") && !groupObj.get("branchId").toString().equals("null") && !groupObj.get("branchId").toString().trim().equals("") ? groupObj.get("branchId").toString() : null);
                                mcmGroupDetail.setCityId(groupObj.has("cityId") && !groupObj.get("cityId").toString().equals("null") && !groupObj.get("cityId").toString().trim().equals("") ? groupObj.get("cityId").toString() : null);
                                mcmGroupDetail.setCreateBy("sys");
                                mcmGroupDetail.setCreateDate(DateUtility.getCurrentDate());
                                mcmGroupDetail.setDistrictId(groupObj.has("districtId") && !groupObj.get("districtId").toString().equals("null") && !groupObj.get("districtId").toString().trim().equals("") ? groupObj.get("districtId").toString() : null);
                                mcmGroupDetail.setFloId(groupObj.has("floId") && !groupObj.get("floId").toString().equals("null") && !groupObj.get("floId").toString().trim().equals("") ? groupObj.get("floId").toString() : null);
                                mcmGroupDetail.setFloName(groupObj.has("floName") && !groupObj.get("floName").toString().equals("null") && !groupObj.get("floName").toString().trim().equals("") ? groupObj.get("floName").toString() : null);
                                mcmGroupDetail.setGroupId(groupObj.has("groupId") && !groupObj.get("groupId").toString().equals("null") && !groupObj.get("groupId").toString().trim().equals("") ? groupObj.get("groupId").toString() : null);
                                mcmGroupDetail.setGroupName(groupObj.has("groupName") && !groupObj.get("groupName").toString().equals("null") && !groupObj.get("groupName").toString().trim().equals("") ? groupObj.get("groupName").toString() : null);
                                mcmGroupDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                                mcmGroupDetail.setLocalityId(groupObj.has("localityId") && !groupObj.get("localityId").toString().equals("null") && !groupObj.get("localityId").toString().trim().equals("") ? groupObj.get("localityId").toString() : null);
                                mcmGroupDetail.setSanctionDate(groupObj.has("sanctionDate") && !groupObj.get("sanctionDate").toString().equals("null") && !groupObj.get("sanctionDate").toString().trim().equals("") ? DateUtility.parseStringToDate(groupObj.get("sanctionDate").toString(), "YYYY-MM-DD") : null);
                                mcmGroupDetail.setStateId(groupObj.has("stateId") && !groupObj.get("stateId").toString().equals("null") && !groupObj.get("stateId").toString().trim().equals("") ? groupObj.get("stateId").toString() : null);
                                mcmGroupDetail.setVillageName(groupObj.has("villageName") && !groupObj.get("villageName").toString().equals("null") && !groupObj.get("villageName").toString().trim().equals("") ? groupObj.get("villageName").toString() : null);

                            } else {
                                mcmGroupDetail = new McmGroupDetail();

                                mcmGroupDetail.setBranchId(groupObj.has("branchId") && !groupObj.get("branchId").toString().equals("null") && !groupObj.get("branchId").toString().trim().equals("") ? groupObj.get("branchId").toString() : null);
                                mcmGroupDetail.setCityId(groupObj.has("cityId") && !groupObj.get("cityId").toString().equals("null") && !groupObj.get("cityId").toString().trim().equals("") ? groupObj.get("cityId").toString() : null);
                                mcmGroupDetail.setCreateBy("sys");
                                mcmGroupDetail.setCreateDate(DateUtility.getCurrentDate());
                                mcmGroupDetail.setDistrictId(groupObj.has("districtId") && !groupObj.get("districtId").toString().equals("null") && !groupObj.get("districtId").toString().trim().equals("") ? groupObj.get("districtId").toString() : null);
                                mcmGroupDetail.setFloId(groupObj.has("floId") && !groupObj.get("floId").toString().equals("null") && !groupObj.get("floId").toString().trim().equals("") ? groupObj.get("floId").toString() : null);
                                mcmGroupDetail.setFloName(groupObj.has("floName") && !groupObj.get("floName").toString().equals("null") && !groupObj.get("floName").toString().trim().equals("") ? groupObj.get("floName").toString() : null);
                                mcmGroupDetail.setGroupId(groupObj.has("groupId") && !groupObj.get("groupId").toString().equals("null") && !groupObj.get("groupId").toString().trim().equals("") ? groupObj.get("groupId").toString() : null);
                                mcmGroupDetail.setGroupName(groupObj.has("groupName") && !groupObj.get("groupName").toString().equals("null") && !groupObj.get("groupName").toString().trim().equals("") ? groupObj.get("groupName").toString() : null);
                                mcmGroupDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                                mcmGroupDetail.setLocalityId(groupObj.has("localityId") && !groupObj.get("localityId").toString().equals("null") && !groupObj.get("localityId").toString().trim().equals("") ? groupObj.get("localityId").toString() : null);
                                mcmGroupDetail.setSanctionDate(groupObj.has("sanctionDate") && !groupObj.get("sanctionDate").toString().equals("null") && !groupObj.get("sanctionDate").toString().trim().equals("") ? DateUtility.parseStringToDate(groupObj.get("sanctionDate").toString(), "YYYY-MM-DD") : null);
                                mcmGroupDetail.setStateId(groupObj.has("stateId") && !groupObj.get("stateId").toString().equals("null") && !groupObj.get("stateId").toString().trim().equals("") ? groupObj.get("stateId").toString() : null);
                                mcmGroupDetail.setVillageName(groupObj.has("villageName") && !groupObj.get("villageName").toString().equals("null") && !groupObj.get("villageName").toString().trim().equals("") ? groupObj.get("villageName").toString() : null);
                                mcmGroupDetail.setMcCode(groupObj.has("mcCode") && !groupObj.get("mcCode").toString().equals("null") && !groupObj.get("mcCode").toString().trim().equals("") ? groupObj.get("mcCode").toString() : null);
                            }

                            mcmGroupDetail = mcmTriggerHelper.setMcmGroupDetail(mcmGroupDetail);

                        } else {
                            for (int a = 0; a < appArray.length(); a++) {
                                appObj = appArray.getJSONObject(a);

                                appdetails = mcmTriggerHelper.getMcmApplicationDetailsDetail(appObj.get("Application_Id").toString());

                                if (appdetails != null) {

                                    appdetails.setApplicationId(appObj.has("Application_Id") && !appObj.get("Application_Id").toString().equals("null") && !appObj.get("Application_Id").toString().trim().equals("") ? appObj.get("Application_Id").toString() : null);

                                    appdetails.setAppraisalflag(appObj.has("appraisalflag") && !appObj.get("appraisalflag").toString().equals("null") && !appObj.get("appraisalflag").toString().trim().equals("") ? appObj.get("appraisalflag").toString() : null);

                                    appdetails.setBankAccountNumber(appObj.has("bankAccountNumber") && !appObj.get("bankAccountNumber").toString().equals("null") && !appObj.get("bankAccountNumber").toString().trim().equals("") ? appObj.get("bankAccountNumber").toString() : null);

                                    appdetails.setCliAmount(appObj.has("cliAmount") && !appObj.get("cliAmount").toString().equals("null") && !appObj.get("cliAmount").toString().trim().equals("") ? appObj.get("cliAmount").toString() : null);

                                    appdetails.setGenesisLoanAmount(appObj.has("genesis_Loan_Amount") && !appObj.get("genesis_Loan_Amount").toString().equals("null") && !appObj.get("genesis_Loan_Amount").toString().trim().equals("") ? appObj.get("genesis_Loan_Amount").toString() : null);

                                    appdetails.setGenesisTerm(appObj.has("genesis_Term") && !appObj.get("genesis_Term").toString().equals("null") && !appObj.get("genesis_Term").toString().trim().equals("") ? appObj.get("genesis_Term").toString() : null);

                                    appdetails.setMaxEMIAmount(appObj.has("Max_EMI_Amount") && !appObj.get("Max_EMI_Amount").toString().equals("null") && !appObj.get("Max_EMI_Amount").toString().trim().equals("") ? appObj.get("Max_EMI_Amount").toString() : null);

                                    appdetails.setIfscCode(appObj.has("ifscCode") && !appObj.get("ifscCode").toString().equals("null") && !appObj.get("ifscCode").toString().trim().equals("") ? appObj.get("ifscCode").toString() : null);

                                    appdetails.setModeOfDisbursement(appObj.has("modeOfDisbursement") && !appObj.get("modeOfDisbursement").toString().equals("null") && !appObj.get("modeOfDisbursement").toString().trim().equals("") ? appObj.get("modeOfDisbursement").toString() : null
                                    );
                                    appdetails.setNetDisbursementAmt(appObj.has("netDisbursementAmt") && !appObj.get("netDisbursementAmt").toString().equals("null") && !appObj.get("netDisbursementAmt").toString().trim().equals("") ? appObj.get("netDisbursementAmt").toString() : null);

                                    appdetails.setProcFee(appObj.has("procFee") && !appObj.get("procFee").toString().equals("null") && !appObj.get("procFee").toString().trim().equals("") ? appObj.get("procFee").toString() : null);

                                    appdetails.setAccountValidationStatus(appObj.has("accountValidationStatus") && !appObj.get("accountValidationStatus").toString().equals("null") && !appObj.get("accountValidationStatus").toString().trim().equals("") ? appObj.get("accountValidationStatus").toString() : null);

                                    appdetails.setGroupId(groupObj.has("groupId") && !groupObj.get("groupId").toString().equals("null") && !groupObj.get("groupId").toString().trim().equals("") ? groupObj.get("groupId").toString() : null);

                                    appdetails.setLosId(appObj.has("LOSId") && !appObj.get("LOSId").toString().equals("null") && !appObj.get("LOSId").toString().trim().equals("") ? appObj.get("LOSId").toString() : null);

                                    appdetails.setFlsCode(appObj.has("flsCode") && !appObj.get("flsCode").toString().equals("null") && !appObj.get("flsCode").toString().trim().equals("") ? appObj.get("flsCode").toString() : null);
                                    appdetails.setFlsName(appObj.has("flsName") && !appObj.get("flsName").toString().equals("null") && !appObj.get("flsName").toString().trim().equals("") ? appObj.get("flsName").toString() : null);
                                    appdetails.setMobileNumber(appObj.has("mobileNumber") && !appObj.get("mobileNumber").toString().equals("null") && !appObj.get("mobileNumber").toString().trim().equals("") ? appObj.get("mobileNumber").toString() : null);
                                    
                                    if((appObj.has("accountValidationStatus") && !appObj.get("accountValidationStatus").toString().equals("null") && !appObj.get("accountValidationStatus").toString().trim().equals("") ? appObj.get("accountValidationStatus").toString() : "").equalsIgnoreCase("pass"))//||
                                            //(appObj.has("accountValidationStatus") && !appObj.get("accountValidationStatus").toString().equals("null") && !appObj.get("accountValidationStatus").toString().trim().equals("") ? appObj.get("accountValidationStatus").toString() : "").equalsIgnoreCase("Transaction is already exists with success status")
                                    {
                                        appdetails.setIsAccountValid(FrameworkAppConstants.CONSTANT_YES);
                                    }else
                                    {
                                        appdetails.setIsAccountValid(null);
                                    }
                                    mcmTriggerHelper.setMcmApplicationDetailsDetail(appdetails);

                                    if (appObj.has("Applicants")) {
                                        applicantArray = appObj.getJSONArray("Applicants");
                                        setApplicantDetails(applicantArray, appObj.get("Application_Id").toString(), groupObj.get("groupId").toString());
                                    }

                                } else {

                                    appdetails = new McmApplicationDetails();
                                    appdetails.setApplicationId(appObj.has("Application_Id") && !appObj.get("Application_Id").toString().equals("null") && !appObj.get("Application_Id").toString().trim().equals("") ? appObj.get("Application_Id").toString() : null);

                                    appdetails.setAppraisalflag(appObj.has("appraisalflag") && !appObj.get("appraisalflag").toString().equals("null") && !appObj.get("appraisalflag").toString().trim().equals("") ? appObj.get("appraisalflag").toString() : null);

                                    appdetails.setBankAccountNumber(appObj.has("bankAccountNumber") && !appObj.get("bankAccountNumber").toString().equals("null") && !appObj.get("bankAccountNumber").toString().trim().equals("") ? appObj.get("bankAccountNumber").toString() : null);

                                    appdetails.setCliAmount(appObj.has("cliAmount") && !appObj.get("cliAmount").toString().equals("null") && !appObj.get("cliAmount").toString().trim().equals("") ? appObj.get("cliAmount").toString() : null);

                                    appdetails.setGenesisLoanAmount(appObj.has("genesis_Loan_Amount") && !appObj.get("genesis_Loan_Amount").toString().equals("null") && !appObj.get("genesis_Loan_Amount").toString().trim().equals("") ? appObj.get("genesis_Loan_Amount").toString() : null);

                                    appdetails.setGenesisTerm(appObj.has("genesis_Term") && !appObj.get("genesis_Term").toString().equals("null") && !appObj.get("genesis_Term").toString().trim().equals("") ? appObj.get("genesis_Term").toString() : null);

                                    appdetails.setMaxEMIAmount(appObj.has("Max_EMI_Amount") && !appObj.get("Max_EMI_Amount").toString().equals("null") && !appObj.get("Max_EMI_Amount").toString().trim().equals("") ? appObj.get("Max_EMI_Amount").toString() : null);

                                    appdetails.setIfscCode(appObj.has("ifscCode") && !appObj.get("ifscCode").toString().equals("null") && !appObj.get("ifscCode").toString().trim().equals("") ? appObj.get("ifscCode").toString() : null);

                                    appdetails.setModeOfDisbursement(appObj.has("modeOfDisbursement") && !appObj.get("modeOfDisbursement").toString().equals("null") && !appObj.get("modeOfDisbursement").toString().trim().equals("") ? appObj.get("modeOfDisbursement").toString() : null
                                    );
                                    appdetails.setNetDisbursementAmt(appObj.has("netDisbursementAmt") && !appObj.get("netDisbursementAmt").toString().equals("null") && !appObj.get("netDisbursementAmt").toString().trim().equals("") ? appObj.get("netDisbursementAmt").toString() : null);

                                    appdetails.setProcFee(appObj.has("procFee") && !appObj.get("procFee").toString().equals("null") && !appObj.get("procFee").toString().trim().equals("") ? appObj.get("procFee").toString() : null);

                                    appdetails.setAccountValidationStatus(appObj.has("accountValidationStatus") && !appObj.get("accountValidationStatus").toString().equals("null") && !appObj.get("accountValidationStatus").toString().trim().equals("") ? appObj.get("accountValidationStatus").toString() : null);

                                    appdetails.setGroupId(groupObj.has("groupId") && !groupObj.get("groupId").toString().equals("null") && !groupObj.get("groupId").toString().trim().equals("") ? groupObj.get("groupId").toString() : null);

                                    appdetails.setLosId(appObj.has("LOSId") && !appObj.get("LOSId").toString().equals("null") && !appObj.get("LOSId").toString().trim().equals("") ? appObj.get("LOSId").toString() : null);
                                    appdetails.setFlsCode(appObj.has("flsCode") && !appObj.get("flsCode").toString().equals("null") && !appObj.get("flsCode").toString().trim().equals("") ? appObj.get("flsCode").toString() : null);
                                    appdetails.setFlsName(appObj.has("flsName") && !appObj.get("flsName").toString().equals("null") && !appObj.get("flsName").toString().trim().equals("") ? appObj.get("flsName").toString() : null);
                                    appdetails.setMobileNumber(appObj.has("mobileNumber") && !appObj.get("mobileNumber").toString().equals("null") && !appObj.get("mobileNumber").toString().trim().equals("") ? appObj.get("mobileNumber").toString() : null);
                                    
                                    if((appObj.has("accountValidationStatus") && !appObj.get("accountValidationStatus").toString().equals("null") && !appObj.get("accountValidationStatus").toString().trim().equals("") ? appObj.get("accountValidationStatus").toString() : "").equalsIgnoreCase("pass"))//||
                                            //(appObj.has("accountValidationStatus") && !appObj.get("accountValidationStatus").toString().equals("null") && !appObj.get("accountValidationStatus").toString().trim().equals("") ? appObj.get("accountValidationStatus").toString() : "").equalsIgnoreCase("Transaction is already exists with success status")
                                    {
                                        appdetails.setIsAccountValid(FrameworkAppConstants.CONSTANT_YES);
                                    }else
                                    {
                                        appdetails.setIsAccountValid(null);
                                    }

                                    mcmTriggerHelper.setMcmApplicationDetailsDetail(appdetails);

                                    if (appObj.has("Applicants")) {

                                        //System.out.println("Inside Applicants >>>>>>>>>");
                                        applicantArray = appObj.getJSONArray("Applicants");
                                        setApplicantDetails(applicantArray, appObj.get("Application_Id").toString(), groupObj.get("groupId").toString());

                                    }
                                }

                            }
                        }

                    }

                    mcmGroupDetail = mcmTriggerHelper.getMcmGroupDetails(groupObj.get("groupId").toString());

                    if (mcmGroupDetail != null) {
                        mcmGroupDetail.setBranchId(groupObj.has("branchId") && !groupObj.get("branchId").toString().equals("null") && !groupObj.get("branchId").toString().trim().equals("") ? groupObj.get("branchId").toString() : null);
                        mcmGroupDetail.setCityId(groupObj.has("cityId") && !groupObj.get("cityId").toString().equals("null") && !groupObj.get("cityId").toString().trim().equals("") ? groupObj.get("cityId").toString() : null);
                        mcmGroupDetail.setCreateBy("sys");
                        mcmGroupDetail.setCreateDate(DateUtility.getCurrentDate());
                        mcmGroupDetail.setDistrictId(groupObj.has("districtId") && !groupObj.get("districtId").toString().equals("null") && !groupObj.get("districtId").toString().trim().equals("") ? groupObj.get("districtId").toString() : null);
                        mcmGroupDetail.setFloId(groupObj.has("floId") && !groupObj.get("floId").toString().equals("null") && !groupObj.get("floId").toString().trim().equals("") ? groupObj.get("floId").toString() : null);
                        mcmGroupDetail.setFloName(groupObj.has("floName") && !groupObj.get("floName").toString().equals("null") && !groupObj.get("floName").toString().trim().equals("") ? groupObj.get("floName").toString() : null);
                        mcmGroupDetail.setGroupId(groupObj.has("groupId") && !groupObj.get("groupId").toString().equals("null") && !groupObj.get("groupId").toString().trim().equals("") ? groupObj.get("groupId").toString() : null);
                        mcmGroupDetail.setGroupName(groupObj.has("groupName") && !groupObj.get("groupName").toString().equals("null") && !groupObj.get("groupName").toString().trim().equals("") ? groupObj.get("groupName").toString() : null);
                        mcmGroupDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                        mcmGroupDetail.setLocalityId(groupObj.has("localityId") && !groupObj.get("localityId").toString().equals("null") && !groupObj.get("localityId").toString().trim().equals("") ? groupObj.get("localityId").toString() : null);
                        mcmGroupDetail.setSanctionDate(groupObj.has("sanctionDate") && !groupObj.get("sanctionDate").toString().equals("null") && !groupObj.get("sanctionDate").toString().trim().equals("") ? DateUtility.parseStringToDate(groupObj.get("sanctionDate").toString(), "YYYY-MM-DD") : null);
                        mcmGroupDetail.setStateId(groupObj.has("stateId") && !groupObj.get("stateId").toString().equals("null") && !groupObj.get("stateId").toString().trim().equals("") ? groupObj.get("stateId").toString() : null);
                        mcmGroupDetail.setVillageName(groupObj.has("villageName") && !groupObj.get("villageName").toString().equals("null") && !groupObj.get("villageName").toString().trim().equals("") ? groupObj.get("villageName").toString() : null);

                    } else {
                        mcmGroupDetail = new McmGroupDetail();

                        mcmGroupDetail.setBranchId(groupObj.has("branchId") && !groupObj.get("branchId").toString().equals("null") && !groupObj.get("branchId").toString().trim().equals("") ? groupObj.get("branchId").toString() : null);
                        mcmGroupDetail.setCityId(groupObj.has("cityId") && !groupObj.get("cityId").toString().equals("null") && !groupObj.get("cityId").toString().trim().equals("") ? groupObj.get("cityId").toString() : null);
                        mcmGroupDetail.setCreateBy("sys");
                        mcmGroupDetail.setCreateDate(DateUtility.getCurrentDate());
                        mcmGroupDetail.setDistrictId(groupObj.has("districtId") && !groupObj.get("districtId").toString().equals("null") && !groupObj.get("districtId").toString().trim().equals("") ? groupObj.get("districtId").toString() : null);
                        mcmGroupDetail.setFloId(groupObj.has("floId") && !groupObj.get("floId").toString().equals("null") && !groupObj.get("floId").toString().trim().equals("") ? groupObj.get("floId").toString() : null);
                        mcmGroupDetail.setFloName(groupObj.has("floName") && !groupObj.get("floName").toString().equals("null") && !groupObj.get("floName").toString().trim().equals("") ? groupObj.get("floName").toString() : null);
                        mcmGroupDetail.setGroupId(groupObj.has("groupId") && !groupObj.get("groupId").toString().equals("null") && !groupObj.get("groupId").toString().trim().equals("") ? groupObj.get("groupId").toString() : null);
                        mcmGroupDetail.setGroupName(groupObj.has("groupName") && !groupObj.get("groupName").toString().equals("null") && !groupObj.get("groupName").toString().trim().equals("") ? groupObj.get("groupName").toString() : null);
                        mcmGroupDetail.setIsActive(FrameworkAppConstants.CONSTANT_YES);
                        mcmGroupDetail.setLocalityId(groupObj.has("localityId") && !groupObj.get("localityId").toString().equals("null") && !groupObj.get("localityId").toString().trim().equals("") ? groupObj.get("localityId").toString() : null);
                        mcmGroupDetail.setSanctionDate(groupObj.has("sanctionDate") && !groupObj.get("sanctionDate").toString().equals("null") && !groupObj.get("sanctionDate").toString().trim().equals("") ? DateUtility.parseStringToDate(groupObj.get("sanctionDate").toString(), "YYYY-MM-DD") : null);
                        mcmGroupDetail.setStateId(groupObj.has("stateId") && !groupObj.get("stateId").toString().equals("null") && !groupObj.get("stateId").toString().trim().equals("") ? groupObj.get("stateId").toString() : null);
                        mcmGroupDetail.setVillageName(groupObj.has("villageName") && !groupObj.get("villageName").toString().equals("null") && !groupObj.get("villageName").toString().trim().equals("") ? groupObj.get("villageName").toString() : null);
                        mcmGroupDetail.setMcCode(groupObj.has("mcCode") && !groupObj.get("mcCode").toString().equals("null") && !groupObj.get("mcCode").toString().trim().equals("") ? groupObj.get("mcCode").toString() : null);
                    }

                    mcmGroupDetail = mcmTriggerHelper.setMcmGroupDetail(mcmGroupDetail);

                    McCodeMstr mcmCodeMstr = mcmTriggerHelper.getMcmCodeMstr(groupObj.get("mcCode").toString());

                }
            }
            //---------------end table data
//        }catch(CustomNonFatalException e)
//        {
//            mav.addObject("status", "402");
//            mav.addObject("message",e.getMessage());
        } catch (JwtException e) {
            mav.addObject("status", "400");
            mav.addObject("message", e.getMessage());
        } catch (JSONException e) {
            mav.addObject("status", "400");
            mav.addObject("message", e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("message", e.getMessage());
            e.printStackTrace();
        }
        return mav;
    }

    private void setApplicantDetails(JSONArray applicantArray, String appId, String groupId) {
        //System.out.println("setApplicantDetails >>>>>>>>");
        McmApplicantsDetails applicantDetails = null;
        try {
            for (int a = 0; a < applicantArray.length(); a++) {
                int index = a;
                JSONObject applicantObj = applicantArray.getJSONObject(a);
                applicantDetails = mcmTriggerHelper.getMcmApplicantDetail(applicantObj.get("applicantID").toString(), appId);
                if (applicantDetails != null) {
                    applicantDetails.setApplicantId(applicantObj.has("applicantID") && !applicantObj.get("applicantID").toString().equals("null") && !applicantObj.get("applicantID").toString().trim().equals("") ? applicantObj.get("applicantID").toString() : null);
                    applicantDetails.setApplicantName(applicantObj.has("applicantName") && !applicantObj.get("applicantName").toString().equals("null") && !applicantObj.get("applicantName").toString().trim().equals("") ? applicantObj.get("applicantName").toString() : null);
//                    applicantDetails.setApplicantId(applicantObj.get("applicantID").toString());
//                    applicantDetails.setApplicantName(applicantObj.get("applicantName").toString());
                    applicantDetails.setApplicationId(appId);
                    applicantDetails.setGroupId(groupId);
                    //System.out.println("index >>>>>>"+index+applicantObj.get("applicantName").toString());
                    if (applicantObj.has("relationShip")) {
                        //System.out.println("index >>>>>>"+index+applicantObj.get("applicantName").toString());
//                        applicantDetails.setRealationWithApplicant(applicantObj.get("relationWithApplicant").toString());
                        applicantDetails.setRealationWithApplicant(applicantObj.has("relationShip") && !applicantObj.get("relationShip").toString().equals("null") && !applicantObj.get("relationShip").toString().trim().equals("") ? applicantObj.get("relationShip").toString() : null);
                    } else {
                        applicantDetails.setRealationWithApplicant("");
                    }
                    if (applicantObj.has("LOSId")) {
//                    applicantDetails.setLosId(applicantObj.get("LOSId").toString());
                        applicantDetails.setLosId(applicantObj.has("LOSId") && !applicantObj.get("LOSId").toString().equals("null") && !applicantObj.get("LOSId").toString().trim().equals("") ? applicantObj.get("LOSId").toString() : null);
                    } else {
                        applicantDetails.setLosId("");
                    }
                    mcmTriggerHelper.setMcmApplicantDetail(applicantDetails);

                } else {
                    applicantDetails = new McmApplicantsDetails();

                    applicantDetails.setApplicantId(applicantObj.has("applicantID") && !applicantObj.get("applicantID").toString().equals("null") && !applicantObj.get("applicantID").toString().trim().equals("") ? applicantObj.get("applicantID").toString() : null);
                    applicantDetails.setApplicantName(applicantObj.has("applicantName") && !applicantObj.get("applicantName").toString().equals("null") && !applicantObj.get("applicantName").toString().trim().equals("") ? applicantObj.get("applicantName").toString() : null);

//                    applicantDetails.setApplicantId(applicantObj.get("applicantID").toString());
//                    applicantDetails.setApplicantName(applicantObj.get("applicantName").toString());
                    applicantDetails.setApplicationId(appId);
                    applicantDetails.setGroupId(groupId);
                    //System.out.println("index >>>>>>"+index+applicantObj.get("applicantName").toString());
                    if (applicantObj.has("relationWithApplicant")) {
                        //System.out.println("index >>>>>>"+index+applicantObj.get("applicantName").toString());
//                        applicantDetails.setRealationWithApplicant(applicantObj.get("relationWithApplicant").toString());
                        applicantDetails.setRealationWithApplicant(applicantObj.has("relationWithApplicant") && !applicantObj.get("relationWithApplicant").toString().equals("null") && !applicantObj.get("relationWithApplicant").toString().trim().equals("") ? applicantObj.get("relationWithApplicant").toString() : null);

                    } else {
                        applicantDetails.setRealationWithApplicant("");
                    }

                    if (applicantObj.has("LOSId")) {
//                    applicantDetails.setLosId(applicantObj.get("LOSId").toString());
                        applicantDetails.setLosId(applicantObj.has("LOSId") && !applicantObj.get("LOSId").toString().equals("null") && !applicantObj.get("LOSId").toString().trim().equals("") ? applicantObj.get("LOSId").toString() : null);
                    } else {
                        applicantDetails.setLosId("");
                    }

                    mcmTriggerHelper.setMcmApplicantDetail(applicantDetails);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/getMcmGroupDetail", method = RequestMethod.POST)
    public ModelAndView LeadGenDetail(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try {

            if (!reqJson.has("grpId") || reqJson.get("grpId") == null || "".equals(reqJson.get("grpId").toString().trim())) {
                throw new CustomNonFatalException("grpId not entered", "grpId not entered");
            }
            if (!reqJson.has("mcCode") || reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException("mcCode not entered", "mcCode not entered");
            }

            Long grpId = Long.parseLong(reqJson.get("grpId").toString());
            String mcCode = reqJson.get("mcCode").toString();
            String groupId = reqJson.get("groupId").toString();

            //System.out.println("grpId" + grpId + "::::::::groupId>>>>>" + groupId);
            McmGroupDetailVO groupDetail = mcmTriggerHelper.getMcmGroupDetail(groupId);

            List<McmMpngDisbursementAccount> lstDisbAcc = mcmTriggerHelper.getMcmMpngDisbursementAccount(mcCode);

            mav.addObject("status", "200");
            mav.addObject("lstDisbAcc", lstDisbAcc);
            mav.addObject("groupDetails", groupDetail);

        } catch (CustomNonFatalException cnfe) {

            cnfe.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("getMcmGroupDetail", cnfe);
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getMcmGroupDetail", e);
        }
        return mav;
    }

    @RequestMapping(value = "/addMembersToGroup", method = RequestMethod.POST)
    public ModelAndView addMembersToGroup(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        String response = null;
        try {
            if (!reqJson.has("groupDetails") || reqJson.get("groupDetails") == null || "".equals(reqJson.get("groupDetails").toString().trim())) {
                throw new CustomNonFatalException("groupDetails should not be null", "groupDetails should not be null");
            }

            if (!reqJson.has("groupId") || reqJson.get("groupId") == null || "".equals(reqJson.get("groupId").toString().trim())) {
                throw new CustomNonFatalException("groupId null", "groupId should not be null");
            }

            //System.out.println("group Details" + reqJson.get("groupDetails"));
            String groupIdtoAssign = reqJson.get("groupId").toString();

            //System.out.println("groupIdtoAssign :::::::::" + groupIdtoAssign);
            JSONArray dataArray = (JSONArray) reqJson.get("groupDetails");

            for (int j = 0; j < dataArray.length(); j++) {
                JSONObject dataObj = (JSONObject) dataArray.get(j);

                if (dataObj.get("isChecked").toString() == "true") {

                    //System.out.println("group Details" + dataObj.get("groupId"));
                    String groupId = dataObj.get("groupId").toString();

//                      McmGroupDetail groupDetails=mcmTriggerHelper.getMcmGroupDetailBtGroupId(groupId);
//                       groupDetails.setGroupId(groupIdtoAssign);
//                       mcmTriggerHelper.updateMCMGroupDetail(groupDetails);//,dataObj.get("applicationId")
//                       
                    McmApplicationDetails applications = mcmTriggerHelper.getMcmApplicationByGroupId(groupId, dataObj.get("applicationId").toString());
                    applications.setGroupId(groupIdtoAssign);
                    mcmTriggerHelper.updateMCMApplicationDetails(applications);

                    JSONArray applicantArray = (JSONArray) dataObj.get("applicants");
                    {

                        for (int k = 0; k < applicantArray.length(); k++) {
                            JSONObject appObj = (JSONObject) applicantArray.get(k);
                            String applicantId = appObj.get("applicantId").toString();
                            McmApplicantsDetails applicants = mcmTriggerHelper.getMcmApplicantByGroupId(groupId, applicantId);
                            applicants.setGroupId(groupIdtoAssign);
                            mcmTriggerHelper.upDateMCMApplicantDetails(applicants);
                        }
                    }

                }

            }

            mav.addObject("status", "200");
            mav.addObject("message", "success");
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("message", e.getMessage());

        }

        return mav;
    }

    @RequestMapping(value = "/addMembers", method = RequestMethod.POST)
    public ModelAndView addMembers(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {

        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        try {
            if (!reqJson.has("groupId") || reqJson.get("groupId") == null || "".equals(reqJson.get("groupId").toString().trim())) {
                throw new CustomNonFatalException("groupId required", "groupId required");
            }

            if (!reqJson.has("type") || reqJson.get("type") == null || "".equals(reqJson.get("type").toString().trim())) {
                throw new CustomNonFatalException("grpId not entered", "grpId not entered");
            }

            if (!reqJson.has("mcCode") || reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException("grpId not entered", "grpId not entered");
            }

            List<McmGroupMemberVo> groupDetail = mcmTriggerHelper.getaddMembersDetail(reqJson.get("mcCode").toString(), reqJson.get("groupId").toString());

            mav.addObject("status", "200");
            mav.addObject("lstMcmDashBoardVO", groupDetail);

        } catch (Exception e) {

            mav.addObject("status", "400");
            mav.addObject("message", e.getMessage());
        }
        return mav;
    }

    @RequestMapping(value = "/initiateDisbursement", method = RequestMethod.POST)
    public ModelAndView initiateDisbursement(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {

        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        McmApplicationDetails applicationdetails=null;
        try {
            if (!reqJson.has("groupDetails") || reqJson.get("groupDetails") == null || "".equals(reqJson.get("groupDetails").toString().trim())) {
                throw new CustomNonFatalException("groupDetails should not be null", "groupDetails should not be null");
            }
            if (!reqJson.has("collectionDate") || reqJson.get("collectionDate") == null || "".equals(reqJson.get("collectionDate").toString().trim())) {
                throw new CustomNonFatalException("collection Date should not be null", "collection Date  not entered");
            }
            if (!reqJson.has("collectionTime") || reqJson.get("collectionTime") == null || "".equals(reqJson.get("collectionTime").toString().trim())) {
                throw new CustomNonFatalException("collectionTime not entered", "collectionTime not entered");
            }
            if (!reqJson.has("bankAccount") || reqJson.get("bankAccount") == null || "".equals(reqJson.get("bankAccount").toString().trim())) {
                throw new CustomNonFatalException("Bank Account not entered", "Bank Account not entered");
            }

            String collectionDate = reqJson.get("collectionDate").toString();
            String collectionTime = reqJson.get("collectionTime").toString();
            String bankAccount = reqJson.get("bankAccount").toString();
            String mcCode = reqJson.get("mcCode").toString();

//            System.out.println(reqJson.get("groupDetails").toString());
            //System.out.println("collectionDate >>>>>>>>" + collectionDate + "collectionTime >>>>>>" + collectionTime + "bankAccount >>>>>" + bankAccount);
            McmMpngDisbursementAccount mcmAccountDetails = mcmTriggerHelper.getAccountDetailsByAccountNo(bankAccount);
            JSONObject dataobj = (JSONObject) reqJson.get("groupDetails");
            mcmRequestGroupFormationVO groupFormationVo = new mcmRequestGroupFormationVO();
            mcmRequestGroupVO groupVo = new mcmRequestGroupVO();
            mcmRequestGroupDetails groupDetails = new mcmRequestGroupDetails();
            List<mcmRequestGroupDetails> groupVoLst = new ArrayList<mcmRequestGroupDetails>();
            String groupId = dataobj.get("groupId").toString();
            McmGroupDetail mcmGroupDetail = mcmTriggerHelper.getMcmGroupDetails(groupId);
            // set data to update records of group for initiating disbursement
            mcmGroupDetail.setGroupId(groupId);
            mcmGroupDetail.setCollectionDate(collectionDate);
            mcmGroupDetail.setCollectionTime(collectionTime);
            mcmGroupDetail.setMcmAccBankName(mcmAccountDetails.getDisbBank());
            mcmGroupDetail.setMcmAccNo(bankAccount);//mcmAccountDetails.getDisbaccount()
                        // set data to Sending to web service 

            groupDetails.setGroupId(groupId);
            groupDetails.setVillageName(mcmGroupDetail.getVillageName());
            groupDetails.setCity("");//mcmGroupDetail.getCityId()
            groupDetails.setFLSCode(mcmGroupDetail.getFloId());//mcmGroupDetail.getFloId()
            groupDetails.setMCCode(mcmGroupDetail.getMcCode());//

            groupDetails.setMCMAccountNumber(bankAccount);

            groupDetails.setMCMBank_branch_name("");//dataobj.get("").toString()
            groupDetails.setMCMBank_name(mcmAccountDetails.getDisbBank());//dataobj.get("").toString()
            groupDetails.setCollectionDate(collectionDate);
            groupDetails.setCollectionTime(collectionTime);//collectionTime
            groupDetails.setMCMIFSC_code("");//dataobj.get("").toString()

            mcmApplicationVOReq applicationVo = null;

            List<mcmApplicationVOReq> applicationReqLst = new ArrayList<mcmApplicationVOReq>();

            JSONArray memberArray = dataobj.getJSONArray("lstGroupMember");

            //System.out.println("memberArray >>>>>>>>"+memberArray);
            for (int j = 0; j < memberArray.length(); j++) {
                JSONObject memberObj = (JSONObject) memberArray.get(j);

                //System.out.println("memberObj"+memberObj.get("applicationId").toString());
                applicationVo = new mcmApplicationVOReq();

                applicationVo.setApplicationId(memberObj.get("applicationId").toString());
                applicationVo.setAccountNumber(memberObj.get("bankAccountNumber").toString());
                applicationVo.setBank_branch_name("");
                applicationVo.setBank_name("");
                applicationVo.setIFSC_code(memberObj.get("ifscCode").toString());
                applicationVo.setLoanAmount(memberObj.get("loanAmount").toString());
                applicationVo.setModeofDiscursement(memberObj.get("modeOfDisbursement").toString());
                
                applicationdetails=mcmTriggerHelper.getMcmApplicationByGroupId(groupId, memberObj.get("applicationId").toString());
               
                applicationdetails.setApplicationId(memberObj.get("applicationId").toString());
                applicationdetails.setIfscCode(memberObj.get("ifscCode").toString());
                applicationdetails.setBankAccountNumber(memberObj.get("bankAccountNumber").toString());
                applicationdetails.setModeOfDisbursement(memberObj.get("modeOfDisbursement").toString());
//                applicationdetails.setStatus(FrameworkAppConstants.INITIATE_DISBURSEMENT_SUCCESS);
                mcmTriggerHelper.updateMCMApplicationDetails(applicationdetails);

                System.out.println("LOS ID"+applicationdetails.getLosId());
                applicationVo.setLOSId(applicationdetails.getLosId()); 
                applicationReqLst.add(applicationVo);

            }

            groupDetails.setApplication(applicationReqLst);
            groupVoLst.add(groupDetails);
            groupVo.setGroupLst(groupVoLst);
            groupFormationVo.setContents(groupVo);

            String response = mcmTriggerHelper.callGroupFormationService(groupFormationVo);
            
            

            if(response.equalsIgnoreCase("success"))
            {
                mcmGroupDetail.setStatus(FrameworkAppConstants.INITIATE_DISBURSEMENT_SUCCESS);
                
            }else
            {
                mcmGroupDetail.setStatus(FrameworkAppConstants.INITIATE_DISBURSEMENT_FAILS);
            }
       
            mcmTriggerHelper.setMcmGroupDetail(mcmGroupDetail);
            
           

            //System.out.println("response ::::::::" + response);
            if (response.equalsIgnoreCase("success")) {
                mav.addObject("status", "200");
                mav.addObject("message", "success");

            } else {
                mav.addObject("status", "400");
                mav.addObject("message", "fails");

            }

        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("message", e.getMessage());

        }

        return mav;
    }

//    @RequestMapping(value = "/mcm/soundex", method = RequestMethod.POST)
//    public ModelAndView callSoundexService(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
//        ModelAndView mav = new ModelAndView("home");
//        
//        try
//        {
//           FusyServiceReqVO fusyReq=new FusyServiceReqVO();
//           List<FusyServiceReqDetailsVO> detailsLst=new ArrayList<FusyServiceReqDetailsVO>();
//            FusyServiceReqDetailsVO details=new FusyServiceReqDetailsVO();
//            
//            details.setFirstName("O P");
//            details.setLastName("SHARMA");
//            details.setFirstNamePan("OM PRAKASH");
//            details.setLastNamePan("SHARMA");
//            details.setReferenceID("123");
//            detailsLst.add(details);
////            fusyReq.setApplicantsDetails(detailsLst);
//            
//            
//            String response=mcmTriggerHelper.callSoundexService(fusyReq);
//            
//           
//           
//        }catch(Exception e)
//        {
//            e.printStackTrace();
//            mav.addObject("status", "400");
//            mav.addObject("msg", e.getMessage());
//            CustomLogger.error("getMcmGroupDetail", e);
//        }
//        
//        return mav;
//    }
}
