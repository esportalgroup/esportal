/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import com.engage.components.core.helper.LoanAssetHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.DealerPendingData;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Controller
@Component
public class LeadGenDetail {
    
    @Autowired
    LoanAssetHelper assetHelper;

    @RequestMapping(value = "/LeadGenDetail", method = RequestMethod.POST)
    public ModelAndView LeadGenDetail(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        DealerPendingData pendingData=null;

        try {

//            System.out.println(":::::::::::::loanAssetDetail ::::::::::::");
            if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("applicationId not entered", "applicationId not entered");
            }

            String applicationId = reqJson.get("applicationId").toString();
            
                        mav.addObject("status", "200");
                    
        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("loanAssetDetail", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("loanAssetDetail", e);
        }
        return mav;
    }
}
