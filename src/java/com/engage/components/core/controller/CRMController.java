/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.components.core.helper.CRMHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.crm.vo.CRMSearchReq;
import com.engage.components.esportal.crm.vo.CRMSearchRes;
import com.engage.components.esportal.vo.RMApplicationDetailVO;
import com.engage.framework.util.log4j.CustomLogger;
import com.google.gson.JsonArray;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Component
@Controller
public class CRMController {
    
    @Autowired
    CRMHelper cRMHelper;
            
    @RequestMapping(value = "/crmSearch", method = RequestMethod.POST)
    public ModelAndView crmSearch(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");

        try {
            JSONObject reqJson = new JSONObject(payload);

            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("userId not found", "userId not found");
            }

            Long userId = Long.parseLong(reqJson.get("userId").toString());

            CRMSearchReq cRMSearchReq=new CRMSearchReq();
            cRMSearchReq.setAadhaar(reqJson.has("aadhaar") && reqJson.get("aadhaar") != null && !"".equals(reqJson.get("aadhaar").toString().trim()) ? reqJson.get("aadhaar").toString().trim() : "");
            cRMSearchReq.setDob(reqJson.has("dob") && reqJson.get("dob") != null && !"".equals(reqJson.get("dob").toString().trim()) ? reqJson.get("dob").toString().trim() : "");
            cRMSearchReq.setDrivingLicence(reqJson.has("drivingLicence") && reqJson.get("drivingLicence") != null && !"".equals(reqJson.get("drivingLicence").toString().trim()) ? reqJson.get("drivingLicence").toString().trim() : "");
            cRMSearchReq.setMobile(reqJson.has("mobile") && reqJson.get("mobile") != null && !"".equals(reqJson.get("mobile").toString().trim()) ? reqJson.get("mobile").toString().trim() : "");
            cRMSearchReq.setEmail(reqJson.has("email") && reqJson.get("email") != null && !"".equals(reqJson.get("email").toString().trim()) ? reqJson.get("email").toString().trim() : "");
            cRMSearchReq.setLan(reqJson.has("lan") && reqJson.get("lan") != null && !"".equals(reqJson.get("lan").toString().trim()) ? reqJson.get("lan").toString().trim() : "");
            cRMSearchReq.setName(reqJson.has("name") && reqJson.get("name") != null && !"".equals(reqJson.get("name").toString().trim()) ? reqJson.get("name").toString().trim() : "");
            cRMSearchReq.setPan(reqJson.has("pan") && reqJson.get("pan") != null && !"".equals(reqJson.get("pan").toString().trim()) ? reqJson.get("pan").toString().trim() : "");
            cRMSearchReq.setUcId(reqJson.has("ucId") && reqJson.get("ucId") != null && !"".equals(reqJson.get("ucId").toString().trim()) ? reqJson.get("ucId").toString().trim() : "");
            cRMSearchReq.setVoterId(reqJson.has("voterId") && reqJson.get("voterId") != null && !"".equals(reqJson.get("voterId").toString().trim()) ? reqJson.get("voterId").toString().trim() : "");
            
            if(cRMSearchReq.getAadhaar().equals("") && cRMSearchReq.getDrivingLicence().equals("") && cRMSearchReq.getEmail().equals("") && cRMSearchReq.getLan().equals("") && cRMSearchReq.getMobile().equals("") && cRMSearchReq.getPan().equals("") && cRMSearchReq.getUcId().equals("") && cRMSearchReq.getVoterId().equals("")){
                mav.addObject("status", "400");
            mav.addObject("msg", "Please Enter Any One Unique ID");
            }else{
                
                String op = cRMHelper.sendCrmSearchData(cRMSearchReq);
                
//                JSONObject crmSearchResult = new JSONObject(op);
//                JSONArray crmSearchArr = crmSearchResult.getJSONArray(op);
                JSONArray crmSearchArr = new JSONArray(op);
                if(crmSearchArr.length() >0 ){
                    System.out.println("crmSearchArr.length() "+crmSearchArr.length());
                JSONObject crmSearchObj;
                List<CRMSearchRes> lstCRMSearch=new ArrayList<CRMSearchRes>();
                CRMSearchRes RMSearch;
                for(int i=0;i<crmSearchArr.length();i++){
                    crmSearchObj=crmSearchArr.getJSONObject(i);
                    RMSearch=new CRMSearchRes();
                    
                    RMSearch.setAddress(crmSearchObj.getString("Address") != null && !crmSearchObj.getString("Address").equals("") ? crmSearchObj.getString("Address") : "");
                    RMSearch.setEmail(crmSearchObj.getString("Email") != null && !crmSearchObj.getString("Email").equals("") ? crmSearchObj.getString("Email") : "");
                    RMSearch.setLan(crmSearchObj.getString("LAN") != null && !crmSearchObj.getString("LAN").equals("") ? crmSearchObj.getString("LAN") : "");
                    RMSearch.setLoanAmt(crmSearchObj.getString("Loan_Amount") != null && !crmSearchObj.getString("Loan_Amount").equals("") ? crmSearchObj.getString("Loan_Amount") : "");
                    RMSearch.setLoanType(crmSearchObj.getString("Loan_Type") != null && !crmSearchObj.getString("Loan_Type").equals("") ? crmSearchObj.getString("Loan_Type") : "");
                    RMSearch.setMobile(crmSearchObj.getString("Mobile") != null && !crmSearchObj.getString("Mobile").equals("") ? crmSearchObj.getString("Mobile") : "");
                    RMSearch.setName(crmSearchObj.getString("Name") != null && !crmSearchObj.getString("Name").equals("") ? crmSearchObj.getString("Name") : "");
                    RMSearch.setOnBordingDate(crmSearchObj.getString("On_Boarding_Date") != null && !crmSearchObj.getString("On_Boarding_Date").equals("") ? crmSearchObj.getString("On_Boarding_Date") : "");
                    RMSearch.setProduct(crmSearchObj.getString("Product") != null && !crmSearchObj.getString("Product").equals("") ? crmSearchObj.getString("Product") : "");
                    RMSearch.setUcId(crmSearchObj.getString("UCID") != null && !crmSearchObj.getString("UCID").equals("") ? crmSearchObj.getString("UCID") : "");
                    
                    lstCRMSearch.add(RMSearch);
                }
             mav.addObject("data", lstCRMSearch);
            mav.addObject("status", "200");
                }else{
            mav.addObject("status", "400");
            mav.addObject("msg", "No record found");
                }
            }

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("crmSearch", e);
        }
        return mav;
    }
}
