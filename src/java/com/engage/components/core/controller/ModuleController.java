package com.engage.components.core.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
//import com.engage.components.core.entity.bean.Module;
import com.engage.components.core.helper.CoreServiceHelper;

@Component
@Controller
public class ModuleController {
	
	
	@Autowired
	CoreServiceHelper coreServiceHelper;
	
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public ModelAndView gethomePage() throws IOException {
//		System.out.println("index page page");
		
		List moduleList = coreServiceHelper.getAllModules();
		ModelAndView mav = new ModelAndView("home");
        mav.addObject("modules", "moduleList");
		return mav;
	}
	
	@RequestMapping(value = "startmodule", method = RequestMethod.GET)
    public ModelAndView startModule(@RequestParam("id") String id,@Headers Map<String, Object> headerMap ) {
//        System.out.println("start::"+id);
        
       
        
        List moduleList = coreServiceHelper.getAllModules();
		ModelAndView mav = new ModelAndView("home");
        mav.addObject("modules", "moduleList");
       
		return mav;
    }
	
	@RequestMapping(value = "stopmodule", method = RequestMethod.GET)
    public ModelAndView stopModule(@RequestParam("id") String id, @Headers Map<String, Object> headerMap) {
//        System.out.println("stop::"+id);
        
        
        
        List moduleList = coreServiceHelper.getAllModules();
		ModelAndView mav = new ModelAndView("home");
        mav.addObject("modules", "moduleList");
       
		return mav;
    }
}
