/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

/**
 *
 * @author DattatrayT
 */

import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.MandateTriggerDataApplicantsDetails;
import com.engage.components.core.entity.bean.MandateTriggerDataApplication;
import com.engage.components.core.entity.bean.MandateTriggerDataDocument;
import com.engage.components.core.entity.bean.MandateTriggerRequestLog;
import com.engage.components.core.entity.bean.MandateTriggerTokenUser;
import com.engage.components.core.entity.bean.MandateTriggerTokenRequestLog;
import com.engage.components.core.helper.MandateTriggerHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.MandateDocVo;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.engage.components.esportal.vo.OpsApplicationVO;
import com.engage.components.esportal.vo.OpsDocumentVO;
import com.engage.components.esportal.vo.OpsUpdateContentVO;
import com.engage.components.esportal.vo.OpsUpdateReqVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@Controller
public class MandateTriggerController {
    
    @Autowired
    MandateTriggerHelper mandateTriggerHelper;
    
    private static final String AUTHENTICATION_SCHEME = "Bearer";
    
    @RequestMapping(value = "/mandateTrigger/getToken", method = RequestMethod.GET)
    public ModelAndView getToken(HttpServletResponse response,
            @RequestParam(value = "userid", required = true) String userid,
            @RequestParam(value = "password", required = true) String password
    ) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        
        try {
            
            if(userid == null || (userid!=null && (userid.equals("") || userid.equalsIgnoreCase("null")))){
                throw new CustomNonFatalException("userID not found","userID not found");
            }
            
            if(password == null || (password!=null && (password.equals("") || password.equalsIgnoreCase("null")))){
                throw new CustomNonFatalException("password not found","password not found");
            }
            
            MandateTriggerTokenUser mandateTriggerTokenUser=mandateTriggerHelper.getMandateTriggerTokenUser(userid);
            if(mandateTriggerTokenUser != null){
                if(!password.equals(mandateTriggerTokenUser.getPassword())){
                    throw new CustomNonFatalException("user not found","user not found");
                }
            }else{
                throw new CustomNonFatalException("user not found","user not found");
            }
            
            String jwt=mandateTriggerHelper.createJWT();
            
            MandateTriggerTokenRequestLog mandateTriggerTokenRequestLog=new MandateTriggerTokenRequestLog();
            mandateTriggerTokenRequestLog.setCreateDate(DateUtility.getCurrentDate());
            mandateTriggerTokenRequestLog.setModDate(DateUtility.getCurrentDate());
            mandateTriggerTokenRequestLog.setRequest("userid : "+userid+" password : "+password);
            mandateTriggerTokenRequestLog.setResponse("jwt : "+jwt);
            mandateTriggerHelper.setMandateTriggerTokenRequestLog(mandateTriggerTokenRequestLog);
            
            if (jwt != null && !jwt.equals("")) {
                mav.addObject("status", "200");
                mav.addObject("token", jwt);
            }else{
                throw new CustomNonFatalException("please try again","please try again");
            }
            
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("mandateTrigger/getToken", e);
        }
        return mav;
    }
    
    @RequestMapping(value = "/mandateTrigger/trigger", method = RequestMethod.POST)
    public ModelAndView trigger(@RequestHeader("Authorization") String auth, @RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        
        String token = auth.substring(AUTHENTICATION_SCHEME.length()).trim();
        
        try {
            JSONObject reqJson = new JSONObject(payload);
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            
            if (!reqJson.has("Application") || reqJson.get("Application") == null || "".equals(reqJson.get("Application").toString().trim()) || reqJson.get("Application").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("Application not found", "Application not found");
            }
            mandateTriggerHelper.parseJWT(token);
            MandateTriggerRequestLog mandateTriggerRequestLog=new MandateTriggerRequestLog();
            mandateTriggerRequestLog.setCreateDate(DateUtility.getCurrentDate());
            mandateTriggerRequestLog.setModDate(DateUtility.getCurrentDate());
            mandateTriggerRequestLog.setRequest(payload);
            mandateTriggerRequestLog=mandateTriggerHelper.setMandateTriggerRequestLog(mandateTriggerRequestLog);
            
            JSONArray Application = reqJson.getJSONArray("Application");
            JSONObject applicationObj=null;
            JSONArray documentArray = null;
            JSONObject documentObj=null;
            JSONArray applicantArray = null;
            JSONObject applicantObj=null;
            
            List<MandateTriggerDataApplication> oldApplicationLst=null;
            MandateTriggerDataApplication mandateTriggerDataApplication=null;
            MandateTriggerDataDocument mandateTriggerDataDocument = null;
            MandateTriggerDataApplicantsDetails mandateTriggerDataApplicantsDetails = null;
            Boolean isDocApproved = false, isDocFlsQ = false;
            for(int i=0; i< Application.length(); i++){
                
                isDocFlsQ = false;
                applicationObj = new JSONObject();
                applicationObj=Application.getJSONObject(i);
                
                mandateTriggerDataApplication = new MandateTriggerDataApplication();
                
                //Application Start
                if (!applicationObj.has("ApplicationID") || applicationObj.get("ApplicationID") == null || "".equals(applicationObj.get("ApplicationID").toString().trim()) || applicationObj.get("ApplicationID").toString().equalsIgnoreCase("null")) {
                    throw new CustomNonFatalException("ApplicationID not found", "ApplicationID not found");
                }
                if (applicationObj.get("ApplicationID").toString().length() > 30) {
                    throw new CustomNonFatalException("ApplicationID Is Too Long", "ApplicationID Is Too Long");
                }
                if (!applicationObj.has("LOSID") || applicationObj.get("LOSID") == null || "".equals(applicationObj.get("LOSID").toString().trim()) || applicationObj.get("LOSID").toString().equalsIgnoreCase("null")) {
                    throw new CustomNonFatalException("LOSID of application not found", "LOSID of application not found");
                }
                if (applicationObj.get("LOSID").toString().length() > 30) {
                    throw new CustomNonFatalException("LOSID Is Too Long", "LOSID Is Too Long");
                }
                oldApplicationLst = new ArrayList<MandateTriggerDataApplication>();
                oldApplicationLst = mandateTriggerHelper.getMandateTriggerDataApplicationLst(applicationObj.get("ApplicationID").toString());
                if(oldApplicationLst != null){
                    for(MandateTriggerDataApplication applicationOld : oldApplicationLst){
                        applicationOld.setIsLatest(FrameworkAppConstants.CONSTANT_NO);
                        applicationOld.setDeactiveDate(DateUtility.getCurrentDate());
                        applicationOld=mandateTriggerHelper.setMandateTriggerDataApplication(applicationOld);
                    }
                }
                mandateTriggerDataApplication.setApplicationId(applicationObj.get("ApplicationID").toString());
                mandateTriggerDataApplication.setLosId(applicationObj.get("LOSID").toString());
                mandateTriggerDataApplication.setCreateDate(DateUtility.getCurrentDate());
                mandateTriggerDataApplication.setModDate(DateUtility.getCurrentDate());
                mandateTriggerDataApplication.setIsLatest(FrameworkAppConstants.CONSTANT_YES);
                if (applicationObj.has("Status") && applicationObj.get("Status") != null && !"".equals(applicationObj.get("Status").toString().trim()) && !applicationObj.get("Status").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setLosStatus(applicationObj.get("Status").toString().trim());
                }
                if (applicationObj.has("OPSStatus") && applicationObj.get("OPSStatus") != null && !"".equals(applicationObj.get("OPSStatus").toString().trim()) && !applicationObj.get("OPSStatus").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setStatus(applicationObj.get("OPSStatus").toString().trim());
                }else{
                    mandateTriggerDataApplication.setStatus("PENDING");
                }
                if (applicationObj.has("Remark") && applicationObj.get("Remark") != null && !"".equals(applicationObj.get("Remark").toString().trim()) && !applicationObj.get("Remark").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setRemark(applicationObj.get("Remark").toString().trim());
                }
                if (applicationObj.has("SanctionDate") && applicationObj.get("SanctionDate") != null && !"".equals(applicationObj.get("SanctionDate").toString().trim()) && !applicationObj.get("SanctionDate").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setSanctionDate(applicationObj.get("SanctionDate").toString().trim());
                }
                if (applicationObj.has("PropertyAgreement") && applicationObj.get("PropertyAgreement") != null && !"".equals(applicationObj.get("PropertyAgreement").toString().trim()) && !applicationObj.get("PropertyAgreement").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPropertyAgreement(applicationObj.get("PropertyAgreement").toString().trim());
                }
                if (applicationObj.has("ProductType") && applicationObj.get("ProductType") != null && !"".equals(applicationObj.get("ProductType").toString().trim()) && !applicationObj.get("ProductType").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setProductType(applicationObj.get("ProductType").toString().trim());
                }
                if (applicationObj.has("PhysicalMandateEMI") && applicationObj.get("PhysicalMandateEMI") != null && !"".equals(applicationObj.get("PhysicalMandateEMI").toString().trim()) && !applicationObj.get("PhysicalMandateEMI").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPhysicalMandateEMI(applicationObj.get("PhysicalMandateEMI").toString().trim());
                }
                if (applicationObj.has("PhysicalMandateBranchname") && applicationObj.get("PhysicalMandateBranchname") != null && !"".equals(applicationObj.get("PhysicalMandateBranchname").toString().trim()) && !applicationObj.get("PhysicalMandateBranchname").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPhysicalMandateBranchname(applicationObj.get("PhysicalMandateBranchname").toString().trim());
                }
                if (applicationObj.has("PhysicalMandateBankname") && applicationObj.get("PhysicalMandateBankname") != null && !"".equals(applicationObj.get("PhysicalMandateBankname").toString().trim()) && !applicationObj.get("PhysicalMandateBankname").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPhysicalMandateBankname(applicationObj.get("PhysicalMandateBankname").toString().trim());
                }
                if (applicationObj.has("PhysicalMandateBankACnumber") && applicationObj.get("PhysicalMandateBankACnumber") != null && !"".equals(applicationObj.get("PhysicalMandateBankACnumber").toString().trim()) && !applicationObj.get("PhysicalMandateBankACnumber").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPhysicalMandateBankACnumber(applicationObj.get("PhysicalMandateBankACnumber").toString().trim());
                }
                if (applicationObj.has("PhysicalMandateApplicationNumber") && applicationObj.get("PhysicalMandateApplicationNumber") != null && !"".equals(applicationObj.get("PhysicalMandateApplicationNumber").toString().trim()) && !applicationObj.get("PhysicalMandateApplicationNumber").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPhysicalMandateApplicationNumber(applicationObj.get("PhysicalMandateApplicationNumber").toString().trim());
                }
                if (applicationObj.has("PDCIFSCcode") && applicationObj.get("PDCIFSCcode") != null && !"".equals(applicationObj.get("PDCIFSCcode").toString().trim()) && !applicationObj.get("PDCIFSCcode").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPDCIFSCcode(applicationObj.get("PDCIFSCcode").toString().trim());
                }
                if (applicationObj.has("PDCBranchname") && applicationObj.get("PDCBranchname") != null && !"".equals(applicationObj.get("PDCBranchname").toString().trim()) && !applicationObj.get("PDCBranchname").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPDCBranchname(applicationObj.get("PDCBranchname").toString().trim());
                }
                if (applicationObj.has("PDCBankname") && applicationObj.get("PDCBankname") != null && !"".equals(applicationObj.get("PDCBankname").toString().trim()) && !applicationObj.get("PDCBankname").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPDCBankname(applicationObj.get("PDCBankname").toString().trim());
                }
                if (applicationObj.has("PDCBankACnumber") && applicationObj.get("PDCBankACnumber") != null && !"".equals(applicationObj.get("PDCBankACnumber").toString().trim()) && !applicationObj.get("PDCBankACnumber").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setPDCBankACnumber(applicationObj.get("PDCBankACnumber").toString().trim());
                }
                if (applicationObj.has("InstallmentDate") && applicationObj.get("InstallmentDate") != null && !"".equals(applicationObj.get("InstallmentDate").toString().trim()) && !applicationObj.get("InstallmentDate").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setInstallmentDate(applicationObj.get("InstallmentDate").toString().trim());
                }
                if (applicationObj.has("FLSName") && applicationObj.get("FLSName") != null && !"".equals(applicationObj.get("FLSName").toString().trim()) && !applicationObj.get("FLSName").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setFLSName(applicationObj.get("FLSName").toString().trim());
                }
                if (applicationObj.has("FLSId") && applicationObj.get("FLSId") != null && !"".equals(applicationObj.get("FLSId").toString().trim()) && !applicationObj.get("FLSId").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setFLSId(applicationObj.get("FLSId").toString().trim());
                }
                if (applicationObj.has("CreditVerificationDate") && applicationObj.get("CreditVerificationDate") != null && !"".equals(applicationObj.get("CreditVerificationDate").toString().trim()) && !applicationObj.get("CreditVerificationDate").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setCreditVerificationDate(applicationObj.get("CreditVerificationDate").toString().trim());
                }
                if (applicationObj.has("Builder_Name") && applicationObj.get("Builder_Name") != null && !"".equals(applicationObj.get("Builder_Name").toString().trim()) && !applicationObj.get("Builder_Name").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setBuilder_Name(applicationObj.get("Builder_Name").toString().trim());
                }
                if (applicationObj.has("BranchName") && applicationObj.get("BranchName") != null && !"".equals(applicationObj.get("BranchName").toString().trim()) && !applicationObj.get("BranchName").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setBranchName(applicationObj.get("BranchName").toString().trim());
                }
                if (applicationObj.has("BranchId") && applicationObj.get("BranchId") != null && !"".equals(applicationObj.get("BranchId").toString().trim()) && !applicationObj.get("BranchId").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setBranchId(applicationObj.get("BranchId").toString().trim());
                }
                if (applicationObj.has("APFProject") && applicationObj.get("APFProject") != null && !"".equals(applicationObj.get("APFProject").toString().trim()) && !applicationObj.get("APFProject").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setAPFProject(applicationObj.get("APFProject").toString().trim());
                }
                if (applicationObj.has("Amount") && applicationObj.get("Amount") != null && !"".equals(applicationObj.get("Amount").toString().trim()) && !applicationObj.get("Amount").toString().equalsIgnoreCase("null")) {
                    mandateTriggerDataApplication.setAmount(applicationObj.get("Amount").toString().trim());
                }
                mandateTriggerDataApplication=mandateTriggerHelper.setMandateTriggerDataApplication(mandateTriggerDataApplication);
                
                //Document Start
                if (!applicationObj.has("Documents") || applicationObj.get("Documents") == null || "".equals(applicationObj.get("Documents").toString().trim()) || applicationObj.get("Documents").toString().equalsIgnoreCase("null")) {
                    throw new CustomNonFatalException("Documents not found", "Documents not found");
                }
                documentArray = new JSONArray();
                documentArray = applicationObj.getJSONArray("Documents");
                for(int docInd=0; docInd < documentArray.length(); docInd ++){
                    isDocApproved = false;
                    documentObj = new JSONObject();
                    documentObj = documentArray.getJSONObject(docInd);
                    
                    if (!documentObj.has("LOSID") || documentObj.get("LOSID") == null || "".equals(documentObj.get("LOSID").toString().trim()) || documentObj.get("LOSID").toString().equalsIgnoreCase("null")) {
                        throw new CustomNonFatalException("LOSID of document not found", "LOSID of document not found");
                    }
                    if (!documentObj.has("DocRef") || documentObj.get("DocRef") == null || "".equals(documentObj.get("DocRef").toString().trim()) || documentObj.get("DocRef").toString().equalsIgnoreCase("null")) {
                        throw new CustomNonFatalException("DocRef not found", "DocRef not found");
                    }
                    if (!documentObj.has("DocCategoryName") || documentObj.get("DocCategoryName") == null || "".equals(documentObj.get("DocCategoryName").toString().trim()) || documentObj.get("DocCategoryName").toString().equalsIgnoreCase("null")) {
                        throw new CustomNonFatalException("DocCategoryName not found", "DocCategoryName not found");
                    }
                    mandateTriggerDataDocument = new MandateTriggerDataDocument();
                    mandateTriggerDataDocument.setLosId(documentObj.get("LOSID").toString().trim());
                    mandateTriggerDataDocument.setDocRef(documentObj.get("DocRef").toString().trim());
                    mandateTriggerDataDocument.setDocCategoryName(documentObj.get("DocCategoryName").toString().trim());
                    mandateTriggerDataDocument.setMandateTriggerDataApplication(mandateTriggerDataApplication);
                    mandateTriggerDataDocument.setCreateDate(DateUtility.getCurrentDate());
                    mandateTriggerDataDocument.setModDate(DateUtility.getCurrentDate());
                    if (documentObj.has("DocStatus") && documentObj.get("DocStatus") != null && !"".equals(documentObj.get("DocStatus").toString().trim()) && !documentObj.get("DocStatus").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataDocument.setLosDocStatus(documentObj.get("DocStatus").toString().trim());
                        if(documentObj.get("DocStatus").toString().trim().equalsIgnoreCase("Approve")){
                            isDocApproved = true;
                        }
                        if(documentObj.get("DocStatus").toString().trim().equalsIgnoreCase("FLS QUEUE")){
                            isDocFlsQ = true;
                        }
                    }
                    if (documentObj.has("DocRemark") && documentObj.get("DocRemark") != null && !"".equals(documentObj.get("DocRemark").toString().trim()) && !documentObj.get("DocRemark").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataDocument.setRemark(documentObj.get("DocRemark").toString().trim());
                    }
                    if (documentObj.has("DocName") && documentObj.get("DocName") != null && !"".equals(documentObj.get("DocName").toString().trim()) && !documentObj.get("DocName").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataDocument.setDocName(documentObj.get("DocName").toString().trim());
                    }
                    if (documentObj.has("ApplicantID") && documentObj.get("ApplicantID") != null && !"".equals(documentObj.get("ApplicantID").toString().trim()) && !documentObj.get("ApplicantID").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataDocument.setApplicantID(documentObj.get("ApplicantID").toString().trim());
                    }
                    mandateTriggerDataDocument.setQ1("YES");
                    mandateTriggerDataDocument.setQ2("YES");
                    mandateTriggerDataDocument.setQ3("YES");
                    mandateTriggerDataDocument.setQ4("YES");
                    mandateTriggerDataDocument.setQ5("YES");
                    mandateTriggerDataDocument.setQ6("YES");
                    mandateTriggerDataDocument.setQ7("YES");
                    mandateTriggerDataDocument.setQ8("YES");
                    mandateTriggerDataDocument.setQ9("YES");
                    mandateTriggerDataDocument.setQ10("YES");
                    mandateTriggerDataDocument.setQ11("YES");
                    mandateTriggerDataDocument.setQ12("YES");
                    mandateTriggerDataDocument.setQ13("YES");
                    mandateTriggerDataDocument.setQ14("YES");
                    mandateTriggerDataDocument.setQ15("YES");
                    mandateTriggerDataDocument.setQ16("YES");
                    mandateTriggerDataDocument.setQ17("YES");
                    mandateTriggerDataDocument.setQ18("YES");
                    mandateTriggerDataDocument.setQ19("YES");
                    if(isDocApproved == true){
                        mandateTriggerDataDocument.setQ20("YES");
                        mandateTriggerDataDocument.setQ21("YES");
                        mandateTriggerDataDocument.setQ22("YES");
                        mandateTriggerDataDocument.setQ23("YES");
                        mandateTriggerDataDocument.setQ24("YES");
                        mandateTriggerDataDocument.setQ25("YES");
                        mandateTriggerDataDocument.setOpsAction("Approve");
                    }
                    mandateTriggerDataDocument = mandateTriggerHelper.setMandateTriggerDataDocument(mandateTriggerDataDocument);
                }
                
                if(isDocFlsQ == true){
                    mandateTriggerDataApplication.setStatus("OPS RECHECK");
                    mandateTriggerDataApplication=mandateTriggerHelper.setMandateTriggerDataApplication(mandateTriggerDataApplication);
                }
                
                //Applicant Details Start
                if (!applicationObj.has("applicants") || applicationObj.get("applicants") == null || "".equals(applicationObj.get("applicants").toString().trim()) || applicationObj.get("applicants").toString().equalsIgnoreCase("null")) {
                    throw new CustomNonFatalException("applicants not found", "applicants not found");
                }
                applicantArray = new JSONArray();
                applicantArray = applicationObj.getJSONArray("applicants");
                for(int appId=0; appId < applicantArray.length(); appId++){
                    applicantObj = new JSONObject();
                    applicantObj = applicantArray.getJSONObject(appId);
                    mandateTriggerDataApplicantsDetails = new MandateTriggerDataApplicantsDetails();
                    mandateTriggerDataApplicantsDetails.setMandateTriggerDataApplication(mandateTriggerDataApplication);
                    mandateTriggerDataApplicantsDetails.setCreateDate(DateUtility.getCurrentDate());
                    mandateTriggerDataApplicantsDetails.setModDate(DateUtility.getCurrentDate());
                    if (applicantObj.has("Name") && applicantObj.get("Name") != null && !"".equals(applicantObj.get("Name").toString().trim()) && !applicantObj.get("Name").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataApplicantsDetails.setName(applicantObj.get("Name").toString().trim());
                    }
                    if (applicantObj.has("id") && applicantObj.get("id") != null && !"".equals(applicantObj.get("id").toString().trim()) && !applicantObj.get("id").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataApplicantsDetails.setId(applicantObj.get("id").toString().trim());
                    }
                    if (applicantObj.has("isFinancial") && applicantObj.get("isFinancial") != null && !"".equals(applicantObj.get("isFinancial").toString().trim()) && !applicantObj.get("isFinancial").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataApplicantsDetails.setIsFinancial(applicantObj.get("isFinancial").toString().trim());
                    }
                    if (applicantObj.has("isPrimary") && applicantObj.get("isPrimary") != null && !"".equals(applicantObj.get("isPrimary").toString().trim()) && !applicantObj.get("isPrimary").toString().equalsIgnoreCase("null")) {
                        mandateTriggerDataApplicantsDetails.setIsPrimary(applicantObj.get("isPrimary").toString().trim());
                    }
                    mandateTriggerDataApplicantsDetails = mandateTriggerHelper.setMandateTriggerDataApplicantsDetails(mandateTriggerDataApplicantsDetails);
                }
            }

            MandateTriggerRequestLog mandateTriggerRequestLogNew=mandateTriggerHelper.getMandateTriggerRequestLog(mandateTriggerRequestLog.getLogId());
            mandateTriggerRequestLogNew.setApplicationId(mandateTriggerDataApplication.getRefid().toString());
            mandateTriggerRequestLogNew.setResponse("Success");
            mandateTriggerRequestLogNew = mandateTriggerHelper.setMandateTriggerRequestLog(mandateTriggerRequestLogNew);
            
            mav.addObject("status", "200");
            mav.addObject("msg", "success");
            mav.addObject("refid", mandateTriggerDataApplication.getRefid());
            
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("mandateTrigger/trigger", e);
        }
        return mav;
    }
    
    
    @RequestMapping(value = "/fetchOpsPortalApplication", method = RequestMethod.POST)
    public ModelAndView fetchOpsPortalApplication(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        
        try {
            JSONObject reqJson = new JSONObject(payload);
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            
            if (!reqJson.has("refid") || reqJson.get("refid") == null || "".equals(reqJson.get("refid").toString().trim()) || reqJson.get("refid").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("refid not found", "refid not found");
            }
            Long refid=Long.parseLong(reqJson.get("refid").toString());
            MandateTriggerDataApplication mandateTriggerDataApplication=mandateTriggerHelper.getMandateTriggerDataApplicationByRefid(refid);
            if(mandateTriggerDataApplication == null){
                throw new CustomNonFatalException("application data not found", "application data not found");
            }
            
            List<MandateTriggerDataApplicantsDetails> applicantsLst = mandateTriggerHelper.getMandateTriggerDataApplicantsDetails(refid);
            if(applicantsLst == null){
                throw new CustomNonFatalException("applicants data not found", "applicants data not found");
            }
            
            List<MandateDocVo> docLst=mandateTriggerHelper.getDocument(refid);
            if(docLst == null){
                throw new CustomNonFatalException("document data not found", "document data not found");
            }
            
            mav.addObject("status", "200");
            mav.addObject("data", mandateTriggerDataApplication);
            mav.addObject("docs", docLst);
            mav.addObject("applicantsLst", applicantsLst);
//            mav.addObject("refid", mandateTriggerDataApplication.getRefid());
            
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("fetchOpsPortalApplication", e);
        }
        return mav;
    }
    
    
    @RequestMapping(value = "/saveOpsPortalApplication", method = RequestMethod.POST)
    public ModelAndView saveOpsPortalApplication(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        
        try {
            JSONObject reqJson = new JSONObject(payload);
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            
            if (!reqJson.has("refid") || reqJson.get("refid") == null || "".equals(reqJson.get("refid").toString().trim()) || reqJson.get("refid").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("refid not found", "refid not found");
            }
            if (!reqJson.has("docs") || reqJson.get("docs") == null || "".equals(reqJson.get("docs").toString().trim()) || reqJson.get("docs").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("docs not found", "docs not found");
            }
            if (!reqJson.has("isSubmit") || reqJson.get("isSubmit") == null || "".equals(reqJson.get("isSubmit").toString().trim()) || reqJson.get("isSubmit").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("isSubmit not found", "isSubmit not found");
            }
            if (!reqJson.has("opsSTatus") || reqJson.get("opsSTatus") == null || "".equals(reqJson.get("opsSTatus").toString().trim()) || reqJson.get("opsSTatus").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("opsSTatus not found", "opsSTatus not found");
            }
            
            Long refid=Long.parseLong(reqJson.get("refid").toString());
            MandateTriggerDataApplication mandateTriggerDataApplication=mandateTriggerHelper.getMandateTriggerDataApplicationByRefid(refid);
            if(mandateTriggerDataApplication == null){
                throw new CustomNonFatalException("application data not found", "application data not found");
            }
            String Remark=null;
            OpsApplicationVO opsApplicationVO = null;
            OpsDocumentVO opsDocumentVO = null;
            OpsUpdateContentVO opsUpdateContentVO = null;
            OpsUpdateReqVO opsUpdateReqVO = null;
            MandateTriggerDataDocument mandateTriggerDataDocument = null;
            
            opsUpdateReqVO = new OpsUpdateReqVO();
            opsUpdateContentVO = new OpsUpdateContentVO();
            List<OpsDocumentVO> opsDocumentVOLst = new ArrayList<OpsDocumentVO>();
            opsApplicationVO = new OpsApplicationVO();
            opsApplicationVO.setId(mandateTriggerDataApplication.getLosId());

            JSONArray documents = reqJson.getJSONArray("docs");
            JSONObject doc = null;
            for(int i=0;i<documents.length();i++){
                Remark = null;
                doc = new JSONObject();
                doc = documents.getJSONObject(i);
                opsDocumentVO = new OpsDocumentVO();
                opsDocumentVO.setId(doc.getString("losId").toString());
                opsDocumentVO.setOps_Decision__c(doc.getString("opsAction").toString());
                if(doc.has("reason") && doc.get("reason") != null && !doc.get("reason").toString().trim().equals("") && !doc.get("reason").toString().trim().equals("null")){
                    Remark=doc.getString("reason").trim();
                    if (Remark != null && Remark.length() > 0 && Remark.charAt(Remark.length() - 1) == ',') {
                        Remark = Remark.substring(0, Remark.length() - 1);
                        opsDocumentVO.setOps_Remarks__c(Remark);
                    }
                }
                opsDocumentVOLst.add(opsDocumentVO);
                
                mandateTriggerDataDocument=new MandateTriggerDataDocument();
                mandateTriggerDataDocument = mandateTriggerHelper.getMandateTriggerDataDocumentById(Long.parseLong(doc.get("documentId").toString().trim()), refid);
                if(mandateTriggerDataDocument != null){
                    mandateTriggerDataDocument.setOpsAction(doc.getString("opsAction").trim());
                    mandateTriggerDataDocument.setQ1(doc.getString("q1").trim());
                    mandateTriggerDataDocument.setQ2(doc.getString("q2").trim());
                    mandateTriggerDataDocument.setQ3(doc.getString("q3").trim());
                    mandateTriggerDataDocument.setQ4(doc.getString("q4").trim());
                    mandateTriggerDataDocument.setQ5(doc.getString("q5").trim());
                    mandateTriggerDataDocument.setQ6(doc.getString("q6").trim());
                    mandateTriggerDataDocument.setQ7(doc.getString("q7").trim());
                    mandateTriggerDataDocument.setQ8(doc.getString("q8").trim());
                    mandateTriggerDataDocument.setQ9(doc.getString("q9").trim());
                    mandateTriggerDataDocument.setQ10(doc.getString("q10").trim());
                    mandateTriggerDataDocument.setQ11(doc.getString("q11").trim());
                    mandateTriggerDataDocument.setQ12(doc.getString("q12").trim());
                    mandateTriggerDataDocument.setQ13(doc.getString("q13").trim());
                    mandateTriggerDataDocument.setQ14(doc.getString("q14").trim());
                    mandateTriggerDataDocument.setQ15(doc.getString("q15").trim());
                    mandateTriggerDataDocument.setQ16(doc.getString("q16").trim());
                    mandateTriggerDataDocument.setQ17(doc.getString("q17").trim());
                    mandateTriggerDataDocument.setQ18(doc.getString("q18").trim());
                    mandateTriggerDataDocument.setQ19(doc.getString("q19").trim());
                    if(doc.has("q20") && doc.get("q20") != null && !doc.get("q20").toString().trim().equals("") && !doc.get("q20").toString().trim().equals("null")){
                        mandateTriggerDataDocument.setQ20(doc.getString("q20").trim());
                    }
                    if(doc.has("q21") && doc.get("q21") != null && !doc.get("q21").toString().trim().equals("") && !doc.get("q21").toString().trim().equals("null")){
                        mandateTriggerDataDocument.setQ21(doc.getString("q21").trim());
                    }
                    if(doc.has("q22") && doc.get("q22") != null && !doc.get("q22").toString().trim().equals("") && !doc.get("q22").toString().trim().equals("null")){
                        mandateTriggerDataDocument.setQ22(doc.getString("q22").trim());
                    }
                    if(doc.has("q23") && doc.get("q23") != null && !doc.get("q23").toString().trim().equals("") && !doc.get("q23").toString().trim().equals("null")){
                        mandateTriggerDataDocument.setQ23(doc.getString("q23").trim());
                    }
                    if(doc.has("q24") && doc.get("q24") != null && !doc.get("q24").toString().trim().equals("") && !doc.get("q24").toString().trim().equals("null")){
                        mandateTriggerDataDocument.setQ24(doc.getString("q24").trim());
                    }
                    if(doc.has("q25") && doc.get("q25") != null && !doc.get("q25").toString().trim().equals("") && !doc.get("q25").toString().trim().equals("null")){
                        mandateTriggerDataDocument.setQ25(doc.getString("q25").trim());
                    }
                    mandateTriggerDataDocument.setReason(Remark);
                    mandateTriggerDataDocument = mandateTriggerHelper.setMandateTriggerDataDocument(mandateTriggerDataDocument);
                }
            }
            
            opsUpdateContentVO.setOpsApplicationVO(opsApplicationVO);
            opsUpdateContentVO.setOpsDocumentVO(opsDocumentVOLst);
            
            opsUpdateReqVO.setOpsUpdateContentVO(opsUpdateContentVO);
            
            if (reqJson.get("isSubmit").toString().equalsIgnoreCase("true")) {
                String op = mandateTriggerHelper.updateOPS(opsUpdateReqVO);
                JSONObject opsResponse = new JSONObject(op);
                if (opsResponse.has("status") && opsResponse.get("status") != null && opsResponse.get("status").toString().equalsIgnoreCase("SUCCESS")) {
                    mandateTriggerDataApplication.setStatus(reqJson.get("opsSTatus").toString().trim());
                    mandateTriggerDataApplication.setModDate(DateUtility.getCurrentDate());
                    mandateTriggerDataApplication.setSubmitDate(DateUtility.getCurrentDate());
                    mandateTriggerDataApplication=mandateTriggerHelper.setMandateTriggerDataApplication(mandateTriggerDataApplication);
                    mav.addObject("status", "200");
                    mav.addObject("data", "Success");
                } else if (opsResponse.has("errorMessage") && opsResponse.get("errorMessage") != null) {
                    mav.addObject("status", "400");
                    mav.addObject("msg", opsResponse.get("errorMessage").toString());
                } else {
                    mav.addObject("status", "400");
                    mav.addObject("msg", "Error While Requesting");
                }
            } else {
                    mav.addObject("status", "200");
                    mav.addObject("data", "Success");
            }
            
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("fetchOpsPortalApplication", e);
        }
        return mav;
    }
    
//    public boolean setMandateTriggerTokenRequestLog(String payload){
//            
//        MandateTriggerTokenRequestLog mandateTriggerTokenRequestLog=new MandateTriggerTokenRequestLog();
//        mandateTriggerTokenRequestLog.setRequest(payload);
//        mandateTriggerTokenRequestLog.setCreateDate(DateUtility.getCurrentDate());
//        mandateTriggerTokenRequestLog.setModDate(DateUtility.getCurrentDate());
//        mandateTriggerHelper.setMandateTriggerTokenRequestLog(mandateTriggerTokenRequestLog);
//        return true;
//    }
    
    
    @RequestMapping(value = "/opsCheckSetLock", method = RequestMethod.POST)
    public ModelAndView opsCheckSetLock(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        
        try {
            JSONObject reqJson = new JSONObject(payload);
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            
            if (!reqJson.has("refid") || reqJson.get("refid") == null || "".equals(reqJson.get("refid").toString().trim()) || reqJson.get("refid").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("refid not found", "refid not found");
            }
            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim()) || reqJson.get("userId").toString().equalsIgnoreCase("null")) {
                throw new CustomNonFatalException("userId not found", "userId not found");
            }
            
            Long refid=Long.parseLong(reqJson.get("refid").toString());
            Long userId=Long.parseLong(reqJson.get("userId").toString());
            
            Boolean valid=mandateTriggerHelper.opsCheckSetLock(refid, userId.toString());
            
            mav.addObject("status", "200");
            mav.addObject("data", valid);
            
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("fetchOpsPortalApplication", e);
        }
        return mav;
    }
}
