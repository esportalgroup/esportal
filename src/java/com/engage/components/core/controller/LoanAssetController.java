/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsInvoiceDocBean;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import com.engage.components.core.entity.bean.FrameworkMstrLogin;
import com.engage.components.core.helper.LoanAssetHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.DealearApproveRejReq;
import com.engage.components.esportal.vo.DealerApproveRejVO;
import com.engage.components.esportal.vo.DealerPendingData;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Controller
@Component
public class LoanAssetController {

    @Autowired
    LoanAssetHelper assetHelper;

    @RequestMapping(value = "/loanAssetDetail", method = RequestMethod.POST)
    public ModelAndView getLoanAssetDetails(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        DealerPendingData pendingData = null;

        try {

//            System.out.println(":::::::::::::loanAssetDetail ::::::::::::");
            if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("applicationId not entered", "applicationId not entered");
            }

            String applicationId = reqJson.get("applicationId").toString();

//            System.out.println("loanAssetDetail ::applicationId"+applicationId);
            EsApplicantsDetailBean applicantsDetailBean = assetHelper.getApplicantsDetailsByAppId(applicationId);

            if (applicantsDetailBean != null) {

                pendingData = new DealerPendingData();
                pendingData.setApplication_ID(applicantsDetailBean.getApplicationId());
                pendingData.setApplicant_Name(applicantsDetailBean.getApplicantName());
                pendingData.setApplication_Date(applicantsDetailBean.getApplicationDate());
                pendingData.setCo_Applicant_Name(applicantsDetailBean.getCoapplicantName());
                pendingData.setDSA_ID(applicantsDetailBean.getDsaId());
                pendingData.setDSA_Name(applicantsDetailBean.getDsaName());
                pendingData.setId(applicantsDetailBean.getId());
                pendingData.setFLS_ID(applicantsDetailBean.getFlsId());
                pendingData.setFLS_Name(applicantsDetailBean.getFlsName());

                pendingData.setSupplier_ID(applicantsDetailBean.getSupplierId());
                pendingData.setSupplier_Name(applicantsDetailBean.getSupplierName());
                pendingData.setStatus(applicantsDetailBean.getStatus());

                EsAssetDetailBean assetDetailBean = assetHelper.getAssetDetailByAppId(applicationId);

                if (assetDetailBean != null) {
                    assetDetailBean.getAssetCost();
                    pendingData.setAssetCost_ESR(assetDetailBean.getAssetCostESR());

                    pendingData.setAssetCost_Ins(applicationId);
                    assetDetailBean.getAssetCostIns();
                    pendingData.setAssetCost_OnboardingPrice(applicationId);
                    assetDetailBean.getAssetCostOnboardingPrice();
                    pendingData.setAssetCost_Tax(assetDetailBean.getAssetCostTax());
                    pendingData.setAsset_Make(assetDetailBean.getAssetMake());
                    pendingData.setAsset_Manufacturer(assetDetailBean.getAssetManufacturer());
                    pendingData.setAssetModel(assetDetailBean.getAssetModel());
//                    pendingData.setChassisNumber(assetDetailBean.getChassisNumber());
//                    pendingData.setEngineNumber(assetDetailBean.getEngineNumber());

                    EsLoanDetailBean loanDetailBean = assetHelper.getLoanDetailByAppId(applicationId);

                    if (loanDetailBean != null) {

                        pendingData.setCLI_Premium(loanDetailBean.getCliPremium());
                        pendingData.setDoc_Charges(loanDetailBean.getDocCharges());
                        pendingData.setECS_charges(loanDetailBean.getECSCharges());
                        pendingData.setInvoiceNumber(loanDetailBean.getInvoiceNumber());

                        pendingData.setInvoice_DocID(loanDetailBean.getInvoice_DocID());
                        pendingData.setLoan_Amount(loanDetailBean.getLoanAmount());
                        pendingData.setMargin_Money(loanDetailBean.getMarginMoney());
                        pendingData.setMarginMoney_DocID(loanDetailBean.getMarginMoney_DocID());
                        pendingData.setProcessing_Fees(loanDetailBean.getProcessingFee());
                        pendingData.setStampDuty(loanDetailBean.getStampDuty());
                        mav.addObject("status", "200");
                        mav.addObject("loanDetail", loanDetailBean);
                    } else {
                        mav.addObject("status", "400");
                        mav.addObject("msg", "loan Detail Not Found.");
                    }
                } else {
                    mav.addObject("status", "400");
                    mav.addObject("msg", "asset Detail Not Found.");
                }

            } else {
                pendingData = assetHelper.getPendingDataFromSession(applicationId, session);

//                mav.addObject("msg", "applicant Detail Not Found.");
            }
            mav.addObject("pendingData", pendingData);
            mav.addObject("status", "200");

        } catch (CustomNonFatalException cnfe) {
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("loanAssetDetail", cnfe);
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("loanAssetDetail", e);
        }
        return mav;
    }

    @RequestMapping(value = "/approveOrRejectDetails", method = RequestMethod.POST)
    public ModelAndView approveOrRejectPendindDetails(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        String response = "";
        EsAssetDetailBean assetDetails = null;
        EsApplicantsDetailBean applicantDetails = null;
        EsLoanDetailBean loanDetails = null;
        EsInvoiceDocBean invoiceDoc = null;
        JSONObject dataObj = null;
        try {
            if (!reqJson.has("applicantDetail") || reqJson.get("applicantDetail") == null || "".equals(reqJson.get("applicantDetail").toString().trim())) {
                throw new CustomNonFatalException("applicantDetail not null", "applicantDetail not null");
            }

            System.out.println("applicantDetail" + reqJson.get("applicantDetail").toString());

            dataObj = (JSONObject) reqJson.get("applicantDetail");
            String status = reqJson.get("actionType").toString();
            String applicationId = dataObj.getString("application_ID");
            System.out.println("status :::" + status + "applicationId" + applicationId);
            System.out.println("dataObj" + dataObj);

            DealerApproveRejVO delReq = new DealerApproveRejVO();
            DealearApproveRejReq data = new DealearApproveRejReq();
            data.setId(dataObj.get("id").toString());
            data.setDealer_Portal_Status__c(status);
            data.setEngine_number__c(dataObj.getString("engineNumber"));
            data.setChassis_number__c(dataObj.getString("chassisNumber"));
            data.setInvoice_number__c(dataObj.getString("invoiceNumber"));
            delReq.setContent(data);
            response = assetHelper.approveAndRejectDEtailsFromWS(delReq);

            if (response.equalsIgnoreCase("success")) {
                response = "";
                assetDetails = assetHelper.getAssetDetailByAppId(applicationId);
                if (assetDetails != null) {
                    assetDetails.setApplicationId(dataObj.getString("application_ID"));
//           assetDetails.setAssetCost(dataObj.getString(status));
                    assetDetails.setAssetMake(dataObj.getString("asset_Make"));
                    assetDetails.setAssetManufacturer(dataObj.getString("asset_Manufacturer"));
                    assetDetails.setAssetModel(dataObj.getString("assetModel"));
                    assetDetails.setAssetCostOnboardingPrice(dataObj.getString("assetCost_OnboardingPrice"));
                    assetDetails.setAssetCostTax(dataObj.getString("assetCost_Tax"));
                    assetDetails.setAssetCostESR(dataObj.getString("assetCost_ESR"));
                    assetDetails.setAssetCostIns(dataObj.getString("assetCost_Ins"));
//                assetDetails.setEngineNumber(dataObj.getString("engineNumber"));
//                assetDetails.setChassisNumber(dataObj.getString("chassisNumber"));
                    assetDetails.setCreDate(DateUtility.getCurrentDate());

                } else {
                    assetDetails = new EsAssetDetailBean();
                    assetDetails.setApplicationId(dataObj.getString("application_ID"));
//           assetDetails.setAssetCost(dataObj.getString(status));
                    assetDetails.setAssetMake(dataObj.getString("asset_Make"));
                    assetDetails.setAssetManufacturer(dataObj.getString("asset_Manufacturer"));
                    assetDetails.setAssetModel(dataObj.getString("assetModel"));
                    assetDetails.setAssetCostOnboardingPrice(dataObj.getString("assetCost_OnboardingPrice"));
                    assetDetails.setAssetCostTax(dataObj.getString("assetCost_Tax"));
                    assetDetails.setAssetCostESR(dataObj.getString("assetCost_ESR"));
                    assetDetails.setAssetCostIns(dataObj.getString("assetCost_Ins"));
//                assetDetails.setEngineNumber(dataObj.getString("engineNumber"));
//                assetDetails.setChassisNumber(dataObj.getString("chassisNumber"));
                    assetDetails.setCreDate(DateUtility.getCurrentDate());

                }
            }

            response = assetHelper.setAssetDetails(assetDetails);

            applicantDetails = assetHelper.getApplicantsDetailsByAppId(applicationId);

            if (applicantDetails != null) {
                applicantDetails.setApplicationId(dataObj.getString("application_ID"));
                applicantDetails.setApplicantName(dataObj.getString("applicant_Name"));
                applicantDetails.setSupplierId(dataObj.getString("supplier_ID"));
                applicantDetails.setSupplierName(dataObj.getString("supplier_Name"));
                applicantDetails.setDsaId(dataObj.getString("dsa_ID"));
                applicantDetails.setDsaName(dataObj.getString("dsa_Name"));
                applicantDetails.setFlsId(dataObj.getString("fls_ID"));
                applicantDetails.setFlsName(dataObj.getString("fls_Name"));
                applicantDetails.setCoapplicantName(dataObj.getString("co_Applicant_Name"));
                applicantDetails.setId(dataObj.get("id").toString());
                applicantDetails.setCreDate(DateUtility.getCurrentDate());
                applicantDetails.setStatus(status);
                applicantDetails.setApplicationDate(dataObj.getString("application_Date"));
            } else {
                applicantDetails = new EsApplicantsDetailBean();
                applicantDetails.setApplicationId(dataObj.getString("application_ID"));
                applicantDetails.setApplicantName(dataObj.getString("applicant_Name"));
                applicantDetails.setSupplierId(dataObj.getString("supplier_ID"));
                applicantDetails.setSupplierName(dataObj.getString("supplier_Name"));
                applicantDetails.setDsaId(dataObj.getString("dsa_ID"));
                applicantDetails.setId(dataObj.get("id").toString());
                applicantDetails.setDsaName(dataObj.getString("dsa_Name"));
                applicantDetails.setFlsId(dataObj.getString("fls_ID"));
                applicantDetails.setFlsName(dataObj.getString("fls_Name"));
                applicantDetails.setCoapplicantName(dataObj.getString("co_Applicant_Name"));
                applicantDetails.setCreDate(DateUtility.getCurrentDate());
                applicantDetails.setApplicationDate(dataObj.getString("application_Date"));

                applicantDetails.setStatus(status);
            }

            if (response.equalsIgnoreCase("success")) {
                response = "";
                response = assetHelper.setApplicantDetails(applicantDetails);

            }

            loanDetails = assetHelper.getLoanDetailByAppId(applicationId);
            if (loanDetails != null) {
                loanDetails.setApplicationId(dataObj.getString("application_ID"));
                loanDetails.setMarginMoney(dataObj.getString("margin_Money"));
                loanDetails.setProcessingFee(dataObj.getString("processing_Fees"));
                loanDetails.setECSCharges(dataObj.getString("ecs_charges"));
                loanDetails.setDocCharges(dataObj.getString("doc_Charges"));
                loanDetails.setStampDuty(dataObj.getString("stampDuty"));
                loanDetails.setCliPremium(dataObj.getString("cli_Premium"));
//                loanDetails.setApplicationDate(dataObj.getString("application_Date"));
                loanDetails.setLoanAmount(dataObj.getString("loan_Amount"));
//           loanDetails.setDisbuursemenrAmount(dataObj.getString(""));
                loanDetails.setInvoiceNumber(dataObj.getString("invoiceNumber"));
                loanDetails.setInvoice_DocID(dataObj.getString("invoice_DocID"));
                loanDetails.setMarginMoney_DocID(dataObj.getString("marginMoney_DocID"));

            } else {

                loanDetails = new EsLoanDetailBean();
                loanDetails.setApplicationId(dataObj.getString("application_ID"));
                loanDetails.setMarginMoney(dataObj.getString("margin_Money"));
                loanDetails.setProcessingFee(dataObj.getString("processing_Fees"));
                loanDetails.setECSCharges(dataObj.getString("ecs_charges"));
                loanDetails.setDocCharges(dataObj.getString("doc_Charges"));
                loanDetails.setStampDuty(dataObj.getString("stampDuty"));
                loanDetails.setCliPremium(dataObj.getString("cli_Premium"));
//                loanDetails.setApplicationDate(dataObj.getString("application_Date"));
                loanDetails.setLoanAmount(dataObj.getString("loan_Amount"));
//           loanDetails.setDisbuursemenrAmount(dataObj.getString(""));
                loanDetails.setInvoiceNumber(dataObj.getString("invoiceNumber"));
                loanDetails.setInvoice_DocID(dataObj.getString("invoice_DocID"));
                loanDetails.setMarginMoney_DocID(dataObj.getString("marginMoney_DocID"));

            }

            if (response.equalsIgnoreCase("success")) {
                response = "";
                response = assetHelper.setLoanDetails(loanDetails);

            }

            invoiceDoc = assetHelper.getInvoiceDocDetailsByAppId(applicationId);
            if (invoiceDoc != null) {
                invoiceDoc.setApplicationId(applicationId);
                invoiceDoc.setEngineNumber(dataObj.getString("engineNumber"));
                invoiceDoc.setChassisNumber(dataObj.getString("chassisNumber"));
                invoiceDoc.setInvoiceNumber(dataObj.getString("invoiceNumber"));
//                invoiceDoc.setInvoiceDate(DateUtility.parseStringToDate(dataObj.getString("invoiceDate")));
                invoiceDoc.setCreDate(DateUtility.getCurrentDate());
                invoiceDoc.setModDate(DateUtility.getCurrentDate());

            } else {
                invoiceDoc = new EsInvoiceDocBean();
                invoiceDoc.setApplicationId(applicationId);
                invoiceDoc.setEngineNumber(dataObj.getString("engineNumber"));
                invoiceDoc.setChassisNumber(dataObj.getString("chassisNumber"));
                invoiceDoc.setInvoiceNumber(dataObj.getString("invoiceNumber"));
//                invoiceDoc.setInvoiceDate(DateUtility.parseStringToDate(dataObj.getString("invoiceDate")));
                invoiceDoc.setCreDate(DateUtility.getCurrentDate());
                invoiceDoc.setModDate(DateUtility.getCurrentDate());

            }

            if (response.equalsIgnoreCase("success")) {
                response = "";
                response = assetHelper.setInvoiceDocDetails(invoiceDoc);

            }

            if (response.equalsIgnoreCase("success")) {
                mav.addObject("status", "200");
                mav.addObject("msg", response);
            } else {
                mav.addObject("status", "400");
                mav.addObject("msg", "error while sending data to the web service");
            }

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("approveOrRejectPendindDetails", e);

        }
        return mav;
    }

}
