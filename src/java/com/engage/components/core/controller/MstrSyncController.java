/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.components.core.helper.MsterSyncHelper;
import com.engage.framework.util.log4j.CustomLogger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Sagar
 */

@Controller
@Component
public class MstrSyncController {
    
    
   @Autowired
   MsterSyncHelper mstrSyncHelper;
   
   
    @RequestMapping(value = "/dealer/gettoken", method = RequestMethod.GET)
    public ModelAndView getWSToken()
    {
            ModelAndView mav = new ModelAndView("home");
            String token=null;
            
            try
            {
               token=mstrSyncHelper.getAuthToken();
               mav.addObject("token",token);
               mav.addObject("status","200");

            }catch(Exception e)
            {
               mav.addObject("message",e.getMessage());
               mav.addObject("status","400");
               CustomLogger.error("gettoken", e);
            }
            return mav;

    }
    
    
    @RequestMapping(value = "/dealer/syncmstr", method = RequestMethod.POST)
    public ModelAndView syncMaster(@RequestBody String payload)
    {
        ModelAndView mav = new ModelAndView("home");
        String response=null;
        String token=null;
        try
        {
            JSONObject obj=new JSONObject(payload);
            token=obj.get("token").toString();
            System.out.println("token :::::"+token);
            response=mstrSyncHelper.synsMstrs(token);
        }
        catch(Exception e)
        {
        }
        return mav;
    }
}
