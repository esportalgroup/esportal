/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.McmApplicationDetails;
import com.engage.components.core.entity.bean.McmDisbursementDetails;
import com.engage.components.core.entity.bean.McmGroupDetail;
import com.engage.components.core.entity.bean.McmMpngDisbursementAccount;
import com.engage.components.core.helper.DashboardHelper;
import com.engage.components.core.helper.MCMPostDisbursementHelper;
import com.engage.components.core.helper.McmTriggerHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.mcm.vo.mcmDisbursementDocDetailVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupDetails;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupDetailsDocDisbursement;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupDocDisbursementVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupFormationDocDisbursementVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupFormationVO;
import com.engage.components.esportal.mcm.vo.mcmRequestGroupVO;
import com.engage.components.esportal.vo.McmDashBoardVO;
import com.engage.components.esportal.vo.McmGroupDetailVO;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ven00253
 */

@Controller
@Component
public class MCMPostDisbursementCtrl 
{
        
    @Autowired
    MCMPostDisbursementHelper disbursementHelper;
    
    @Autowired
    McmTriggerHelper mcmTriggerHelper;
    
    @RequestMapping(value = "/mcmDisbursementDocDetail" , method = RequestMethod.POST)
    public ModelAndView getMcmDisbursementDocDetail(@RequestBody String payload) throws JSONException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try { 

            if (!reqJson.has("grpId") || reqJson.get("grpId") == null || "".equals(reqJson.get("grpId").toString().trim())) {
                throw new CustomNonFatalException("grpId not entered", "grpId not entered");
            }
            if (!reqJson.has("mcCode") || reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException("mcCode not entered", "mcCode not entered");
            }

            Long grpId = Long.parseLong(reqJson.get("grpId").toString());
            String mcCode = reqJson.get("mcCode").toString();
            String groupId = reqJson.get("groupId").toString();

            //System.out.println("grpId" + grpId + "::::::::groupId>>>>>" + groupId);
            McmGroupDetailVO groupDetail = mcmTriggerHelper.getMcmGroupDetail(groupId);

            mav.addObject("status", "200");
            mav.addObject("groupDetails", groupDetail);

        } catch (CustomNonFatalException cnfe) {

            cnfe.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", cnfe.getMessage());
            CustomLogger.error("getMcmGroupDetail", cnfe);
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("getMcmGroupDetail", e);
        }
        return mav;
    }
    
    
    @RequestMapping(value = "/initiateDOC", method = RequestMethod.POST)
    public ModelAndView initiateDOC(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {

        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);
        McmApplicationDetails applicationdetails=null;
        try {
            
            String mcCode = reqJson.get("mcCode").toString();

            JSONObject dataobj = (JSONObject) reqJson.get("groupDetails");
			
            mcmRequestGroupFormationDocDisbursementVO groupFormationVo = new mcmRequestGroupFormationDocDisbursementVO();
            
            mcmRequestGroupDocDisbursementVO groupVo = new mcmRequestGroupDocDisbursementVO();
            
            mcmRequestGroupDetailsDocDisbursement groupDetails = new mcmRequestGroupDetailsDocDisbursement();
			
            List<mcmRequestGroupDetailsDocDisbursement> groupVoLst = new ArrayList<mcmRequestGroupDetailsDocDisbursement>();
            String groupId = dataobj.get("groupId").toString();
			
            McmGroupDetail mcmGroupDetailData = mcmTriggerHelper.getMcmGroupDetails(groupId);
			
           
            groupDetails.setGroupId(groupId);
            groupDetails.setVillageName(mcmGroupDetailData.getVillageName());
            groupDetails.setCity("");//mcmGroupDetailData.getCityId()
            groupDetails.setFLSCode(mcmGroupDetailData.getFloId());
            groupDetails.setMCCode(mcmGroupDetailData.getMcCode());

            groupDetails.setMCMAccountNumber(mcmGroupDetailData.getMcmAccNo());

            groupDetails.setMCMBank_branch_name("");
            groupDetails.setMCMBank_name(mcmGroupDetailData.getMcmAccBankName());
            groupDetails.setCollectionDate(mcmGroupDetailData.getCollectionDate());
            groupDetails.setCollectionTime(mcmGroupDetailData.getCollectionTime());
            groupDetails.setMCMIFSC_code("");

            mcmDisbursementDocDetailVO applicationVo = null;

            List<mcmDisbursementDocDetailVO> applicationReqLst = new ArrayList<mcmDisbursementDocDetailVO>();

            JSONArray memberArray = dataobj.getJSONArray("lstGroupMember");

            //System.out.println("memberArray >>>>>>>>"+memberArray);
            for (int j = 0; j < memberArray.length(); j++) {
                JSONObject memberObj = (JSONObject) memberArray.get(j);

                //System.out.println("memberObj"+memberObj.get("applicationId").toString());
                applicationVo = new mcmDisbursementDocDetailVO();

                applicationVo.setApplicationId(memberObj.get("applicationId").toString());
                applicationVo.setAccountNumber(memberObj.get("bankAccountNumber").toString());
                applicationVo.setBank_branch_name("");
                applicationVo.setBank_name("");
                applicationVo.setIFSC_code(memberObj.get("ifscCode").toString());
                applicationVo.setLoanAmount(memberObj.get("loanAmount").toString());
                applicationVo.setModeofDiscursement(memberObj.get("modeOfDisbursement").toString());
                applicationVo.setMemberPresent(memberObj.get("MemberPresent").toString());
				
                applicationdetails=mcmTriggerHelper.getMcmApplicationByGroupId(groupId, memberObj.get("applicationId").toString());
               
                applicationdetails.setApplicationId(memberObj.get("applicationId").toString());
                applicationdetails.setIfscCode(memberObj.get("ifscCode").toString());
                applicationdetails.setBankAccountNumber(memberObj.get("bankAccountNumber").toString());
                applicationdetails.setModeOfDisbursement(memberObj.get("modeOfDisbursement").toString());
//                applicationdetails.setStatus(FrameworkAppConstants.INITIATE_DISBURSEMENT_SUCCESS);
                mcmTriggerHelper.updateMCMApplicationDetails(applicationdetails);

                System.out.println("LOS ID"+applicationdetails.getLosId());
                applicationVo.setLOSId(applicationdetails.getLosId()); 
                applicationReqLst.add(applicationVo);

            }

            groupDetails.setApplication(applicationReqLst);
            groupVoLst.add(groupDetails);
            groupVo.setGroupLst(groupVoLst);
            groupFormationVo.setContents(groupVo);

            String response = disbursementHelper.callGroupFormationService(groupFormationVo);
            
            

            if(response.equalsIgnoreCase("success"))
            {
                mcmGroupDetailData.setStatus(FrameworkAppConstants.INI_DOC_GEN_SUCCESS);
                
            }else
            {
                mcmGroupDetailData.setStatus(FrameworkAppConstants.INITIATE_DISBURSEMENT_SUCCESS);
            }
       
            mcmTriggerHelper.setMcmGroupDetail(mcmGroupDetailData);
            
           

            //System.out.println("response ::::::::" + response);
            if (response.equalsIgnoreCase("success")) {
                mav.addObject("status", "200");
                mav.addObject("message", "success");

            } else {
                mav.addObject("status", "400");
                mav.addObject("message", "fails");

            }

        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("message", e.getMessage());

        }

        return mav;
    }
    
   
    @RequestMapping(value = "/initiateEsign", method = RequestMethod.POST)
    public ModelAndView initiateEsign(@RequestBody String payload)
    {
          ModelAndView mav = new ModelAndView("home");
          System.out.println("initiateEsign >>>>>>>");
          
           try {
            JSONObject reqJson = new JSONObject(payload);

            if (reqJson.get("application") == null || "".equals(reqJson.get("application").toString().trim())) {
                throw new CustomNonFatalException("Invalid application", "Invalid application");
            }
              if (reqJson.get("mcCode") == null || "".equals(reqJson.get("mcCode").toString().trim())) {
                throw new CustomNonFatalException("Invalid mcCode", "Invalid mcCode");
            }
              if (reqJson.get("groupId") == null || "".equals(reqJson.get("groupId").toString().trim())) {
                throw new CustomNonFatalException("Invalid groupId", "Invalid groupId");
            }

          
            String mcCode=reqJson.get("mcCode").toString();
            String groupId=reqJson.get("groupId").toString();
            
            
            JSONObject dataObj=(JSONObject) reqJson.get("application");
            
            System.out.println("data >>>>>>>>"+dataObj);
            
            McmGroupDetail mcmGroupDetailData = mcmTriggerHelper.getMcmGroupDetails(groupId);
            
            McmApplicationDetails applicationdetails=mcmTriggerHelper.getMcmApplicationByGroupId(groupId, dataObj.get("applicationId").toString());
            
         
            applicationdetails.setApplicationId(dataObj.get("applicationId").toString());
            applicationdetails.setGroupId(groupId);
            applicationdetails.setAdharNumber(dataObj.get("adharNumber").toString());
           
            applicationdetails.setIsEsigned(FrameworkAppConstants.IS_ESIGNED_SUCCESS);
            
            mcmTriggerHelper.updateMCMApplicationDetails(applicationdetails);
          
            
            List<McmApplicationDetails> applicationsLst= mcmTriggerHelper.getApplicationsByGroupId(groupId);
            
            if(applicationsLst !=null && applicationsLst.size()==0)
            {
                 System.out.println("inside if loop <<<<<< >>>>>>>>"+applicationsLst.size());
                   mcmGroupDetailData.setStatus(FrameworkAppConstants.ESIGN_SUCESS);
                   mcmGroupDetailData.setGroupId(groupId);
                 mcmTriggerHelper.updateMCMGroupDetail(mcmGroupDetailData);
            }
               
            
            mav.addObject("status", "200");
            mav.addObject("message", "success");
        } catch (Exception e) {
            e.printStackTrace();
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("initiateEsign", e);
        }
          return mav;
    
    }
    

}
