/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.components.core.entity.bean.ESDealerProfile;
import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import com.engage.components.core.helper.MsterSyncHelper;
import com.engage.components.core.helper.ProfileDetailsHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.ProfileDetailsVO;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shrikant Katakdhond
 */
@Component
@Controller
public class ProfileDetailsController {

    @Autowired
    MsterSyncHelper mstrSyncHelper;

    @Autowired
    ProfileDetailsHelper detailsHelper;

    @RequestMapping(value = "/profileDetail", method = RequestMethod.POST)
    public ModelAndView forgot(@RequestBody String payload, HttpSession session, HttpServletRequest req) throws IOException, JSONException, CustomNonFatalException, SQLException {
        ModelAndView mav = new ModelAndView("home");
        JSONObject reqJson = new JSONObject(payload);

        try {

            if (!reqJson.has("userId") || reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("User ID not entered", "User ID not entered");
            }

            if (!reqJson.has("userName") || reqJson.get("userName") == null || "".equals(reqJson.get("userName").toString().trim())) {
                throw new CustomNonFatalException("User Name not entered", "User Name not entered");
            }

            if (!reqJson.has("userRole") || reqJson.get("userRole") == null || "".equals(reqJson.get("userRole").toString().trim())) {
                throw new CustomNonFatalException("User Role not entered", "User Role not entered");
            }

            String userId = reqJson.get("userId").toString();
            String userName = reqJson.get("userName").toString();
            String role = reqJson.get("userRole").toString();

//             mstrSyncHelper.getSyncProfileDetailLogin(userName,role,req);
//            ProfileDetailsVO detailsVO = (ProfileDetailsVO) req.getSession().getAttribute("profileDetails");
            ESDealerProfile dealerProfile = detailsHelper.getDealerProfileBYDealerId(userName);
            if (dealerProfile != null) {
                mav.addObject("status", "200");
                mav.addObject("profileDetails", dealerProfile);
            } else {
                dealerProfile = new ESDealerProfile();
                mav.addObject("status", "200");
                mav.addObject("profileDetails", dealerProfile);
            }

        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("profileDetail", e);
        }
        return mav;
    }
}
