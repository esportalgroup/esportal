/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import static com.cmss.engage.framework.util.AESAlgoClass.decrypt;
import com.cmss.engage.framework.util.ApplicationPropertyManager;
import com.cmss.engage.framework.util.DateUtility;
import com.cmss.engage.framework.util.FrameworkAppConstants;
import com.engage.components.core.entity.bean.RcuTriggerApplicant;
import com.engage.components.core.entity.bean.RcuTriggerDocument;
import com.engage.components.core.entity.bean.RcuTriggerLog;
import com.engage.components.core.entity.bean.RcuTriggerStatus;
import com.engage.components.core.helper.RCUTriggerHelper;
import org.springframework.http.ResponseEntity;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import com.engage.components.esportal.vo.RCUTriggerApplicantVo;
import com.engage.components.esportal.vo.RCUTriggerDocumentVo;
import com.engage.components.esportal.vo.RCUTriggerVo;
import com.engage.components.esportal.vo.LosRequestVO;
import com.engage.framework.util.log4j.CustomLogger;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author SurajK
 */
@Component
@Controller
public class RCUTriggerController {

    @Autowired
    RCUTriggerHelper rCUTriggerHelper;

    @RequestMapping(value = "/SangamRCUTrigger/RCUTriggerAdd", method = RequestMethod.POST)
    public ModelAndView addRcuTrigger(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        RcuTriggerLog sangamRcuTriggerLogId = null;
        try {
            JSONObject reqJson = new JSONObject(payload);

            if (!reqJson.has("applicationId") || reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("applicationId not found", "applicationId not found");
            }
            if (!reqJson.has("flsName") || reqJson.get("flsName") == null || "".equals(reqJson.get("flsName").toString().trim())) {
                throw new CustomNonFatalException("flsName not found", "flsName not found");
            }
            if (!reqJson.has("flsId") || reqJson.get("flsId") == null || "".equals(reqJson.get("flsId").toString().trim())) {
                throw new CustomNonFatalException("flsId not found", "flsId not found");
            }
            if (!reqJson.has("rcuVendorName") || reqJson.get("rcuVendorName") == null || "".equals(reqJson.get("rcuVendorName").toString().trim())) {
                throw new CustomNonFatalException("rcuVendorName not found", "rcuVendorName not found");
            }
            if (!reqJson.has("rcuVendorId") || reqJson.get("rcuVendorId") == null || "".equals(reqJson.get("rcuVendorId").toString().trim())) {
                throw new CustomNonFatalException("rcuVendorId not found", "rcuVendorId not found");
            }
            if (!reqJson.has("city") || reqJson.get("city") == null || "".equals(reqJson.get("city").toString().trim())) {
                throw new CustomNonFatalException("ProfileName not found", "ProfileName not found");
            }
            if (!reqJson.has("product") || reqJson.get("product") == null || "".equals(reqJson.get("product").toString().trim())) {
                throw new CustomNonFatalException("product not found", "product not found");
            }
            if (!reqJson.has("productCategory") || reqJson.get("productCategory") == null || "".equals(reqJson.get("productCategory").toString().trim())) {
                throw new CustomNonFatalException("productCategory not found", "productCategory not found");
            }
            if (!reqJson.has("applicant") || reqJson.get("applicant") == null || "".equals(reqJson.get("applicant").toString().trim())) {
                throw new CustomNonFatalException("applicant not found", "applicant not found");
            }
            String applicationId = reqJson.get("applicationId").toString();
            RcuTriggerLog sangamRcuTriggerLog = new RcuTriggerLog();
            sangamRcuTriggerLog.setApplicationId(applicationId);
            sangamRcuTriggerLog.setRequest(payload);
            sangamRcuTriggerLog.setRequestTime(DateUtility.getCurrentDate());
            sangamRcuTriggerLog.setReqType(FrameworkAppConstants.RCU_TRIGGER_LOG_DATA);
            //Insert in Log
            sangamRcuTriggerLogId = rCUTriggerHelper.setSangamRcuTriggerLog(sangamRcuTriggerLog);

            RcuTriggerStatus sangamRcuTriggerStatus = rCUTriggerHelper.getSangamRcuTriggerStatus(applicationId);
            if (sangamRcuTriggerStatus == null) {
                sangamRcuTriggerStatus = new RcuTriggerStatus();
                sangamRcuTriggerStatus.setApplicationId(applicationId);
                sangamRcuTriggerStatus.setFlsName(reqJson.get("flsName").toString());
                sangamRcuTriggerStatus.setFlsId(reqJson.get("flsId").toString());
                sangamRcuTriggerStatus.setRcuVendorName(reqJson.get("rcuVendorName").toString());
                sangamRcuTriggerStatus.setRcuVendorId(reqJson.get("rcuVendorId").toString());
                sangamRcuTriggerStatus.setCity(reqJson.get("city").toString());
                sangamRcuTriggerStatus.setProduct(reqJson.get("product").toString());
                sangamRcuTriggerStatus.setProductCategory(reqJson.get("productCategory").toString());
                sangamRcuTriggerStatus.setRcuStatus(FrameworkAppConstants.RCU_TRIGGER_PENDING);
            } else {
                throw new CustomNonFatalException("Application ID Already Exist", "Application ID Already Exist");
            }

            JSONArray applicantArr = reqJson.getJSONArray("applicant");
            for (int intI = 0; intI < applicantArr.length(); intI++) {
                JSONObject applicantDetails = (JSONObject) applicantArr.get(intI);
                if (!applicantDetails.has("applicantName") || applicantDetails.get("applicantName") == null || "".equals(applicantDetails.get("applicantName").toString().trim())) {
                    throw new CustomNonFatalException("applicantName not found", "applicantName not found");
                }
                if (!applicantDetails.has("applicantId") || applicantDetails.get("applicantId") == null || "".equals(applicantDetails.get("applicantId").toString().trim())) {
                    throw new CustomNonFatalException("applicantId not found", "applicantId not found");
                }
                if (!applicantDetails.has("applicantType") || applicantDetails.get("applicantType") == null || "".equals(applicantDetails.get("applicantType").toString().trim())) {
                    throw new CustomNonFatalException("applicantType not found", "applicantType not found");
                }
                if (!applicantDetails.has("currentAddress") || applicantDetails.get("currentAddress") == null || "".equals(applicantDetails.get("currentAddress").toString().trim())) {
                    throw new CustomNonFatalException("currentAddress not found", "currentAddress not found");
                }
                if (!applicantDetails.has("document") || applicantDetails.get("document") == null || "".equals(applicantDetails.get("document").toString().trim())) {
                    throw new CustomNonFatalException("document not found", "document not found");
                }

                String applicantId = applicantDetails.get("applicantId").toString();
                RcuTriggerApplicant sangamRcuTriggerApplicant = rCUTriggerHelper.getSangamRcuTriggerApplicant(applicationId, applicantId);
                if (sangamRcuTriggerApplicant == null) {
                    sangamRcuTriggerApplicant = new RcuTriggerApplicant();
                    sangamRcuTriggerApplicant.setApplicationId(applicationId);
                    sangamRcuTriggerApplicant.setApplicantId(applicantId);
                    sangamRcuTriggerApplicant.setApplicantName(applicantDetails.get("applicantName").toString());
                    sangamRcuTriggerApplicant.setApplicantType(applicantDetails.get("applicantType").toString());
                    sangamRcuTriggerApplicant.setCurrentAddress(applicantDetails.get("currentAddress").toString());
                    sangamRcuTriggerApplicant.setCreatedDate(DateUtility.getCurrentDate());

                } else {
                    throw new CustomNonFatalException("Applicant Already Exist", "Applicant Already Exist");
                }
                JSONArray documentArr = applicantDetails.getJSONArray("document");
                for (int intJ = 0; intJ < documentArr.length(); intJ++) {
                    JSONObject documentDetails = (JSONObject) documentArr.get(intJ);
                    if (!documentDetails.has("docId") || documentDetails.get("docId") == null || "".equals(documentDetails.get("docId").toString().trim())) {
                        throw new CustomNonFatalException("docId not found", "docId not found");
                    }
                    if (!documentDetails.has("docName") || documentDetails.get("docName") == null || "".equals(documentDetails.get("docName").toString().trim())) {
                        throw new CustomNonFatalException("docName not found", "docName not found");
                    }
//                    if (!documentDetails.has("versionId") || documentDetails.get("versionId") == null || "".equals(documentDetails.get("versionId").toString().trim())) {
//                        throw new CustomNonFatalException("versionId not found", "versionId not found");
//                    }
                    if (!documentDetails.has("docchecktype") || documentDetails.get("docchecktype") == null || "".equals(documentDetails.get("docchecktype").toString().trim())) {
                        throw new CustomNonFatalException("docchecktype not found", "docchecktype not found");
                    }
                    if (!documentDetails.has("uuid") || documentDetails.get("uuid") == null || "".equals(documentDetails.get("uuid").toString().trim())) {
                        throw new CustomNonFatalException("uuid not found", "uuid not found");
                    }
//                    if (!documentDetails.has("fileName") || documentDetails.get("fileName") == null || "".equals(documentDetails.get("fileName").toString().trim())) {
//                        throw new CustomNonFatalException("fileName not found", "fileName not found");
//                    }

                    RcuTriggerDocument sangamRcuTriggerDocument = new RcuTriggerDocument();
                    sangamRcuTriggerDocument.setApplicationId(applicationId);
                    sangamRcuTriggerDocument.setApplicantId(applicantId);
                    sangamRcuTriggerDocument.setDocId(documentDetails.get("docId").toString());
                    sangamRcuTriggerDocument.setDocName(documentDetails.get("docName").toString());
                    if (documentDetails.has("versionId") && documentDetails.get("versionId") != null && !"".equals(documentDetails.get("versionId").toString().trim()) && !documentDetails.get("versionId").toString().equalsIgnoreCase("null")) {
                        sangamRcuTriggerDocument.setDocVersion(Long.parseLong(documentDetails.get("versionId").toString()));
                    }
                    sangamRcuTriggerDocument.setUuid(documentDetails.get("uuid").toString());
                    if (documentDetails.has("fileName") && documentDetails.get("fileName") != null && !"".equals(documentDetails.get("fileName").toString().trim()) && !documentDetails.get("fileName").toString().equalsIgnoreCase("null")) {
                        sangamRcuTriggerDocument.setFileName(documentDetails.get("fileName").toString());
                    }
                    
                    if (documentDetails.has("childDocId") && documentDetails.get("childDocId") != null && !"".equals(documentDetails.get("childDocId").toString().trim()) && !documentDetails.get("childDocId").toString().equalsIgnoreCase("null")) {
                        sangamRcuTriggerDocument.setChildDocId(documentDetails.get("childDocId").toString());
                    }
                    if (documentDetails.has("childDocName") && documentDetails.get("childDocName") != null && !"".equals(documentDetails.get("childDocName").toString().trim()) && !documentDetails.get("childDocName").toString().equalsIgnoreCase("null")) {
                        sangamRcuTriggerDocument.setChildDocName(documentDetails.get("childDocName").toString());
                    }
                    sangamRcuTriggerDocument.setDocCheckType(documentDetails.get("docchecktype").toString());
                    sangamRcuTriggerDocument.setCreatedDate(DateUtility.getCurrentDate());
                    rCUTriggerHelper.setSangamRcuTriggerDocument(sangamRcuTriggerDocument);

                }
                rCUTriggerHelper.setSangamRcuTriggerApplicant(sangamRcuTriggerApplicant);
            }
            rCUTriggerHelper.setSangamRcuTriggerStatus(sangamRcuTriggerStatus);
            mav.addObject("status", "200");
            mav.addObject("message", "Success");
            if (sangamRcuTriggerLogId != null) {
                sangamRcuTriggerLogId.setResponse(mav.toString());
                sangamRcuTriggerLogId.setResponseTime(DateUtility.getCurrentDate());
               
            }
        } catch (JSONException e) {
            mav.addObject("status", "300");
            mav.addObject("message", e.getMessage());
            if (sangamRcuTriggerLogId != null) {
                sangamRcuTriggerLogId.setResponse(mav.toString());
                sangamRcuTriggerLogId.setResponseTime(DateUtility.getCurrentDate());
                rCUTriggerHelper.setSangamRcuTriggerLog(sangamRcuTriggerLogId);
            }
            CustomLogger.error("cudUserProfile", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "300");
            mav.addObject("message", e.getMessage());
            if (sangamRcuTriggerLogId != null) {
                sangamRcuTriggerLogId.setResponse(mav.toString());
                sangamRcuTriggerLogId.setResponseTime(DateUtility.getCurrentDate());
                rCUTriggerHelper.setSangamRcuTriggerLog(sangamRcuTriggerLogId);
            }
            CustomLogger.error("cudUserProfile", e);
        }

        return mav;
    }

    @RequestMapping(value = "/getRCUTriggerDocumentDetails", method = RequestMethod.POST)
    public ModelAndView getRCUTriggerDocumentDetails(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");
//        System.out.println(payload);
        try {
            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("Invalid applicationId", "Invalid applicationId");
            }

            Long userId = Long.parseLong(reqJson.get("userId").toString());
            String applicationId = reqJson.get("applicationId").toString();
            Boolean isDisabled = false;
            RcuTriggerStatus sangamRcuTriggerStatus = rCUTriggerHelper.getSangamRcuTriggerStatus(applicationId);
            if (sangamRcuTriggerStatus == null) {
                throw new CustomNonFatalException("Application does not exist", "Application does not exist");
            }
            if (sangamRcuTriggerStatus.getRcuStatus().equalsIgnoreCase(FrameworkAppConstants.RCU_TRIGGER_SUBMISSION_COMPLETED)) {
                isDisabled = true;
            }
            List<RcuTriggerApplicant> sangamRcuTriggerApplicant = rCUTriggerHelper.getSangamRcuTriggerApplicantByApplication(applicationId);
            if (sangamRcuTriggerApplicant == null) {
                throw new CustomNonFatalException("No Applicant Exist", "No Applicant Exist");
            }
            RCUTriggerVo rCUTriggerVo = new RCUTriggerVo();
            rCUTriggerVo.setApplicationId(sangamRcuTriggerStatus.getApplicationId());
            rCUTriggerVo.setCity(sangamRcuTriggerStatus.getCity());
            rCUTriggerVo.setFlsName(sangamRcuTriggerStatus.getFlsName());
            rCUTriggerVo.setProduct(sangamRcuTriggerStatus.getProduct());
            rCUTriggerVo.setProductCategory(sangamRcuTriggerStatus.getProductCategory());
            rCUTriggerVo.setRcuStatus(sangamRcuTriggerStatus.getRcuStatus());
            rCUTriggerVo.setRcuVendorName(sangamRcuTriggerStatus.getRcuVendorName());
            rCUTriggerVo.setRemark(sangamRcuTriggerStatus.getRcuRemark());
            rCUTriggerVo.setSamplingOutcome(sangamRcuTriggerStatus.getRcuOutcomeStatus());
            List<RCUTriggerApplicantVo> applicantList = new ArrayList();

            for (RcuTriggerApplicant applicants : sangamRcuTriggerApplicant) {
                RCUTriggerApplicantVo rCUTriggerApplicantVo = new RCUTriggerApplicantVo();
                rCUTriggerApplicantVo.setApplicantId(applicants.getApplicantId());
                rCUTriggerApplicantVo.setApplicantName(applicants.getApplicantName());
                rCUTriggerApplicantVo.setApplicantType(applicants.getApplicantType());
                rCUTriggerApplicantVo.setApplicantAddress(applicants.getCurrentAddress());
                List<RCUTriggerDocumentVo> docList = new ArrayList();
                List<RcuTriggerDocument> document = rCUTriggerHelper.getSangamRcuTriggerDocumentByApplicant(applicationId, applicants.getApplicantId());
                if (document != null) {
                    for (RcuTriggerDocument doc : document) {
                        RCUTriggerDocumentVo rCUTriggerDocumentVo = new RCUTriggerDocumentVo();
                        rCUTriggerDocumentVo.setDocId(doc.getDocId());
                        rCUTriggerDocumentVo.setDocName(doc.getDocName());
                        rCUTriggerDocumentVo.setChildDocId(doc.getChildDocId());
                        rCUTriggerDocumentVo.setChildDocName(doc.getChildDocName());
//                        rCUTriggerDocumentVo.setDocPath(doc.getDocPath());
                        rCUTriggerDocumentVo.setDocVersion(doc.getDocVersion()+"");
                        rCUTriggerDocumentVo.setDocumentUid(doc.getId().toString());
                        rCUTriggerDocumentVo.setFileName(doc.getFileName());
                        rCUTriggerDocumentVo.setScreeningCompleted(doc.getScreeningCompleted());
                        rCUTriggerDocumentVo.setPickForSampling(doc.getPickForSampling());
                        rCUTriggerDocumentVo.setSamplingStatus(doc.getSamplingStatus());
                        rCUTriggerDocumentVo.setUuid(doc.getUuid());
                        docList.add(rCUTriggerDocumentVo);
                    }
                    rCUTriggerApplicantVo.setDocuments(docList);
                }
                applicantList.add(rCUTriggerApplicantVo);
                rCUTriggerVo.setApplicantDetails(applicantList);
            }
            mav.addObject("isDisabled", isDisabled);
            mav.addObject("rcuDocumentData", rCUTriggerVo);
            mav.addObject("status", "200");
        } catch (Exception e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("getRCUTriggerDocumentDetails", e);
        }
        return mav;

    }

    @RequestMapping(value = "/rcuTriggerDocDownload", method = RequestMethod.GET)
    public ModelAndView rcuTriggerDocDownload(HttpServletResponse response,
//            @RequestParam(value = "docVersion", required = true) String docVersion,
            @RequestParam(value = "uuid", required = true) String uuid,
            @RequestParam(value = "applicationId", required = true) String applicationId
    ) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        try {
//            String op = rCUTriggerHelper.getDocumentFromMiddelware(uuid, docVersion,applicationId);
              String op = rCUTriggerHelper.getDocumentFromMiddelware(uuid, applicationId);

            File file;
            JSONObject dmsResponse = new JSONObject(op);
            if (dmsResponse.has("status") && dmsResponse.get("status").toString().equalsIgnoreCase("1")) {
                if (dmsResponse.has("data")) {
                    JSONObject dmsData = new JSONObject(dmsResponse.get("data").toString());
                    file = new File(dmsData.get("fileName").toString() + "." + dmsData.get("ext").toString());
                    String mimeType = URLConnection.guessContentTypeFromName(file.getName());
                    if (mimeType == null) {
                        mimeType = "application/octet-stream";
                    }
                    response.setContentType(mimeType);
                    response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
                    String base64Image = dmsData.get("fileStream").toString();
                    byte base64Byte[] = Base64.decodeBase64(base64Image);
                    response.setContentLength((int) base64Byte.length);
                    InputStream inputStream = new ByteArrayInputStream(base64Byte);
                    FileCopyUtils.copy(inputStream, response.getOutputStream());
                    inputStream.close();
                }
            }
            
        } catch (JSONException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("rcuTriggerDocDownload", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("rcuTriggerDocDownload", e);
        } catch (IOException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
            CustomLogger.error("rcuTriggerDocDownload", e);
        }

        return mav;
    }

    public String getCanonicalFileName(String path) {
        if (path != null && !path.equalsIgnoreCase("")) {
            File file = new File(path);
            return file.getName();
        } else {
            return "";
        }
    }

    @RequestMapping(value = "/saveRcuTrigger", method = RequestMethod.POST)
    public ModelAndView saveRcuTrigger(@RequestBody String payload) throws IOException {
        ModelAndView mav = new ModelAndView("home");
        try {
            JSONObject reqJson = new JSONObject(payload);
            if (reqJson.get("userId") == null || "".equals(reqJson.get("userId").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("applicationId") == null || "".equals(reqJson.get("applicationId").toString().trim())) {
                throw new CustomNonFatalException("Invalid applicationId", "Invalid applicationId");
            }
            if (reqJson.get("applicantData") == null || "".equals(reqJson.get("applicantData").toString().trim())) {
                throw new CustomNonFatalException("Invalid applicantData", "Invalid applicantData");
            }
            if (reqJson.get("samplingOutcome") == null || "".equals(reqJson.get("samplingOutcome").toString().trim())) {
                throw new CustomNonFatalException("Invalid User", "Invalid User");
            }
            if (reqJson.get("rcuSubmitType") == null || "".equals(reqJson.get("rcuSubmitType").toString().trim())) {
                throw new CustomNonFatalException("Invalid rcuSubmitType", "Invalid rcuSubmitType");
            }

            String applicationId = reqJson.get("applicationId").toString();
            String userId = reqJson.get("userId").toString();

            JSONArray applicantArr = reqJson.getJSONArray("applicantData");
            for (int intI = 0; intI < applicantArr.length(); intI++) {
                JSONObject applicantDetails = (JSONObject) applicantArr.get(intI);

                if (applicantDetails.get("applicantId") == null || "".equals(applicantDetails.get("applicantId").toString().trim())) {
                    throw new CustomNonFatalException("Invalid applicantId", "Invalid applicantId");
                }

                RcuTriggerApplicant sangamRcuTriggerApplicant = rCUTriggerHelper.getSangamRcuTriggerApplicant(applicationId, applicantDetails.get("applicantId").toString());
                if (sangamRcuTriggerApplicant != null) {
                    sangamRcuTriggerApplicant.setRcuModBy(userId);
                    sangamRcuTriggerApplicant.setRcuModDate(DateUtility.getCurrentDate());
                }
                JSONArray documentArr = applicantDetails.getJSONArray("documents");
                for (int intJ = 0; intJ < documentArr.length(); intJ++) {
                    JSONObject documentDetails = (JSONObject) documentArr.get(intJ);

                    if (documentDetails.get("documentUid") == null || "".equals(documentDetails.get("documentUid").toString().trim())) {
                        throw new CustomNonFatalException("Invalid documentUid", "Invalid documentUid");
                    }
                    if (documentDetails.get("screeningCompleted") == null || "".equals(documentDetails.get("screeningCompleted").toString().trim())) {
                        throw new CustomNonFatalException("Invalid screeningCompleted", "Invalid screeningCompleted");
                    }
                    if (documentDetails.get("pickForSampling") == null || "".equals(documentDetails.get("pickForSampling").toString().trim())) {
                        throw new CustomNonFatalException("Invalid pickForSampling", "Invalid pickForSampling");
                    }
                    if (documentDetails.get("samplingStatus") == null || "".equals(documentDetails.get("samplingStatus").toString().trim())) {
                        throw new CustomNonFatalException("Invalid samplingStatus", "Invalid samplingStatus");
                    }

                    RcuTriggerDocument sangamRcuTriggerDocument = rCUTriggerHelper.getDocumentById(Long.parseLong(documentDetails.get("documentUid").toString()));

                    if (sangamRcuTriggerDocument != null) {
                        sangamRcuTriggerDocument.setRcuModBy(userId);
                        sangamRcuTriggerDocument.setRcuModDate(DateUtility.getCurrentDate());

                        if (documentDetails.get("screeningCompleted").toString().equalsIgnoreCase("true")) {
                            sangamRcuTriggerDocument.setScreeningCompleted(Boolean.TRUE);
                        } else {
                            sangamRcuTriggerDocument.setScreeningCompleted(Boolean.FALSE);
                        }
                        sangamRcuTriggerDocument.setPickForSampling(documentDetails.get("pickForSampling").toString());
                        sangamRcuTriggerDocument.setSamplingStatus(documentDetails.get("samplingStatus").toString());
                        rCUTriggerHelper.setSangamRcuTriggerDocument(sangamRcuTriggerDocument);
                    }
                }
                RcuTriggerStatus sangamRcuTriggerStatus = rCUTriggerHelper.getSangamRcuTriggerStatus(applicationId);
                if (sangamRcuTriggerStatus != null) {
                    if (reqJson.has("rcuRemark") && reqJson.get("rcuRemark") != null && !"".equals(reqJson.get("rcuRemark").toString().trim())) {
                        sangamRcuTriggerStatus.setRcuRemark(reqJson.get("rcuRemark").toString());
                    }
                    if (reqJson.get("rcuSubmitType").toString().equalsIgnoreCase(FrameworkAppConstants.STR_CONSTANT_YES)) {

                        //Call LOS service 
                        LosRequestVO losRequestVO = new LosRequestVO();
                        losRequestVO.setFI_Status__c(reqJson.get("samplingOutcome").toString());
                        /*This is directly to LOS(SAP).Middleware was developed which will 
                        consume this service.So removing this and using Middleware service.
                       */
//                        ResponseEntity responseEntity = rCUTriggerHelper.updateLos(losRequestVO, applicationId, userId);
                        String op=rCUTriggerHelper.updateStatusMiddleWare(losRequestVO, applicationId, userId);
                        /*
                        ************************
                        CHANGE BELOW LOGIC AS PER RESPONSE
                        ************************
                        */ 
                        if (op != null && op != "" ) {
                            sangamRcuTriggerStatus.setRcuStatus(FrameworkAppConstants.RCU_TRIGGER_SUBMISSION_COMPLETED);
                            sangamRcuTriggerStatus.setSubmitedDate(DateUtility.getCurrentDate());
                            sangamRcuTriggerStatus.setIsSubmited(FrameworkAppConstants.CONSTANT_YES);
                        } else {
                            throw new CustomNonFatalException("Unable to update status.Please try after sometime.", "Unable to update status.Please try after sometime.");
                        }
                    } else {
                        sangamRcuTriggerStatus.setRcuStatus(FrameworkAppConstants.RCU_TRIGGER_SAVED);
                    }
                    sangamRcuTriggerStatus.setRcuOutcomeStatus(reqJson.get("samplingOutcome").toString());
                    sangamRcuTriggerStatus.setRcuModBy(userId);
                    sangamRcuTriggerStatus.setRcuModDate(DateUtility.getCurrentDate());

                }
                rCUTriggerHelper.setSangamRcuTriggerStatus(sangamRcuTriggerStatus);
                rCUTriggerHelper.setSangamRcuTriggerApplicant(sangamRcuTriggerApplicant);

            }
            mav.addObject("status", "200");
            mav.addObject("msg", "Updated Successfully");

        } catch (JSONException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("saveRcuTrigger", e);
        } catch (CustomNonFatalException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("saveRcuTrigger", e);
        } catch (NumberFormatException e) {
            mav.addObject("status", "400");
            mav.addObject("msg", e.getMessage());
//            e.printStackTrace();
            CustomLogger.error("saveRcuTrigger", e);
        }
        return mav;
    }

}
