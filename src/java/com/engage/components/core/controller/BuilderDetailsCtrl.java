/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.engage.components.core.controller;

import com.engage.components.core.entity.bean.EsApplicantsDetailBean;
import com.engage.components.core.entity.bean.EsAssetDetailBean;
import com.engage.components.core.entity.bean.EsLoanDetailBean;
import com.engage.components.core.entity.bean.EsMstrAccount;
import com.engage.components.core.entity.bean.EsMstrApfProject;
import com.engage.components.core.helper.BuilderDetailHelper;
import com.engage.components.core.util.excepton.CustomNonFatalException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author VEN00288
 */

@Controller
@Component
public class BuilderDetailsCtrl {
    
    @Autowired
    BuilderDetailHelper builderHelper;
    
    
    @RequestMapping(value = "/builderDetail", method = RequestMethod.POST)
    public ModelAndView getBuilderDetails(@RequestBody String payload)
    {
        ModelAndView mav= new ModelAndView("home");
         JSONObject reqJson = null;
        
        try
        {
            reqJson = new JSONObject(payload);
            if (!reqJson.has("builderC") || reqJson.get("builderC") == null || "".equals(reqJson.get("builderC").toString().trim())) {
                throw new CustomNonFatalException("builderC Is Missing", "builderC Is Missing");
            }

            String builderC = reqJson.get("builderC").toString();
            
            System.out.println("builderC>>>>>>>>>>"+builderC);
            
            EsMstrAccount accontData = builderHelper.getBuilderAccountsDetails(builderC);
            
            System.out.println("accontData>>>>>>>>>>"+accontData.getBuilderCode());

            if (accontData != null) {
                mav.addObject("accontData", accontData);

               EsMstrApfProject apfProject = builderHelper.getEsMstrApfProjectInfo(builderC);
               
                System.out.println("apfProject>>>>>>>>>>"+apfProject.getApfProjectC());

                if (apfProject != null) {
                    mav.addObject("apfProject", apfProject);
                    mav.addObject("status", "200");

                } else {
                    mav.addObject("status", "400");
                    mav.addObject("msg", "apf Project details Not Found.");
                }
         
            } else {
                mav.addObject("status", "400");
                mav.addObject("msg", "Builder Accounts Details Not Found.");
            }
            
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return mav;
    }
    
}
