
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="objLogDetails" type="{http://tempuri.org/}clsUserLogDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "objLogDetails"
})
@XmlRootElement(name = "InsertUserLogDetails")
public class InsertUserLogDetails {

    protected ClsUserLogDetails objLogDetails;

    /**
     * Gets the value of the objLogDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ClsUserLogDetails }
     *     
     */
    public ClsUserLogDetails getObjLogDetails() {
        return objLogDetails;
    }

    /**
     * Sets the value of the objLogDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClsUserLogDetails }
     *     
     */
    public void setObjLogDetails(ClsUserLogDetails value) {
        this.objLogDetails = value;
    }

}
