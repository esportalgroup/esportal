
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsertUserLogDetailsOldResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertUserLogDetailsOldResult"
})
@XmlRootElement(name = "InsertUserLogDetailsOldResponse")
public class InsertUserLogDetailsOldResponse {

    @XmlElement(name = "InsertUserLogDetailsOldResult")
    protected String insertUserLogDetailsOldResult;

    /**
     * Gets the value of the insertUserLogDetailsOldResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsertUserLogDetailsOldResult() {
        return insertUserLogDetailsOldResult;
    }

    /**
     * Sets the value of the insertUserLogDetailsOldResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsertUserLogDetailsOldResult(String value) {
        this.insertUserLogDetailsOldResult = value;
    }

}
