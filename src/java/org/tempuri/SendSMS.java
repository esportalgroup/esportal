
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MSG_FROM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSG_TO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSG_STATUS" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MSG_TAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSG_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="USER_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="USE_GSM" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MODULEID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "msgfrom",
    "msgto",
    "msgstatus",
    "msgtag",
    "msgtext",
    "userid",
    "usegsm",
    "moduleid"
})
@XmlRootElement(name = "SendSMS")
public class SendSMS {

    @XmlElement(name = "MSG_FROM")
    protected String msgfrom;
    @XmlElement(name = "MSG_TO")
    protected String msgto;
    @XmlElement(name = "MSG_STATUS")
    protected int msgstatus;
    @XmlElement(name = "MSG_TAG")
    protected String msgtag;
    @XmlElement(name = "MSG_TEXT")
    protected String msgtext;
    @XmlElement(name = "USER_ID")
    protected String userid;
    @XmlElement(name = "USE_GSM")
    protected int usegsm;
    @XmlElement(name = "MODULEID")
    protected int moduleid;

    /**
     * Gets the value of the msgfrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSGFROM() {
        return msgfrom;
    }

    /**
     * Sets the value of the msgfrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSGFROM(String value) {
        this.msgfrom = value;
    }

    /**
     * Gets the value of the msgto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSGTO() {
        return msgto;
    }

    /**
     * Sets the value of the msgto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSGTO(String value) {
        this.msgto = value;
    }

    /**
     * Gets the value of the msgstatus property.
     * 
     */
    public int getMSGSTATUS() {
        return msgstatus;
    }

    /**
     * Sets the value of the msgstatus property.
     * 
     */
    public void setMSGSTATUS(int value) {
        this.msgstatus = value;
    }

    /**
     * Gets the value of the msgtag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSGTAG() {
        return msgtag;
    }

    /**
     * Sets the value of the msgtag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSGTAG(String value) {
        this.msgtag = value;
    }

    /**
     * Gets the value of the msgtext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSGTEXT() {
        return msgtext;
    }

    /**
     * Sets the value of the msgtext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSGTEXT(String value) {
        this.msgtext = value;
    }

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSERID() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSERID(String value) {
        this.userid = value;
    }

    /**
     * Gets the value of the usegsm property.
     * 
     */
    public int getUSEGSM() {
        return usegsm;
    }

    /**
     * Sets the value of the usegsm property.
     * 
     */
    public void setUSEGSM(int value) {
        this.usegsm = value;
    }

    /**
     * Gets the value of the moduleid property.
     * 
     */
    public int getMODULEID() {
        return moduleid;
    }

    /**
     * Sets the value of the moduleid property.
     * 
     */
    public void setMODULEID(int value) {
        this.moduleid = value;
    }

}
