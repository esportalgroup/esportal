
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchMenuResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchMenuResult"
})
@XmlRootElement(name = "FetchMenuResponse")
public class FetchMenuResponse {

    @XmlElement(name = "FetchMenuResult")
    protected String fetchMenuResult;

    /**
     * Gets the value of the fetchMenuResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFetchMenuResult() {
        return fetchMenuResult;
    }

    /**
     * Sets the value of the fetchMenuResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFetchMenuResult(String value) {
        this.fetchMenuResult = value;
    }

}
