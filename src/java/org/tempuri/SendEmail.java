
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipients" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mailfrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MailCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MailBCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Body" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="File_attachment1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="File_attachment2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="File_attachment3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sentdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModuleId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BodyFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PassWord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userId",
    "recipients",
    "mailfrom",
    "mailCC",
    "mailBCC",
    "subject",
    "body",
    "fileAttachment1",
    "fileAttachment2",
    "fileAttachment3",
    "sentdate",
    "moduleId",
    "bodyFormat",
    "passWord"
})
@XmlRootElement(name = "SendEmail")
public class SendEmail {

    @XmlElement(name = "UserId")
    protected String userId;
    protected String recipients;
    @XmlElement(name = "Mailfrom")
    protected String mailfrom;
    @XmlElement(name = "MailCC")
    protected String mailCC;
    @XmlElement(name = "MailBCC")
    protected String mailBCC;
    @XmlElement(name = "Subject")
    protected String subject;
    @XmlElement(name = "Body")
    protected String body;
    @XmlElement(name = "File_attachment1")
    protected String fileAttachment1;
    @XmlElement(name = "File_attachment2")
    protected String fileAttachment2;
    @XmlElement(name = "File_attachment3")
    protected String fileAttachment3;
    protected String sentdate;
    @XmlElement(name = "ModuleId")
    protected int moduleId;
    @XmlElement(name = "BodyFormat")
    protected String bodyFormat;
    @XmlElement(name = "PassWord")
    protected String passWord;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the recipients property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipients() {
        return recipients;
    }

    /**
     * Sets the value of the recipients property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipients(String value) {
        this.recipients = value;
    }

    /**
     * Gets the value of the mailfrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailfrom() {
        return mailfrom;
    }

    /**
     * Sets the value of the mailfrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailfrom(String value) {
        this.mailfrom = value;
    }

    /**
     * Gets the value of the mailCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailCC() {
        return mailCC;
    }

    /**
     * Sets the value of the mailCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailCC(String value) {
        this.mailCC = value;
    }

    /**
     * Gets the value of the mailBCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailBCC() {
        return mailBCC;
    }

    /**
     * Sets the value of the mailBCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailBCC(String value) {
        this.mailBCC = value;
    }

    /**
     * Gets the value of the subject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the value of the subject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBody(String value) {
        this.body = value;
    }

    /**
     * Gets the value of the fileAttachment1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileAttachment1() {
        return fileAttachment1;
    }

    /**
     * Sets the value of the fileAttachment1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileAttachment1(String value) {
        this.fileAttachment1 = value;
    }

    /**
     * Gets the value of the fileAttachment2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileAttachment2() {
        return fileAttachment2;
    }

    /**
     * Sets the value of the fileAttachment2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileAttachment2(String value) {
        this.fileAttachment2 = value;
    }

    /**
     * Gets the value of the fileAttachment3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileAttachment3() {
        return fileAttachment3;
    }

    /**
     * Sets the value of the fileAttachment3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileAttachment3(String value) {
        this.fileAttachment3 = value;
    }

    /**
     * Gets the value of the sentdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSentdate() {
        return sentdate;
    }

    /**
     * Sets the value of the sentdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSentdate(String value) {
        this.sentdate = value;
    }

    /**
     * Gets the value of the moduleId property.
     * 
     */
    public int getModuleId() {
        return moduleId;
    }

    /**
     * Sets the value of the moduleId property.
     * 
     */
    public void setModuleId(int value) {
        this.moduleId = value;
    }

    /**
     * Gets the value of the bodyFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodyFormat() {
        return bodyFormat;
    }

    /**
     * Sets the value of the bodyFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodyFormat(String value) {
        this.bodyFormat = value;
    }

    /**
     * Gets the value of the passWord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassWord() {
        return passWord;
    }

    /**
     * Sets the value of the passWord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassWord(String value) {
        this.passWord = value;
    }

}
